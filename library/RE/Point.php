<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Geometry
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * Represents a location in a two dimensional coordinate system.
 *
 * Frequently used in conjunction with {@link RE_Size} to represent
 * rectangles (see {@link RE_Rect}).
 *
 * @package    Geometry
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Point
{
  /**** Class Constants ****/

    /**
     * Precision for comparing equality of float values.
     */
    const FLOAT_COMPARISON_PRECISION = 1e-4;



  /**** Instance Variables ****/

    /**
     * Stores the horizontal (x) coordinate value.
     * @var float
     */
    public $x = 0.0;

    /**
     * Stores the vertical (y) coordinate value.
     * @var float
     */
    public $y = 0.0;



  /**** Class Methods ****/

    /**
     * Returns a new RE_Point object from the string representation,
     * usually formatted as "{x, y}".
     *
     * The string is scanned for two numbers, which are used as the x and y
     * coordinates, in that order. If the string does not contain two numbers,
     * any numbers found are used, and the remaining values are assumed to be
     * zero. All values are interpreted as floats.
     * 
     * @methodgroup Class Methods
     * @param string $string
     * @return RE_Point
     */
    public static function fromString($string)
    {
        preg_match('/(-?\d+\.?\d*)(.+?(-?\d+\.?\d*))?/ms', $string, $matches);
        return new self((isset($matches[1]) ? $matches[1] : 0.0), (isset($matches[3]) ? $matches[3] : 0.0));
    }



  /**** Public Interface ****/


  /* Object Lifecycle */

    /**
     * Object constructor signatures:
     *
     *  - Initialize at the origin {0, 0}:
     *      new RE_Point();
     *
     *  - Initialize at a specific x and y coordinate:
     *      new RE_Point(number $x, number $y);
     *
     *  - Clone an existing point:
     *      new RE_Point(RE_Point $point);
     *
     * @methodgroup Object Lifecycle
     * @param mixed $param1
     * @param mixed $param2
     * @throws BadMethodCallException if the constructor arguments cannot be deduced.
     */
    public function __construct($param1 = null, $param2 = null)
    {
        if (isset($param1, $param2)) {
            /* Initialize at a specific x and y coordinate.
             */
            $this->x = (float)$param1;
            $this->y = (float)$param2;

        } else if ((! isset($param1)) && (! isset($param2))) {
            /* Initialize at the origin. Done.
             */

        } else if (($param1 instanceof RE_Point) &&
                   (! isset($param2))) {
            /* Clone an existing point. Copy the values.
             */
            $this->x = $param1->x;
            $this->y = $param1->y;

        } else {
            throw new BadMethodCallException('Unrecognized arguments provided to object constructor.');
        }
    }


  /* Object Magic Methods */

    /**
     * Returns the coordinates as a string of the form "{x, y}".
     *
     * @methodgroup Object Magic Methods
     * @return string
     */
    public function __toString()
    {
        return '{' . $this->x . ', ' . $this->y . '}';
    }


  /* Accessor Methods */

    /**
     * Returns the x (horizontal) coordinate value.
     *
     * @methodgroup Accessor Methods
     * @return float
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * Sets the horizontal (x) coordinate value.
     *
     * @methodgroup Accessor Methods
     * @param float $x
     */
    public function setX($x)
    {
        $this->x = (float)$x;
    }

    /**
     * Returns the y (vertical) coordinate value.
     *
     * @methodgroup Accessor Methods
     * @return float
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * Sets the vertical (y) coordinate value.
     *
     * @methodgroup Accessor Methods
     * @param float $y
     */
    public function setY($y)
    {
        $this->y = (float)$y;
    }


  /* Calculation */

    /**
     * Calculates and returns the distance between the specified point and this
     * point.
     *
     * @methodgroup Calculation
     * @param RE_Point $point
     * @return float
     */
    public function distanceToPoint(RE_Point $point)
    {
        return sqrt(pow(($this->x - $point->x), 2) + pow(($this->y - $point->y), 2));
    }


  /* Test and Comparison */

    /**
     * Returns true if the point is at the origin {0, 0}.
     *
     * @methodgroup Test and Comparison
     * @return boolean
     */
    public function isOrigin()
    {
        if ((abs($this->x) < RE_Point::FLOAT_COMPARISON_PRECISION) &&
            (abs($this->y) < RE_Point::FLOAT_COMPARISON_PRECISION)) {
                return true;
        }
        return false;
    }

    /**
     * Alias for {@link isOrigin()}
     *
     * @methodgroup Test and Comparison
     * @return boolean
     */
    public function isZeroPoint()
    {
        return $this->isOrigin();
    }

    /**
     * Returns true if the specified point has identical coordinates to
     * this point.
     *
     * @methodgroup Test and Comparison
     * @param RE_Point $point
     * @return boolean
     */
    public function isEqualToPoint(RE_Point $point)
    {
        if ((abs($this->x - $point->x) < RE_Point::FLOAT_COMPARISON_PRECISION) &&
            (abs($this->y - $point->y) < RE_Point::FLOAT_COMPARISON_PRECISION)) {
                return true;
        }
        return false;
    }

    /**
     * Returns true if this point lies anywhere within the specified rect's
     * bounds.
     *
     * @methodgroup Test and Comparison
     * @param RE_Rect $rect
     * @return boolean
     */
    public function inRect(RE_Rect $rect)
    {
        return $rect->containsPoint($this);
    }

}
