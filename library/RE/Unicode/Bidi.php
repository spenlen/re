<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Text
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */

/* Portions of this source file contain data obtained from Unicode Data Files:
 *
 * COPYRIGHT AND PERMISSION NOTICE
 *
 * Copyright (c) 1991-2008 Unicode, Inc. All rights reserved. Distributed under
 * the Terms of Use in http://www.unicode.org/copyright.html.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of the Unicode data files and any associated documentation (the "Data Files")
 * or Unicode software and any associated documentation (the "Software") to deal
 * in the Data Files or Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute,
 * and/or sell copies of the Data Files or Software, and to permit persons to
 * whom the Data Files or Software are furnished to do so, provided that (a) the
 * above copyright notice(s) and this permission notice appear with all copies
 * of the Data Files or Software, (b) both the above copyright notice(s) and this
 * permission notice appear in associated documentation, and (c) there is clear
 * notice in each modified Data File or in the Software as well as in the
 * documentation associated with the Data File(s) or Software that the data or
 * software has been modified.
 *
 * THE DATA FILES AND SOFTWARE ARE PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT OR
 * CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE,
 * DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THE DATA FILES OR SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder shall
 * not be used in advertising or otherwise to promote the sale, use or other
 * dealings in these Data Files or Software without prior written authorization
 * of the copyright holder.
 */


/**
 * Abstract utility class which provides bi-directional (bidi) character
 * lookup and classification information for Unicode characters which are
 * critical for text layout engines.
 *
 * IMPORTANT: This class file was generated automatically by the
 * generateUnicodeServices.php script, located in the /tools/pdf directory
 * of the framework distribution. If you need to make changes to this class,
 * please modify that script and regenerate the class instead of changing this
 * file by hand.
 *
 * @package    Text
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
abstract class RE_Unicode_Bidi
{
  /**** Class Variables ****/

    /**
     * Lookup array containing text direction classes for Latin-1 characters
     * only. Used as an optimization in {@link getTextDirectionClasses()}.
     *
     * @var array
     * @ignore Ignored to prevent phpDocumentor from attempting to make a
     *   complete copy of the array in the generated API documentation.
     */
    private static $_latinBidiDirectionClasses = array('BN',
        'BN', 'BN', 'BN', 'BN', 'BN', 'BN', 'BN', 'BN',
        'S', 'B', 'S', 'WS', 'B', 'BN', 'BN', 'BN',
        'BN', 'BN', 'BN', 'BN', 'BN', 'BN', 'BN', 'BN',
        'BN', 'BN', 'BN', 'B', 'B', 'B', 'S', 'WS',
        'ON', 'ON', 'ET', 'ET', 'ET', 'ON', 'ON', 'ON',
        'ON', 'ON', 'ES', 'CS', 'ES', 'CS', 'CS', 'EN',
        'EN', 'EN', 'EN', 'EN', 'EN', 'EN', 'EN', 'EN',
        'EN', 'CS', 'ON', 'ON', 'ON', 'ON', 'ON', 'ON',
        'L', 'L', 'L', 'L', 'L', 'L', 'L', 'L',
        'L', 'L', 'L', 'L', 'L', 'L', 'L', 'L',
        'L', 'L', 'L', 'L', 'L', 'L', 'L', 'L',
        'L', 'L', 'ON', 'ON', 'ON', 'ON', 'ON', 'ON',
        'L', 'L', 'L', 'L', 'L', 'L', 'L', 'L',
        'L', 'L', 'L', 'L', 'L', 'L', 'L', 'L',
        'L', 'L', 'L', 'L', 'L', 'L', 'L', 'L',
        'L', 'L', 'ON', 'ON', 'ON', 'ON', 'BN', 'BN',
        'BN', 'BN', 'BN', 'BN', 'B', 'BN', 'BN', 'BN',
        'BN', 'BN', 'BN', 'BN', 'BN', 'BN', 'BN', 'BN',
        'BN', 'BN', 'BN', 'BN', 'BN', 'BN', 'BN', 'BN',
        'BN', 'BN', 'BN', 'BN', 'BN', 'BN', 'BN', 'CS',
        'ON', 'ET', 'ET', 'ET', 'ET', 'ON', 'ON', 'ON',
        'ON', 'L', 'ON', 'ON', 'BN', 'ON', 'ON', 'ET',
        'ET', 'EN', 'EN', 'ON', 'L', 'ON', 'ON', 'ON',
        'EN', 'L', 'ON', 'ON', 'ON', 'ON', 'ON', 'L',
        'L', 'L', 'L', 'L', 'L', 'L', 'L', 'L',
        'L', 'L', 'L', 'L', 'L', 'L', 'L', 'L',
        'L', 'L', 'L', 'L', 'L', 'L', 'ON', 'L',
        'L', 'L', 'L', 'L', 'L', 'L', 'L', 'L',
        'L', 'L', 'L', 'L', 'L', 'L', 'L', 'L',
        'L', 'L', 'L', 'L', 'L', 'L', 'L', 'L',
        'L', 'L', 'L', 'L', 'L', 'L', 'ON', 'L',
        'L', 'L', 'L', 'L', 'L', 'L', 'L');

    /**
     * Lookup array describing the text direction class ranges. See discussion
     * in {@link getTextDirectionClasses()}.
     *
     * The first element is intentionally null. The calculations return offsets
     * which are 1-based, not zero-based.
     *
     * @var array
     * @ignore Ignored to prevent phpDocumentor from attempting to make a
     *   complete copy of the array in the generated API documentation.
     */
    private static $_bidiDirectionClasses = array(null,
        array(0x100, 0x2b8, 'L'), array(0x2b9, 0x2ba, 'ON'),
        array(0x2bb, 0x2c1, 'L'), array(0x2c2, 0x2cf, 'ON'),
        array(0x2d0, 0x2d1, 'L'), array(0x2d2, 0x2df, 'ON'),
        array(0x2e0, 0x2e4, 'L'), array(0x2e5, 0x2ed, 'ON'),
        array(0x2ee, 0x2ee, 'L'), array(0x2ef, 0x2ff, 'ON'),
        array(0x300, 0x36f, 'NSM'), array(0x370, 0x373, 'L'),
        array(0x374, 0x375, 'ON'), array(0x376, 0x377, 'L'),
        array(0x37a, 0x37d, 'L'), array(0x37e, 0x37e, 'ON'),
        array(0x384, 0x385, 'ON'), array(0x386, 0x386, 'L'),
        array(0x387, 0x387, 'ON'), array(0x388, 0x38a, 'L'),
        array(0x38c, 0x38c, 'L'), array(0x38e, 0x3a1, 'L'),
        array(0x3a3, 0x3f5, 'L'), array(0x3f6, 0x3f6, 'ON'),
        array(0x3f7, 0x482, 'L'), array(0x483, 0x489, 'NSM'),
        array(0x48a, 0x523, 'L'), array(0x531, 0x556, 'L'),
        array(0x559, 0x55f, 'L'), array(0x561, 0x587, 'L'),
        array(0x589, 0x589, 'L'), array(0x58a, 0x58a, 'ON'),
        array(0x591, 0x5bd, 'NSM'), array(0x5be, 0x5be, 'R'),
        array(0x5bf, 0x5bf, 'NSM'), array(0x5c0, 0x5c0, 'R'),
        array(0x5c1, 0x5c2, 'NSM'), array(0x5c3, 0x5c3, 'R'),
        array(0x5c4, 0x5c5, 'NSM'), array(0x5c6, 0x5c6, 'R'),
        array(0x5c7, 0x5c7, 'NSM'), array(0x5d0, 0x5ea, 'R'),
        array(0x5f0, 0x5f4, 'R'), array(0x600, 0x603, 'AN'),
        array(0x606, 0x607, 'ON'), array(0x608, 0x608, 'AL'),
        array(0x609, 0x60a, 'ET'), array(0x60b, 0x60b, 'AL'),
        array(0x60c, 0x60c, 'CS'), array(0x60d, 0x60d, 'AL'),
        array(0x60e, 0x60f, 'ON'), array(0x610, 0x61a, 'NSM'),
        array(0x61b, 0x61b, 'AL'), array(0x61e, 0x61f, 'AL'),
        array(0x621, 0x64a, 'AL'), array(0x64b, 0x65e, 'NSM'),
        array(0x660, 0x669, 'AN'), array(0x66a, 0x66a, 'ET'),
        array(0x66b, 0x66c, 'AN'), array(0x66d, 0x66f, 'AL'),
        array(0x670, 0x670, 'NSM'), array(0x671, 0x6d5, 'AL'),
        array(0x6d6, 0x6dc, 'NSM'), array(0x6dd, 0x6dd, 'AN'),
        array(0x6de, 0x6e4, 'NSM'), array(0x6e5, 0x6e6, 'AL'),
        array(0x6e7, 0x6e8, 'NSM'), array(0x6e9, 0x6e9, 'ON'),
        array(0x6ea, 0x6ed, 'NSM'), array(0x6ee, 0x6ef, 'AL'),
        array(0x6f0, 0x6f9, 'EN'), array(0x6fa, 0x70d, 'AL'),
        array(0x70f, 0x70f, 'BN'), array(0x710, 0x710, 'AL'),
        array(0x711, 0x711, 'NSM'), array(0x712, 0x72f, 'AL'),
        array(0x730, 0x74a, 'NSM'), array(0x74d, 0x7a5, 'AL'),
        array(0x7a6, 0x7b0, 'NSM'), array(0x7b1, 0x7b1, 'AL'),
        array(0x7c0, 0x7ea, 'R'), array(0x7eb, 0x7f3, 'NSM'),
        array(0x7f4, 0x7f5, 'R'), array(0x7f6, 0x7f9, 'ON'),
        array(0x7fa, 0x7fa, 'R'), array(0x901, 0x902, 'NSM'),
        array(0x903, 0x939, 'L'), array(0x93c, 0x93c, 'NSM'),
        array(0x93d, 0x940, 'L'), array(0x941, 0x948, 'NSM'),
        array(0x949, 0x94c, 'L'), array(0x94d, 0x94d, 'NSM'),
        array(0x950, 0x950, 'L'), array(0x951, 0x954, 'NSM'),
        array(0x958, 0x961, 'L'), array(0x962, 0x963, 'NSM'),
        array(0x964, 0x972, 'L'), array(0x97b, 0x97f, 'L'),
        array(0x981, 0x981, 'NSM'), array(0x982, 0x983, 'L'),
        array(0x985, 0x98c, 'L'), array(0x98f, 0x990, 'L'),
        array(0x993, 0x9a8, 'L'), array(0x9aa, 0x9b0, 'L'),
        array(0x9b2, 0x9b2, 'L'), array(0x9b6, 0x9b9, 'L'),
        array(0x9bc, 0x9bc, 'NSM'), array(0x9bd, 0x9c0, 'L'),
        array(0x9c1, 0x9c4, 'NSM'), array(0x9c7, 0x9c8, 'L'),
        array(0x9cb, 0x9cc, 'L'), array(0x9cd, 0x9cd, 'NSM'),
        array(0x9ce, 0x9ce, 'L'), array(0x9d7, 0x9d7, 'L'),
        array(0x9dc, 0x9dd, 'L'), array(0x9df, 0x9e1, 'L'),
        array(0x9e2, 0x9e3, 'NSM'), array(0x9e6, 0x9f1, 'L'),
        array(0x9f2, 0x9f3, 'ET'), array(0x9f4, 0x9fa, 'L'),
        array(0xa01, 0xa02, 'NSM'), array(0xa03, 0xa03, 'L'),
        array(0xa05, 0xa0a, 'L'), array(0xa0f, 0xa10, 'L'),
        array(0xa13, 0xa28, 'L'), array(0xa2a, 0xa30, 'L'),
        array(0xa32, 0xa33, 'L'), array(0xa35, 0xa36, 'L'),
        array(0xa38, 0xa39, 'L'), array(0xa3c, 0xa3c, 'NSM'),
        array(0xa3e, 0xa40, 'L'), array(0xa41, 0xa42, 'NSM'),
        array(0xa47, 0xa48, 'NSM'), array(0xa4b, 0xa4d, 'NSM'),
        array(0xa51, 0xa51, 'NSM'), array(0xa59, 0xa5c, 'L'),
        array(0xa5e, 0xa5e, 'L'), array(0xa66, 0xa6f, 'L'),
        array(0xa70, 0xa71, 'NSM'), array(0xa72, 0xa74, 'L'),
        array(0xa75, 0xa75, 'NSM'), array(0xa81, 0xa82, 'NSM'),
        array(0xa83, 0xa83, 'L'), array(0xa85, 0xa8d, 'L'),
        array(0xa8f, 0xa91, 'L'), array(0xa93, 0xaa8, 'L'),
        array(0xaaa, 0xab0, 'L'), array(0xab2, 0xab3, 'L'),
        array(0xab5, 0xab9, 'L'), array(0xabc, 0xabc, 'NSM'),
        array(0xabd, 0xac0, 'L'), array(0xac1, 0xac5, 'NSM'),
        array(0xac7, 0xac8, 'NSM'), array(0xac9, 0xac9, 'L'),
        array(0xacb, 0xacc, 'L'), array(0xacd, 0xacd, 'NSM'),
        array(0xad0, 0xad0, 'L'), array(0xae0, 0xae1, 'L'),
        array(0xae2, 0xae3, 'NSM'), array(0xae6, 0xaef, 'L'),
        array(0xaf1, 0xaf1, 'ET'), array(0xb01, 0xb01, 'NSM'),
        array(0xb02, 0xb03, 'L'), array(0xb05, 0xb0c, 'L'),
        array(0xb0f, 0xb10, 'L'), array(0xb13, 0xb28, 'L'),
        array(0xb2a, 0xb30, 'L'), array(0xb32, 0xb33, 'L'),
        array(0xb35, 0xb39, 'L'), array(0xb3c, 0xb3c, 'NSM'),
        array(0xb3d, 0xb3e, 'L'), array(0xb3f, 0xb3f, 'NSM'),
        array(0xb40, 0xb40, 'L'), array(0xb41, 0xb44, 'NSM'),
        array(0xb47, 0xb48, 'L'), array(0xb4b, 0xb4c, 'L'),
        array(0xb4d, 0xb4d, 'NSM'), array(0xb56, 0xb56, 'NSM'),
        array(0xb57, 0xb57, 'L'), array(0xb5c, 0xb5d, 'L'),
        array(0xb5f, 0xb61, 'L'), array(0xb62, 0xb63, 'NSM'),
        array(0xb66, 0xb71, 'L'), array(0xb82, 0xb82, 'NSM'),
        array(0xb83, 0xb83, 'L'), array(0xb85, 0xb8a, 'L'),
        array(0xb8e, 0xb90, 'L'), array(0xb92, 0xb95, 'L'),
        array(0xb99, 0xb9a, 'L'), array(0xb9c, 0xb9c, 'L'),
        array(0xb9e, 0xb9f, 'L'), array(0xba3, 0xba4, 'L'),
        array(0xba8, 0xbaa, 'L'), array(0xbae, 0xbb9, 'L'),
        array(0xbbe, 0xbbf, 'L'), array(0xbc0, 0xbc0, 'NSM'),
        array(0xbc1, 0xbc2, 'L'), array(0xbc6, 0xbc8, 'L'),
        array(0xbca, 0xbcc, 'L'), array(0xbcd, 0xbcd, 'NSM'),
        array(0xbd0, 0xbd0, 'L'), array(0xbd7, 0xbd7, 'L'),
        array(0xbe6, 0xbf2, 'L'), array(0xbf3, 0xbf8, 'ON'),
        array(0xbf9, 0xbf9, 'ET'), array(0xbfa, 0xbfa, 'ON'),
        array(0xc01, 0xc03, 'L'), array(0xc05, 0xc0c, 'L'),
        array(0xc0e, 0xc10, 'L'), array(0xc12, 0xc28, 'L'),
        array(0xc2a, 0xc33, 'L'), array(0xc35, 0xc39, 'L'),
        array(0xc3d, 0xc3d, 'L'), array(0xc3e, 0xc40, 'NSM'),
        array(0xc41, 0xc44, 'L'), array(0xc46, 0xc48, 'NSM'),
        array(0xc4a, 0xc4d, 'NSM'), array(0xc55, 0xc56, 'NSM'),
        array(0xc58, 0xc59, 'L'), array(0xc60, 0xc61, 'L'),
        array(0xc62, 0xc63, 'NSM'), array(0xc66, 0xc6f, 'L'),
        array(0xc78, 0xc7e, 'ON'), array(0xc7f, 0xc7f, 'L'),
        array(0xc82, 0xc83, 'L'), array(0xc85, 0xc8c, 'L'),
        array(0xc8e, 0xc90, 'L'), array(0xc92, 0xca8, 'L'),
        array(0xcaa, 0xcb3, 'L'), array(0xcb5, 0xcb9, 'L'),
        array(0xcbc, 0xcbc, 'NSM'), array(0xcbd, 0xcc4, 'L'),
        array(0xcc6, 0xcc8, 'L'), array(0xcca, 0xccb, 'L'),
        array(0xccc, 0xccd, 'NSM'), array(0xcd5, 0xcd6, 'L'),
        array(0xcde, 0xcde, 'L'), array(0xce0, 0xce1, 'L'),
        array(0xce2, 0xce3, 'NSM'), array(0xce6, 0xcef, 'L'),
        array(0xcf1, 0xcf2, 'ON'), array(0xd02, 0xd03, 'L'),
        array(0xd05, 0xd0c, 'L'), array(0xd0e, 0xd10, 'L'),
        array(0xd12, 0xd28, 'L'), array(0xd2a, 0xd39, 'L'),
        array(0xd3d, 0xd40, 'L'), array(0xd41, 0xd44, 'NSM'),
        array(0xd46, 0xd48, 'L'), array(0xd4a, 0xd4c, 'L'),
        array(0xd4d, 0xd4d, 'NSM'), array(0xd57, 0xd57, 'L'),
        array(0xd60, 0xd61, 'L'), array(0xd62, 0xd63, 'NSM'),
        array(0xd66, 0xd75, 'L'), array(0xd79, 0xd7f, 'L'),
        array(0xd82, 0xd83, 'L'), array(0xd85, 0xd96, 'L'),
        array(0xd9a, 0xdb1, 'L'), array(0xdb3, 0xdbb, 'L'),
        array(0xdbd, 0xdbd, 'L'), array(0xdc0, 0xdc6, 'L'),
        array(0xdca, 0xdca, 'NSM'), array(0xdcf, 0xdd1, 'L'),
        array(0xdd2, 0xdd4, 'NSM'), array(0xdd6, 0xdd6, 'NSM'),
        array(0xdd8, 0xddf, 'L'), array(0xdf2, 0xdf4, 'L'),
        array(0xe01, 0xe30, 'L'), array(0xe31, 0xe31, 'NSM'),
        array(0xe32, 0xe33, 'L'), array(0xe34, 0xe3a, 'NSM'),
        array(0xe3f, 0xe3f, 'ET'), array(0xe40, 0xe46, 'L'),
        array(0xe47, 0xe4e, 'NSM'), array(0xe4f, 0xe5b, 'L'),
        array(0xe81, 0xe82, 'L'), array(0xe84, 0xe84, 'L'),
        array(0xe87, 0xe88, 'L'), array(0xe8a, 0xe8a, 'L'),
        array(0xe8d, 0xe8d, 'L'), array(0xe94, 0xe97, 'L'),
        array(0xe99, 0xe9f, 'L'), array(0xea1, 0xea3, 'L'),
        array(0xea5, 0xea5, 'L'), array(0xea7, 0xea7, 'L'),
        array(0xeaa, 0xeab, 'L'), array(0xead, 0xeb0, 'L'),
        array(0xeb1, 0xeb1, 'NSM'), array(0xeb2, 0xeb3, 'L'),
        array(0xeb4, 0xeb9, 'NSM'), array(0xebb, 0xebc, 'NSM'),
        array(0xebd, 0xebd, 'L'), array(0xec0, 0xec4, 'L'),
        array(0xec6, 0xec6, 'L'), array(0xec8, 0xecd, 'NSM'),
        array(0xed0, 0xed9, 'L'), array(0xedc, 0xedd, 'L'),
        array(0xf00, 0xf17, 'L'), array(0xf18, 0xf19, 'NSM'),
        array(0xf1a, 0xf34, 'L'), array(0xf35, 0xf35, 'NSM'),
        array(0xf36, 0xf36, 'L'), array(0xf37, 0xf37, 'NSM'),
        array(0xf38, 0xf38, 'L'), array(0xf39, 0xf39, 'NSM'),
        array(0xf3a, 0xf3d, 'ON'), array(0xf3e, 0xf47, 'L'),
        array(0xf49, 0xf6c, 'L'), array(0xf71, 0xf7e, 'NSM'),
        array(0xf7f, 0xf7f, 'L'), array(0xf80, 0xf84, 'NSM'),
        array(0xf85, 0xf85, 'L'), array(0xf86, 0xf87, 'NSM'),
        array(0xf88, 0xf8b, 'L'), array(0xf90, 0xf97, 'NSM'),
        array(0xf99, 0xfbc, 'NSM'), array(0xfbe, 0xfc5, 'L'),
        array(0xfc6, 0xfc6, 'NSM'), array(0xfc7, 0xfcc, 'L'),
        array(0xfce, 0xfd4, 'L'), array(0x1000, 0x102c, 'L'),
        array(0x102d, 0x1030, 'NSM'), array(0x1031, 0x1031, 'L'),
        array(0x1032, 0x1037, 'NSM'), array(0x1038, 0x1038, 'L'),
        array(0x1039, 0x103a, 'NSM'), array(0x103b, 0x103c, 'L'),
        array(0x103d, 0x103e, 'NSM'), array(0x103f, 0x1057, 'L'),
        array(0x1058, 0x1059, 'NSM'), array(0x105a, 0x105d, 'L'),
        array(0x105e, 0x1060, 'NSM'), array(0x1061, 0x1070, 'L'),
        array(0x1071, 0x1074, 'NSM'), array(0x1075, 0x1081, 'L'),
        array(0x1082, 0x1082, 'NSM'), array(0x1083, 0x1084, 'L'),
        array(0x1085, 0x1086, 'NSM'), array(0x1087, 0x108c, 'L'),
        array(0x108d, 0x108d, 'NSM'), array(0x108e, 0x1099, 'L'),
        array(0x109e, 0x10c5, 'L'), array(0x10d0, 0x10fc, 'L'),
        array(0x1100, 0x1159, 'L'), array(0x115f, 0x11a2, 'L'),
        array(0x11a8, 0x11f9, 'L'), array(0x1200, 0x1248, 'L'),
        array(0x124a, 0x124d, 'L'), array(0x1250, 0x1256, 'L'),
        array(0x1258, 0x1258, 'L'), array(0x125a, 0x125d, 'L'),
        array(0x1260, 0x1288, 'L'), array(0x128a, 0x128d, 'L'),
        array(0x1290, 0x12b0, 'L'), array(0x12b2, 0x12b5, 'L'),
        array(0x12b8, 0x12be, 'L'), array(0x12c0, 0x12c0, 'L'),
        array(0x12c2, 0x12c5, 'L'), array(0x12c8, 0x12d6, 'L'),
        array(0x12d8, 0x1310, 'L'), array(0x1312, 0x1315, 'L'),
        array(0x1318, 0x135a, 'L'), array(0x135f, 0x135f, 'NSM'),
        array(0x1360, 0x137c, 'L'), array(0x1380, 0x138f, 'L'),
        array(0x1390, 0x1399, 'ON'), array(0x13a0, 0x13f4, 'L'),
        array(0x1401, 0x1676, 'L'), array(0x1680, 0x1680, 'WS'),
        array(0x1681, 0x169a, 'L'), array(0x169b, 0x169c, 'ON'),
        array(0x16a0, 0x16f0, 'L'), array(0x1700, 0x170c, 'L'),
        array(0x170e, 0x1711, 'L'), array(0x1712, 0x1714, 'NSM'),
        array(0x1720, 0x1731, 'L'), array(0x1732, 0x1734, 'NSM'),
        array(0x1735, 0x1736, 'L'), array(0x1740, 0x1751, 'L'),
        array(0x1752, 0x1753, 'NSM'), array(0x1760, 0x176c, 'L'),
        array(0x176e, 0x1770, 'L'), array(0x1772, 0x1773, 'NSM'),
        array(0x1780, 0x17b6, 'L'), array(0x17b7, 0x17bd, 'NSM'),
        array(0x17be, 0x17c5, 'L'), array(0x17c6, 0x17c6, 'NSM'),
        array(0x17c7, 0x17c8, 'L'), array(0x17c9, 0x17d3, 'NSM'),
        array(0x17d4, 0x17da, 'L'), array(0x17db, 0x17db, 'ET'),
        array(0x17dc, 0x17dc, 'L'), array(0x17dd, 0x17dd, 'NSM'),
        array(0x17e0, 0x17e9, 'L'), array(0x17f0, 0x17f9, 'ON'),
        array(0x1800, 0x180a, 'ON'), array(0x180b, 0x180d, 'NSM'),
        array(0x180e, 0x180e, 'WS'), array(0x1810, 0x1819, 'L'),
        array(0x1820, 0x1877, 'L'), array(0x1880, 0x18a8, 'L'),
        array(0x18a9, 0x18a9, 'NSM'), array(0x18aa, 0x18aa, 'L'),
        array(0x1900, 0x191c, 'L'), array(0x1920, 0x1922, 'NSM'),
        array(0x1923, 0x1926, 'L'), array(0x1927, 0x1928, 'NSM'),
        array(0x1929, 0x192b, 'L'), array(0x1930, 0x1931, 'L'),
        array(0x1932, 0x1932, 'NSM'), array(0x1933, 0x1938, 'L'),
        array(0x1939, 0x193b, 'NSM'), array(0x1940, 0x1940, 'ON'),
        array(0x1944, 0x1945, 'ON'), array(0x1946, 0x196d, 'L'),
        array(0x1970, 0x1974, 'L'), array(0x1980, 0x19a9, 'L'),
        array(0x19b0, 0x19c9, 'L'), array(0x19d0, 0x19d9, 'L'),
        array(0x19de, 0x19ff, 'ON'), array(0x1a00, 0x1a16, 'L'),
        array(0x1a17, 0x1a18, 'NSM'), array(0x1a19, 0x1a1b, 'L'),
        array(0x1a1e, 0x1a1f, 'L'), array(0x1b00, 0x1b03, 'NSM'),
        array(0x1b04, 0x1b33, 'L'), array(0x1b34, 0x1b34, 'NSM'),
        array(0x1b35, 0x1b35, 'L'), array(0x1b36, 0x1b3a, 'NSM'),
        array(0x1b3b, 0x1b3b, 'L'), array(0x1b3c, 0x1b3c, 'NSM'),
        array(0x1b3d, 0x1b41, 'L'), array(0x1b42, 0x1b42, 'NSM'),
        array(0x1b43, 0x1b4b, 'L'), array(0x1b50, 0x1b6a, 'L'),
        array(0x1b6b, 0x1b73, 'NSM'), array(0x1b74, 0x1b7c, 'L'),
        array(0x1b80, 0x1b81, 'NSM'), array(0x1b82, 0x1ba1, 'L'),
        array(0x1ba2, 0x1ba5, 'NSM'), array(0x1ba6, 0x1ba7, 'L'),
        array(0x1ba8, 0x1ba9, 'NSM'), array(0x1baa, 0x1baa, 'L'),
        array(0x1bae, 0x1bb9, 'L'), array(0x1c00, 0x1c2b, 'L'),
        array(0x1c2c, 0x1c33, 'NSM'), array(0x1c34, 0x1c35, 'L'),
        array(0x1c36, 0x1c37, 'NSM'), array(0x1c3b, 0x1c49, 'L'),
        array(0x1c4d, 0x1c7f, 'L'), array(0x1d00, 0x1dbf, 'L'),
        array(0x1dc0, 0x1de6, 'NSM'), array(0x1dfe, 0x1dff, 'NSM'),
        array(0x1e00, 0x1f15, 'L'), array(0x1f18, 0x1f1d, 'L'),
        array(0x1f20, 0x1f45, 'L'), array(0x1f48, 0x1f4d, 'L'),
        array(0x1f50, 0x1f57, 'L'), array(0x1f59, 0x1f59, 'L'),
        array(0x1f5b, 0x1f5b, 'L'), array(0x1f5d, 0x1f5d, 'L'),
        array(0x1f5f, 0x1f7d, 'L'), array(0x1f80, 0x1fb4, 'L'),
        array(0x1fb6, 0x1fbc, 'L'), array(0x1fbd, 0x1fbd, 'ON'),
        array(0x1fbe, 0x1fbe, 'L'), array(0x1fbf, 0x1fc1, 'ON'),
        array(0x1fc2, 0x1fc4, 'L'), array(0x1fc6, 0x1fcc, 'L'),
        array(0x1fcd, 0x1fcf, 'ON'), array(0x1fd0, 0x1fd3, 'L'),
        array(0x1fd6, 0x1fdb, 'L'), array(0x1fdd, 0x1fdf, 'ON'),
        array(0x1fe0, 0x1fec, 'L'), array(0x1fed, 0x1fef, 'ON'),
        array(0x1ff2, 0x1ff4, 'L'), array(0x1ff6, 0x1ffc, 'L'),
        array(0x1ffd, 0x1ffe, 'ON'), array(0x2000, 0x200a, 'WS'),
        array(0x200b, 0x200d, 'BN'), array(0x200e, 0x200e, 'L'),
        array(0x200f, 0x200f, 'R'), array(0x2010, 0x2027, 'ON'),
        array(0x2028, 0x2028, 'WS'), array(0x2029, 0x2029, 'B'),
        array(0x202a, 0x202a, 'LRE'), array(0x202b, 0x202b, 'RLE'),
        array(0x202c, 0x202c, 'PDF'), array(0x202d, 0x202d, 'LRO'),
        array(0x202e, 0x202e, 'RLO'), array(0x202f, 0x202f, 'CS'),
        array(0x2030, 0x2034, 'ET'), array(0x2035, 0x2043, 'ON'),
        array(0x2044, 0x2044, 'CS'), array(0x2045, 0x205e, 'ON'),
        array(0x205f, 0x205f, 'WS'), array(0x2060, 0x2064, 'BN'),
        array(0x206a, 0x206f, 'BN'), array(0x2070, 0x2070, 'EN'),
        array(0x2071, 0x2071, 'L'), array(0x2074, 0x2079, 'EN'),
        array(0x207a, 0x207b, 'ES'), array(0x207c, 0x207e, 'ON'),
        array(0x207f, 0x207f, 'L'), array(0x2080, 0x2089, 'EN'),
        array(0x208a, 0x208b, 'ES'), array(0x208c, 0x208e, 'ON'),
        array(0x2090, 0x2094, 'L'), array(0x20a0, 0x20b5, 'ET'),
        array(0x20d0, 0x20f0, 'NSM'), array(0x2100, 0x2101, 'ON'),
        array(0x2102, 0x2102, 'L'), array(0x2103, 0x2106, 'ON'),
        array(0x2107, 0x2107, 'L'), array(0x2108, 0x2109, 'ON'),
        array(0x210a, 0x2113, 'L'), array(0x2114, 0x2114, 'ON'),
        array(0x2115, 0x2115, 'L'), array(0x2116, 0x2118, 'ON'),
        array(0x2119, 0x211d, 'L'), array(0x211e, 0x2123, 'ON'),
        array(0x2124, 0x2124, 'L'), array(0x2125, 0x2125, 'ON'),
        array(0x2126, 0x2126, 'L'), array(0x2127, 0x2127, 'ON'),
        array(0x2128, 0x2128, 'L'), array(0x2129, 0x2129, 'ON'),
        array(0x212a, 0x212d, 'L'), array(0x212e, 0x212e, 'ET'),
        array(0x212f, 0x2139, 'L'), array(0x213a, 0x213b, 'ON'),
        array(0x213c, 0x213f, 'L'), array(0x2140, 0x2144, 'ON'),
        array(0x2145, 0x2149, 'L'), array(0x214a, 0x214d, 'ON'),
        array(0x214e, 0x214f, 'L'), array(0x2153, 0x215f, 'ON'),
        array(0x2160, 0x2188, 'L'), array(0x2190, 0x2211, 'ON'),
        array(0x2212, 0x2212, 'ES'), array(0x2213, 0x2213, 'ET'),
        array(0x2214, 0x2335, 'ON'), array(0x2336, 0x237a, 'L'),
        array(0x237b, 0x2394, 'ON'), array(0x2395, 0x2395, 'L'),
        array(0x2396, 0x23e7, 'ON'), array(0x2400, 0x2426, 'ON'),
        array(0x2440, 0x244a, 'ON'), array(0x2460, 0x2487, 'ON'),
        array(0x2488, 0x249b, 'EN'), array(0x249c, 0x24e9, 'L'),
        array(0x24ea, 0x269d, 'ON'), array(0x26a0, 0x26ab, 'ON'),
        array(0x26ac, 0x26ac, 'L'), array(0x26ad, 0x26bc, 'ON'),
        array(0x26c0, 0x26c3, 'ON'), array(0x2701, 0x2704, 'ON'),
        array(0x2706, 0x2709, 'ON'), array(0x270c, 0x2727, 'ON'),
        array(0x2729, 0x274b, 'ON'), array(0x274d, 0x274d, 'ON'),
        array(0x274f, 0x2752, 'ON'), array(0x2756, 0x2756, 'ON'),
        array(0x2758, 0x275e, 'ON'), array(0x2761, 0x2794, 'ON'),
        array(0x2798, 0x27af, 'ON'), array(0x27b1, 0x27be, 'ON'),
        array(0x27c0, 0x27ca, 'ON'), array(0x27cc, 0x27cc, 'ON'),
        array(0x27d0, 0x27ff, 'ON'), array(0x2800, 0x28ff, 'L'),
        array(0x2900, 0x2b4c, 'ON'), array(0x2b50, 0x2b54, 'ON'),
        array(0x2c00, 0x2c2e, 'L'), array(0x2c30, 0x2c5e, 'L'),
        array(0x2c60, 0x2c6f, 'L'), array(0x2c71, 0x2c7d, 'L'),
        array(0x2c80, 0x2ce4, 'L'), array(0x2ce5, 0x2cea, 'ON'),
        array(0x2cf9, 0x2cff, 'ON'), array(0x2d00, 0x2d25, 'L'),
        array(0x2d30, 0x2d65, 'L'), array(0x2d6f, 0x2d6f, 'L'),
        array(0x2d80, 0x2d96, 'L'), array(0x2da0, 0x2da6, 'L'),
        array(0x2da8, 0x2dae, 'L'), array(0x2db0, 0x2db6, 'L'),
        array(0x2db8, 0x2dbe, 'L'), array(0x2dc0, 0x2dc6, 'L'),
        array(0x2dc8, 0x2dce, 'L'), array(0x2dd0, 0x2dd6, 'L'),
        array(0x2dd8, 0x2dde, 'L'), array(0x2de0, 0x2dff, 'NSM'),
        array(0x2e00, 0x2e30, 'ON'), array(0x2e80, 0x2e99, 'ON'),
        array(0x2e9b, 0x2ef3, 'ON'), array(0x2f00, 0x2fd5, 'ON'),
        array(0x2ff0, 0x2ffb, 'ON'), array(0x3000, 0x3000, 'WS'),
        array(0x3001, 0x3004, 'ON'), array(0x3005, 0x3007, 'L'),
        array(0x3008, 0x3020, 'ON'), array(0x3021, 0x3029, 'L'),
        array(0x302a, 0x302f, 'NSM'), array(0x3030, 0x3030, 'ON'),
        array(0x3031, 0x3035, 'L'), array(0x3036, 0x3037, 'ON'),
        array(0x3038, 0x303c, 'L'), array(0x303d, 0x303f, 'ON'),
        array(0x3041, 0x3096, 'L'), array(0x3099, 0x309a, 'NSM'),
        array(0x309b, 0x309c, 'ON'), array(0x309d, 0x309f, 'L'),
        array(0x30a0, 0x30a0, 'ON'), array(0x30a1, 0x30fa, 'L'),
        array(0x30fb, 0x30fb, 'ON'), array(0x30fc, 0x30ff, 'L'),
        array(0x3105, 0x312d, 'L'), array(0x3131, 0x318e, 'L'),
        array(0x3190, 0x31b7, 'L'), array(0x31c0, 0x31e3, 'ON'),
        array(0x31f0, 0x321c, 'L'), array(0x321d, 0x321e, 'ON'),
        array(0x3220, 0x3243, 'L'), array(0x3250, 0x325f, 'ON'),
        array(0x3260, 0x327b, 'L'), array(0x327c, 0x327e, 'ON'),
        array(0x327f, 0x32b0, 'L'), array(0x32b1, 0x32bf, 'ON'),
        array(0x32c0, 0x32cb, 'L'), array(0x32cc, 0x32cf, 'ON'),
        array(0x32d0, 0x32fe, 'L'), array(0x3300, 0x3376, 'L'),
        array(0x3377, 0x337a, 'ON'), array(0x337b, 0x33dd, 'L'),
        array(0x33de, 0x33df, 'ON'), array(0x33e0, 0x33fe, 'L'),
        array(0x33ff, 0x33ff, 'ON'), array(0x3400, 0x4db5, 'L'),
        array(0x4dc0, 0x4dff, 'ON'), array(0x4e00, 0x9fc3, 'L'),
        array(0xa000, 0xa48c, 'L'), array(0xa490, 0xa4c6, 'ON'),
        array(0xa500, 0xa60c, 'L'), array(0xa60d, 0xa60f, 'ON'),
        array(0xa610, 0xa62b, 'L'), array(0xa640, 0xa65f, 'L'),
        array(0xa662, 0xa66e, 'L'), array(0xa66f, 0xa672, 'NSM'),
        array(0xa673, 0xa673, 'ON'), array(0xa67c, 0xa67d, 'NSM'),
        array(0xa67e, 0xa67f, 'ON'), array(0xa680, 0xa697, 'L'),
        array(0xa700, 0xa721, 'ON'), array(0xa722, 0xa787, 'L'),
        array(0xa788, 0xa788, 'ON'), array(0xa789, 0xa78c, 'L'),
        array(0xa7fb, 0xa801, 'L'), array(0xa802, 0xa802, 'NSM'),
        array(0xa803, 0xa805, 'L'), array(0xa806, 0xa806, 'NSM'),
        array(0xa807, 0xa80a, 'L'), array(0xa80b, 0xa80b, 'NSM'),
        array(0xa80c, 0xa824, 'L'), array(0xa825, 0xa826, 'NSM'),
        array(0xa827, 0xa827, 'L'), array(0xa828, 0xa82b, 'ON'),
        array(0xa840, 0xa873, 'L'), array(0xa874, 0xa877, 'ON'),
        array(0xa880, 0xa8c3, 'L'), array(0xa8c4, 0xa8c4, 'NSM'),
        array(0xa8ce, 0xa8d9, 'L'), array(0xa900, 0xa925, 'L'),
        array(0xa926, 0xa92d, 'NSM'), array(0xa92e, 0xa946, 'L'),
        array(0xa947, 0xa951, 'NSM'), array(0xa952, 0xa953, 'L'),
        array(0xa95f, 0xa95f, 'L'), array(0xaa00, 0xaa28, 'L'),
        array(0xaa29, 0xaa2e, 'NSM'), array(0xaa2f, 0xaa30, 'L'),
        array(0xaa31, 0xaa32, 'NSM'), array(0xaa33, 0xaa34, 'L'),
        array(0xaa35, 0xaa36, 'NSM'), array(0xaa40, 0xaa42, 'L'),
        array(0xaa43, 0xaa43, 'NSM'), array(0xaa44, 0xaa4b, 'L'),
        array(0xaa4c, 0xaa4c, 'NSM'), array(0xaa4d, 0xaa4d, 'L'),
        array(0xaa50, 0xaa59, 'L'), array(0xaa5c, 0xaa5f, 'L'),
        array(0xac00, 0xd7a3, 'L'), array(0xd800, 0xfa2d, 'L'),
        array(0xfa30, 0xfa6a, 'L'), array(0xfa70, 0xfad9, 'L'),
        array(0xfb00, 0xfb06, 'L'), array(0xfb13, 0xfb17, 'L'),
        array(0xfb1d, 0xfb1d, 'R'), array(0xfb1e, 0xfb1e, 'NSM'),
        array(0xfb1f, 0xfb28, 'R'), array(0xfb29, 0xfb29, 'ES'),
        array(0xfb2a, 0xfb36, 'R'), array(0xfb38, 0xfb3c, 'R'),
        array(0xfb3e, 0xfb3e, 'R'), array(0xfb40, 0xfb41, 'R'),
        array(0xfb43, 0xfb44, 'R'), array(0xfb46, 0xfb4f, 'R'),
        array(0xfb50, 0xfbb1, 'AL'), array(0xfbd3, 0xfd3d, 'AL'),
        array(0xfd3e, 0xfd3f, 'ON'), array(0xfd50, 0xfd8f, 'AL'),
        array(0xfd92, 0xfdc7, 'AL'), array(0xfdf0, 0xfdfc, 'AL'),
        array(0xfdfd, 0xfdfd, 'ON'), array(0xfe00, 0xfe0f, 'NSM'),
        array(0xfe10, 0xfe19, 'ON'), array(0xfe20, 0xfe26, 'NSM'),
        array(0xfe30, 0xfe4f, 'ON'), array(0xfe50, 0xfe50, 'CS'),
        array(0xfe51, 0xfe51, 'ON'), array(0xfe52, 0xfe52, 'CS'),
        array(0xfe54, 0xfe54, 'ON'), array(0xfe55, 0xfe55, 'CS'),
        array(0xfe56, 0xfe5e, 'ON'), array(0xfe5f, 0xfe5f, 'ET'),
        array(0xfe60, 0xfe61, 'ON'), array(0xfe62, 0xfe63, 'ES'),
        array(0xfe64, 0xfe66, 'ON'), array(0xfe68, 0xfe68, 'ON'),
        array(0xfe69, 0xfe6a, 'ET'), array(0xfe6b, 0xfe6b, 'ON'),
        array(0xfe70, 0xfe74, 'AL'), array(0xfe76, 0xfefc, 'AL'),
        array(0xfeff, 0xfeff, 'BN'), array(0xff01, 0xff02, 'ON'),
        array(0xff03, 0xff05, 'ET'), array(0xff06, 0xff0a, 'ON'),
        array(0xff0b, 0xff0b, 'ES'), array(0xff0c, 0xff0c, 'CS'),
        array(0xff0d, 0xff0d, 'ES'), array(0xff0e, 0xff0f, 'CS'),
        array(0xff10, 0xff19, 'EN'), array(0xff1a, 0xff1a, 'CS'),
        array(0xff1b, 0xff20, 'ON'), array(0xff21, 0xff3a, 'L'),
        array(0xff3b, 0xff40, 'ON'), array(0xff41, 0xff5a, 'L'),
        array(0xff5b, 0xff65, 'ON'), array(0xff66, 0xffbe, 'L'),
        array(0xffc2, 0xffc7, 'L'), array(0xffca, 0xffcf, 'L'),
        array(0xffd2, 0xffd7, 'L'), array(0xffda, 0xffdc, 'L'),
        array(0xffe0, 0xffe1, 'ET'), array(0xffe2, 0xffe4, 'ON'),
        array(0xffe5, 0xffe6, 'ET'), array(0xffe8, 0xffee, 'ON'),
        array(0xfff9, 0xfffd, 'ON'), array(0x10000, 0x1000b, 'L'),
        array(0x1000d, 0x10026, 'L'), array(0x10028, 0x1003a, 'L'),
        array(0x1003c, 0x1003d, 'L'), array(0x1003f, 0x1004d, 'L'),
        array(0x10050, 0x1005d, 'L'), array(0x10080, 0x100fa, 'L'),
        array(0x10100, 0x10100, 'L'), array(0x10101, 0x10101, 'ON'),
        array(0x10102, 0x10102, 'L'), array(0x10107, 0x10133, 'L'),
        array(0x10137, 0x1013f, 'L'), array(0x10140, 0x1018a, 'ON'),
        array(0x10190, 0x1019b, 'ON'), array(0x101d0, 0x101fc, 'L'),
        array(0x101fd, 0x101fd, 'NSM'), array(0x10280, 0x1029c, 'L'),
        array(0x102a0, 0x102d0, 'L'), array(0x10300, 0x1031e, 'L'),
        array(0x10320, 0x10323, 'L'), array(0x10330, 0x1034a, 'L'),
        array(0x10380, 0x1039d, 'L'), array(0x1039f, 0x103c3, 'L'),
        array(0x103c8, 0x103d5, 'L'), array(0x10400, 0x1049d, 'L'),
        array(0x104a0, 0x104a9, 'L'), array(0x10800, 0x10805, 'R'),
        array(0x10808, 0x10808, 'R'), array(0x1080a, 0x10835, 'R'),
        array(0x10837, 0x10838, 'R'), array(0x1083c, 0x1083c, 'R'),
        array(0x1083f, 0x1083f, 'R'), array(0x10900, 0x10919, 'R'),
        array(0x1091f, 0x1091f, 'ON'), array(0x10920, 0x10939, 'R'),
        array(0x1093f, 0x1093f, 'R'), array(0x10a00, 0x10a00, 'R'),
        array(0x10a01, 0x10a03, 'NSM'), array(0x10a05, 0x10a06, 'NSM'),
        array(0x10a0c, 0x10a0f, 'NSM'), array(0x10a10, 0x10a13, 'R'),
        array(0x10a15, 0x10a17, 'R'), array(0x10a19, 0x10a33, 'R'),
        array(0x10a38, 0x10a3a, 'NSM'), array(0x10a3f, 0x10a3f, 'NSM'),
        array(0x10a40, 0x10a47, 'R'), array(0x10a50, 0x10a58, 'R'),
        array(0x12000, 0x1236e, 'L'), array(0x12400, 0x12462, 'L'),
        array(0x12470, 0x12473, 'L'), array(0x1d000, 0x1d0f5, 'L'),
        array(0x1d100, 0x1d126, 'L'), array(0x1d129, 0x1d166, 'L'),
        array(0x1d167, 0x1d169, 'NSM'), array(0x1d16a, 0x1d172, 'L'),
        array(0x1d173, 0x1d17a, 'BN'), array(0x1d17b, 0x1d182, 'NSM'),
        array(0x1d183, 0x1d184, 'L'), array(0x1d185, 0x1d18b, 'NSM'),
        array(0x1d18c, 0x1d1a9, 'L'), array(0x1d1aa, 0x1d1ad, 'NSM'),
        array(0x1d1ae, 0x1d1dd, 'L'), array(0x1d200, 0x1d241, 'ON'),
        array(0x1d242, 0x1d244, 'NSM'), array(0x1d245, 0x1d245, 'ON'),
        array(0x1d300, 0x1d356, 'ON'), array(0x1d360, 0x1d371, 'L'),
        array(0x1d400, 0x1d454, 'L'), array(0x1d456, 0x1d49c, 'L'),
        array(0x1d49e, 0x1d49f, 'L'), array(0x1d4a2, 0x1d4a2, 'L'),
        array(0x1d4a5, 0x1d4a6, 'L'), array(0x1d4a9, 0x1d4ac, 'L'),
        array(0x1d4ae, 0x1d4b9, 'L'), array(0x1d4bb, 0x1d4bb, 'L'),
        array(0x1d4bd, 0x1d4c3, 'L'), array(0x1d4c5, 0x1d505, 'L'),
        array(0x1d507, 0x1d50a, 'L'), array(0x1d50d, 0x1d514, 'L'),
        array(0x1d516, 0x1d51c, 'L'), array(0x1d51e, 0x1d539, 'L'),
        array(0x1d53b, 0x1d53e, 'L'), array(0x1d540, 0x1d544, 'L'),
        array(0x1d546, 0x1d546, 'L'), array(0x1d54a, 0x1d550, 'L'),
        array(0x1d552, 0x1d6a5, 'L'), array(0x1d6a8, 0x1d7cb, 'L'),
        array(0x1d7ce, 0x1d7ff, 'EN'), array(0x1f000, 0x1f02b, 'ON'),
        array(0x1f030, 0x1f093, 'ON'), array(0x20000, 0x2a6d6, 'L'),
        array(0x2f800, 0x2fa1d, 'L'), array(0xe0001, 0xe0001, 'BN'),
        array(0xe0020, 0xe007f, 'BN'), array(0xe0100, 0xe01ef, 'NSM'),
        array(0xf0000, 0xffffd, 'L'), array(0x100000, 0x10fffd, 'L'));

    /**
     * Lookup array describing mirrored characters for bidirectional text. See
     * discussion in {@link getBidiMirroredCharacters()}.
     *
     * @var array
     * @ignore Ignored to prevent phpDocumentor from attempting to make a
     *   complete copy of the array in the generated API documentation.
     */
    private static $_bidiMirroredCharacters = array(
        0x29 => false, 0x2a => false, 0x3d => false, 0x3f => false,
        0x5c => false, 0x5e => false, 0x7c => false, 0x7e => false,
        0xac => false, 0xbc => false, 0xf3a => false, 0xf3b => false,
        0xf3c => false, 0xf3d => false, 0x169b => false, 0x169c => false,
        0x2039 => false, 0x203a => false, 0x2045 => false, 0x2046 => false,
        0x207d => false, 0x207e => false, 0x208d => false, 0x208e => false,
        0x2140 => false, 0x2201 => false, 0x2202 => false, 0x2203 => false,
        0x2204 => false, 0x2208 => false, 0x2209 => false, 0x220a => false,
        0x220b => false, 0x220c => false, 0x220d => false, 0x2211 => false,
        0x2215 => false, 0x2216 => false, 0x221a => false, 0x221b => false,
        0x221c => false, 0x221d => false, 0x221f => false, 0x2220 => false,
        0x2221 => false, 0x2222 => false, 0x2224 => false, 0x2226 => false,
        0x222b => false, 0x222c => false, 0x222d => false, 0x222e => false,
        0x222f => false, 0x2230 => false, 0x2231 => false, 0x2232 => false,
        0x2233 => false, 0x2239 => false, 0x223b => false, 0x223c => false,
        0x223d => false, 0x223e => false, 0x223f => false, 0x2240 => false,
        0x2241 => false, 0x2242 => false, 0x2243 => false, 0x2244 => false,
        0x2245 => false, 0x2246 => false, 0x2247 => false, 0x2248 => false,
        0x2249 => false, 0x224a => false, 0x224b => false, 0x224c => false,
        0x2252 => false, 0x2253 => false, 0x2254 => false, 0x2255 => false,
        0x225f => false, 0x2260 => false, 0x2262 => false, 0x2264 => false,
        0x2265 => false, 0x2266 => false, 0x2267 => false, 0x2268 => false,
        0x2269 => false, 0x226a => false, 0x226b => false, 0x226e => false,
        0x226f => false, 0x2270 => false, 0x2271 => false, 0x2272 => false,
        0x2273 => false, 0x2274 => false, 0x2275 => false, 0x2276 => false,
        0x2277 => false, 0x2278 => false, 0x2279 => false, 0x227a => false,
        0x227b => false, 0x227c => false, 0x227d => false, 0x227e => false,
        0x227f => false, 0x2280 => false, 0x2281 => false, 0x2282 => false,
        0x2283 => false, 0x2284 => false, 0x2285 => false, 0x2286 => false,
        0x2287 => false, 0x2288 => false, 0x2289 => false, 0x228a => false,
        0x228b => false, 0x228c => false, 0x228f => false, 0x2290 => false,
        0x2291 => false, 0x2292 => false, 0x2298 => false, 0x22a2 => false,
        0x22a3 => false, 0x22a6 => false, 0x22a7 => false, 0x22a8 => false,
        0x22a9 => false, 0x22aa => false, 0x22ab => false, 0x22ac => false,
        0x22ad => false, 0x22ae => false, 0x22af => false, 0x22b0 => false,
        0x22b1 => false, 0x22b2 => false, 0x22b3 => false, 0x22b4 => false,
        0x22b5 => false, 0x22b6 => false, 0x22b7 => false, 0x22b8 => false,
        0x22be => false, 0x22bf => false, 0x22c9 => false, 0x22ca => false,
        0x22cb => false, 0x22cc => false, 0x22cd => false, 0x22d0 => false,
        0x22d1 => false, 0x22d6 => false, 0x22d7 => false, 0x22d8 => false,
        0x22d9 => false, 0x22da => false, 0x22db => false, 0x22dc => false,
        0x22dd => false, 0x22de => false, 0x22df => false, 0x22e0 => false,
        0x22e1 => false, 0x22e2 => false, 0x22e3 => false, 0x22e4 => false,
        0x22e5 => false, 0x22e6 => false, 0x22e7 => false, 0x22e8 => false,
        0x22e9 => false, 0x22ea => false, 0x22eb => false, 0x22ec => false,
        0x22ed => false, 0x22f0 => false, 0x22f1 => false, 0x22f2 => false,
        0x22f3 => false, 0x22f4 => false, 0x22f5 => false, 0x22f6 => false,
        0x22f7 => false, 0x22f8 => false, 0x22f9 => false, 0x22fa => false,
        0x22fb => false, 0x22fc => false, 0x22fd => false, 0x22fe => false,
        0x22ff => false, 0x2308 => false, 0x2309 => false, 0x230a => false,
        0x230b => false, 0x2320 => false, 0x2321 => false, 0x2329 => false,
        0x232a => false, 0x2768 => false, 0x2769 => false, 0x276a => false,
        0x276b => false, 0x276c => false, 0x276d => false, 0x276e => false,
        0x276f => false, 0x2770 => false, 0x2771 => false, 0x2772 => false,
        0x2773 => false, 0x2774 => false, 0x2775 => false, 0x27c0 => false,
        0x27c3 => false, 0x27c4 => false, 0x27c5 => false, 0x27c6 => false,
        0x27c8 => false, 0x27c9 => false, 0x27cc => false, 0x27d3 => false,
        0x27d4 => false, 0x27d5 => false, 0x27d6 => false, 0x27dc => false,
        0x27dd => false, 0x27de => false, 0x27e2 => false, 0x27e3 => false,
        0x27e4 => false, 0x27e5 => false, 0x27e6 => false, 0x27e7 => false,
        0x27e8 => false, 0x27e9 => false, 0x27ea => false, 0x27eb => false,
        0x27ec => false, 0x27ed => false, 0x27ee => false, 0x27ef => false,
        0x2983 => false, 0x2984 => false, 0x2985 => false, 0x2986 => false,
        0x2987 => false, 0x2988 => false, 0x2989 => false, 0x298a => false,
        0x298b => false, 0x298c => false, 0x298d => false, 0x298e => false,
        0x298f => false, 0x2990 => false, 0x2991 => false, 0x2992 => false,
        0x2993 => false, 0x2994 => false, 0x2995 => false, 0x2996 => false,
        0x2997 => false, 0x2998 => false, 0x299b => false, 0x299c => false,
        0x299d => false, 0x299e => false, 0x299f => false, 0x29a0 => false,
        0x29a1 => false, 0x29a2 => false, 0x29a3 => false, 0x29a4 => false,
        0x29a5 => false, 0x29a6 => false, 0x29a7 => false, 0x29a8 => false,
        0x29a9 => false, 0x29aa => false, 0x29ab => false, 0x29ac => false,
        0x29ad => false, 0x29ae => false, 0x29af => false, 0x29b8 => false,
        0x29c0 => false, 0x29c1 => false, 0x29c2 => false, 0x29c3 => false,
        0x29c4 => false, 0x29c5 => false, 0x29c9 => false, 0x29ce => false,
        0x29cf => false, 0x29d0 => false, 0x29d1 => false, 0x29d2 => false,
        0x29d4 => false, 0x29d5 => false, 0x29d8 => false, 0x29d9 => false,
        0x29da => false, 0x29db => false, 0x29dc => false, 0x29e1 => false,
        0x29e3 => false, 0x29e4 => false, 0x29e5 => false, 0x29e8 => false,
        0x29e9 => false, 0x29f4 => false, 0x29f5 => false, 0x29f6 => false,
        0x29f7 => false, 0x29f8 => false, 0x29f9 => false, 0x29fc => false,
        0x29fd => false, 0x2a0a => false, 0x2a0b => false, 0x2a0c => false,
        0x2a0d => false, 0x2a0e => false, 0x2a0f => false, 0x2a10 => false,
        0x2a11 => false, 0x2a12 => false, 0x2a13 => false, 0x2a14 => false,
        0x2a15 => false, 0x2a16 => false, 0x2a17 => false, 0x2a18 => false,
        0x2a19 => false, 0x2a1a => false, 0x2a1b => false, 0x2a1c => false,
        0x2a1e => false, 0x2a1f => false, 0x2a20 => false, 0x2a21 => false,
        0x2a24 => false, 0x2a26 => false, 0x2a29 => false, 0x2a2b => false,
        0x2a2c => false, 0x2a2d => false, 0x2a2e => false, 0x2a34 => false,
        0x2a35 => false, 0x2a3c => false, 0x2a3d => false, 0x2a3e => false,
        0x2a57 => false, 0x2a58 => false, 0x2a64 => false, 0x2a65 => false,
        0x2a6a => false, 0x2a6b => false, 0x2a6c => false, 0x2a6d => false,
        0x2a6f => false, 0x2a70 => false, 0x2a73 => false, 0x2a74 => false,
        0x2a79 => false, 0x2a7a => false, 0x2a7b => false, 0x2a7c => false,
        0x2a7d => false, 0x2a7e => false, 0x2a7f => false, 0x2a80 => false,
        0x2a81 => false, 0x2a82 => false, 0x2a83 => false, 0x2a84 => false,
        0x2a85 => false, 0x2a86 => false, 0x2a87 => false, 0x2a88 => false,
        0x2a89 => false, 0x2a8a => false, 0x2a8b => false, 0x2a8c => false,
        0x2a8d => false, 0x2a8e => false, 0x2a8f => false, 0x2a90 => false,
        0x2a91 => false, 0x2a92 => false, 0x2a93 => false, 0x2a94 => false,
        0x2a95 => false, 0x2a96 => false, 0x2a97 => false, 0x2a98 => false,
        0x2a99 => false, 0x2a9a => false, 0x2a9b => false, 0x2a9c => false,
        0x2a9d => false, 0x2a9e => false, 0x2a9f => false, 0x2aa0 => false,
        0x2aa1 => false, 0x2aa2 => false, 0x2aa3 => false, 0x2aa6 => false,
        0x2aa7 => false, 0x2aa8 => false, 0x2aa9 => false, 0x2aaa => false,
        0x2aab => false, 0x2aac => false, 0x2aad => false, 0x2aaf => false,
        0x2ab0 => false, 0x2ab1 => false, 0x2ab2 => false, 0x2ab3 => false,
        0x2ab4 => false, 0x2ab5 => false, 0x2ab6 => false, 0x2ab7 => false,
        0x2ab8 => false, 0x2ab9 => false, 0x2aba => false, 0x2abb => false,
        0x2abc => false, 0x2abd => false, 0x2abe => false, 0x2abf => false,
        0x2ac0 => false, 0x2ac1 => false, 0x2ac2 => false, 0x2ac3 => false,
        0x2ac4 => false, 0x2ac5 => false, 0x2ac6 => false, 0x2ac7 => false,
        0x2ac8 => false, 0x2ac9 => false, 0x2aca => false, 0x2acb => false,
        0x2acc => false, 0x2acd => false, 0x2ace => false, 0x2acf => false,
        0x2ad0 => false, 0x2ad1 => false, 0x2ad2 => false, 0x2ad3 => false,
        0x2ad4 => false, 0x2ad5 => false, 0x2ad6 => false, 0x2adc => false,
        0x2ade => false, 0x2ae2 => false, 0x2ae3 => false, 0x2ae4 => false,
        0x2ae5 => false, 0x2ae6 => false, 0x2aec => false, 0x2aed => false,
        0x2aee => false, 0x2af3 => false, 0x2af7 => false, 0x2af8 => false,
        0x2af9 => false, 0x2afa => false, 0x2afb => false, 0x2afd => false,
        0x2e02 => false, 0x2e03 => false, 0x2e04 => false, 0x2e05 => false,
        0x2e09 => false, 0x2e0a => false, 0x2e0c => false, 0x2e0d => false,
        0x2e1c => false, 0x2e1d => false, 0x2e20 => false, 0x2e21 => false,
        0x2e22 => false, 0x2e23 => false, 0x2e24 => false, 0x2e25 => false,
        0x2e26 => false, 0x2e27 => false, 0x2e28 => false, 0x2e29 => false,
        0x3008 => false, 0x3009 => false, 0x300a => false, 0x300b => false,
        0x300c => false, 0x300d => false, 0x300e => false, 0x300f => false,
        0x3010 => false, 0x3011 => false, 0x3014 => false, 0x3015 => false,
        0x3016 => false, 0x3017 => false, 0x3018 => false, 0x3019 => false,
        0x301a => false, 0x301b => false, 0xfe59 => false, 0xfe5a => false,
        0xfe5b => false, 0xfe5c => false, 0xfe5d => false, 0xfe5e => false,
        0xfe64 => false, 0xfe65 => false, 0xff08 => false, 0xff09 => false,
        0xff1c => false, 0xff1e => false, 0xff3b => false, 0xff3d => false,
        0xff5b => false, 0xff5d => false, 0xff5f => false, 0xff60 => false,
        0xff62 => false, 0xff63 => false, 0x1d6db => false, 0x1d715 => false,
        0x1d74f => false, 0x1d789 => false, 0x1d7c3 => false);



  /**** Class Methods ****/

    /**
     * Returns an array containing the Unicode text direction classes for each of
     * the character codes. The keys of the resulting array match the keys of
     * the source array.
     *
     * @param array $characterCodes
     * @return array
     */
    public static function getTextDirectionClasses(array $characterCodes) {
        $segmentCount     = 856;
        $searchRange      = 512;
        $searchIterations = 10;

        $textDirectionClasses = array();
        foreach ($characterCodes as $key => $characterCode) {

            /* If the character code is invalid, use neutral.
             */
            if (($characterCode < 0) || ($characterCode > 0x10fffd)) {
                $lineBreakClasses[$key] = 'ON';
                continue;
            }

            /* Optimize for Latin-1 characters, one of the more common cases.
             */
            if ($characterCode < 256) {
                $textDirectionClasses[$key] = self::$_latinBidiDirectionClasses[$characterCode];
                continue;
            }

            /* Determine where to start the binary search. The segments are
             * ordered from lowest-to-highest. We are looking for the first
             * segment whose end code is greater than or equal to our character
             * code.
             *
             * If the end code at the top of the search range is larger, then
             * our target is probably below it.
             *
             * If it is smaller, our target is probably above it, so move the
             * search range to the end of the segment list.
             */
            if (self::$_bidiDirectionClasses[$searchRange][1] >= $characterCode) {
                $searchIndex = $searchRange;
            } else {
                $searchIndex = $segmentCount;
            }

            /* The text direction class is located by doing a binary search of the
             * array. No matter the size of the array, this allows us to locate
             * the value in exactly $searchIterations iterations.
             */
            $segmentIndex = 0;
            for ($i = 1; $i <= $searchIterations; $i++) {
                if (self::$_bidiDirectionClasses[$searchIndex][1] >= $characterCode) {
                    $segmentIndex = $searchIndex;
                    $searchIndex -= $searchRange >> $i;
                } else {
                    $searchIndex += $searchRange >> $i;
                }
            }

            /* If the segment's start code is greater than our character code,
             * that character does not have an explicit value. Use neutral.
             */
            if (self::$_bidiDirectionClasses[$segmentIndex][0] > $characterCode) {
                $textDirectionClasses[$key] = 'ON';
            } else {
                $textDirectionClasses[$key] = self::$_bidiDirectionClasses[$segmentIndex][2];
            }

        }
        return $textDirectionClasses;
    }

}
