<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage TextLayout
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * RE_Pdf_TextContainer objects are used to define arbitrarily-sized and
 * shaped regions in which to layout text. These regions are used by
 * {@link RE_Pdf_LayoutManager} to calculate individual line fragment
 * rectangles during text layout.
 *
 * Note that unlike a PDF page, the text container's coordinate system starts
 * at (0,0) in the upper-left corner of the container.
 *
 * If the text container has been attached to a layout manager, it must notify
 * the layout manager any its size or shape changes by calling
 * {@link RE_Pdf_LayoutManager::textContainerChangedGeometry()} so that any
 * layout that may have already been performed can be recalculated.
 *
 * @package    Pdf
 * @subpackage TextLayout
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
abstract class RE_Pdf_TextContainer
{
  /**** Class Constants ****/


  /* Line Sweep Direction */

    /**
     * Characters in the line of text move from left to right.
     */
    const LINE_SWEEP_RIGHT = 0;

    /**
     * Characters in the line of text move from right to left.
     */
    const LINE_SWEEP_LEFT = 1;

    /**
     * Characters in the line of text move from top to bottom.
     */
    const LINE_SWEEP_DOWN = 2;

    /**
     * Characters in the line of text move from bottom to top.
     */
    const LINE_SWEEP_UP = 3;


  /* Line Movement Direction */

    /**
     * New lines of text move from top to bottom.
     */
    const LINE_MOVEMENT_DOWN = 0;

    /**
     * New lines of text move from bottom to top.
     */
    const LINE_MOVEMENT_UP = 1;

    /**
     * New lines of text move from left to right.
     */
    const LINE_MOVEMENT_RIGHT = 2;

    /**
     * New lines of text move from right to left.
     */
    const LINE_MOVEMENT_LEFT = 3;


  /* Vertical Alignment */

    /**
     * Align the text container's contents with its top edge.
     */
    const VERTICAL_ALIGN_TOP = 0;

    /**
     * Vertically center the text container's contents.
     */
    const VERTICAL_ALIGN_MIDDLE = 1;

    /**
     * Align the text container's contents with its bottom edge.
     */
    const VERTICAL_ALIGN_BOTTOM = 2;


  /* Container Sizing */

    /**
     * Number used to represent "infinite" dimensions (1.0e7 points is roughly
     * 2.19 miles or 3.52 kilometers). This is approximately the largest
     * floating point value that can preserve pixel-level precision. Due to the
     * nature of floating point numbers, which lose precision as they get grow,
     * larger values may introduce layout anomalies... if you manage to fill
     * all of that space.
     */
    const LARGE_NUMBER_FOR_TEXT = 1.0e7;



  /**** Instance Variables ****/

    /**
     * The text container's internal bounding rectangle. Uses its own
     * independent coordinate space.
     * @var RE_Rect
     */
    protected $_boundingRect = null;

    /**
     * The distance in points of the space from the head and tail edges of the
     * line fragment rectangle that the typesetter should leave empty. Must be
     * positive or zero.
     * @var float
     */
    protected $_lineFragmentPadding = 0.0;

    /**
     * Vertical alignment of the text container's contents.
     * @var integer
     */
    protected $_verticalAlignment = RE_Pdf_TextContainer::VERTICAL_ALIGN_TOP;

    /**
     * Reference to the text container's layout manager.
     * @var RE_Pdf_LayoutManager
     */
    protected $_layoutManager = null;

    /**
     * PDF canvas on which this text container should draw its contents.
     * @var RE_Pdf_Canvas_Interface
     */
    protected $_canvas = null;

    /**
     * The origin on the PDF canvas (lower-left corner) for the text container.
     * @var RE_Point
     */
    protected $_origin = null;



  /**** Abstract Interface ****/

    /**
     * Returns a new line fragment rectangle by adjusting the size and/or
     * origin of the proposed line fragment rectangle so that the new rectangle
     * is completely contained within the text container's region. If the
     * proposed rectangle cannot be made to fit, returns an empty rectangle.
     *
     * If the proposed rectangle is too wide, it should be trimmed according
     * to the line sweep direction provided. For example, if the sweep
     * direction is {@link LINE_SWEEP_LEFT} (right-to-left text), the left
     * edge should be trimmed instead of the right edge.
     *
     * If the text container defines a non-contiguous region, and the
     * proposed rectangle cannot completely fit within the region perpendicular
     * to the axis of the movement direction, the returned rectangle may be
     * moved in that direction as far as necessary so that it does fit, or
     * return an empty rectangle if movement would not help.
     *
     * When complete, return the unused portion of the proposed rectangle
     * that is still available for additional text as the remaining rect,
     * or an empty rectangle is there is no remainder.
     *
     * NOTE: Subclasses must make no assumptions with regard to the origin
     * or size of the proposed rectangle. Subclasses must always examine
     * the proposed rectangle to make sure it intersects the text container's
     * bounds.
     *
     * @param RE_Rect $proposedRect
     * @param integer $sweepDirection    One of the LINE_SWEEP_ constants defined in this class.
     * @param integer $movementDirection One of the LINE_MOVEMENT_ constants defined in this class.
     * @param RE_Rect $remainingRect
     * @return RE_Rect
     */
    abstract public function lineFragmentRectForProposedRect(
        RE_Rect $proposedRect, $sweepDirection, $movementDirection, RE_Rect $remainingRect);



  /**** Public Interface ****/


  /* Object Lifecycle */

    /**
     * Object constructor signatures:
     *
     *  - Create a new text container with an "infinite" size (see
     *    {@link LARGE_NUMBER_FOR_TEXT}):
     *      new RE_Pdf_TextContainer();
     *
     *  - Create a new text container with a specific size:
     *      new RE_Pdf_TextContainer(RE_Size $containerSize);
     *
     *  - Create a new text container on the specified PDF canvas using the
     *    specified bounding rectangle:
     *      new RE_Pdf_TextContainer(RE_Pdf_Canvas_Interface $canvas, RE_Rect $boundingRect);
     *
     * @param mixed $param1
     * @param mixed $param2
     * @throws BadMethodCallException if the constructor arguments cannot be deduced.
     */
    public function __construct($param1 = null, $param2 = null)
    {
        if (($param1 instanceof RE_Pdf_Canvas_Interface) && ($param2 instanceof RE_Rect)) {
            $this->setCanvasAndOrigin($param1, $param2->getOrigin());
            $this->_boundingRect = clone $param2;
            $this->_boundingRect->setX(0);
            $this->_boundingRect->setY(0);

        } else if (($param1 instanceof RE_Size) && (! isset($param2))) {
            $this->_boundingRect = new RE_Rect(new RE_Point(), $param1);

        } else if ((! isset($param1)) && (! isset($param2))) {
            $this->_boundingRect = new RE_Rect(new RE_Point(), new RE_Size(
                RE_Pdf_TextContainer::LARGE_NUMBER_FOR_TEXT, RE_Pdf_TextContainer::LARGE_NUMBER_FOR_TEXT));

        } else {
            throw new BadMethodCallException('Unrecognized arguments provided to object constructor.');
        }


        // deprecated:
        $this->_isAvailableForLayout = true;
        $this->_layoutIsComplete = false;
        $this->_glyphRange = new RE_Range();
    }


  /* Object Magic Methods */

    /**
     * Returns a string describing the text container's size.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->_boundingRect->getSize()->__toString();
    }


  /* Container Size */

    /**
     * Returns the size of the text container's bounding rectangle.
     *
     * @return RE_Size
     */
    public function getContainerSize()
    {
        return clone $this->_boundingRect->getSize();
    }

    /**
     * Sets the size of the text container's bounding rectangle.
     *
     * If the text container has been attached to a layout manager, notifies
     * the layout manager of the updated geometry.
     *
     * @param RE_Size $containerSize
     */
    public function setContainerSize(RE_Size $containerSize)
    {
        if ($this->_boundingRect->getSize()->isEqualToSize($containerSize)) {
            return;
        }
        $this->_boundingRect->setSize($containerSize);

        $layoutManager = $this->getLayoutManager();
        if (! is_null($layoutManager)) {
            $layoutManager->textContainerChangedGeometry($this);
        }
    }


  /* Text Layout */

    /**
     * Returns the layout manager which is responsible for filling this
     * text container.
     *
     * @return RE_Pdf_LayoutManager Returns null if no layout manager has
     *   been set.
     */
    public function getLayoutManager()
    {
        return $this->_layoutManager;
    }

    /**
     * Sets the layout manager which is responsible for filling this text
     * container.
     *
     * This method is automatically called when you add the text container
     * to a layout manager; you should never need to call it directly.
     *
     * @param RE_Pdf_LayoutManager $layoutManager (optional) If null, removes
     *   the layout manager.
     */
    public function setLayoutManager(RE_Pdf_LayoutManager $layoutManager = null)
    {
        $this->_layoutManager = $layoutManager;
    }

    /**
     * Returns the line fragment padding.
     *
     * @return float
     */
    public function getLineFragmentPadding()
    {
        return $this->_lineFragmentPadding;
    }

    /**
     * Sets the distance in points of the space from the head and tail edges of
     * the line fragment rectangle that the typesetter should leave empty.
     *
     * Note that this value is not designed to express text margins; use
     * paragraph margin attributes instead.
     *
     * If the text container has been attached to a layout manager, notifies
     * the layout manager of the updated geometry.
     *
     * @param float $lineFragmentPadding
     * @throws RangeException if the line fragment padding is less than zero.
     */
    public function setLineFragmentPadding($lineFragmentPadding)
    {
        $padding = (float)$lineFragmentPadding;
        if ($padding < 0) {
            throw new RangeException("Line fragment padding must be positive or zero: $lineFragmentPadding");
        }
        if (abs($this->_lineFragmentPadding - $padding) < 1e-4) {
            return;
        }
        $this->_lineFragmentPadding = $padding;
        if (! empty($this->_layoutManager)) {
            $this->_layoutManager->textContainerChangedGeometry($this);
        }
    }

    /**
     * Returns the vertical alignment of the text container's contents.
     *
     * @return integer One of the VERTICAL_ALIGN_ constants defined in this class.
     */
    public function getVerticalAlignment()
    {
        return $this->_verticalAlignment;
    }

    /**
     * Sets the vertical alignment of the text container's contents.
     *
     * The vertical alignment must be one of the following values:
     *  - {@link RE_Pdf_TextContainer::VERTICAL_ALIGN_TOP}
     *  - {@link RE_Pdf_TextContainer::VERTICAL_ALIGN_MIDDLE}
     *  - {@link RE_Pdf_TextContainer::VERTICAL_ALIGN_BOTTOM}
     *
     * NOTE: The stock layout manager and typesetter classes can only handle
     * vertical alignments other than {@link VERTICAL_ALIGN_TOP} for simple
     * rectangular text containers (see {@link isSimpleRectangularTextContainer()}).
     * If you are creating a non-simple rectangular text container subclass, you
     * must also use a custom typesetter to achieve middle or bottom alignment.
     *
     * @param integer $verticalAlignment
     * @throws InvalidArgumentException if the alignment type is invalid.
     */
    public function setVerticalAlignment($verticalAlignment)
    {
        switch ($verticalAlignment) {
            case RE_Pdf_TextContainer::VERTICAL_ALIGN_TOP:
            case RE_Pdf_TextContainer::VERTICAL_ALIGN_MIDDLE:
            case RE_Pdf_TextContainer::VERTICAL_ALIGN_BOTTOM:
                $this->_verticalAlignment = $verticalAlignment;
                break;
            default:
                throw new InvalidArgumentException("Invalid alignment type: $alignment");
        }
    }

    /**
     * Returns true if the text container is a simple, non-rotated, solid
     * rectangular region with no holes or gaps.
     *
     * The layout manager and typesetter objects can perform some layout
     * optimizations for simple rectangles.
     *
     * Subclasses that are not simple rectangluar shapes must override this
     * method and return false.
     *
     * @return boolean
     */
    public function isSimpleRectangularTextContainer()
    {
        return true;
    }


  /* PDF Drawing */

    /**
     * Returns the PDF canvas on which the text container should draw its
     * contents.
     *
     * @return RE_Pdf_Canvas_Interface Returns null if no canvas has been set.
     */
    public function getCanvas()
    {
        return $this->_canvas;
    }

    /**
     * Returns the origin (the lower-left corner) of the text container on
     * the PDF canvas.
     *
     * @return RE_Point Returns null if no origin has been set.
     */
    public function getOrigin()
    {
        return clone $this->_origin;
    }

    /**
     * Sets the PDF canvas on which the text container should draw its contents
     * and its origin (the lower-left corner) on that canvas.
     *
     * @param RE_Pdf_Canvas_Interface $canvas
     * @param RE_Point                $origin
     */
    public function setCanvasAndOrigin(RE_Pdf_Canvas_Interface $canvas, RE_Point $origin)
    {
        $this->_canvas = $canvas;
        $this->_origin = clone $origin;
    }


  /* Debugging */

    /**
     * Draws debug rectangles for the text container's bounds.
     */
    public function drawDebug()
    {
        $canvas = $this->getCanvas();
        $canvas->setStrokeColor(RE_Color::greenColor());
        RE_Path::strokeRect($canvas, new RE_Rect(new RE_Point(), $this->getContainerSize()));
    }



  /**** Internal Interface ****/

    /**
     * Ensures that the proposed rectangle fits completely within the text
     * container's bounding rectangle. Adjusts the rectangle as necessary
     * according to the line movement and sweep directions and returns the
     * modified rectangle. Subclasses may further refine this rectangle to
     * ensure it fits within the text container's internal shape.
     *
     * @param RE_Rect $proposedRect
     * @param integer $sweepDirection
     * @param integer $movementDirection
     * @return RE_Rect Returns an empty rect if the proposed rect cannot be
     *   made to fit.
     */
    protected function _constrainProposedRectToBoundingRect($proposedRect, $sweepDirection, $movementDirection)
    {
        /* Return an empty rect if the proposed rect does not intersect with
         * our bounding rect.
         */
        if (! $this->_boundingRect->intersectsRect($proposedRect)) {
            return new RE_Rect();
        }

        $lineFragmentRect = clone $proposedRect;

        /* Ensure that the proposed rect can be fully contained within our
         * bounding rect -- rect heights may not be altered for vertical line
         * movement, and rect widths may not be altered for horizontal line
         * movement. Slide the rect along the appropriate axis if necessary.
         * If the rect will not fit, return an empty rect.
         */
        switch ($movementDirection) {
        case RE_Pdf_TextContainer::LINE_MOVEMENT_DOWN:
            if ($lineFragmentRect->getY() < 0) {
                $lineFragmentRect->setY(0);
            }
            if ($lineFragmentRect->maxY() > $this->_boundingRect->maxY()) {
                /* The proposed rect is too tall to fit. Return an empty rect.
                 */
                return new RE_Rect();
            }
            break;

        case RE_Pdf_TextContainer::LINE_MOVEMENT_UP:
            if ($lineFragmentRect->maxY() > $this->_boundingRect->maxY()) {
                $lineFragmentRect->setY($lineFragmentRect->maxY() - $this->_boundingRect->maxY());
            }
            if ($lineFragmentRect->getY() < 0) {
                /* The proposed rect is too tall to fit. Return an empty rect.
                 */
                return new RE_Rect();
            }
            break;

        case RE_Pdf_TextContainer::LINE_MOVEMENT_RIGHT:
            if ($lineFragmentRect->getX() < 0) {
                $lineFragmentRect->setX(0);
            }
            if ($lineFragmentRect->maxX() > $this->_boundingRect->maxX()) {
                /* The proposed rect is too wide to fit. Return an empty rect.
                 */
                return new RE_Rect();
            }
            break;

        case RE_Pdf_TextContainer::LINE_MOVEMENT_LEFT:
            if ($lineFragmentRect->maxX() > $this->_boundingRect->maxX()) {
                $lineFragmentRect->setX($lineFragmentRect->maxX() - $this->_boundingRect->maxX());
            }
            if ($lineFragmentRect->getX() < 0) {
                /* The proposed rect is too wide to fit. Return an empty rect.
                 */
                return new RE_Rect();
            }
            break;

        default:
            throw new InvalidArgumentException("Invalid line movement direction: $movementDirection");
        }


        switch ($sweepDirection) {
        case RE_Pdf_TextContainer::LINE_SWEEP_RIGHT:
        case RE_Pdf_TextContainer::LINE_SWEEP_LEFT:
            if ($lineFragmentRect->getX() < 0) {
                $lineFragmentRect->setWidth(
                    $lineFragmentRect->getWidth() + $lineFragmentRect->getX()  // adding a negative number
                    );
                $lineFragmentRect->setX(0);
            }
            if ($lineFragmentRect->maxX() > $this->_boundingRect->maxX()) {
                $lineFragmentRect->setWidth(
                    $lineFragmentRect->getWidth() - ($lineFragmentRect->maxX() - $this->_boundingRect->maxX())
                    );
            }
            break;

        case RE_Pdf_TextContainer::LINE_SWEEP_DOWN:
        case RE_Pdf_TextContainer::LINE_SWEEP_UP:
            if ($lineFragmentRect->getY() < 0) {
                $lineFragmentRect->setHeight(
                    $lineFragmentRect->getHeight() + $lineFragmentRect->getY()  // adding a negative number
                    );
                $lineFragmentRect->setY(0);
            }
            if ($lineFragmentRect->maxY() > $this->_boundingRect->maxY()) {
                $lineFragmentRect->setHeight(
                    $lineFragmentRect->getHeight() - ($lineFragmentRect->maxY() - $this->_boundingRect->maxY())
                    );
            }
            break;

        default:
            throw new InvalidArgumentException("Invalid line sweep direction: $sweepDirection");
        }

        return $lineFragmentRect;
    }





/* EVERYTHING BELOW THIS LINE IS DEPRECATED AND WILL BE MOVED TO RE_Pdf_LayoutManager IN THE NEXT UPDATE */





    /**
     * Flag indicating whether additional text can be laid out inside the
     * text container.
     * @var boolean
     */
    protected $_isAvailableForLayout = true;

    /**
     * Flag indicating that the layout manager has completed text layout inside
     * the text container.
     * @var boolean
     */
    protected $_layoutIsComplete = false;

    /**
     * Range of glyphs that are laid out inside this text container.
     * @var RE_Range
     */
    protected $_glyphRange = null;

    /**
     * Sets whether or not the text container is available for additional
     * text layout.
     *
     * This method is automatically called by the layout manager during text
     * layout. You should never need to call it directly.
     *
     * @param boolean $isAvailableForLayout
     */
    public function setIsAvailableForLayout($isAvailableForLayout)
    {
        $this->_isAvailableForLayout = $isAvailableForLayout;
    }

    /**
     * Sets to true when the layout manager has completed laying out text
     * inside the text container.
     *
     * This method is automatically called by the layout manager during text
     * layout. You should never need to call it directly.
     *
     * @param boolean $layoutIsComplete
     */
    public function setLayoutIsComplete($layoutIsComplete)
    {
        $this->_layoutIsComplete = $layoutIsComplete;
    }

    /**
     * Sets the range of glyphs that are laid out inside the text container.
     *
     * This method is automatically called by the layout manager during text
     * layout. You should never need to call it directly.
     *
     * @param RE_Range $glyphRange
     */
    public function setGlyphRange(RE_Range $glyphRange)
    {
        $this->_glyphRange->setLocation($glyphRange->getLocation());
        $this->_glyphRange->setLength($glyphRange->getLength());
    }

    /**
     * Returns true if additional text can be laid out inside the text container.
     *
     * @return boolean
     */
    public function isAvailableForLayout()
    {
        return $this->_isAvailableForLayout;
    }

    /**
     * Returns true if the layout manager has completed laying out text inside
     * the text container.
     *
     * @return boolean
     */
    public function layoutIsComplete()
    {
        return $this->_layoutIsComplete;
    }

    /**
     * Returns the range of glyphs that are laid out inside this text container.
     *
     * @return RE_Range
     */
    public function getGlyphRange()
    {
        return $this->_glyphRange;
    }

}
