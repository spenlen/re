<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage TextLayout
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */

/**
 * A RE_Pdf_LayoutManager manages the layout and drawing of the attributed
 * string stored in a {@link RE_Pdf_Text} object, mapping its Unicode
 * characters to glyphs, then laying out those glyphs inside one or more
 * {@link RE_Pdf_TextContainer} objects on a PDF canvas.
 *
 * @package    Pdf
 * @subpackage TextLayout
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Pdf_LayoutManager
{
  /**** Class Constants ****/


  /* Private Class Constants - Not intended for use outside this class */

    /* Array indices used for glyph stream. */
    const _GLYPH_NUMBER_INDEX    = 0;
    const _GLYPH_CHARACTER_INDEX = 1;



  /**** Class Variables ****/

    /**
     * The shared default typesetter object.
     * @var RE_Pdf_Typesetter
     */
    protected static $_defaultTypesetter = null;

    /**
     * Shared layout manager object used by {@link drawText()}.
     * @var RE_Pdf_LayoutManager
     */
    protected static $_sharedLayoutManager = null;

    /**
     */
    protected static $_shouldDrawDebug = false;



  /**** Instance Variables ****/

    /**
     * This layout manager's current text object.
     * @var RE_Pdf_Text
     */
    protected $_text = null;

    /**
     * Typesetter object used by this layout manager.
     * @var RE_Pdf_Typesetter
     */
    protected $_typesetter = null;

    /**
     * Glyph generator object used by this layout manager.
     * @var RE_Pdf_GlyphGenerator
     */
    protected $_glyphGenerator = null;

    /**
     * Delegate object which receives layout notification messages.
     * @var object
     */
    protected $_delegate = null;

    /**
     * Glyph stream. Implemented as a multi-dimensional array where the first
     * dimension contains the glyph numbers, the second dimension contains the
     * mapped character indices, and glyph attributes are stored at higher
     * dimensions.
     * @var array
     */
    protected $_glyphStream = array();

    /**
     * Number of glyphs in the glyph stream.
     * @var integer
     */
    protected $_glyphCount = 0;

    /**
     * Index of the first glyph in the glyph stream that has not been laid out.
     * @var integer
     */
    protected $_firstUnlaidGlyphIndex = 0;

    /**
     * Array of text containers in which to lay out characters.
     * @var array
     */
    protected $_textContainers = array();

    /**
     * Stores calculated layout data.
     */
    protected $_lineFragmentRects = array();
    protected $_lineFragmentUsedRects = array();
    protected $_glyphRunLocations = array();



  /**** Class Methods ****/

    /**
     * Convenience method for drawing a text object on a single PDF canvas
     * in a single, solid rectangle.
     *
     * Handles the generation and assembly of all supporting text layout
     * objects. If you need more control over the layout process, or wish
     * to obtain the actual layout information, you must assemble the layout
     * objects yourself.
     *
     * @param RE_Pdf_Text             $text   Source text.
     * @param RE_Pdf_Canvas_Interface $canvas Drawing canvas.
     * @param RE_Rect                 $rect   Bounding rectangle.
     */
    public static function drawText(RE_Pdf_Text $text, RE_Pdf_Canvas_Interface $canvas, RE_Rect $rect)
    {
        if (false) {
            /* NOTE: This method is deprecated and will be removed in the next update */
            self::getDefaultTypesetter()->drawTextOnPageInRect($text, $canvas, $rect);
            return;
        }

        if (is_null(self::$_sharedLayoutManager)) {
            self::$_sharedLayoutManager = new self();
            self::$_sharedLayoutManager->addTextContainer(new RE_Pdf_TextContainer_Rect());
        }

        $textContainers = self::$_sharedLayoutManager->getTextContainers();
        $textContainers[0]->setContainerSize($rect->getSize());
        $textContainers[0]->setCanvasAndOrigin($canvas, $rect->getOrigin());

        /* NOTE: Calling setText() is a shortcut for this class convenience
         * method. When assembling the layout objects in your own code, you
         * should call $text->setLayoutManager($layoutManager) instead.
         */
        self::$_sharedLayoutManager->setText($text);
        self::$_sharedLayoutManager->draw();

        self::$_sharedLayoutManager->setText(null);
    }

    /**
     * Returns the shared default typesetter object that is used by all new
     * layout managers.
     *
     * @return RE_Pdf_Typesetter
     */
    public static function getDefaultTypesetter()
    {
        if (empty(self::$_defaultTypesetter)) {
            self::$_defaultTypesetter = new RE_Pdf_Typesetter_Simple();
        }
        return self::$_defaultTypesetter;
    }

    /**
     * Sets a new shared default typesetter object, which will be used as the
     * default typesetter for all subsequently-created layout managers.
     *
     * @param RE_Pdf_Typesetter $typesetter
     */
    public static function setDefaultTypesetter(RE_Pdf_Typesetter $typesetter)
    {
        self::$_defaultTypesetter = $typesetter;
    }



  /**** Public Interface ****/


  /* Accessor Methods */

    /**
     * Returns the layout manager's text object.
     *
     * @return RE_Pdf_Text
     */
    public function getText()
    {
        return $this->_text;
    }

    /**
     * Replaces or removes the layout manager's text object.
     *
     * This method is automatically called when setting the layout manager for
     * a text object via {@link RE_Pdf_Text::setLayoutManager()}. You should
     * never need to call it directly.
     *
     * @param RE_Pdf_Text $text (optional) If null, removes the text object.
     */
    public function setText(RE_Pdf_Text $text = null)
    {
        if (isset($this->_text)) {
            $oldRange = new RE_Range(0, $this->_text->getLength());
            if (isset($text)) {
                $changeInLength = $this->_text->getLength() - $text->getLength();
            } else {
                $changeInLength = -$this->_text->getLength();
            }
            $this->invalidateGlyphsForCharacterRange($oldRange, $changeInLength);
            $this->invalidateLayoutForCharacterRange($oldRange);
        }
        $this->_text = $text;
    }

    /**
     * Returns the typesetter object used by this layout manager.
     *
     * @return RE_Pdf_Typesetter
     */
    public function getTypesetter()
    {
        if (is_null($this->_typesetter)) {
            $this->setTypesetter(RE_Pdf_LayoutManager::getDefaultTypesetter());
        }
        return $this->_typesetter;
    }

    /**
     * Sets a new typesetter object for this layout manager.
     *
     * If any glyph generation or layout has already been performed, it is
     * invalidated, and will be performed again using the new typesetter.
     *
     * @param RE_Pdf_Typesetter $typesetter
     */
    public function setTypesetter(RE_Pdf_Typesetter $typesetter)
    {
        $text = $this->getText();
        if (! empty($text)) {
            $oldRange = new RE_Range(0, $text->getLength());
            $this->invalidateGlyphsForCharacterRange($oldRange, 0);
            $this->invalidateLayoutForCharacterRange($oldRange);
        }
        $this->_typesetter = $typesetter;
    }

    /**
     * Returns the layout manager's delegate object.
     *
     * @return object Returns null if no delegate has been set.
     */
    public function getDelegate()
    {
        return $this->_delegate;
    }

    /**
     * Sets the layout manager's delegate object. Delegates will receive
     * notifications at various points during the text layout process. See
     * {@link RE_Pdf_LayoutManager_DelegateInterface}.
     *
     * @param object $delegate
     */
    public function setDelegate($delegate)
    {
        $this->_delegate = $delegate;
    }


  /* Text Container Management */

    /**
     * Returns an ordered array of the layout manager's text containers.
     *
     * @return array
     */
    public function getTextContainers()
    {
        return $this->_textContainers;
    }

    /**
     * Appends the specified text container to the array of text containers
     * used by this layout manager. There must be at least one text container
     * in order to perform layout.
     *
     * @param RE_Pdf_TextContainer $textContainer
     */
    public function addTextContainer(RE_Pdf_TextContainer $textContainer)
    {
        $textContainer->setLayoutManager($this);
        $this->_textContainers[] = $textContainer;
    }

    /**
     * Inserts the specified text container into the array of text containers
     * used by this layout manager at the specified index.
     *
     * This method invalidates glyph generation and layout for all subsequent
     * text containers.
     *
     * @param RE_Pdf_TextContainer $textContainer
     * @param integer              $index
     */
    public function insertTextContainer(RE_Pdf_TextContainer $textContainer, $index)
    {
        $textContainer->setLayoutManager($this);

        if ($index == count($this->_textContainers)) {
            $this->_textContainers[] = $textContainer;
            return;
        }

//////////////////////////////// invalidate glyphs and layout
        array_splice($this->_textContainers, $index, 0, array($textContainer));
    }

    /**
     * Removes the specified text container from the array of text containers
     * used by this layout manager at the specified index.
     *
     * This method invalidates glyph generation and layout for all subsequent
     * text containers.
     *
     * @param integer $index
     */
    public function removeTextContainer($index)
    {
        if ($index >= count($this->_textContainers)) {
            return;
        }

        $this->_textContainers[$index]->setLayoutManager(null);

//////////////////////////////// invalidate glyphs and layout
        array_splice($this->_textContainers, $index, 1);
    }

    /**
     * Indicates that text layout has completed for the specified text
     * container. If the end-of-text flag is true, indicates that this is
     * the final text container used in laying out the text.
     *
     * If this layout manager has a delegate, may also call the following
     * delegate methods if implemented:
     *   - {@link RE_Pdf_LayoutManager_DelegateInterface::didCompleteLayoutForTextContainer()}
     *   - {@link RE_Pdf_LayoutManager_DelegateInterface::ranOutOfTextContainers()}
     *
     * @param RE_Pdf_TextContainer $textContainer
     * @param boolean              $atEndOfText
     */
    public function didCompleteLayoutForTextContainer(RE_Pdf_TextContainer $textContainer, $atEndOfText)
    {
        $textContainer->setLayoutIsComplete(true);
        if (! $atEndOfText) {
            $textContainer->setIsAvailableForLayout(false);
        }

        if (isset($this->_delegate)) {
            if (method_exists($this->_delegate, 'didCompleteLayoutForTextContainer')) {
                $this->_delegate->didCompleteLayoutForTextContainer($this, $textContainer, $atEndOfText);
            }
            if (method_exists($this->_delegate, 'ranOutOfTextContainers')) {
                $ranOut = true;
                foreach ($this->_textContainers as $container) {
                    if ($container->isAvailableForLayout()) {
                        $ranOut = false;
                        break;
                    }
                }
                if ($ranOut) {
                    $this->_delegate->ranOutOfTextContainers($this);
                }
            }
        }
    }


  /* Glyph Generation */

    /**
     * Returns the glyph generator object used by this layout manager.
     *
     * @return RE_Pdf_GlyphGenerator
     */
    public function getGlyphGenerator()
    {
        if (is_null($this->_glyphGenerator)) {
            $this->setGlyphGenerator(RE_Pdf_GlyphGenerator::defaultGlyphGenerator());
        }
        return $this->_glyphGenerator;
    }

    /**
     * Sets a new glyph generator object for this layout manager.
     *
     * @param RE_Pdf_GlyphGenerator $glyphGenerator
     */
    public function setGlyphGenerator(RE_Pdf_GlyphGenerator $glyphGenerator)
    {
        $this->_glyphGenerator = $glyphGenerator;
    }

    /**
     * Inserts a single glyph into the glyph stream at the specified index
     * location and maps it to the specified character index.
     *
     * This method should only be used during the glyph generation or
     * typesetting phases, and usually only by the glyph generator or
     * typesetter objects themselves.
     *
     * The sequence of glyphs in the glyph stream must always coincide with
     * the order of the characters in the text string. Multiple glyphs may
     * map to a single character, or multiple characters may map to a single
     * glyph, but all glyphs for a particular character must appear after the
     * glyphs for all previous characters and before the glyphs for all
     * subsequent characters.
     *
     * If the glyph should be mapped to several characters (such as with
     * ligatures), the character index should indicate the first character it
     * is mapped to.
     *
     * Glyphs that are inserted during typesetting that would not otherwise
     * map to a particular character (hyphens or tab leader characters, for
     * example) are mapped to the nearest previous character that has glyphs.
     *
     * @param integer $glyph
     * @param integer $glyphIndex
     * @param integer $characterIndex
     */
    public function insertGlyph($glyph, $glyphIndex, $characterIndex)
    {
        $glyphData = array(
            RE_Pdf_LayoutManager::_GLYPH_NUMBER_INDEX    => $glyph,
            RE_Pdf_LayoutManager::_GLYPH_CHARACTER_INDEX => $characterIndex
            );

        if ($glyphIndex == $this->_glyphCount) {
            $this->_glyphStream[] = $glyphData;
        } else {
            array_splice($this->_glyphStream, $glyphIndex, 0, array($glyphData));
        }
        $this->_glyphCount++;
    }

    /**
     * Inserts multiple glyphs into the glyph stream starting at the specified
     * index all at once, mapping the glyphs to characters consecutively starting
     * with the specified character index.
     *
     * See discussion in {@link insertGlyphAtGlyphIndexCharacterIndex()}.
     *
     * @param array   $glyphs
     * @param integer $glyphIndex
     * @param integer $characterIndex
     */
    public function insertGlyphs(array $glyphs, $glyphIndex, $characterIndex)
    {
        $newGlyphData = array();
        foreach ($glyphs as $glyph) {
            $newGlyphData[] = array(
                RE_Pdf_LayoutManager::_GLYPH_NUMBER_INDEX    => $glyph,
                RE_Pdf_LayoutManager::_GLYPH_CHARACTER_INDEX => $characterIndex++
                );
        }

        if ($glyphIndex == $this->_glyphCount) {
            $this->_glyphStream = array_merge($this->_glyphStream, $newGlyphData);
        } else {
            array_splice($this->_glyphStream, $glyphIndex, 0, $newGlyphData);
        }
        $this->_glyphCount += count($newGlyphData);
    }

    /**
     * Replaces the glyph at the specified glyph index. Maintains the current
     * glyph-to-character mapping.
     *
     * This method should only be used during the typesetting phase, and
     * usually only by the typesetter object itself.
     *
     * @param integer $glyph
     * @param integer $glyphIndex
     */
    public function replaceGlyph($glyphIndex, $glyph)
    {
        if (($glyphIndex >= 0) && ($glyphIndex < $this->_glyphCount)) {
            $this->_glyphStream[$glyphIndex][RE_Pdf_LayoutManager::_GLYPH_NUMBER_INDEX] = $glyph;
        }
    }

    /**
     * Changes the glyph-to-character mapping for the glyph at the specified
     * glyph index.
     *
     * This method should only be used during the typesetting phase, and
     * usually only by the typesetter object itself.
     *
     * @param integer $glyph
     * @param integer $glyphIndex
     */
    public function setCharacterIndexForGlyphIndex($characterIndex, $glyphIndex)
    {
        if (($glyphIndex >= 0) && ($glyphIndex < $this->_glyphCount)) {
            $this->_glyphStream[$glyphIndex][RE_Pdf_LayoutManager::_GLYPH_CHARACTER_INDEX] = $characterIndex;
        }
    }

    /**
     * Deletes the glyphs in the specified range from the glyph stream.
     *
     * This method doesn't perform any invalidation or generation of glyphs or
     * layout. It is only intended for use during the glyph generation or
     * typesetting phases, and usually only by the glyph generator or the
     * typesetter objects themselves.
     *
     * @param RE_Range $glyphRange
     * @throws RangeException if any part of the range is invalid.
     */
    public function deleteGlyphs(RE_Range $glyphRange)
    {
        if ($glyphRange->maxRange() >= $this->_glyphCount) {
            throw new RangeException('Glyph range is invalid');
        }
        array_splice($this->_glyphStream, $glyphRange->getLocation(), $glyphRange->getLength());
        $this->_glyphCount -= $glyphRange->getLength();
    }


  /* Glyph Information */

    /**
     * Returns the number of glyphs in the glyph stream.
     *
     * This method forces glyph generation for the entire character range.
     *
     * @return integer
     */
    public function getGlyphCount()
    {
        $this->_generateGlyphsIfNeededForCharacterRange(new RE_Range(0, $this->getText()->getLength()));
        return $this->_glyphCount;
    }

    /**
     * Returns true if the specified glyph index is valid.
     *
     * @param integer $glyphIndex
     * @return boolean
     */
    public function isValidGlyphIndex($glyphIndex)
    {
        if (($glyphIndex >= 0) && ($glyphIndex < $this->_glyphCount)) {
            return true;
        }
        return false;
    }

    /**
     * Returns the character index for the first character mapped to the
     * specified glyph.
     *
     * Performs glyph generation if needed.
     *
     * @param integer $glyphIndex
     * @return integer Returns null if the glyph index is out of range.
     */
    public function getCharacterIndexForGlyphIndex($glyphIndex)
    {
        if ($glyphIndex < 0) {
            return null;
        }
        if ($glyphIndex >= $this->_glyphCount) {
            /** @todo Be smarter about the generated glyph range here. */
            $this->_generateGlyphsIfNeededForCharacterRange(new RE_Range(0, $this->getText()->getLength()));
            if ($glyphIndex >= $this->_glyphCount) {
                return null;
            }
        }
        return $this->_glyphStream[$glyphIndex][RE_Pdf_LayoutManager::_GLYPH_CHARACTER_INDEX];
    }

    /**
     * Returns the range of characters that are mapped to the specified glyph
     * range. Optionally sets the actual glyph range to the full range of glyphs
     * mapped to the resulting character range.
     *
     * Performs glyph generation if needed.
     *
     * @param RE_Range $glyphRange
     * @param RE_Range $actualGlyphRange (optional)
     * @return RE_Range Returns an empty range if the glyph range is invalid.
     */
    public function getCharacterRangeForGlyphRange(RE_Range $glyphRange, RE_Range $actualGlyphRange = null)
    {
        if ($glyphRange->maxRange() >= $this->_glyphCount) {
            /** @todo Be smarter about the generated glyph range here. */
            $this->_generateGlyphsIfNeededForCharacterRange(new RE_Range(0, $this->getText()->getLength()));
            if ($glyphRange->maxRange() >= $this->_glyphCount) {
                return new RE_Range();
            }
        }

        $startCharacterIndex = $this->_glyphStream[$glyphRange->getLocation()][RE_Pdf_LayoutManager::_GLYPH_CHARACTER_INDEX];
        $endCharacterIndex   = $this->_glyphStream[$glyphRange->maxRange() - 1][RE_Pdf_LayoutManager::_GLYPH_CHARACTER_INDEX];
        $characterRange = new RE_Range($startCharacterIndex, ($endCharacterIndex - $startCharacterIndex));

        if (isset($actualGlyphRange)) {
            /** @todo Calculate actual glyph range. */
            throw new Exception('Not implemented yet.');
        }

        return $characterRange;
    }

    /**
     * Returns the glyph index for the first glyph mapped to the specified
     * character.
     *
     * Performs glyph generation if needed.
     *
     * @param integer $characterIndex
     * @return integer Returns null if the character index is out of range.
     */
    public function getGlyphIndexForCharacterIndex($characterIndex)
    {
        if ($characterIndex >= $this->getText()->getLength()) {
            return null;
        }

        $this->_generateGlyphsIfNeededForCharacterRange(new RE_Range(0, $characterIndex));

        /** @todo Replace this with a binary search. */
        for ($glyphIndex = 0; $glyphIndex < $this->_glyphCount; $glyphIndex++) {
            if ($this->_glyphStream[$glyphIndex][RE_Pdf_LayoutManager::_GLYPH_CHARACTER_INDEX] >= $characterIndex) {
                return $glyphIndex;
            }
        }

        return null;
    }

    /**
     * Returns the range of glyphs that are mapped to the specified character
     * range, optionally setting the actual character range to the full range
     * of characters mapped to the resulting glyph range.
     *
     * Performs glyph generation if needed.
     *
     * @param RE_Range $characterRange
     * @param RE_Range $actualCharacterRange (optional)
     * @return RE_Range
     */
    public function getGlyphRangeForCharacterRange(RE_Range $characterRange, RE_Range $actualCharacterRange = null)
    {
        $this->_generateGlyphsIfNeededForCharacterRange($characterRange);

        /** @todo Replace this with a binary search. */
        $startCharacterIndex = $characterRange->getLocation();
        $startGlyphIndex     = 0;
        for ($glyphIndex = 0; $glyphIndex < $this->_glyphCount; $glyphIndex++) {
            if ($this->_glyphStream[$glyphIndex][RE_Pdf_LayoutManager::_GLYPH_CHARACTER_INDEX] >= $startCharacterIndex) {
                $startGlyphIndex = $glyphIndex;
                break;
            }
        }

        /** @todo Replace this with a binary search. */
        $endCharacterIndex = $characterRange->maxRange() - 1;
        $endGlyphIndex     = PHP_INT_MAX;
        for ($glyphIndex = $startGlyphIndex; $glyphIndex < $this->_glyphCount; $glyphIndex++) {
            if ($this->_glyphStream[$glyphIndex][RE_Pdf_LayoutManager::_GLYPH_CHARACTER_INDEX] > $endCharacterIndex) {
                $endGlyphIndex = $glyphIndex - 1;
                break;
            }
        }
        if ($endGlyphIndex >= $this->_glyphCount) {
            $endGlyphIndex = $this->_glyphCount - 1;
        }

        $glyphRange = new RE_Range($startGlyphIndex, ($endGlyphIndex - $startGlyphIndex + 1));

        if (isset($actualCharacterRange)) {
            /** @todo Calculate actual character range. */
            throw new Exception('Not implemented yet.');
        }

        return $glyphRange;
    }

    /**
     * Returns the glyph at the specified index.
     *
     * Performs glyph generation if needed.
     *
     * @param integer $glyphIndex
     * @return integer Returns null if the glyph index is out of range.
     */
    public function getGlyphAtIndex($glyphIndex)
    {
        if ($glyphIndex < 0) {
            return null;
        }
        if ($glyphIndex >= $this->_glyphCount) {
            $this->_generateGlyphsIfNeededForCharacterRange(new RE_Range(0, $this->getText()->getLength()));
            if ($glyphIndex >= $this->_glyphCount) {
                return null;
            }
        }
        return $this->_glyphStream[$glyphIndex][RE_Pdf_LayoutManager::_GLYPH_NUMBER_INDEX];
    }

    /**
     * Returns an array of the glyphs in the specified range. Does not include
     * any null glyphs or any glyphs that are not drawn.
     *
     * Performs glyph generation if needed.
     *
     * @param RE_Range $glyphRange
     * @return array
     * @throws RangeException if any part of the range is invalid.
     */
    public function getGlyphsInRange(RE_Range $glyphRange)
    {
        $startIndex = $glyphRange->getLocation();
        $maxRange   = $glyphRange->maxRange();
        if (($maxRange - 1) > $this->_glyphCount) {
            /** @todo Be smarter about the generated glyph range here. */
            $this->_generateGlyphsIfNeededForCharacterRange(new RE_Range(0, $this->getText()->getLength()));
            if (($maxRange - 1) > $this->_glyphCount) {
                throw new RangeException('Glyph range is invalid');
            }
        }

        $glyphs = array();
        for ($glyphIndex = $startIndex; $glyphIndex < $maxRange; $glyphIndex++) {
            if (! isset($this->_glyphStream[$glyphIndex][RE_Pdf_LayoutManager::_GLYPH_NUMBER_INDEX])) {
                continue;  // skip null glyphs
            }
            $glyphs[] = $this->_glyphStream[$glyphIndex][RE_Pdf_LayoutManager::_GLYPH_NUMBER_INDEX];
        }

        return $glyphs;
    }

    /**
     * Fills the specified arrays with the glyphs and corresponding character
     * indexes in the specified range. Returns the number of glyphs in the
     * resulting arrays.
     *
     * This method is designed for use primarily by the typesetter to obtain
     * a working range of glyphs and associated data necessary to perform
     * layout. As such, unlike {@link getGlyphsInRange()}, it returns all
     * glyphs currently in the glyph stream, including null glyphs and glyphs
     * that are not drawn.
     *
     * Performs glyph generation if needed.
     *
     * @todo Add inscription, elasticity, bidi level arrays
     *
     * @param RE_Range $glyphRange
     * @param array    &$glyphs
     * @param array    &$characterIndexes
     * @return integer
     * @throws RangeException if any part of the range is invalid.
     */
    public function getGlyphsInRangeForLayout(RE_Range $glyphRange, array &$glyphs, array &$characterIndexes)
    {
        $startIndex = $glyphRange->getLocation();
        $maxRange   = $glyphRange->maxRange();
        if (($maxRange - 1) > $this->_glyphCount) {
            /** @todo Be smarter about the generated glyph range here. */
            $this->_generateGlyphsIfNeededForCharacterRange(new RE_Range(0, $this->getText()->getLength()));
            if (($maxRange - 1) > $this->_glyphCount) {
                throw new RangeException('Glyph range is invalid');
            }
        }
        $glyphs           = array();
        $characterIndexes = array();
        $glyphCount = 0;
        for ($glyphIndex = $startIndex; $glyphIndex < $maxRange; $glyphIndex++, $glyphCount++) {
            $glyphs[$glyphIndex]           = $this->_glyphStream[$glyphIndex][RE_Pdf_LayoutManager::_GLYPH_NUMBER_INDEX];
            $characterIndexes[$glyphIndex] = $this->_glyphStream[$glyphIndex][RE_Pdf_LayoutManager::_GLYPH_CHARACTER_INDEX];
        }
        return $glyphCount;
    }


  /* Glyph Layout */

    /**
     * Returns the index for the first character in the layout manager that
     * has not been laid out.
     *
     * @return integer Returns null if all characters have been layed out.
     */
    public function getFirstUnlaidCharacterIndex()
    {
        if ($this->_firstUnlaidGlyphIndex >= $this->_glyphCount) {
            return null;
        }
        return $this->getCharacterIndexForGlyphIndex($this->_firstUnlaidGlyphIndex);
    }

    /**
     * Returns the index for the first glyph in the layout manager that
     * has not been laid out.
     *
     * @return integer Returns null if all glyphs have been layed out.
     */
    public function getFirstUnlaidGlyphIndex()
    {
        if ($this->_firstUnlaidGlyphIndex >= $this->_glyphCount) {
            return null;
        }
        return $this->_firstUnlaidGlyphIndex;
    }

    /**
     * Indicates the text container in which the glyphs in the specified range
     * should be laid out.
     *
     * The typesetter calls this method first, then sets the line fragment
     * rectangle by calling {@link setLineFragmentRectForGlyphRange()},
     * followed by setting the offset from the line fragment's orgin by
     * calling {@link setLocationForStartOfGlyphRange()}.
     *
     * This method should only be used during the typesetting phase, and
     * usually only by the typesetter object itself.
     *
     * @param RE_Range             $glyphRange
     * @param RE_Pdf_TextContainer $textContainer
     */
    public function setTextContainerForGlyphRange(RE_Range $glyphRange, RE_Pdf_TextContainer $textContainer)
    {
        $oldGlyphRange = $textContainer->getGlyphRange();
        if ($oldGlyphRange->isEmpty()) {
            $textContainer->setGlyphRange($glyphRange);
        } else {
            $textContainer->setGlyphRange($oldGlyphRange->rangeByUnioningRange($glyphRange));
        }
    }

    /**
     * Sets the line fragment bounding rectangle within the text container
     * for the glyphs in the specified range. Also sets the portion of the line
     * fragment rect that is actually used for glyphs or other drawn content
     * (including the text container's line fragment padding), which may be
     * smaller.
     *
     * Line fragment and used rectangles are always represented using the text
     * container's coordinate space.
     *
     * The typesetter calls this method second, after indicating the text container
     * used for the glyph range by calling {@link setTextContainerForGlyphRange()},
     * and before setting the offset from the line fragment's orgin by calling
     * {@link setLocationForStartOfGlyphRange()}.
     *
     * This method should only be used during the typesetting phase, and
     * usually only by the typesetter object itself.
     *
     * @param RE_Range $glyphRange
     * @param RE_Rect  $lineFragmentRect
     * @param RE_Rect  $usedRect
     */
    public function setLineFragmentRectForGlyphRange(RE_Range $glyphRange, RE_Rect $lineFragmentRect, RE_Rect $usedRect)
    {
        $glyphIndex = $glyphRange->getLocation();
        $this->_lineFragmentRects[$glyphIndex]     = clone $lineFragmentRect;
        $this->_lineFragmentUsedRects[$glyphIndex] = clone $usedRect;
    }

    /**
     * Sets the location (the offset from the line fragment's origin) for the
     * first glyph in the glyph range.
     *
     * Setting a location for a glyph range indicates that its first glyph is not
     * nominally spaced after the previous glyph. The first glyph in a line
     * fragment always starts a new range. This method is also used to offset
     * glyphs to achieve baseline shift, kerning, superscript and subscript,
     * full justification, etc.
     *
     * Glyph locations are always represented using the text container's
     * coordinate space.
     *
     * The typesetter calls this method last, after indicating the text container
     * used for the glyph range by calling {@link setTextContainerForGlyphRange()},
     * and setting the line fragment rectangle by calling
     * {@link setLineFragmentRectForGlyphRange()}.
     *
     * This method should only be used during the typesetting phase, and
     * usually only by the typesetter object itself.
     *
     * @param RE_Range $glyphRange
     * @param RE_Point $point
     */
    public function setLocationForStartOfGlyphRange(RE_Range $glyphRange, RE_Point $point)
    {
        $glyphIndex = $glyphRange->getLocation();
        $this->_glyphRunLocations[$glyphIndex] = array(clone $glyphRange, clone $point);
    }


  /* Layout Information */

    /**
     * Returns the full range of glyphs that are laid out in the specified
     * text container.
     *
     * This method causes glyph generation and layout for and up to the text
     * container.
     *
     * @param RE_Pdf_TextContainer $textContainer
     * @return RE_Range May be empty.
     */
    function getGlyphRangeForTextContainer(RE_Pdf_TextContainer $textContainer)
    {
        foreach ($this->_textContainers as $container) {
            if ($container !== $textContainer) {
                continue;
            }
            while (! $container->layoutIsComplete()) {
                $glyphCount = $this->getGlyphCount(); /** @todo Hack! */
                $firstUnlaidGlyphIndex = $this->getFirstUnlaidGlyphIndex();
                if (is_null($firstUnlaidGlyphIndex)) {
                    break;
                }
                $this->_firstUnlaidGlyphIndex =
                    $this->getTypesetter()->layoutGlyphsInLayoutManager($this, $firstUnlaidGlyphIndex);
            }
            return clone $container->getGlyphRange();
        }
        return new RE_Range();
    }

    /**
     * Returns the {@link RE_Pdf_TextContainer} object in which the glyph at
     * the specified index is laid out. Optionally sets $effectiveRange to the
     * full range of glyphs that are laid out in the returned text container.
     *
     * This method causes glyph generation and layout for and up to the text
     * container containing the glyph.
     *
     * @param integer  $glyphIndex
     * @param RE_Range $effectiveRange (optional)
     * @return RE_Pdf_TextContainer Returns null if the glyph index is out
     *   of bounds.
     */
    public function getTextContainerForGlyphIndex($glyphIndex, RE_Range $effectiveRange = null)
    {
        if ($glyphIndex > $this->getFirstUnlaidGlyphIndex()) {
//////////////////////////////// trigger layout
        }
        foreach ($this->_textContainers as $container) {
            $glyphRange = $container->getGlyphRange();
            if (! $glyphRange->containsLocation($glyphIndex)) {
                continue;
            }
            if (isset($effectiveRange)) {
                $effectiveRange->setLocation($glyphRange->getLocation());
                $effectiveRange->setLength($glyphRange->getLength());
            }
            return $container;
        }
        return null;
    }

    /**
     * Returns a single enclosing rectangle for the text container that is
     * actually used for glyphs or other drawn content.
     *
     * Used rectangles are always represented using the text container's
     * coordinate space.
     *
     * This method does not causes glyph generation or layout; ensure that it
     * is only called after layout has been completed.
     *
     * @param RE_Pdf_TextContainer $textContainer
     * @return RE_Rect
     */
    public function getUsedRectForTextContainer(RE_Pdf_TextContainer $textContainer)
    {
        $containerGlyphRange = $this->getGlyphRangeForTextContainer($textContainer);
        $startGlyphIndex = $containerGlyphRange->getLocation();
        $endGlyphIndex   = $containerGlyphRange->maxRange() - 1;

        $usedRect = new RE_Rect();

        foreach ($this->_lineFragmentUsedRects as $glyphIndex => $rect) {
            if ($glyphIndex > $endGlyphIndex) {
                break;
            } else if ($glyphIndex >= $startGlyphIndex) {
                if (! isset($usedRect)) {
                    $usedRect = clone $rect;
                } else {
                    $usedRect = $usedRect->rectByUnioningRect($rect);
                }
            }
        }

        return $usedRect;
    }

    /**
     * Returns the bounding rectangle for the line fragment in which the glyph
     * at the specified index has been laid out. Optionally sets $effectiveRange
     * to the full range of glyphs that are laid out in the returned rectangle.
     *
     * Line fragment rectangles are always represented using the text container's
     * coordinate space.
     *
     * This method causes glyph generation and layout for and up to the line
     * fragment containing the glyph.
     *
     * @param integer  $glyphIndex
     * @param RE_Range $effectiveRange (optional)
     * @return RE_Rect Returns an empty rect if the glyph index is out of bounds.
     */
    public function getLineFragmentRectForGlyphIndex($glyphIndex, RE_Range $effectiveRange = null)
    {
        $lineFragmentRect = null;

        $startIndex = 0;
        $endIndex   = 0;
        foreach ($this->_lineFragmentRects as $index => $rect) {
            if ($index <= $glyphIndex) {
                $startIndex = $index;
                $lineFragmentRect = $rect;
            } else if ($index > $glyphIndex) {
                $endIndex = $index - 1;
                break;
            }
        }

        if (! isset($lineFragmentRect)) {
            if (isset($effectiveRange)) {
                $effectiveRange->setLocation(0);
                $effectiveRange->setLength(0);
            }
            return new RE_Rect();
        }

        if (isset($effectiveRange)) {
            $effectiveRange->setLocation($startIndex);
            $effectiveRange->setLength($endIndex - $startIndex + 1);
        }

        return $lineFragmentRect;
    }

    /**
     * Returns the portion of bounding rectangle for the line fragment that is
     * actually used for glyphs or other drawn content in which the glyph at the
     * specified index has been laid out. Optionally sets $effectiveRange to the
     * full range of glyphs that are laid out in the returned rectangle.
     *
     * Used rectangles are always represented using the text container's
     * coordinate space.
     *
     * This method causes glyph generation and layout for and up to the line
     * fragment containing the glyph.
     *
     * @param integer  $glyphIndex
     * @param RE_Range $effectiveRange (optional)
     * @return RE_Rect Returns an empty rect if the glyph index is out of bounds.
     */
    public function getLineFragmentUsedRectForGlyphIndex($glyphIndex, RE_Range $effectiveRange = null)
    {
        $usedRect = null;

        $startIndex = 0;
        $endIndex   = 0;
        foreach ($this->_lineFragmentUsedRects as $index => $rect) {
            if ($index <= $glyphIndex) {
                $startIndex = $index;
                $usedRect = $rect;
            } else if ($index > $glyphIndex) {
                $endIndex = $index - 1;
                break;
            }
        }

        if (! isset($usedRect)) {
            if (isset($effectiveRange)) {
                $effectiveRange->setLocation(0);
                $effectiveRange->setLength(0);
            }
            return new RE_Rect();
        }

        if (isset($effectiveRange)) {
            $effectiveRange->setLocation($startIndex);
            $effectiveRange->setLength($endIndex - $startIndex + 1);
        }

        return $usedRect;
    }

    /**
     * Returns the full range of glyphs that contain the specified glyph index
     * which can be drawn using only the advance width from the font itself,
     * i.e., without any baseline shift, kerning, etc.
     *
     * @param integer $glyphIndex
     * @return RE_Range Returns an empty range if the glyph index is out
     *   of bounds.
     */
    public function getRangeOfNominallySpacedGlyphsContainingIndex($glyphIndex)
    {
        foreach ($this->_glyphRunLocations as $glyphRun) {
            if ($glyphRun[0]->containsLocation($glyphIndex)) {
                return $glyphRun[0];
            }
        }

        return new RE_Range();
    }

    /**
     * Returns the location of the glyph at the specified index, relative
     * to the origin of its line fragment rectangle.
     *
     * @param integer $glyphIndex
     * @return RE_Point Returns an empty point if the glyph index is out
     *   of bounds.
     */
    public function getLocationForGlyphAtIndex($glyphIndex)
    {
        if (isset($this->_glyphRunLocations[$glyphIndex])) {
            return $this->_glyphRunLocations[$glyphIndex][1];
        }

        /* If the glyph does not have an explicit location set by the
         * typesetter, calculate its location using the glyph advancements
         * from the nominally-spaced glyph run that contains it.
         */
        foreach ($this->_glyphRunLocations as $glyphRun) {
            if ($glyphRun[0]->containsLocation($glyphIndex)) {

                $runStartGlyphIndex     = $glyphRun[0]->getLocation();
                $runStartCharacterIndex = $this->getCharacterIndexForGlyphIndex($runStartGlyphIndex);

                $text = $this->getText();

                $font = RE_Pdf_Font::fontWithName(
                    $text->getAttributeAtIndex(RE_AttributedString::ATTRIBUTE_FONT, $runStartCharacterIndex)
                    );

                $widths = $font->widthsForGlyphs($this->getGlyphsInRange(
                    new RE_Range($runStartGlyphIndex, ($glyphIndex - $runStartGlyphIndex))
                    ));

                $totalWidth = (array_sum($widths) / $font->getUnitsPerEm())
                            * $text->getAttributeAtIndex(RE_AttributedString::ATTRIBUTE_FONT_SIZE, $runStartCharacterIndex);

                $point = clone $glyphRun[1];
                $point->setX($point->getX() + $totalWidth);

                return $point;

            }
        }

        return new RE_Point();
    }


  /* Glyph and Layout Invalidation */

    /**
     * Invalidates the cached glyphs for the characters in the specified range.
     * This will cause the glyphs to be regenerated during the next layout
     * phase.
     *
     * This method should only be used during the typesetting phase, and
     * usually only by the typesetter object itself.
     *
     * @param RE_Range $range
     * @param integer  $changeInLength
     * @param RE_Range $actualRange    (optional)
     */
    public function invalidateGlyphsForCharacterRange(RE_Range $range, $changeInLength, RE_Range $actualRange = null)
    {
        /**
         * @todo Only invalidate the specified range.
         */
        $this->_glyphStream = array();
        $this->_glyphCount  = 0;
    }

    /**
     * Invalidates the layout for the glyphs mapped to the characters in the
     * specified range. This will cause layout to be performed for the glyphs
     * during the next layout phase.
     *
     * @param RE_Range $range
     * @param RE_Range $actualRange    (optional)
     */
    public function invalidateLayoutForCharacterRange(RE_Range $range, RE_Range $actualRange = null)
    {
        /**
         * @todo Only invalidate the specified range.
         */
        $this->_firstUnlaidGlyphIndex = 0;
        $this->_lineFragmentRects     = array();
        $this->_lineFragmentUsedRects = array();
        $this->_glyphRunLocations     = array();

        $emptyRange = new RE_Range();
        foreach ($this->_textContainers as $textContainer) {
            $textContainer->setIsAvailableForLayout(true);
            $textContainer->setLayoutIsComplete(false);
            $textContainer->setGlyphRange($emptyRange);
        }
    }

    /**
     * Invalidates the layout information and cached glyphs for the characters
     * contained in the specified text container and all subsequent text
     * container objects.
     *
     * This method must be called by text container subclasses any time the
     * size or internal geometry of the container changes.
     *
     * @param RE_Range $range
     */
    public function textContainerChangedGeometry(RE_Pdf_TextContainer $textContainer)
    {
        /**
         * @todo Implement this method
         */
    }


  /* Drawing */

    /**
     * Draws the contents of all of the layout manager's text containers.
     *
     * Performs glyph generation and layout if needed.
     */
    public function draw()
    {
        /* Must use for loop here, checking the count on each iteration, as
         * performing layout can cause additional text containers to be added.
         */
        for ($i = 0; $i < count($this->_textContainers); $i++) {
            $this->drawTextContainer($this->_textContainers[$i]);
        }
    }

    /**
     * Draws the contents of the specified text container.
     *
     * Performs glyph generation and layout if needed.
     *
     * @param RE_Pdf_TextContainer $textContainer
     * @throws RuntimeException if the text container has not been linked to
     *    a PDF canvas.
     */
    public function drawTextContainer(RE_Pdf_TextContainer $textContainer)
    {
        $containerGlyphRange = $this->getGlyphRangeForTextContainer($textContainer);
        if ($containerGlyphRange->getLength() < 1) {
            return;
        }

        $canvas = $textContainer->getCanvas();
        $origin = $textContainer->getOrigin();
        if (is_null($canvas) || is_null($origin)) {
            throw new RuntimeException('The text container has not been linked to a PDF canvas.');
        }

        $origin->setY($origin->getY() + $textContainer->getContainerSize()->getHeight());

        $verticalAlignment = $textContainer->getVerticalAlignment();
        if (($verticalAlignment != RE_Pdf_TextContainer::VERTICAL_ALIGN_TOP) &&
            ($textContainer->isSimpleRectangularTextContainer())) {

                $difference = $textContainer->getContainerSize()->getHeight()
                            - $this->getUsedRectForTextContainer($textContainer)->getHeight();

                switch ($verticalAlignment) {

                case RE_Pdf_TextContainer::VERTICAL_ALIGN_BOTTOM:
                    $origin->setY($origin->getY() - $difference);
                    break;

                case RE_Pdf_TextContainer::VERTICAL_ALIGN_MIDDLE:
                    $origin->setY($origin->getY() - ($difference / 2));
                    break;

                }
        }

        $this->drawBackgroundForGlyphRange($containerGlyphRange, $canvas, $origin);

        $this->drawGlyphsForGlyphRange($containerGlyphRange, $canvas, $origin);

        if (self::$_shouldDrawDebug) {
            $this->drawDebugForTextContainer($textContainer);
        }
    }

    /**
     * Draws the background elements for the specified glyph range on the
     * specified canvas using the specified point as the origin for the text
     * container's coordinate system.
     *
     * All glyphs in the glyph range must belong to the same text container.
     * The behavior for other ranges is undefined.
     *
     * Performs glyph generation and layout if needed.
     *
     * @param RE_Range                $glyphRange
     * @param RE_Pdf_Canvas_Interface $canvas
     * @param RE_Point                $origin
     */
    public function drawBackgroundForGlyphRange(RE_Range $glyphRange, RE_Pdf_Canvas_Interface $canvas, RE_Point $origin)
    {
        return;

        $effectiveRange = new RE_Range($glyphRange->getLocation(), 0);
        while ($effectiveRange->maxRange() < $glyphRange->maxRange()) {

            $color = getAttributeAtIndex(RE_AttributedString::ATTRIBUTE_BACKGROUND_COLOR,
                $effectiveRange->getLocation(), $effectiveRange);
            if ($color) {

                // get each line used fragment in glyph range
                // foreach:

                // calculate rectangle for effective glyph range within line fragment used rect
                // paint background rect

            }
        }
    }

    /**
     * Draws the actual glyphs for the specified glyph range on the specified
     * canvas using the specified point as the origin for the text container's
     * coordinate system.
     *
     * All glyphs in the glyph range must belong to the same text container.
     * The behavior for other ranges is undefined.
     *
     * Performs glyph generation and layout if needed.
     *
     * @param RE_Range                $glyphRange
     * @param RE_Pdf_Canvas_Interface $canvas
     * @param RE_Point                $origin
     */
    public function drawGlyphsForGlyphRange(RE_Range $glyphRange, RE_Pdf_Canvas_Interface $canvas, RE_Point $origin)
    {
        $text = $this->getText();

        $lastFontName  = null;
        $lastFontSize  = null;

        $attributeNames = array(
            RE_AttributedString::ATTRIBUTE_COLOR,
            RE_AttributedString::ATTRIBUTE_STROKE_WIDTH,
            RE_AttributedString::ATTRIBUTE_STROKE_COLOR,
            );
        $effectiveRange = new RE_Range();

        $lastFontColor   = null;
        $lastStrokeWidth = null;
        $lastStrokeColor = null;

        $canvas->saveGraphicsState();

        $canvas->writeBytes(" BT\n");

        $glyphIndex    = $glyphRange->getLocation();
        $maxGlyphIndex = $glyphRange->maxRange();
        while ($glyphIndex < $maxGlyphIndex) {

            $workingRange = $this->getRangeOfNominallySpacedGlyphsContainingIndex($glyphIndex);
            if ($workingRange->isEmpty()) {
                $glyphIndex++;
                continue;
            }

            $lineFragmentRect = $this->getLineFragmentRectForGlyphIndex($glyphIndex);
            if ($lineFragmentRect->isEmpty()) {
                throw new RE_Pdf_Exception("Line fragment rectangle is empty for glyph index: $glyphIndex");
            }

            $characterIndex = $this->getCharacterIndexForGlyphIndex($glyphIndex);

            $fontName = $text->getAttributeAtIndex(RE_AttributedString::ATTRIBUTE_FONT,      $characterIndex);
            $fontSize = $text->getAttributeAtIndex(RE_AttributedString::ATTRIBUTE_FONT_SIZE, $characterIndex);

            if (($lastFontName != $fontName) || ($lastFontSize != $fontSize)) {
                $font = RE_Pdf_Font::fontWithName($fontName);
                $font->setFontAndSize($canvas, $fontSize);
                $lastFontName = $fontName;
                $lastFontSize = $fontSize;
            }

            $runLocation = $this->getLocationForGlyphAtIndex($glyphIndex);

            $x = $origin->x + $lineFragmentRect->origin->x + $runLocation->x;
            $y = $origin->y - $lineFragmentRect->origin->y - $runLocation->y;

            $canvas->writeBytes(' 1 0 0 1');
            RE_Pdf_Object_Numeric::writeValue($canvas, $x);
            RE_Pdf_Object_Numeric::writeValue($canvas, $y);
            $canvas->writeBytes(" Tm\n");

            while ($workingRange->containsLocation($glyphIndex)) {

                $characterIndex = $this->getCharacterIndexForGlyphIndex($glyphIndex);
                $attributes = $text->getAttributesAtIndex($attributeNames, $characterIndex, $effectiveRange);

                if ($lastFontColor !== $attributes[RE_AttributedString::ATTRIBUTE_COLOR]) {
                    $canvas->setFillColor($attributes[RE_AttributedString::ATTRIBUTE_COLOR]);
                    $lastFontColor = $attributes[RE_AttributedString::ATTRIBUTE_COLOR];
                }

                if ($lastStrokeWidth !== $attributes[RE_AttributedString::ATTRIBUTE_STROKE_WIDTH]) {
                    if ($attributes[RE_AttributedString::ATTRIBUTE_STROKE_WIDTH] == 0) {
                        $canvas->writeBytes(" 0 Tr\n");
                    } else if ($attributes[RE_AttributedString::ATTRIBUTE_STROKE_WIDTH] > 0) {
                        RE_Pdf_Object_Numeric::writeValue($canvas, $attributes[RE_AttributedString::ATTRIBUTE_STROKE_WIDTH]);
                        $canvas->writeBytes(" w\n 1 Tr\n");
                    } else {
                        RE_Pdf_Object_Numeric::writeValue($canvas, abs($attributes[RE_AttributedString::ATTRIBUTE_STROKE_WIDTH]));
                        $canvas->writeBytes(" w\n 2 Tr\n");
                    }
                    $lastStrokeWidth = $attributes[RE_AttributedString::ATTRIBUTE_STROKE_WIDTH];
                }

                if ($lastStrokeColor !== $attributes[RE_AttributedString::ATTRIBUTE_STROKE_COLOR]) {
                    $canvas->setStrokeColor($attributes[RE_AttributedString::ATTRIBUTE_STROKE_COLOR]);
                    $lastStrokeColor = $attributes[RE_AttributedString::ATTRIBUTE_STROKE_COLOR];
                }

                $attributeRange = new RE_Range($glyphIndex,
                    ($this->getGlyphIndexForCharacterIndex($effectiveRange->maxRange() - 1) - $glyphIndex + 1));

                $styleRunRange = $attributeRange->rangeByIntersectingRange($workingRange);
                $font->showGlyphs($canvas, $this->getGlyphsInRange($styleRunRange));

                $glyphIndex = $styleRunRange->maxRange();
            }

        }

        $canvas->writeBytes(" ET\n");

        $canvas->restoreGraphicsState();
    }

    /**
     */
    public function drawUnderlineForGlyphRange(RE_Range $glyphRange, RE_Pdf_Canvas_Interface $canvas, RE_Point $origin)
    {
        return;

        $effectiveRange = new RE_Range($glyphRange->getLocation(), 0);
        while ($effectiveRange->maxRange() < $glyphRange->maxRange()) {

            $style = getAttributeAtIndex(RE_AttributedString::ATTRIBUTE_UNDERLINE_STYLE,
                $effectiveRange->getLocation(), $effectiveRange);
            if ($style != RE_AttributedString::UNDERLINE_STYLE_NONE) {

                // get each line used fragment in glyph range
                // foreach:

                // calculate line for effective glyph range within line fragment used rect
                // paint line

            }
        }
    }

    /**
     */
    public function drawStrikethroughForGlyphRange(RE_Range $glyphRange, RE_Pdf_Canvas_Interface $canvas, RE_Point $origin)
    {
        return;

        $effectiveRange = new RE_Range($glyphRange->getLocation(), 0);
        while ($effectiveRange->maxRange() < $glyphRange->maxRange()) {

            $style = getAttributeAtIndex(RE_AttributedString::ATTRIBUTE_STRIKETHROUGH_STYLE,
                $effectiveRange->getLocation(), $effectiveRange);
            if ($style != RE_AttributedString::UNDERLINE_STYLE_NONE) {

                // get each line used fragment in glyph range
                // foreach:

                // calculate line for effective glyph range within line fragment used rect
                // paint line

            }
        }
    }


  /* Debugging */

    /**
     * Draws debug rectangles for all of the layout manager's text containers.
     *
     * Performs glyph generation and layout if needed.
     */
    public function drawDebug()
    {
        foreach ($this->_textContainers as $textContainer) {
            $this->drawDebugForTextContainer($textContainer);
        }
    }

    /**
     * Draws debug rectangles for the specified text container.
     *
     * Performs glyph generation and layout if needed.
     *
     * @param RE_Pdf_TextContainer $textContainer
     * @throws RuntimeException if the text container has not been linked to
     *    a PDF canvas.
     */
    public function drawDebugForTextContainer(RE_Pdf_TextContainer $textContainer)
    {
        $containerGlyphRange = $this->getGlyphRangeForTextContainer($textContainer);
        if ($containerGlyphRange->getLength() < 1) {
            return;
        }

        $canvas = $textContainer->getCanvas();
        $origin = $textContainer->getOrigin();
        if (is_null($canvas) || is_null($origin)) {
            throw new RuntimeException('The text container has not been linked to a PDF canvas.');
        }

        $canvas->saveGraphicsState();

        $defaultLineWidth = RE_Path::getDefaultLineWidth();
        RE_Path::setDefaultLineWidth(0.5);

        $lineGState = new RE_Pdf_Object_Dictionary();
        $lineGState['Type'] = 'ExtGState';
        $lineGState['CA']   = new RE_Pdf_Object_Numeric(0.3);
        $lineGStateName = $canvas->addResource('ExtGState', $lineGState);
        RE_Pdf_Object_Name::writeValue($canvas, $lineGStateName);
        $canvas->writeBytes(" gs\n");


        $verticalAlignment = $textContainer->getVerticalAlignment();
        if (($verticalAlignment != RE_Pdf_TextContainer::VERTICAL_ALIGN_TOP) &&
            ($textContainer->isSimpleRectangularTextContainer())) {

                $difference = $textContainer->getContainerSize()->getHeight()
                            - $this->getUsedRectForTextContainer($textContainer)->getHeight();

                switch ($verticalAlignment) {

                case RE_Pdf_TextContainer::VERTICAL_ALIGN_BOTTOM:
                    $origin->setY($origin->getY() - $difference);
                    break;

                case RE_Pdf_TextContainer::VERTICAL_ALIGN_MIDDLE:
                    $origin->setY($origin->getY() - ($difference / 2));
                    break;

                }
        }

        /* Flip and translate the coordinate system so the layout rectangles
         * can be drawn using their natural geometry.
         */
        $canvas->writeBytes(' 1 0 0 -1');
        RE_Pdf_Object_Numeric::writeValue($canvas, $origin->getX());
        RE_Pdf_Object_Numeric::writeValue($canvas, $origin->getY() + $textContainer->getContainerSize()->getHeight());
        $canvas->writeBytes(" cm\n");

        $textContainer->drawDebug();

        $startGlyphIndex = $containerGlyphRange->getLocation();
        $endGlyphIndex   = $containerGlyphRange->maxRange() - 1;

        $canvas->setStrokeColor(RE_Color::blueColor());
        foreach ($this->_lineFragmentRects as $glyphIndex => $rect) {
            if ($glyphIndex > $endGlyphIndex) {
                break;
            } else if ($glyphIndex >= $startGlyphIndex) {
                RE_Path::strokeRect($canvas, $rect);
            }
        }

        $canvas->setStrokeColor(RE_Color::redColor());
        foreach ($this->_lineFragmentUsedRects as $glyphIndex => $rect) {
            if ($glyphIndex > $endGlyphIndex) {
                break;
            } else if ($glyphIndex >= $startGlyphIndex) {
                RE_Path::strokeRect($canvas, $rect);
            }
        }

        $usedRect = $this->getUsedRectForTextContainer($textContainer);
        $canvas->setStrokeColor(RE_Color::htmlColor('purple'));
        RE_Path::strokeRect($canvas, $usedRect);


        $canvas->restoreGraphicsState();
        RE_Path::setDefaultLineWidth($defaultLineWidth);
    }



  /**** Internal Interface ****/

    /**
     * If not already done, asks the glyph generator to generate glyphs for the
     * specified character range.
     *
     * @param RE_Range $characterRange
     */
    protected function _generateGlyphsIfNeededForCharacterRange(RE_Range $characterRange)
    {
        $glyphIndex = $this->_glyphCount;
        if ($glyphIndex < 1) {
            $characterIndex = 0;
            $numCharacters  = $characterRange->maxRange();
        } else {
            $characterIndex = $this->_glyphStream[$glyphIndex - 1][RE_Pdf_LayoutManager::_GLYPH_CHARACTER_INDEX] + 1;
            if ($characterIndex >= $characterRange->maxRange()) {
                /* Glyphs have already been generated for this character range. Done.
                 */
                return;
            }
            $numCharacters = $characterRange->maxRange() - $characterIndex;
        }
        $this->getGlyphGenerator()->generateGlyphsForLayoutManager($this, $numCharacters, $glyphIndex, $characterIndex);
    }

}
