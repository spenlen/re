<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage DocumentStructure
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * RE_Pdf_ContentStream objects contain
 * 
 * graphical elements
 * 
 * @package    Pdf
 * @subpackage DocumentStructure
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Pdf_ContentStream extends RE_Pdf_Object_Stream implements RE_Pdf_Canvas_Interface
{
  /**** Instance Variables ****/
  
    /**
     * Current depth of the graphics state stack.
     * @var integer
     */
    protected $_graphicsStateStackDepth = 0;
    
    /**
     * Array of procedure set names used by this content stream.
     * @var array
     */
    protected $_procSets = array();
    
    
    
  /**** Public Interface ****/
    
    
  /* Object Lifecycle */
  
    /**
     * Creates a new RE_Pdf_ContentStream, optionally setting the stream
     * dictionary's initial contents with the contents of the specified
     * associative array.
     *
     * @param array $value
     */
    public function __construct($value = null)
    {
        parent::__construct($value);
        $this->_dataProvider = new RE_Pdf_Object_StreamDataProvider_Buffer();
        
        /* Procedure sets are obsolete as of PDF 1.4 (see PDF 1.7 Reference,
         * Section 10.1). To simplify our implementation, always add the two
         * most common ProcSet entries to the stream's resource dictionary.
         * Modern PDF viewers always ignore these entries.
         */
        $this->addProcSet('PDF');
        $this->addProcSet('Text');
    }
  

  /* RE_Pdf_Canvas_Interface Methods */
  
    /**
     * Appends the specified bytes to the content stream
     *
     * @param string $bytes
     */
    public function writeBytes($bytes)
    {
        $this->_dataProvider->appendBytes($bytes);
        $this->notify();        
    }

    /**
     * Appends the entire source file to the content stream.
     *
     * @param string|resource $file Filesystem path or file pointer resource.
     * @throws InvalidArgumentException if the file is not a valid filesystem
     *   path or file pointer resource.
     * @throws RE_Pdf_Exception if an error occurs while attempting to copy
     *   the source file to the content stream.
     */
    public function writeFile($file)
    {
        throw Exception('Not implemented');
    }

    /**
     * Saves the current graphics state by pushing a copy of it onto the
     * graphics state stack, a LIFO stack that allows up to 28 levels of nesting
     * (see PDF 1.7 Reference, Appendix C).
     * 
     * This allows you to make local changes to the graphics state just prior
     * to drawing certain objects without affecting the rest of the canvas.
     * When finished, restore the saved graphics state by calling
     * {@link restoreGraphicsState()}.
     *
     * @throws BadMethodCallException if the nesting depth exceeds 28 levels.
     */
    public function saveGraphicsState()
    {
        if (++$this->_graphicsStateStackDepth > 28) {
            throw BadMethodCallException('Graphics state nesting depth cannot exceed 28 levels.');
        }
        $this->_dataProvider->appendBytes(" q\n");
        $this->notify();
    }
    
    /**
     * Restores the most recent graphics state pushed onto the graphics state
     * stack by {@link saveGraphicsState()}.
     * 
     * @throws BadMethodCallException if the nesting depth exceeds 28 levels.
     */
    public function restoreGraphicsState()
    {
        if (--$this->_graphicsStateStackDepth < 0) {
            throw BadMethodCallException('Unbalanced graphics state calls.');
        }
        $this->_dataProvider->appendBytes(" Q\n");        
        $this->notify();
    }
    
    /**
     * Changes the current stroke (line) color for the canvas.
     * 
     * @param RE_Color $color
     */
    public function setStrokeColor(RE_Color $color)
    {
        $color->draw($this, true);
        $this->notify();
    }
    
    /**
     * Changes the current fill color for the canvas.
     * 
     * @param RE_Color $color
     */
    public function setFillColor(RE_Color $color)
    {
        $color->draw($this, false);
        $this->notify();
    }

    /**
     * Adds the specified procedure set to the canvas' resource dictionary.
     * 
     * @param string $setName
     */
    public function addProcSet($setName)
    {
        if (isset($this->_procSets[$setName])) {
            return;
        }
        $this->_procSets[$setName] = true;
        
        if (! isset($this['Resources'])) {
            $this['Resources'] = new RE_Pdf_Object_Dictionary();
        }
        
        if (! isset($this['Resources']['ProcSet'])) {
            $this['Resources']['ProcSet'] = new RE_Pdf_Object_Array();
        } else {
            foreach ($this['Resources']['ProcSet'] as $procSet) {
                if ($procSet->getValue() == $setName) {
                    return;
                }
            }
        }
        
        $this['Resources']['ProcSet'][] = new RE_Pdf_Object_Name($setName);
    }

    /**
     * Adds the specified resource to the canvas' resource dictionary. Returns
     * the unique resource name.
     *
     * @param string        $resourceType
     * @param RE_Pdf_Object $resource
     * @return string
     */
    public function addResource($resourceType, RE_Pdf_Object $resource)
    {
        if (! isset($this['Resources'])) {
            $this['Resources'] = new RE_Pdf_Object_Dictionary();
        }

        if (! isset($this['Resources'][$resourceType])) {
            $this['Resources'][$resourceType] = new RE_Pdf_Object_Dictionary();
        } else {
            foreach ($this['Resources'][$resourceType] as $resourceName => $res) {
                if ($res === $resource) {
                    /* The object already exists in the resource dictionary. Don't
                     * add it again.
                     */
                    return $resourceName;
                }
            }
        }
        
        /* Obtain a unique name for this resource.
         */
        switch ($resourceType) {
        case 'ExtGState':
            $resourceName = 'GS'; break;
        case 'ColorSpace':
            $resourceName = 'CS'; break;
        case 'Pattern':
            $resourceName = 'P';  break;
        case 'Shading':
            $resourceName = 'Sh'; break;
        case 'XObject':
            $resourceName = 'O';  break;
        case 'Font':
            $resourceName = 'F';  break;
        case 'Properties':
            $resourceName = 'PR'; break;
        default:
            $resourceName = strtoupper(substr($resourceType, 0, 3));
        }
        
        $i = 1;
        for ( ; isset($this['Resources'][$resourceType][$resourceName . $i]); $i++);
        $resourceName .= $i;
        
        $this['Resources'][$resourceType][$resourceName] = $resource;
        
        return $resourceName;
    }
    
    
  /* PDF Generation */

    /**
     * Overridden so that indirect objects can be created for the entries in
     * the content stream's resource dictionary. This must wait until now since
     * the ultimate destination PDF document is unknown until this method is
     * called.
     *
     * @param RE_Pdf_OutputDestination_Interface $output
     * @throws InvalidArgumentException if the output destination is not a
     *   {@link RE_Pdf_Document} object.
     */
    public function write(RE_Pdf_OutputDestination_Interface $output)
    {
        if (! $output instanceof RE_Pdf_Document) {
            throw new InvalidArgumentException(
                'Output destination must be a RE_Pdf_Document object; instance of ' . get_class($object) . ' given.');
        }
        
        if (! empty($this['Resources'])) {
            // throw new Exception('Not implemented');
        }
        
        parent::write($output);
    }

}
