<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage TextLayout
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * RE_Pdf_Typesetter objects are responsible for converting streams of Unicode
 * characters to glyphs, performing glyph substitution for ligatures, calculating
 * line wrapping, hyphenation, etc.
 *
 * @package    Pdf
 * @subpackage TextLayout
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
abstract class RE_Pdf_Typesetter
{
  /**** Class Constants ****/


  /* Control Character Actions */

    /**
     * Glyphs for this character should be removed from layout.
     */
    const CONTROL_ACTION_ZERO_ADVANCEMENT = 0;

    /**
     * Treat as ordinary whitespace.
     */
    const CONTROL_ACTION_WHITESPACE = 1;

    /**
     * Treat as a horizontal tab character.
     */
    const CONTROL_ACTION_HORIZONTAL_TAB = 2;

    /**
     * Causes a line break.
     */
    const CONTROL_ACTION_LINE_BREAK = 3;

    /**
     * Causes a paragraph break.
     */
    const CONTROL_ACTION_PARAGRAPH_BREAK = 4;

    /**
     * Causes a text container break.
     */
    const CONTROL_ACTION_CONTAINER_BREAK = 5;



  /**** Instance Variables ****/

    /**
     * Layout manger for the text currently being typeset.
     * @var RE_Pdf_LayoutManager
     */
    protected $_layoutManager = null;

    /**
     * Current text container in which glyphs are currently being laid out.
     * @var RE_Pdf_TextContainer
     */
    protected $_textContainer = null;

    /**
     * The distance in points of the space from the head and tail edges of the
     * line fragment rectangle that the typesetter should leave empty.
     * @var float
     */
    protected $_lineFragmentPadding = 0.0;

    /**
     * Attributed string object containing the characters currently being typeset.
     * @var RE_AttributedString
     */
    protected $_string = null;

    /**
     * Paragraph style object for the text currently being typeset.
     * @var RE_ParagraphStyle
     */
    protected $_currentParagraphStyle = null;

    /**
     * Current working glyph range, excluding paragraph separator glyphs.
     * @var RE_Range
     */
    protected $_paragraphGlyphRange = null;

    /**
     * Current working glyph range, including paragraph separator glyphs.
     * @var RE_Range
     */
    protected $_paragraphSeparatorGlyphRange = null;



  /**** Abstract Interface ****/

    /**
     * Lays out the paragraph in the current glyph range inside the current
     * text container, using the specified line fragment origin, until the
     * start of the next paragraph is reached or the text container is full.
     *
     * When complete, the origin is set to the proposed origin for the next
     * line fragment rectangle, and the index of the next glyph to be laid out
     * is returned.
     *
     * Subclasses must call {@link beginParagraph()}, {@link beginLine()},
     * {@link endLine()}, and {@link endParagraph} during the paragraph layout.
     *
     * @param RE_Point $lineFragmentOrigin
     * @return integer
     */
    abstract public function layoutParagraph(RE_Point $lineFragmentOrigin);



  /**** Public Interface ****/


  /* Layout */

    /**
     * Lays out the glyphs in the specified layout manager starting at the
     * specified glyph index. Continues until all glyphs have been laid out
     * or all of the layout manager's text containers are full. When complete,
     * returns the index of the next glyph that needs to be laid out
     *
     * @param RE_Pdf_LayoutManager $layoutManager
     * @param integer              $startGlyphIndex
     */
    public function layoutGlyphsInLayoutManager(RE_Pdf_LayoutManager $layoutManager, $startGlyphIndex)
    {
        $this->_layoutManager = $layoutManager;
        $this->setString($layoutManager->getText());

        $textContainer = null;

        $nextGlyphIndex = $startGlyphIndex;
        while (true) {

            if (is_null($textContainer)) {

                foreach ($this->getTextContainers() as $container) {
                    if ($container->isAvailableForLayout()) {
                        $textContainer = $container;
                        break;
                    }
                }
                if (is_null($textContainer)) {
                    break;
                }

                $this->_textContainer = $textContainer;
                $this->setLineFragmentPadding($textContainer->getLineFragmentPadding());

                $lineFragmentOrigin = new RE_Point();
            }

            $this->_identifyParagraphRangeForGlyphIndex($nextGlyphIndex);

            $nextGlyphIndex = $this->layoutParagraph($lineFragmentOrigin);

            if ($nextGlyphIndex == $this->getParagraphGlyphRange()->maxRange()) {
                $nextGlyphIndex = $this->getParagraphSeparatorGlyphRange()->maxRange();
            }

            if ($nextGlyphIndex < $this->getParagraphSeparatorGlyphRange()->maxRange()) {
                $layoutManager->didCompleteLayoutForTextContainer($textContainer, false);
                $textContainer = null;

            } else if ($nextGlyphIndex >= $layoutManager->getGlyphCount()){
                $layoutManager->didCompleteLayoutForTextContainer($textContainer, true);
                break;

            } else {
                $action = $this->actionForControlCharacterAtIndex($layoutManager->getCharacterIndexForGlyphIndex($nextGlyphIndex - 1));
                if ($action == RE_Pdf_Typesetter::CONTROL_ACTION_CONTAINER_BREAK) {
                    $layoutManager->didCompleteLayoutForTextContainer($textContainer, false);
                    $textContainer = null;
                }
            }

        }

        return $nextGlyphIndex;
    }


  /* Layout Phase Notification */

    /**
     * Sets up layout parameters at the beginning of paragraph layout.
     *
     * This method is called by {@link layoutParagraph()} before it starts
     * laying out glyphs in a paragraph.
     *
     * This method may be overridden by subclasses to customize the
     * behavior of the typesetter.
     */
    public function beginParagraph()
    {
        $this->_setCurrentParagraphStyle($this->getString()->getAttributeAtIndex(
            RE_AttributedString::ATTRIBUTE_PARAGRAPH_STYLE,
            $this->getLayoutManager()->getCharacterIndexForGlyphIndex($this->getParagraphSeparatorGlyphRange()->getLocation())));
    }

    /**
     * Sets up layout parameters at the beginning of each line of text.
     *
     * This method is called by {@link layoutParagraph()} before it starts
     * laying out glyphs in each line fragment.
     *
     * This method may be overridden by subclasses to customize the
     * behavior of the typesetter.
     *
     * @param integer $glyphIndex The first glyph to be laid out in the line.
     */
    public function beginLine($glyphIndex)
    {}

    /**
     * Sets up layout parameters at the end of each line of text.
     *
     * This method is called by {@link layoutParagraph()} after it finishes
     * laying out glyphs in each line fragment.
     *
     * This method may be overridden by subclasses to customize the
     * behavior of the typesetter.
     *
     * @param RE_Range $glyphRange The range of glyphs laid out in the line.
     */
    public function endLine(RE_Range $glyphRange)
    {}

    /**
     * Sets up layout parameters at the end of paragraph layout.
     *
     * This method is called by {@link layoutParagraph()} after it finishes
     * laying out glyphs in a paragraph.
     *
     * This method may be overridden by subclasses to customize the
     * behavior of the typesetter.
     */
    public function endParagraph()
    {}


  /* Control Character Behavior */

    /**
     * Returns the action that should be performed when the control character
     * at the specified index is encountered during typesetting.
     *
     * @param integer $characterIndex
     * @return integer One of the CONTROL_ACTION_ constants defined in this class.
     */
    public function actionForControlCharacterAtIndex($characterIndex)
    {
        $character = $this->getString()->getCharacterAtIndex($characterIndex);
        switch ($character) {

        case 0x0020:  // U+0020 SPACE
            return RE_Pdf_Typesetter::CONTROL_ACTION_WHITESPACE;

        case 0x0009:  // U+0009 CHARACTER TABULATION
        case 0x0089:  // U+0089 CHARACTER TABULATION WITH JUSTIFICATION
            return RE_Pdf_Typesetter::CONTROL_ACTION_HORIZONTAL_TAB;

        case 0x000b:  // U+000B LINE TABULATION
        case 0x0085:  // U+0085 NEXT LINE (NEL)
        case 0x2028:  // U+2028 LINE SEPARATOR
            return RE_Pdf_Typesetter::CONTROL_ACTION_LINE_BREAK;

        case 0x000a:  // U+000A LINE FEED (LF)
        case 0x000d:  // U+000D CARRIAGE RETURN (CR)
        case 0x2029:  // U+2029 PARAGRAPH SEPARATOR
            return RE_Pdf_Typesetter::CONTROL_ACTION_PARAGRAPH_BREAK;

        case 0x000c:  // U+000C FORM FEED (FF)
            return RE_Pdf_Typesetter::CONTROL_ACTION_CONTAINER_BREAK;

        }
        return RE_Pdf_Typesetter::CONTROL_ACTION_ZERO_ADVANCEMENT;
    }

    /**
     * Returns the tab stop object closest to the specified glyph, tracking
     * in the specified direction, but not farther than the maximum location,
     * from the current paragraph style. Returns null if no tab stop could be
     * located within the specified range.
     *
     * @param float   $glyphLocation
     * @param integer $writingDirection
     * @param float   $maxLocation
     * @return RE_TabStop|null
     */
    public function textTabForGlyphLocationWritingDirectionMaxLocation($glyphLocation, $writingDirection, $maxLocation)
    {
        if ($writingDirection != RE_Pdf_Text::WRITING_DIRECTION_LEFT_TO_RIGHT) {
            throw new Exception('Writing direction $writingDirection is currently unsupported.');
        }

        $finalLocation = 0;

        $tabStops = $this->getCurrentParagraphStyle()->getTabStops();
        foreach ($tabStops as $location => $tabStop) {
            if ($location < $glyphLocation) {
                $finalLocation = $location;
                continue;
            }
            if ($location > $maxLocation) {
                return null;
            }
            return $tabStop;
        }

        $defaultTabInterval = $this->getCurrentParagraphStyle()->getDefaultTabInterval();
        if ($defaultTabInterval == 0) {
            return null;
        }

        while ($finalLocation < $glyphLocation) {
            $finalLocation += $defaultTabInterval;
        }
        if ($finalLocation > $maxLocation) {
            return null;
        }

        return new RE_TabStop(RE_TabStop::TYPE_LEFT, $finalLocation);
    }


  /* Paragraph Typesetting Information */

    /**
     * Returns the attributed string object containing the characters currently
     * being typeset.
     *
     * @return RE_AttributedString
     */
    public function getString()
    {
        return $this->_string;
    }

    /**
     * Sets the attributed string object containing the characters currently
     * being typeset.
     *
     * @param RE_AttributedString $string
     */
    public function setString(RE_AttributedString $string)
    {
        $this->_string = $string;
    }

    /**
     * Returns the paragraph style object for the text currently being typeset.
     *
     * This value is only valid when the typesetter is actually performing
     * layout (that is, inside {@link layoutGlyphsInLayoutManager()}).
     *
     * @return RE_ParagraphStyle
     */
    public function getCurrentParagraphStyle()
    {
        return $this->_currentParagraphStyle;
    }

    /**
     * Sets the glyph ranges of the paragraph currently being laid out. The
     * paragraph glyph range is the range of characters excluding the paragraph
     * separator glyph(s). The paragraph separator glyph range is the same as
     * the paragraph glyph range, but includes the paragraph separator glyph(s).
     *
     * @param RE_Range $paragraphGlyphRange
     * @param RE_Range $paragraphSeparatorGlyphRange
     */
    public function setParagraphGlyphRanges(RE_Range $paragraphGlyphRange, RE_Range $paragraphSeparatorGlyphRange)
    {
        $this->_paragraphGlyphRange          = $paragraphGlyphRange;
        $this->_paragraphSeparatorGlyphRange = $paragraphSeparatorGlyphRange;
    }

    /**
     * Returns the glyph range of the paragraph currently being laid out.
     * Does not include the glyphs for the paragraph separator character(s).
     *
     * @return RE_Range
     */
    public function getParagraphGlyphRange()
    {
        return $this->_paragraphGlyphRange;
    }

    /**
     * Returns the glyph range of the paragraph currently being laid out,
     * including the glyphs for the paragraph separator character(s).
     *
     * @return RE_Range
     */
    public function getParagraphSeparatorGlyphRange()
    {
        return $this->_paragraphSeparatorGlyphRange;
    }

    /**
     * Returns the character range of the paragraph currently being laid out.
     * Does not include the paragraph separator character(s).
     *
     * @return RE_Range
     */
    public function getParagraphCharacterRange()
    {
        return $this->getLayoutManager()->getCharacterRangeForGlyphRange($this->getParagraphGlyphRange());
    }

    /**
     * Returns the character range of the paragraph currently being laid out,
     * including the paragraph separator character(s).
     *
     * @return RE_Range
     */
    public function getParagraphSeparatorCharacterRange()
    {
        return $this->getLayoutManager()->getCharacterRangeForGlyphRange($this->getParagraphSeparatorGlyphRange());
    }

    /**
     * Returns the line spacing (or leading) in points that should be added
     * below the descenders of the glyphs in the used rectangle for the proposed
     * line fragment rectangle.
     *
     * @param integer $glyphIndex
     * @param RE_Rect $proposedLineFragmentRect
     * @return float
     */
    public function getLineSpacingAfterGlyphIndex($glyphIndex, RE_Rect $proposedLineFragmentRect)
    {
        if ($this->getParagraphSeparatorGlyphRange()->containsLocation($glyphIndex)) {
            return $this->getCurrentParagraphStyle()->getLineSpacing();
        }

        $characterIndex = $this->getLayoutManager()->getCharacterIndexForGlyphIndex($glyphIndex);
        if (! is_null($characterIndex)) {
            $paragraphStyle = $this->getString()->getAttributeAtIndex(
                RE_AttributedString::ATTRIBUTE_PARAGRAPH_STYLE, $characterIndex);
            if ($paragraphStyle) {
                return $paragraphStyle->getLineSpacing();
            }
        }
        return 0.0;
    }

    /**
     * Returns the space in points that should be added before the paragraph
     * containing the specified glyph, which is added to the top of the
     * proposed line fragment rectangle (but not the used rectangle for the line).
     *
     * @param integer $glyphIndex
     * @param RE_Rect $proposedLineFragmentRect
     * @return float
     */
    public function getParagraphSpacingBeforeGlyphIndex($glyphIndex, RE_Rect $proposedLineFragmentRect)
    {
        if ($this->getParagraphSeparatorGlyphRange()->containsLocation($glyphIndex)) {
            return $this->getCurrentParagraphStyle()->getParagraphSpacingBefore();
        }

        $characterIndex = $this->getLayoutManager()->getCharacterIndexForGlyphIndex($glyphIndex);
        if (! is_null($characterIndex)) {
            $paragraphStyle = $this->getString()->getAttributeAtIndex(
                RE_AttributedString::ATTRIBUTE_PARAGRAPH_STYLE, $characterIndex);
            if ($paragraphStyle) {
                return $paragraphStyle->getParagraphSpacingBefore();
            }
        }
        return 0.0;
    }

    /**
     * Returns the space in points that should be added after the paragraph
     * containing the specified glyph, which is added to the bottom of the
     * proposed line fragment rectangle (but not the used rectangle for the line).
     *
     * @param integer $glyphIndex
     * @param RE_Rect $proposedLineFragmentRect
     * @return float
     */
    public function getParagraphSpacingAfterGlyphIndex($glyphIndex, RE_Rect $proposedLineFragmentRect)
    {
        if ($this->getParagraphSeparatorGlyphRange()->containsLocation($glyphIndex)) {
            return $this->getCurrentParagraphStyle()->getParagraphSpacingAfter();
        }

        $characterIndex = $this->getLayoutManager()->getCharacterIndexForGlyphIndex($glyphIndex);
        if (! is_null($characterIndex)) {
            $paragraphStyle = $this->getString()->getAttributeAtIndex(
                RE_AttributedString::ATTRIBUTE_PARAGRAPH_STYLE, $characterIndex);
            if ($paragraphStyle) {
                return $paragraphStyle->getParagraphSpacingAfter();
            }
        }
        return 0.0;
    }


  /* Line Breaking Behavior */

    /**
     * Returns true if the typesetter is permitted to break the line before
     * the word beginning at the specified character index. If this method
     * returns false, the typesetter will continue to look for a line breaking
     * opportunity.
     *
     * Subclasses may override this method to customize line breaking behavior.
     *
     * @param integer $characterIndex
     * @return boolean
     */
    public function shouldBreakLineByWordBeforeCharacter($characterIndex)
    {
        return true;
    }

    /**
     * Returns true if the typesetter is permitted to break the line by
     * hyphenating the word at the specified character index. If this method
     * returns false, the typesetter will continue to look for a line breaking
     * opportunity.
     *
     * Subclasses may override this method to customize line breaking behavior.
     *
     * @param integer $characterIndex
     * @return boolean
     */
    public function shouldBreakLineByHyphenatingBeforeCharacter($characterIndex)
    {
        return true;
    }


  /* Text Container Management */

    /**
     * Returns the current text container in which glyphs are currently being
     * laid out.
     *
     * This value is only valid when the typesetter is actually performing
     * layout (that is, inside {@link layoutGlyphsInLayoutManager()}).
     *
     * @return RE_Pdf_TextContainer
     */
    public function getCurrentTextContainer()
    {
        return $this->_textContainer;
    }

    /**
     * Returns an ordered array of text containers for the current layout
     * manager.
     *
     * This value is only valid when the typesetter is actually performing
     * layout (that is, inside {@link layoutGlyphsInLayoutManager()}).
     *
     * @return array
     */
    public function getTextContainers()
    {
        return $this->getLayoutManager()->getTextContainers();
    }

    /**
     * Returns the distance in points of the space from the head and tail edges
     * of the line fragment rectangle that the typesetter should leave empty.
     *
     * This value is usually obtained from the current text container.
     *
     * This value is only valid when the typesetter is actually performing
     * layout (that is, inside {@link layoutGlyphsInLayoutManager()}).
     *
     * @return RE_Pdf_TextContainer
     */
    public function getLineFragmentPadding()
    {
        return $this->_lineFragmentPadding;
    }

    /**
     * Sets the distance in points of the space from the head and tail edges of
     * the line fragment rectangle that the typesetter should leave empty.
     *
     * Note that this value is not designed to express text margins; use
     * paragraph margin attributes instead.
     *
     * This value is only valid when the typesetter is actually performing
     * layout (that is, inside {@link layoutGlyphsInLayoutManager()}).
     *
     * @param float $lineFragmentPadding
     */
    public function setLineFragmentPadding($lineFragmentPadding)
    {
        $this->_lineFragmentPadding = $lineFragmentPadding;
    }


  /* Layout Manager Interaction */

    /**
     * Returns the layout manager for the text currently being typeset.
     *
     * This value is only valid when the typesetter is actually performing
     * layout (that is, inside {@link layoutGlyphsInLayoutManager()}).
     *
     * @return RE_Pdf_LayoutManager
     */
    public function getLayoutManager()
    {
        return $this->_layoutManager;
    }



  /**** Internal Interface ****/

    /**
     * Sets the paragraph style object for the text currently being typeset.
     *
     * @param RE_ParagraphStyle $paragraphStyle
     */
    protected function _setCurrentParagraphStyle(RE_ParagraphStyle $paragraphStyle)
    {
        $this->_currentParagraphStyle = $paragraphStyle;
    }

    /**
     * Identifies and sets the glyph ranges for the characters comprising the
     * paragraph which contains the specified glyph index.
     *
     * @param integer $startGlyphIndex
     */
    protected function _identifyParagraphRangeForGlyphIndex($startGlyphIndex)
    {
        $layoutManager = $this->getLayoutManager();
        $string        = $this->getString();

        $stringRange = new RE_Range(0, $string->getLength());

        $firstCharacterIndex = $layoutManager->getCharacterIndexForGlyphIndex($startGlyphIndex);
        $nextCharacterIndex  = $firstCharacterIndex;

        /* Working in batches of 500 characters, find the next paragraph
         * separator character(s).
         */
        $lastCharacterIndex      = -1;
        $paragraphSeparatorIndex = -1;
        while ($paragraphSeparatorIndex < 0) {

            if ($nextCharacterIndex >= $stringRange->maxRange()) {
                $lastCharacterIndex      = $nextCharacterIndex - 1;
                $paragraphSeparatorIndex = $nextCharacterIndex - 1;
                break;
            }

            $characters = $string->getCharactersInRange(
                $stringRange->rangeByIntersectingRange(new RE_Range($nextCharacterIndex, 500)));

            foreach ($characters as $index => $character) {
                switch ($character) {

                case 0x000a:  // U+000A LINE FEED (LF)
                case 0x000c:  // U+000C FORM FEED (FF)
                case 0x2029:  // U+2029 PARAGRAPH SEPARATOR
                    $lastCharacterIndex      = $index - 1;
                    $paragraphSeparatorIndex = $index;
                    break(2);

                case 0x000d:  // U+000D CARRIAGE RETURN (CR)
                    $lastCharacterIndex      = $index - 1;
                    /* Check for a CR-LF sequence.
                     */
                    if ((isset($characters[$index + 1])) && ($characters[$index + 1] == 0x000a)) {
                        $index++;
                    }
                    $paragraphSeparatorIndex = $index;
                    break(2);

                }
            }

            $nextCharacterIndex += count($characters);
        }

        $this->setParagraphGlyphRanges(
            $layoutManager->getGlyphRangeForCharacterRange(
                new RE_Range($firstCharacterIndex, ($lastCharacterIndex      - $firstCharacterIndex + 1))
                ),
            $layoutManager->getGlyphRangeForCharacterRange(
                new RE_Range($firstCharacterIndex, ($paragraphSeparatorIndex - $firstCharacterIndex + 1))
                )
            );
    }

}
