<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage TextLayout
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * Declares the informal interface used by layout manager delegate objects.
 * 
 * Delegates are not required to support any of these methods (i.e., there is
 * no strict check to verify that the delegate implements this interface).
 * Rather, the delegate can choose to implement only those methods which are
 * of interest.
 * 
 * @package    Pdf
 * @subpackage TextLayout
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
interface RE_Pdf_LayoutManager_DelegateInterface
{
    /**
     * Called when the layout manager has completed text layout for the
     * specified text container. If the end-of-text flag is true, indicates
     * that this is the final text container used in laying out the text.
     * 
     * @param RE_Pdf_LayoutManager $layoutManager
     * @param RE_Pdf_TextContainer $textContainer
     * @param boolean              $atEndOfText
     */
    public function didCompleteLayoutForTextContainer(
        RE_Pdf_LayoutManager $layoutManager, RE_Pdf_TextContainer $textContainer, $atEndOfText);
    
    /**
     * Called when the layout manager still needs to lay out text, but all
     * available text containers are full. Delegates can use this message to
     * add additional text containers to the layout manager, possibly on
     * additional pages.
     * 
     * @param RE_Pdf_LayoutManager $layoutManager
     */
    public function ranOutOfTextContainers(RE_Pdf_LayoutManager $layoutManager);    

}
