<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage TextLayout
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */

/**
 * A RE_Pdf_GlyphGenerator performs the initial step of the text layout process
 * by mapping the Unicode characters in a {@link RE_Pdf_LayoutManager}'s
 * {@link RE_Pdf_Text} object to the appropriate glyphs in the corresponding
 * fonts.
 *
 * @package    Pdf
 * @subpackage TextLayout
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Pdf_GlyphGenerator
{
  /**** Class Variables ****/

    /**
     * Shared default glyph generator object.
     * @var RE_Pdf_GlyphGenerator
     */
    protected static $_defaultGlyphGenerator = null;



  /**** Instance Variables ****/

    /**
     * Layout manager we are generating glyphs for.
     * @var RE_Pdf_LayoutManager
     */
    protected $_layoutManager = null;



  /**** Class Methods ****/

    /**
     * Returns the shared default glyph generator object.
     *
     * @return RE_Pdf_GlyphGenerator
     */
    public static function defaultGlyphGenerator()
    {
        if (empty(self::$_defaultGlyphGenerator)) {
            self::setDefaultGlyphGenerator(new self());
        }
        return self::$_defaultGlyphGenerator;
    }

    /**
     * Sets the shared default glyph generator object.
     *
     * @param RE_Pdf_GlyphGenerator $glyphGenerator
     */
    public static function setDefaultGlyphGenerator(RE_Pdf_GlyphGenerator $glyphGenerator)
    {
        self::$_defaultGlyphGenerator = $glyphGenerator;
    }



  /**** Public Interface ****/


  /* Glyph Generation */

    /**
     * Triggers nominal glyph generation for the specified character range by
     * performing a one-to-one mapping of Unicode characters to glyphs.
     *
     * @param RE_Pdf_LayoutManager $layoutManager
     * @param integer              $numCharacters
     * @param integer              $glyphIndex
     * @param integer              $characterIndex
     */
    public function generateGlyphsForLayoutManager(RE_Pdf_LayoutManager $layoutManager, $numCharacters, $glyphIndex, $characterIndex)
    {
        if ($numCharacters < 1) {
            return;
        }

        $this->_layoutManager = $layoutManager;

        $characterRange = new RE_Range($characterIndex, $numCharacters);
        $effectiveRange = new RE_Range(0, $characterIndex);

        $text = $layoutManager->getText();

        $maxRange = min($characterRange->maxRange(), $text->getLength());
        while ($effectiveRange->maxRange() < $maxRange) {

            $fontName = $text->getAttributeAtIndex(
                RE_AttributedString::ATTRIBUTE_FONT, $effectiveRange->maxRange(), $effectiveRange);
            $font = RE_Pdf_Font::fontWithName($fontName);

            $characters = $text->getCharactersInRange($effectiveRange);

            $glyphs = $font->cmap->glyphNumbersForCharacters($characters);
            $glyphCount = count($glyphs);

            $layoutManager->insertGlyphs($glyphs, $glyphIndex, $characterIndex);

            $glyphIndex     += $glyphCount;
            $characterIndex += $effectiveRange->getLength();

            foreach ($glyphs as $index => $glyph) {
                if ($glyph === RE_Font_Cmap::MISSING_CHARACTER_GLYPH) {
                    $this->_substituteGlyph($text, $index);
                }
            }
        }
    }



  /**** Internal Interface ****/


  /* Glyph Substitution */

    /**
     * Attempts to locate a substitute glyph for the character at the specified
     * glyph index.
     *
     * @param RE_Pdf_Text $text
     * @param integer     $glyphIndex
     */
    protected function _substituteGlyph($text, $glyphIndex)
    {
        $characterIndex = $this->_layoutManager->getCharacterIndexForGlyphIndex($glyphIndex);
        $character = $text->getCharacterAtIndex($characterIndex);
        foreach ($this->_getFallbackFontsForCharacterAtIndex($text, $characterIndex) as $font) {
            $glyph = $font->cmap->glyphNumberForCharacter($character);
            if ($glyph !== RE_Font_Cmap::MISSING_CHARACTER_GLYPH) {
                $text->setAttributeForRange(RE_AttributedString::ATTRIBUTE_FONT, $font->getFontName(RE_Pdf_Font::NAME_POSTSCRIPT, 'en', 'UTF-8'), new RE_Range($characterIndex, 1));
                $this->_layoutManager->replaceGlyph($glyphIndex, $glyph);
                break;
            }
        }
    }

    /**
     * Returns an ordered array of font objects to use when searching for a
     * substitute glyph for the specified character.
     *
     * @param RE_Pdf_Text $text
     * @param integer     $characterIndex
     * @return RE_Pdf_Font[]
     */
    protected function _getFallbackFontsForCharacterAtIndex($text, $characterIndex)
    {
        $fonts = array();
        foreach (array('DejaVuSans', 'SourceHanSansCN-Regular', 'SourceHanSansJP-Normal', 'SourceHanSansKR-Normal') as $fontName) {
            try {
                $fonts[] = RE_Pdf_Font::fontWithName($fontName);
            } catch (Exception $e) {}
        }
        return $fonts;
    }

}
