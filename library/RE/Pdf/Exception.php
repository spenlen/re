<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage Core
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * Exception class for RE_Pdf.
 *
 * If you expect a certain type of exception to be caught and handled by the
 * caller, create a constant for it here and include it in the object being
 * thrown. Example:
 *
 *   throw new RE_Pdf_Exception(
 *       'foo() is not yet implemented', RE_Pdf_Exception::NOT_IMPLEMENTED);
 *
 * This allows the caller to determine the specific type of exception that was
 * thrown without resorting to parsing the descriptive text.
 *
 * IMPORTANT: Do not rely on numeric values of the constants! They are grouped
 * sequentially below for organizational purposes only. The numbers may come to
 * mean something in the future, but they are subject to renumbering at any
 * time. ALWAYS use the symbolic constant names, which are guaranteed never to
 * change, in logical checks! You have been warned.
 *
 * @package    Pdf
 * @subpackage Core
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Pdf_Exception extends Exception
{
  /**** Class Constants ****/


  /* Generic Exceptions */

    /**
     * The feature or option is planned but has not yet been implemented. It
     * should be available in a future revision of the framework.
     */
    const NOT_IMPLEMENTED = 0x0001;

    /**
     * The feature or option has been deprecated and will be removed in a future
     * revision of the framework. The descriptive text accompanying this
     * exception should explain how to use the replacement features or options.
     */
    const DEPRECATED = 0x0002;


  /* Fonts */

    /**
     * The specified glyph number is out of range for this font.
     */
    const GLYPH_OUT_OF_RANGE = 0x0101;

    /**
     * This font program has copyright bits set which prevent it from being
     * embedded in the PDF file. You must specify the no-embed option to use
     * this font.
     */
    const FONT_CANT_BE_EMBEDDED = 0x0102;

    /**
     * The font name did not match any previously instantiated font and is not
     * one of the standard 14 PDF fonts.
     */
    const BAD_FONT_NAME = 0x0103;

    /**
     * The factory method could not determine the type of the font file.
     */
    const CANT_DETERMINE_FONT_TYPE = 0x0104;


  /* Text Layout System */

    /**
     * The specified attribute value for the text object cannot be used.
     */
    const BAD_ATTRIBUTE_VALUE = 0x0201;
    
    
  /* Images */
  
    /**
     * The factory method could not determine the type of the image file.
     */
    const CANT_DETERMINE_IMAGE_TYPE = 0x0301;

}
