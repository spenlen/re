<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage TextLayout
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * RE_Pdf_Text extends {@link RE_AttributedString} by adding attributes and
 * methods used by {@link RE_Pdf_LayoutManager} and to perform text layout on
 * PDF pages.
 * 
 * It also notifies its layout manager any time its string changes, allowing the
 * layout manager to recalculate layout based on the updated characters or
 * attributes.
 *
 * @package    Pdf
 * @subpackage TextLayout
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Pdf_Text extends RE_AttributedString
{
  /**** Class Constants ****/


  /* Writing Directions */

    /**
     * The writing direction is determined automatically using the Unicode Bidi
     * algorithm.
     */
    const WRITING_DIRECTION_NATURAL = 0;

    /**
     * Left-to-right writing direction.
     */
    const WRITING_DIRECTION_LEFT_TO_RIGHT = 1;

    /**
     * Right-to-left writing direction.
     */
    const WRITING_DIRECTION_RIGHT_TO_LEFT = 2;
    
    

  /**** Instance Variables ****/

    /**
     * Current layout manager responsible for laying out and drawing the
     * string's characters.
     * @var RE_Pdf_LayoutManager
     */
    protected $_layoutManager = null;



  /**** Public Interface ****/


  /* Layout Manager Interaction */
  

    /**
     * Returns the layout manager responsible for laying out and drawing the
     * string's characters.
     *
     * @return RE_Pdf_LayoutManager
     */
    public function getLayoutManager()
    {
        return $this->_layoutManager;
    }
    
    /**
     * Sets the layout manager responsible for laying out and drawing the
     * string's characters.
     * 
     * If a layout manager was set previously, notifies it that it is no longer
     * responsible for laying out this string.
     *
     * @param RE_Pdf_LayoutManager $layoutManager
     */
    public function setLayoutManager(RE_Pdf_LayoutManager $layoutManager)
    {
        if (isset($this->_layoutManager)) {
            $this->_layoutManager->setText(null);
        }
        $layoutManager->setText($this);
        $this->_layoutManager = $layoutManager;
    }
    

  /* String Manipulation */

    /**
     * Replaces the entire string with the new source string, applying the
     * optional attributes to the new characters.
     *
     * Overridden to notifiy the layout manager that it needs to invalidate
     * glyph generation and layout for the entire old character range.
     * 
     * @param string $string
     * @param array  $attributes (optional) Array of attribute name/value pairs.
     * @param string $charEncoding (optional) Character encoding of source
     *   string. If omitted, uses the default encoding method specified when
     *   the object was created.
     */
    public function setString($string, array $attributes = null, $charEncoding = null)
    {
        if (isset($this->_layoutManager)) {
            $oldRange = new RE_Range(0, $this->getLength());
            $this->_layoutManager->invalidateGlyphsForCharacterRange($oldRange, 0);
            $this->_layoutManager->invalidateLayoutForCharacterRange($oldRange);            
        }
        parent::setString($string, $attributes, $charEncoding);
    }
    
    /* appendString() does not need to be overridden since the layout manager
     * will automatically detect that it does not have glyph or layout information
     * for the newly-added character range.
     */


  /* Attribute Manipulation */

    /** @todo Add attribute manipulation overrides... */
    
}
