<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage DocumentStructure
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * RE_Pdf_Canvas_Interface defines a paintable surface on which other PDF
 * objects can be drawn, forming the basis for page content streams.
 * 
 * This interface extends {@link RE_Pdf_OutputDestination_Interface} by
 * adding methods to manage the most common attributes of the canvas' graphics
 * state.
 * 
 * @package    Pdf
 * @subpackage DocumentStructure
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
interface RE_Pdf_Canvas_Interface extends RE_Pdf_OutputDestination_Interface
{
    /**
     * Saves the current graphics state by pushing a copy of it onto the
     * graphics state stack, a LIFO stack that allows up to 28 levels of nesting
     * (see PDF 1.7 Reference, Appendix C).
     * 
     * This allows you to make local changes to the graphics state just prior
     * to drawing certain objects without affecting the rest of the canvas.
     * When finished, restore the saved graphics state by calling
     * {@link restoreGraphicsState()}.
     *
     * @throws BadMethodCallException if the nesting level exceeds 28.
     */
    public function saveGraphicsState();
    
    /**
     * Restores the most recent graphics state pushed onto the graphics state
     * stack by {@link saveGraphicsState()}.
     */
    public function restoreGraphicsState();
    
    /**
     * Changes the current stroke (line) color for the canvas.
     * 
     * @param RE_Color $color
     */
    public function setStrokeColor(RE_Color $color);
    
    /**
     * Changes the current fill color for the canvas.
     * 
     * @param RE_Color $color
     */
    public function setFillColor(RE_Color $color);
    
    /**
     * Adds the specified procedure set to the canvas' resource dictionary.
     * 
     * @param string $setName
     */
    public function addProcSet($setName);

    /**
     * Adds the specified resource to the canvas' resource dictionary. Returns
     * the unique resource name.
     *
     * @param string        $resourceType
     * @param RE_Pdf_Object $resource
     * @return string
     */
    public function addResource($resourceType, RE_Pdf_Object $resource);

}
