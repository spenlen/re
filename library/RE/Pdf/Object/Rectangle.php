<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage Core
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * RE_Pdf_Object_Rectangle objects are used to describe locations on pages
 * as well as bounding boxes for various objects such as fonts, XObjects, etc.
 *
 * @package    Pdf
 * @subpackage Core
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Pdf_Object_Rectangle extends RE_Pdf_Object
{
  /**** Class Methods ****/
  
    /**
     * Writes the rectangle value directly to the specified output destination.
     * 
     * @param RE_Pdf_OutputDestination_Interface $output
     * @param RE_Rect                            $value
     */
    public static function writeValue(RE_Pdf_OutputDestination_Interface $output, $value)
    {
        if (! ((is_object($value)) && ($value instanceof RE_Rect))) {
            throw new InvalidArgumentException('Value must be a RE_Rect object');
        }

        /* Rectangles are written as an array of four numbers specifying the
         * lower-left and upper-right coordinates.
         */
        $output->writeBytes('[');
        RE_Pdf_Object_Numeric::writeValue($output, $value->minX());
        RE_Pdf_Object_Numeric::writeValue($output, $value->minY());
        RE_Pdf_Object_Numeric::writeValue($output, $value->maxX());
        RE_Pdf_Object_Numeric::writeValue($output, $value->maxY());
        $output->writeBytes(']');
    }
  


  /**** Public Interface ****/


  /* Concrete Subclass Implementation */

    /**
     * Extracts the rectangle object from the specified file parser at its current
     * read offset. Returns the extracted object and moves the offset past the
     * object and any trailing white space.
     *
     * @param  RE_FileParser_Pdf $parser
     * @return RE_Pdf_Object_Rectangle
     * @throws RE_Pdf_Exception if a rectangle object is not present at the
     *   current parser offset.
     */
    public static function extract(RE_FileParser_Pdf $parser)
    {
        $object = RE_Pdf_Object_Array::extract($parser);
        return new self($object);
    }

    /**
     * Writes the rectangle value to the specified output destination.
     *
     * @param RE_Pdf_OutputDestination_Interface $output
     */
    public function write(RE_Pdf_OutputDestination_Interface $output)
    {
        self::writeValue($output, $this->_value);
    }


  /* Accessor Methods */

    /**
     * Sets the rectangle's value. The value can be specified as either a
     * RE_Rect object or a RE_Pdf_Object_Array object containing four numbers.
     *
     * @param mixed $value
     * @throws InvalidArgumentException if the value is not a RE_Rect or
     *   RE_Pdf_Object_Array object.
     */
    public function setValue($value)
    {
        if (is_null($value)) {
            $rect = new RE_Rect();
            
        } else if (! is_object($value)) {
            throw new InvalidArgumentException('Value must be a RE_Rect or RE_Pdf_Object_Array object');
            
        } else if ($value instanceof RE_Rect) {
            $rect = clone $value;
            
        } else if ($value instanceof RE_Pdf_Object_Array) {
            if (count($value) != 4) {
                throw new InvalidArgumentException('Rectangle array must contain exactly 4 numbers');
            }
            foreach ($value as $key => $obj) {
                if (! $obj instanceof RE_Pdf_Object_Numeric) {
                    throw new InvalidArgumentException("Rectangle array element $key is not a number");
                }
            }

            /* Rectangles may be specified in the PDF document as any two pairs
             * of opposite corners. Calculate the rectangle's origin and width.
             */
            $x1 = $value[0]->getValue();
            $y1 = $value[1]->getValue();
            $x2 = $value[2]->getValue();
            $y2 = $value[3]->getValue();
            $rect = new RE_Rect(min($x1, $x2),  min($y1, $y2),
                                abs($x1 - $x2), abs($y1 - $y2));
            
        } else {
            throw new InvalidArgumentException(
                'Value must be a RE_Rect or RE_Pdf_Object_Array object; '
              . 'instance of ' . get_class($value) . ' given');
        }
        
        parent::setValue($rect);
    }

}
