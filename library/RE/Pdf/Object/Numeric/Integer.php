<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage Core
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * Subclass of {@link RE_Pdf_Object_Numeric} used to represent integer
 * values.
 *
 * Strictly speaking, the range of the object's value is implementation-
 * dependent, but to maintin wide compatibility with existing PDF viewer
 * applications, the value range is limited to 32-bit signed integers
 * (+/- 2^31). See PDF 1.7 Reference, Appendix C.
 *
 * @package    Pdf
 * @subpackage Core
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Pdf_Object_Numeric_Integer extends RE_Pdf_Object_Numeric
{
  /**** Class Methods ****/
  
    /**
     * Writes the integer value directly to the specified output destination.
     * 
     * @param RE_Pdf_OutputDestination_Interface $output
     * @param integer                            $value
     */
    public static function writeValue(RE_Pdf_OutputDestination_Interface $output, $value)
    {
        /* The leading space is required to delimit the integer object from
         * the preceeding object.
         */
        $output->writeBytes(' ' . (integer)$value);
    }
  


  /**** Public Interface ****/


  /* Concrete Subclass Implementation */

    /**
     * Extracts the numeric object from the specified file parser at its current
     * read offset. Returns the extracted object and moves the offset past the
     * object and any trailing white space.
     *
     * @param  RE_FileParser_Pdf $parser
     * @return RE_Pdf_Object_Numeric
     * @throws RE_Pdf_Exception if a numeric object is not present at the
     *   current parser offset.
     */
    public static function extract(RE_FileParser_Pdf $parser)
    {
        $offset = $parser->getOffset();

        $object = RE_Pdf_Object_Numeric::extract($parser);
        if (! $object instanceof self) {
            throw new RE_Pdf_Exception(sprintf('Integer object not found, found numeric object instead at parser offset 0x%x',
                                               $offset));
        }

        return $object;
    }

    /**
     * Writes the integer value to the specified output destination.
     *
     * @param RE_Pdf_OutputDestination_Interface $output
     */
    public function write(RE_Pdf_OutputDestination_Interface $output)
    {
        self::writeValue($output, $this->_value);
    }


  /* Accessor Methods */

    /**
     * Sets the integer value.
     *
     * To maintin wide compatibility with existing PDF viewer applications,
     * the value range is limited to 32-bit signed integers (+/- 2^31).
     * See PDF 1.7 Reference, Appendix C.
     *
     * @param integer $value
     * @throws InvalidArgumentException if the value is outside the 32-bit
     *   signed integer range.
     */
    public function setValue($value)
    {
        if (is_null($value)) {
            $value = 0;
        } else if ($value > 2147483647) {
            throw new InvalidArgumentException(
                'Value is too large for a RE_Pdf_Object_Numeric_Integer; use RE_Pdf_Object_Numeric instead.');
        } else if ($value < -2147483648) {
            throw new InvalidArgumentException(
                'Value is too small for a RE_Pdf_Object_Numeric_Integer; use RE_Pdf_Object_Numeric instead.');
        }
        parent::setValue($value);
    }

}
