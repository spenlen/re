<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage Core
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * RE_Pdf_Object_Boolean objects represent a simple true or false value. They
 * are typically used as flags in {@link RE_Pdf_Object_Dictionary} objects.
 *
 * @package    Pdf
 * @subpackage Core
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Pdf_Object_Boolean extends RE_Pdf_Object
{
  /**** Class Methods ****/
  
    /**
     * Writes the boolean value directly to the specified output destination.
     * 
     * @param RE_Pdf_OutputDestination_Interface $output
     * @param boolean                            $value
     */
    public static function writeValue(RE_Pdf_OutputDestination_Interface $output, $value)
    {
        /* The leading spaces are required to delimit the boolean object from
         * the preceeding object.
         */
        $output->writeBytes((boolean)$value ? ' true' : ' false');
    }
  


  /**** Public Interface ****/


  /* Concrete Subclass Implementation */

    /**
     * Extracts the boolean object from the specified file parser at its current
     * read offset. Returns the extracted object and moves the offset past the
     * object and any trailing white space.
     *
     * @param  RE_FileParser_Pdf $parser
     * @return RE_Pdf_Object_Boolean
     * @throws RE_Pdf_Exception if a boolean object is not present at the
     *   current parser offset.
     */
    public static function extract(RE_FileParser_Pdf $parser)
    {
        $bytes = $parser->readBytes(5);
        if ($bytes === 'false') {
            $object = new self(false);

        } else if (substr($bytes, 0, 4) === 'true') {
            $object = new self(true);
            $parser->skipBytes(-1);

        } else {
            throw new RE_Pdf_Exception(sprintf('Cannot read boolean object at parser offset 0x%x',
                                               ($parser->getOffset() - 5)));
        }
        $parser->skipWhiteSpace();

        return $object;
    }

    /**
     * Writes the boolean value to the specified output destination.
     *
     * @param RE_Pdf_OutputDestination_Interface $output
     */
    public function write(RE_Pdf_OutputDestination_Interface $output)
    {
        self::writeValue($output, $this->_value);
    }

}
