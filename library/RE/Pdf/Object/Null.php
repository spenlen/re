<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage Core
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * RE_Pdf_Object_Null objects are used to indicate the absence of a value.
 *
 * @package    Pdf
 * @subpackage Core
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Pdf_Object_Null extends RE_Pdf_Object
{
  /**** Class Methods ****/
  
    /**
     * Writes the null value directly to the specified output destination.
     * 
     * @param RE_Pdf_OutputDestination_Interface $output
     * @param null                               $value
     */
    public static function writeValue(RE_Pdf_OutputDestination_Interface $output, $value)
    {
        /* The leading space is required to delimit the null object from
         * the preceeding object.
         */
        $output->writeBytes(' null');
    }
  


  /**** Public Interface ****/


  /* Concrete Subclass Implementation */

    /**
     * Extracts the null object from the specified file parser at its current
     * read offset. Returns the extracted object and moves the offset past the
     * object and any trailing white space.
     *
     * @param  RE_FileParser_Pdf $parser
     * @return RE_Pdf_Object_Null
     * @throws RE_Pdf_Exception if a null object is not present at the
     *   current parser offset.
     */
    public static function extract(RE_FileParser_Pdf $parser)
    {
        $bytes = $parser->readBytes(4);
        if ($bytes !== 'null') {
            throw new RE_Pdf_Exception(sprintf('Cannot read null object at parser offset 0x%x',
                                               ($parser->getOffset() - 4)));
        }
        $parser->skipWhiteSpace();

        return new self();
    }

    /**
     * Writes the null value to the specified output destination.
     *
     * @param RE_Pdf_OutputDestination_Interface $output
     */
    public function write(RE_Pdf_OutputDestination_Interface $output)
    {
        self::writeValue($output, $this->_value);
    }


  /* Object Lifecycle */

    /**
     * Creates a new RE_Pdf_Object_Null object.
     *
     * @param null $value
     * @throws InvalidArgumentException if a value is provided.
     */
    public function __construct($value = null)
    {
        if (! is_null($value)) {
            throw new InvalidArgumentException('Null objects cannot have a value.');
        }
    }


  /* Object Magic Methods */

    /**
     * Returns the object value as a string. Useful for debugging.
     *
     * @return string
     */
    public function __toString()
    {
        return 'null';
    }


  /* Accessor Methods */

    /**
     * RE_Pdf_Object_Null objects have no value. Always throws a
     * BadMethodCallException.
     *
     * @param mixed $value
     * @throws BadMethodCallException
     */
    public function setValue($value)
    {
        throw new BadMethodCallException('Null objects cannot have a value.');
    }

}
