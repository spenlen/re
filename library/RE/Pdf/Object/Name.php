<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage Core
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * RE_Pdf_Object_Name objects are identifiers. They are used as the keys
 * of {@link RE_Pdf_Object_Dictionary} objects, and may be used to label
 * XObjects such as images an fonts in a page's content stream.
 *
 * @package    Pdf
 * @subpackage Core
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Pdf_Object_Name extends RE_Pdf_Object
{
  /**** Class Methods ****/

    /**
     * Writes the name value directly to the specified output destination.
     * 
     * @param RE_Pdf_OutputDestination_Interface $output
     * @param string                             $value
     */
    public static function writeValue(RE_Pdf_OutputDestination_Interface $output, $value)
    {
        $output->writeBytes('/' . preg_replace_callback(
            '/[\x00-\x21\x7e-\xff()<>\[\]{}\/%]/', 'RE_Pdf_Object_Name::escapeChar', $value
            ));
    }
  
    /**
     * Helper function for {@link extract()} that unescapes characters from
     * the name string. Used with {@link preg_replace_callback}.
     *
     * @param array $matches
     * @return string
     */
    protected static function unescapeChar($matches)
    {
        return chr(hexdec($matches[0]));
    }

    /**
     * Helper function for {@link writeValue()} that escapes certain characters
     * in the name string. Used with {@link preg_replace_callback}.
     *
     * @param array $matches
     * @return string
     */
    protected static function escapeChar($matches)
    {
        return '#' . dechex(ord($matches[0]));
    }



  /**** Public Interface ****/


  /* Concrete Subclass Implementation */

    /**
     * Extracts the name object from the specified file parser at its current
     * read offset. Returns the extracted object and moves the offset past the
     * object and any trailing white space.
     *
     * @param  RE_FileParser_Pdf $parser
     * @return RE_Pdf_Object_Name
     * @throws RE_Pdf_Exception if a name object is not present at the
     *   current parser offset.
     */
    public static function extract(RE_FileParser_Pdf $parser)
    {
        if ($parser->readBytes(1) !== '/') {
            throw new RE_Pdf_Exception(sprintf('Cannot read name object at parser offset 0x%x',
                                               ($parser->getOffset() - 1)));
        }

        $string = '';

        while (1) {
            $chunk       = $parser->readBytes(32, false);
            $chunkLength = strlen($chunk);
            if ($chunkLength < 1) {
                break;
            }
            $length = strcspn($chunk, "\x00\t\n\x0c\r ()<>[]{}/%");
            if ($length == $chunkLength) {
                $string .= $chunk;
            } else {
                $string .= substr($chunk, 0, $length);
                $parser->skipBytes($length - $chunkLength);
                $parser->skipWhiteSpace();
                break;
            }
        }

        return new self(preg_replace_callback(
            '/#[a-fA-F0-9]{2}/', 'RE_Pdf_Object_Name::unescapeChar', $string
            ));
    }

    /**
     * Writes the name value to the specified output destination.
     *
     * @param RE_Pdf_OutputDestination_Interface $output
     */
    public function write(RE_Pdf_OutputDestination_Interface $output)
    {
        self::writeValue($output, $this->_value);
    }


  /* Object Magic Methods */

    /**
     * Returns the object value as a string. Useful for debugging.
     *
     * @return string
     */
    public function __toString()
    {
        return '/' . $this->_value;
    }


  /* Accessor Methods */

    /**
     * Sets the name value.
     *
     * To maintin wide compatibility with existing PDF viewer applications,
     * the length of the name is limited to 127 bytes (not characters, in
     * case of Unicode name strings). See PDF 1.7 Reference, Appendix C.
     *
     * @param string $value
     * @throws LengthException if the name is longer than 127 bytes.
     */
    public function setValue($value)
    {
        if (strlen($value) > 127) {
            throw new LengthException('Names may not be longer than 127 bytes.');
        }
        parent::setValue($value);
    }

}
