<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage Core
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * RE_Pdf_Object_Numeric objects represent floating-point (real) values.
 * The range and precision of the object's value is implementation-dependent,
 * though generally speaking, any IEEE single-precision floating-point value
 * can be safely used.
 *
 * @package    Pdf
 * @subpackage Core
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Pdf_Object_Numeric extends RE_Pdf_Object
{
  /**** Class Methods ****/
  
    /**
     * Writes the numeric value directly to the specified output destination.
     * 
     * @param RE_Pdf_OutputDestination_Interface $output
     * @param float                              $value
     */
    public static function writeValue(RE_Pdf_OutputDestination_Interface $output, $value)
    {
        /* The leading space is required to delimit the numeric object from
         * the preceeding object.
         *
         * PHP's (string) type coercion may return the float value using
         * exponential format, which is unsupported by PDF. Convert to a
         * fixed-point format and remove unnecessary trailing zeros and/or
         * the decimal point.
         *
         * PDF viewer applications generally recognize only 5 digits of
         * precision for the fractional part (see PDF 1.7 Reference, Appendix C)
         * so conserve space by limiting to 8 digits.
         *
         * rtrim() is called twice, in this order, to avoid changing integral
         * values ending with zeros such as "100.".
         */
        $output->writeBytes(' ' . rtrim(rtrim(sprintf('%.8F', (float)$value), '0'), '.'));
    }
  


  /**** Public Interface ****/


  /* Concrete Subclass Implementation */

    /**
     * Extracts the numeric object from the specified file parser at its current
     * read offset. Returns the extracted object and moves the offset past the
     * object and any trailing white space.
     *
     * If the value is integral, returns a RE_Pdf_Object_Numeric_Integer object.
     *
     * @param  RE_FileParser_Pdf $parser
     * @return RE_Pdf_Object_Numeric|RE_Pdf_Object_Numeric_Integer
     * @throws RE_Pdf_Exception if a numeric object is not present at the
     *   current parser offset.
     */
    public static function extract(RE_FileParser_Pdf $parser)
    {
        $offset = $parser->getOffset();

        $value = '';

        while (1) {
            $chunk       = $parser->readBytes(32, false);
            $chunkLength = strlen($chunk);
            if ($chunkLength < 1) {
                break;
            }
            $length = strspn($chunk, '+-.0123456789');
            if ($length == $chunkLength) {
                $value .= $chunk;
            } else {
                $value .= substr($chunk, 0, $length);
                $parser->skipBytes($length - $chunkLength);
                $parser->skipWhiteSpace();
                break;
            }
        }
        $value = ltrim($value, '+');

        if (strlen($value) < 1) {
            throw new RE_Pdf_Exception(sprintf('Cannot read numeric object at parser offset 0x%x', $offset));
        }

        if ((strpos($value, '.') === false) &&
            ((float)$value >= -2147483648) && ((float)$value <= 2147483647)
           ) {
            return new RE_Pdf_Object_Numeric_Integer($value);
        } else {
            return new self($value);
        }
    }

    /**
     * Writes the numeric value to the specified output destination.
     *
     * @param RE_Pdf_OutputDestination_Interface $output
     */
    public function write(RE_Pdf_OutputDestination_Interface $output)
    {
        self::writeValue($output, $this->_value);
    }


  /* Accessor Methods */

    /**
     * Sets the numeric value.
     *
     * @param float $value
     */
    public function setValue($value)
    {
        if (is_null($value)) {
            $value = 0;
        }
        parent::setValue($value);
    }

}
