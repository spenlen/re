<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage Core
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * A RE_Pdf_Object_Stream object is a special type of dictionary object that
 * has an additional container for an unlimited amount of binary data. Stream
 * objects are used to embed binary data such as images and fonts inside a PDF
 * document, as well as page content streams.
 *
 * @package    Pdf
 * @subpackage Core
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Pdf_Object_Stream extends RE_Pdf_Object_Dictionary
{
  /**** Instance Variables ****/

    /**
     * Data provider object for the binary data stream.
     * @var RE_Pdf_Object_StreamDataProvider_Interface
     */
    protected $_dataProvider = null;



  /**** Class Methods ****/

    /**
     * Helper method for {@link RE_Pdf_Object_Dictionary::extract()} that
     * completes the extraction of the stream object from the source PDF.
     *
     * @param  RE_FileParser_Pdf $parser
     * @param  array             $dictionaryValues
     * @return RE_Pdf_Object_Stream
     * @throws RE_Pdf_Exception if a stream object is not present at the
     *   current parser offset.
     */
    public static function extractStream(RE_FileParser_Pdf $parser, $dictionaryValues)
    {
        /* The stream keyword must be followed immediately by either a
         * single LF character or a CRLF sequence. See PDF 1.7 Reference,
         * section 3.2.7.
         */
        $parser->skipBytes(6);

        $byte = $parser->readBytes(1);
        if ($byte === "\r") {
            $byte = $parser->readBytes(1);
        }
        if ($byte !== "\n") {
            throw new RE_Pdf_Exception('Invalid end-of-line after \'stream\' keyword at parser offset '
                                         . sprintf('0x%x', ($parser->getOffset() - 7)));
        }

        /* Skip over the binary data and check for the endstream keyword.
         * Make note of the current offset here in case the Length entry
         * is an indirect object, which would cause it to be extracted
         * immediately, changing the parser offset.
         */
        $dataOffset = $parser->getOffset();

        if (! isset($dictionaryValues['Length'])) {
            /** @todo Add auto-recovery here to calculate the length. */
            throw new RE_Pdf_Exception('Missing Length for stream object at parser offset '
                                         . sprintf('0x%x', $parser->getOffset()));
        }
        $dataLength = $dictionaryValues['Length']->getValue();

        $parser->moveToOffset($dataOffset);

        $parser->skipBytes($dataLength);
        $parser->skipWhiteSpace();

        if ($parser->readBytes(9) !== 'endstream') {
            throw new RE_Pdf_Exception('Missing \'endstream\' keyword at parser offset '
                                         . sprintf('0x%x', ($parser->getOffset() - 9)));
        }
        $parser->skipWhiteSpace();

        if (empty($dictionaryValues['Filter'])) {
            $filters = null;
        } else {
            if ($dictionaryValues['Filter'] instanceof RE_Pdf_Object_Array) {
                $filters = $dictionaryValues['Filter'];
            } else {
                $filters = new RE_Pdf_Object_Array(array($dictionaryValues['Filter']));
            }
        }
        
        $dataProvider = new RE_Pdf_Object_StreamDataProvider_FileParser(
            $parser, $dataOffset, $dataLength, $filters);

        $object = new self($dictionaryValues);
        $object->setDataProvider($dataProvider);
        return $object;
    }



  /**** Public Interface ****/


  /* Concrete Subclass Implementation */

    /**
     * Extracts the stream object from the specified file parser at its current
     * read offset. Returns the extracted object and moves the offset past the
     * object and any trailing white space.
     *
     * @param  RE_FileParser_Pdf $parser
     * @return RE_Pdf_Object_Stream
     * @throws RE_Pdf_Exception if a stream object is not present at the
     *   current parser offset.
     */
    public static function extract(RE_FileParser_Pdf $parser)
    {
        $offset = $parser->getOffset();

        $object = RE_Pdf_Object_Dictionary::extract($parser);
        if (! $object instanceof self) {
            throw new RE_Pdf_Exception('Stream object not found, found other dictionary object instead '
                                         . 'at parser offset ' . sprintf('0x%x', ($offset)));
        }

        return $object;
    }

    /**
     * Writes the stream object and its contents to the specified output
     * destination, which must be a {@link RE_Pdf_Document} object.
     *
     * @param RE_Pdf_OutputDestination_Interface $output
     * @throws InvalidArgumentException if the output destination is not a
     *   {@link RE_Pdf_Document} object.
     */
    public function write(RE_Pdf_OutputDestination_Interface $output)
    {
        if (! $output instanceof RE_Pdf_Document) {
            throw new InvalidArgumentException(
                'Output destination must be a RE_Pdf_Document object; instance of ' . get_class($object) . ' given.');
        }
        
        if (! $this->_dataProvider) {
            $this['Length'] = 0;
            parent::write($output);
            $output->writeBytes("\nstream\nendstream");
            return;
        }

        if (empty($this['Filter'])) {
            $filters = new RE_Pdf_Object_Array();
        } else {
            if ($this['Filter'] instanceof RE_Pdf_Object_Array) {
                $filters = $this['Filter'];
            } else {
                $filters = new RE_Pdf_Object_Array(array($this['Filter']));
            }
        }

        $length = $this->_dataProvider->getLength($filters);
        if (is_null($length)) {
            $lengthObject = new RE_Pdf_Object_Numeric_Integer();
            $this['Length'] = $output->addObject($lengthObject);
            parent::write($output);
            $output->writeBytes("\nstream\n");
            $length = $this->_dataProvider->write($output, $filters);
            $output->writeBytes("\nendstream");
            $lengthObject->setValue($length);
            $this['Length']->finalize();

        } else {
            $this['Length'] = $length;
            parent::write($output);
            $output->writeBytes("\nstream\n");
            $this->_dataProvider->write($output, $filters);
            $output->writeBytes("\nendstream");
        }
    }


  /* Object Magic Methods */

    /**
     * Returns the object value as a string. Useful for debugging.
     *
     * @return string
     */
    public function __toString()
    {
        return 'stream ' . parent::__toString() . ' endstream';
    }


  /* Accessor Methods */

    /**
     * Returns the data provider object or null if no data provider has been set.
     *
     * @return RE_Pdf_Object_StreamDataProvider_Interface
     */
    public function getDataProvider()
    {
        return $this->_dataProvider;
    }

    /**
     * Sets the data provider object.
     *
     * @param RE_Pdf_Object_StreamDataProvider_Interface $dataProvider
     */
    public function setDataProvider(RE_Pdf_Object_StreamDataProvider_Interface $dataProvider)
    {
        $this->_dataProvider = $dataProvider;
    }


  /* Dictionary Key Type Checking */

    /**
     * Returns the valid object type for important keys in this dictionary.
     *
     * @return string
     */
    public function getValidTypeForKey($key)
    {
        switch ($key) {
        case 'Length':
            return 'RE_Pdf_Object_Numeric';
        }
        return parent::getValidTypeForKey($key);
    }

}
