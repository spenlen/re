<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage Core
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * Stream data provider for use with {@link RE_FileParser} objects.
 *
 * @package    Pdf
 * @subpackage Core
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Pdf_Object_StreamDataProvider_FileParser implements RE_Pdf_Object_StreamDataProvider_Interface
{
  /**** Instance Variables ****/

    /**
     * The file parser containing the stream data.
     * @var RE_FileParser
     */
    protected $_fileParser = null;

    /**
     * Byte offset within the file parser of the start of the stream data.
     * @var integer
     */
    protected $_offset = 0;

    /**
     * Byte length of the stream data.
     * @var integer
     */
    protected $_length = 0;

    /**
     * Array of filters already applied to the stream data in the file parser's
     * data source.
     * @var RE_Pdf_Object_Array
     */
    protected $_originalFilters = null;



  /**** Public Interface ****/


  /* Object Lifecycle */

    /**
     * Creates a new RE_Pdf_Object_StreamDataProvider_FileParser object.
     *
     * @param RE_FileParser       $fileParser
     * @param integer             $offset
     * @param integer             $length
     * @param RE_Pdf_Object_Array $originalFilters (optional)
     */
    public function __construct(RE_FileParser $fileParser, $offset, $length,
        RE_Pdf_Object_Array $originalFilters = null)
    {
        $this->_fileParser      = $fileParser;
        $this->_offset          = $offset;
        $this->_length          = $length;
        $this->_originalFilters = $originalFilters;
    }


  /* RE_Pdf_Object_StreamDataProvider_Interface Methods */

    /**
     * Returns the byte length of the unfiltered (decoded) stream data, if
     * known. If unknown or too expensive to calculate, returns null.
     * 
     * @return integer|null
     */
    public function getDecodedLength()
    {
        if (empty($this->_originalFilters)) {
            return $this->_length;
        }

        return null;
    }

    /**
     * Returns the byte length of the stream data if the specified filters
     * are applied. If unknown or too expensive to calculate, returns null,
     * indicating that the value returned by {@link write()} should be used
     * instead.
     * 
     * @param RE_Pdf_Object_Array $filters
     * @return integer
     */
    public function getLength(RE_Pdf_Object_Array $filters)
    {
        /* If the filters are the same as the original filters from the source
         * PDF document, we already know the length.
         */
        $filterCount = count($filters);
        if ($filterCount == count($this->_originalFilters)) {
            $sameFilters = true;
            for ($i = 0; $i < $filterCount; $i++) {
                if ($filters[$i]->getValue() != $this->_originalFilters[$i]->getValue()) {
                    $sameFilters = false;
                    break;
                }
            }
            if ($sameFilters) {
                return $this->_length;
            }
        }
        
        return null;
    }

    /**
     * Writes the stream data to the specified PDF document's output stream
     * after applying the specified filters, in order. Returns the actual
     * bytes written.
     *
     * @param RE_Pdf_Document     $pdf
     * @param RE_Pdf_Object_Array $filters
     * @return integer
     */
    public function write(RE_Pdf_Document $pdf, RE_Pdf_Object_Array $filters)
    {
        /* If the filters are the same as the original filters from the source
         * PDF document, we can simply copy the data.
         */
        $filterCount = count($filters);
        if ($filterCount == count($this->_originalFilters)) {
            $sameFilters = true;
            for ($i = 0; $i < $filterCount; $i++) {
                if ($filters[$i]->getValue() != $this->_originalFilters[$i]->getValue()) {
                    $sameFilters = false;
                    break;
                }
            }
            if ($sameFilters) {
                $this->_copyToPDF($pdf);
                return $this->_length;
            }
        }

        /* Otherwise, fetch the unfiltered stream data, apply the new filters
         * and write to the destination PDF.
         */
        $bytes = $this->getAllBytes();
        foreach ($filters as $filter) {
            $bytes = RE_Pdf_Filter::encode($filter->getValue(), $bytes);
        }

        $pdf->writeBytes($bytes);
        return strlen($bytes);
    }

    /**
     * Returns the specified bytes from the unfiltered stream data as a binary
     * string.
     * 
     * @param integer $offset
     * @param integer $length
     * @return string|resource
     */
    public function getBytes($offset, $length)
    {
        /** @todo This is incredibly inefficient. Make it better. */
        $bytes = $this->getAllbytes();
        return substr($bytes, $offset, $length);
    }
    
    /**
     * Returns the complete unfiltered stream data as a binary string.
     * 
     * @return string
     */
    public function getAllBytes()
    {
        /* Preserve the existing offset within the file parser.
         */
        $existingOffset = $this->_fileParser->getOffset();
        
        $this->_fileParser->moveToOffset($this->_offset);
        $bytes = $this->_fileParser->readBytes($this->_length);

        if (! empty($this->_originalFilters)) {
            foreach ($this->_originalFilters as $filter) {
                $bytes = RE_Pdf_Filter::decode($filter->getValue(), $bytes);
            }
        }

        $this->_fileParser->moveToOffset($existingOffset);
        
        return $bytes;
    }



  /**** Internal Interface ****/

    /**
     * Copies the unmodified stream data to the specified PDF document.
     *
     * @param RE_Pdf_Document $pdf
     */
    protected function _copyToPDF(RE_Pdf_Document $pdf)
    {
        /* Preserve the existing offset within the file parser.
         */
        $existingOffset = $this->_fileParser->getOffset();

        $this->_fileParser->moveToOffset($this->_offset);
        $length = $this->_length;
        while ($length > 0) {
            $byteCount = min($length, 8192);
            $pdf->writeBytes($this->_fileParser->readBytes($byteCount));
            $length -= $byteCount;
        }

        $this->_fileParser->moveToOffset($existingOffset);
    }

}
