<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage Core
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * {@link RE_Pdf_Object_Stream} objects can store a virtually unlimited
 * amount of binary data, up to several gigabytes in size. In most cases,
 * it is both impractical and unnecessary to keep entire streams in memory.
 * 
 * This interface defines methods that are used to supply and interact with
 * the stream data. Most interactions happen at arm's length allowing highly
 * optimized access to and from both the stream's original source data and
 * the ultimate PDF document containing it.
 * 
 * @package    Pdf
 * @subpackage Core
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
interface RE_Pdf_Object_StreamDataProvider_Interface
{
    /**
     * Returns the byte length of the unfiltered (decoded) stream data, if
     * known. If unknown or too expensive to calculate, returns null.
     * 
     * @return integer|null
     */
    public function getDecodedLength();

    /**
     * Returns the byte length of the stream data if the specified filters
     * are applied. If unknown or too expensive to calculate, returns null,
     * indicating that the value returned by {@link write()} should be used
     * instead.
     * 
     * @param RE_Pdf_Object_Array $filters
     * @return integer
     */
    public function getLength(RE_Pdf_Object_Array $filters);

    /**
     * Writes the stream data to the specified PDF document's output stream
     * after applying the specified filters, in order. Returns the actual
     * bytes written.
     * 
     * Called by {@link RE_Pdf_Object_Stream::write()}, which handles the
     * "stream" and "endstream" keywords; this method should output the raw
     * binary data only.
     *
     * @param RE_Pdf_Document     $pdf
     * @param RE_Pdf_Object_Array $filters
     * @return integer
     */
    public function write(RE_Pdf_Document $pdf, RE_Pdf_Object_Array $filters);
    
    /**
     * Returns the specified bytes from the unfiltered stream data as a binary
     * string.
     * 
     * @param integer $offset
     * @param integer $length
     * @return string|resource
     */
    public function getBytes($offset, $length);
    
    /**
     * Returns the complete unfiltered stream data as a binary string.
     * 
     * @return string
     */
    public function getAllBytes();

}
