<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage Core
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * Editable buffer stream data provider. Used for generating new stream objects.
 *
 * @package    Pdf
 * @subpackage Core
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Pdf_Object_StreamDataProvider_Buffer implements RE_Pdf_Object_StreamDataProvider_Interface
{
  /**** Instance Variables ****/

    /**
     * Stream data as a binary string.
     * @var string
     */
    protected $_buffer = '';

    /**
     * Array of filters already applied to the stream data.
     * @var RE_Pdf_Object_Array
     */
    protected $_originalFilters = null;



  /**** Public Interface ****/


  /* Object Lifecycle */

    /**
     * Creates a new RE_Pdf_Object_StreamDataProvider_Buffer object,
     * optionally pre-filling the buffer with the specified bytes.
     * 
     * If the data has already been filtered, the array of filters applied
     * may also be specified. Note that it will be assumed that any data
     * appended via {@link appendBytes()} will also have these filters
     * applied.
     *
     * @param string              $bytes           (optional)
     * @param RE_Pdf_Object_Array $originalFilters (optional)
     */
    public function __construct($bytes = null, RE_Pdf_Object_Array $originalFilters = null)
    {
        $this->_buffer          = $bytes;
        $this->_originalFilters = $originalFilters;
    }


  /* RE_Pdf_Object_StreamDataProvider_Interface Methods */

    /**
     * Returns the byte length of the unfiltered (decoded) stream data, if
     * known. If unknown or too expensive to calculate, returns null.
     * 
     * @return integer|null
     */
    public function getDecodedLength()
    {
        return strlen($this->_buffer);
    }

    /**
     * Returns the byte length of the stream data if the specified filters
     * are applied. If unknown or too expensive to calculate, returns null,
     * indicating that the value returned by {@link write()} should be used
     * instead.
     * 
     * @param RE_Pdf_Object_Array $filters
     * @return integer
     */
    public function getLength(RE_Pdf_Object_Array $filters)
    {
        if (empty($filters)) {
            return strlen($this->_buffer);
        }
        
        return null;
    }

    /**
     * Writes the stream data to the specified PDF document's output stream
     * after applying the specified filters, in order. Returns the actual
     * bytes written.
     *
     * @param RE_Pdf_Document     $pdf
     * @param RE_Pdf_Object_Array $filters
     * @return integer
     */
    public function write(RE_Pdf_Document $pdf, RE_Pdf_Object_Array $filters)
    {
        $filterCount = count($filters);

        /* If the filters are the same as the original filters from the source
         * data, we can simply copy the data.
         */
        if (isset($this->_originalFilters)) {
            if ($filterCount == count($this->_originalFilters)) {
                $sameFilters = true;
                for ($i = 0; $i < $filterCount; $i++) {
                    if ($filters[$i]->getValue() != $this->_originalFilters[$i]->getValue()) {
                        $sameFilters = false;
                        break;
                    }
                }
                if ($sameFilters) {
                    $pdf->writeBytes($this->_buffer);
                    return strlen($this->_buffer);
                }
            }
        }
        
        /* Otherwise apply the new filters and write to the destination PDF.
         */
        $bytes = $this->_buffer;
        foreach ($filters as $filter) {
            $bytes = RE_Pdf_Filter::encode($filter->getValue(), $bytes);
        }

        $pdf->writeBytes($bytes);
        return strlen($bytes);
    }

    /**
     * Returns the specified bytes from the unfiltered stream data as a binary
     * string.
     * 
     * @param integer $offset
     * @param integer $length
     * @return string|resource
     */
    public function getBytes($offset, $length)
    {
        /** @todo This is incredibly inefficient. Make it better. */
        $bytes = $this->getAllbytes();
        return substr($bytes, $offset, $length);
    }
    
    /**
     * Returns the complete unfiltered stream data as a binary string.
     * 
     * @return string
     */
    public function getAllBytes()
    {
        $bytes = $this->_buffer;

        if (isset($this->_originalFilters)) {
            foreach ($this->_originalFilters as $filter) {
                $bytes = RE_Pdf_Filter::decode($filter->getValue(), $bytes);
            }
        }
        
        return $bytes;
    }


  /* Buffer Manipulation */
  
    /**
     * Replaces the stream data with the specified bytes.
     * 
     * @param string $bytes
     */
    public function setBytes($bytes)
    {
        $this->_buffer = $bytes;
    }

    /**
     * Appends the specified bytes to the end of the stream data.
     * 
     * @param string $bytes
     */
    public function appendBytes($bytes)
    {
        $this->_buffer .= $bytes;
    }

}
