<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage Core
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * RE_Pdf_Object_Indirect objects are used as proxies for actual PDF
 * objects, allowing them to be reused multiple times within a PDF document,
 * or for deferred creation (such as a {@link RE_Pdf_Page} object referring
 * to its parent page tree node, which may not be created until the document
 * itself is finalized).
 *
 * An indirect object can generally be used anywhere a direct object is
 * expected, with only a few small exceptions. In some cases (such as with
 * {@link RE_Pdf_Object_Stream} objects), an indirect object must always
 * be used.
 *
 * Changes to direct object are monitored through the standard SPL Observer
 * pattern. The indirect object only keeps a reference to its direct object
 * when its value has changed, and only long enough so that the new generation
 * of the object can be written to the PDF document.
 *
 * This enables very memory-efficient handling of large PDF objects by
 * allowing you to write objects to the PDF document as soon as you have
 * finished manipulating them. Thereafter, you only need to keep the reference
 * to the small indirect object, and you can free the memory consumed by the
 * large direct object.
 *
 * RE_Pdf_Object_Indirect objects are typically only created by the
 * source PDF document's {@link RE_Pdf_Document::addObject()}, 
 * {@link RE_Pdf_Document::getObject()} and
 * {@link RE_Pdf_Document::extractObject()} methods, which ensure the
 * accuracy of the object and generation numbers. You should never create
 * these objects by hand as you can inadvertently overwrite critical PDF
 * objects, leading to a corrupt PDF document.
 *
 * @package    Pdf
 * @subpackage Core
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Pdf_Object_Indirect extends RE_Pdf_Object implements SplObserver
{
  /**** Instance Variables ****/

    /**
     * Source PDF object number.
     * @var integer
     */
    protected $_objectNumber = 0;

    /**
     * Source PDF object generation number.
     * @var integer
     */
    protected $_generationNumber = 0;

    /**
     * Source PDF document.
     * @var RE_Pdf_Object_Extraction_Interface
     */
    protected $_sourcePDF = null;

    /**
     * Owning PDF document.
     * @var RE_Pdf_Object_Extraction_Interface
     */
    protected $_owningDocument = null;

    /**
     * Type (class name) of direct object, if known.
     * @var string
     */
    protected $_objectType = null;



  /**** Public Interface ****/


  /* Concrete Subclass Implementation */

    /**
     * Extracts the indirect object from the specified file parser at its current
     * read offset. Returns the extracted object and moves the offset past the
     * object and any trailing white space.
     *
     * @param RE_FileParser_Pdf $parser
     * @return RE_Pdf_Object_Indirect
     * @throws RE_Pdf_Exception if an indirect object is not present at the
     *   current parser offset.
     */
    public static function extract(RE_FileParser_Pdf $parser)
    {
        $bytes = $parser->peekBytes(24, false);
        if (! preg_match('/^(\d+) (\d+) R/', $bytes, $matches)) {
            throw new RE_Pdf_Exception(sprintf('Cannot read indirect object at parser offset 0x%x',
                                               $parser->getOffset()));
        }

        $parser->skipBytes(strlen($matches[0]));
        $parser->skipWhiteSpace();

        return new self($matches[1], $matches[2], $parser);
    }

    /**
     * Writes the indirect object reference to the specified output destination.
     *
     * @param RE_Pdf_OutputDestination_Interface $output
     */
    public function write(RE_Pdf_OutputDestination_Interface $output)
    {
        /* The leading space is required to delimit the indirect object from
         * the preceeding object.
         */
        $output->writeBytes(' ' . $this->_objectNumber . ' ' . $this->_generationNumber . ' R');
    }


  /* Object Lifecycle */

    /**
     * Creates a new RE_Pdf_Object_Indirect object, setting the object and
     * generation numbers from the source PDF document.
     *
     * If this is a new object for the source PDF document, the direct object
     * must be supplied in $object. It can be written to the source PDF
     * document immediately by calling the {@link finalize()} method. The
     * source PDF document will automatically finalize any outstanding objects
     * when it itself is finalized.
     *
     * If this is an existing object from the PDF document, the $object is
     * omitted. The type and/or direct object itself will be fetched from the
     * source PDF document only if necessary.
     *
     * @param integer        $objectNumber
     * @param integer        $generationNumber
     * @param RE_Pdf_Object_Extraction_Interface $sourcePDF
     * @param RE_Pdf_Object  $object           (optional)
     * @throws InvalidArgumentException if the $objectNumber or $generationNumber
     *   are invalid.
     */
    public function __construct($objectNumber, $generationNumber,
        RE_Pdf_Object_Extraction_Interface $sourcePDF, RE_Pdf_Object $object = null)
    {
        $this->_objectNumber = (int)$objectNumber;
        if ($this->_objectNumber < 1) {
            throw new InvalidArgumentException("Invalid object number: $objectNumber");
        }

        $this->_generationNumber = (int)$generationNumber;
        if ($this->_generationNumber < 0) {
            throw new InvalidArgumentException("Invalid generation number: $generationNumber");
        }

        $this->_sourcePDF = $sourcePDF;
        if ($sourcePDF instanceof RE_FileParser_Pdf) {
            $this->_owningDocument = $sourcePDF->getOwningDocument();
        } else {
            $this->_owningDocument = $sourcePDF;
        }

        if (! empty($object)) {
            $object->attach($this);
            $this->_value      = $object;
            $this->_objectType = get_class($object);
        }
    }


  /* Object Magic Methods */

    /**
     * Returns the object value as a string. Useful for debugging.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->_objectNumber . ' ' . $this->_generationNumber . ' R'
             . (empty($this->_objectType) ? '' : ' (' . $this->_objectType . ')');
    }


  /* SplObserver Interface Implementation */

    /**
     * Notifies the source PDF document that the direct object has changed,
     * indicating that a new generation of this indirect object must be
     * written to the output destination.
     *
     * This object's observer(s), if any, are never notified of changes to the
     * direct object. This is the benefit of indirect objects: they can be used
     * and referred to freely without observers needing to know anything about
     * their internal state.
     *
     * @param SplSubject $subject
     */
    public function update(SplSubject $subject)
    {
        /* We must keep a reference to the object so we can obtain its current
         * value when RE_Pdf_Document later calls finalize().
         */
        $this->_value = $subject;
        $this->_owningDocument->update($this);
    }


  /* Accessor Methods */

    /**
     * Returns the object number from the source PDF document.
     *
     * @return integer
     */
    public function getObjectNumber()
    {
        return $this->_objectNumber;
    }

    /**
     * Returns the object generation number from the source PDF document.
     *
     * @return integer
     */
    public function getGenerationNumber()
    {
        return $this->_generationNumber;
    }

    /**
     * Returns the source PDF document.
     *
     * @return RE_Pdf_Object_Extraction_Interface
     */
    public function getSourcePDF()
    {
        return $this->_sourcePDF;
    }

    /**
     * Returns the owning PDF document, which may be different from the source
     * document if the indirect object is imported from one PDF to another.
     *
     * @return RE_Pdf_Object_Extraction_Interface
     */
    public function getOwningDocument()
    {
        return $this->_owningDocument;
    }

    /**
     * Proxies to the getValue() method of the direct object.
     *
     * @return RE_Pdf_Object
     */
    public function getValue()
    {
        return $this->getObject()->getValue();
    }

    /**
     * Proxies to the setValue() method of the direct object.
     *
     * @param mixed $value
     */
    public function setValue($value)
    {
        return $this->getObject()->setValue($value);
    }

    /**
     * Returns the direct object.
     *
     * @return RE_Pdf_Object
     */
    public function getObject()
    {
        if (empty($this->_value)) {
            $this->_value = $this->_sourcePDF->extractObject($this->_objectNumber, $this->_generationNumber);
            $this->_value->attach($this);  // watch for changes
            $this->_objectType = get_class($this->_value);
        }
        return $this->_value;
    }

    /**
     * Returns the direct object's type (its RE_Pdf class name).
     *
     * @return string
     */
    public function getObjectType()
    {
        if (empty($this->_objectType)) {
            $object = $this->getObject();
        }
        return $this->_objectType;
    }


  /* PDF Generation */
  
    /**
     * Writes a new generation of the object to the source PDF document.
     */
    public function finalize()
    {
        /* If the direct object has not changed, there is nothing to do.
         */
        if (empty($this->_value)) {
            return;
        }
        
        if ($this->_owningDocument instanceof RE_Pdf_Document) {
            $this->_owningDocument->finalizeObject($this);
            
            /* Once finalized, the new source document is the same as the
             * owning document. This way, if the direct object is extracted
             * again, the correct generation will be returned.
             */
            $this->_sourcePDF = $this->_owningDocument;
        }
    }
    
    /**
     * Called by {@link RE_Pdf_Document} when it is ready to write the new
     * generation of the object to its output destination, usually when the PDF
     * document itself is finalized or in response to a {@link finalize()} call
     * on this indirect object.
     * 
     * This method is called automatically by RE_Pdf_Document; you should
     * never need to call it directly.
     * 
     * @param RE_Pdf_Document $pdf
     */
    public function writeObject(RE_Pdf_Document $pdf)
    {
        $pdf->writeBytes($this->_objectNumber . ' ' . $this->_generationNumber . ' obj');
        $this->_value->write($pdf);
        $pdf->writeBytes("\nendobj\n");

        /* We don't need to keep a reference to this object any longer. If its
         * value changes again, it will call update(), and we'll maintain a
         * reference until the next time finalize() called.
         */
        $this->_value = null;
    }

}
