<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage Core
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * RE_Pdf_Object_String objects represent literal strings used either as
 * content or metadata within the PDF document.
 *
 * @todo Correctly detect and accommodate for UTF-16 BOM.
 * 
 * @package    Pdf
 * @subpackage Core
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Pdf_Object_String extends RE_Pdf_Object
{
  /**** Instance Variables ****/
  
    /**
     * Flag indicating whether to write the value as a binary string.
     * @var boolean
     */
    protected $_writeAsHex = false;
    
    /**
     * Flag indicating whether the string is eligible for encryption. See
     * discussion in {@link setUsesEncryption()}.
     * @var boolean
     */
    protected $_usesEncryption = true;
  
  
  
  /**** Class Methods ****/

    /**
     * Writes the string value directly to the specified output destination.
     * 
     * @param RE_Pdf_OutputDestination_Interface $output
     * @param string                             $value
     */
    public static function writeValue(RE_Pdf_OutputDestination_Interface $output, $value)
    {
        $output->writeBytes('(' . preg_replace_callback(
            '/[\n\r\t\f\x08()\x5c]/', 'RE_Pdf_Object_String::escapeChar', $value
            ) . ')');            
    }

    /**
     * Writes the string value directly to the specified output destination as
     * a hexidecimal string.
     * 
     * @param RE_Pdf_OutputDestination_Interface $output
     * @param string                             $value
     */
    public static function writeHexValue(RE_Pdf_OutputDestination_Interface $output, $value)
    {
        $hex = '';
        for ($i = 0, $len = strlen($value); $i < $len; $i++) {
            $hex .= sprintf('%02x', ord($value[$i]));
        }
        $output->writeBytes('<' . $hex . '>');
    }
  
    /**
     * Helper function for {@link extract()} that unescapes characters from
     * the string. Used with {@link preg_replace_callback}.
     *
     * @param array $matches
     * @return string
     */
    protected static function unescapeChar($matches)
    {
        $escapedChar = substr($matches[0], 1, 1);
        switch ($escapedChar) {
        case 'n':
            return "\n";
        case 'r':
            return "\r";
        case 't':
            return "\t";
        case 'b':
            return "\x08";
        case 'f':
            return "\x0c";
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            return chr(octdec($matches[1]));
        default:  // for all others, the \ is ignored
            return $escapedChar;
        }
    }

    /**
     * Helper function for {@link writeValue()} that escapes certain characters
     * in the string. Used with {@link preg_replace_callback}.
     *
     * @param array $matches
     * @return string
     */
    protected static function escapeChar($matches)
    {
        switch ($matches[0]) {
        case "\n":
            return '\n';
        case "\r":
            return '\r';
        case "\t":
            return '\t';
        case "\x08":
            return '\b';
        case "\x0c":
            return '\f';
        case '(':
            return '\(';
        case ')':
            return '\)';
        case '\\':
            return '\\\\';
        default:
            return $matches[0];
        }
    }



  /**** Public Interface ****/


  /* Concrete Subclass Implementation */

    /**
     * Extracts the string object from the specified file parser at its current
     * read offset. Returns the extracted object and moves the offset past the
     * object and any trailing white space.
     *
     * @param  RE_FileParser_Pdf $parser
     * @return RE_Pdf_Object_String
     * @throws RE_Pdf_Exception if a string object is not present at the
     *   current parser offset.
     */
    public static function extract(RE_FileParser_Pdf $parser)
    {
        $byte = $parser->readBytes(1);

        /* Strings are typically represented as raw binary characters enclosed
         * in balanced parenthesis. Need to check for certain escape sequences.
         */
        if ($byte === '(') {
            $string = '';

            /* Watch the nesting depth of the parenthesis. When we've hit
             * zero, the string is complete.
             */
            $depth = 1;
            while (1) {
                $chunk       = $parser->readBytes(64, false);
                $chunkLength = strlen($chunk);
                if ($chunkLength < 1) {
                    break;
                }
                for ($i = 0; $i < $chunkLength; $i++) {
                    switch ($chunk[$i]) {

                    /* Ignore escaped characters. If the escape sequence starts at
                     * the end of the chunk, back up one byte so it will be processed
                     * in the next iteration.
                     */
                    case '\\':
                        if (++$i == $chunkLength) {
                            $chunk = substr($chunk, 0, -1);
                            $parser->skipBytes(-1);
                        }
                        break;

                    /* Opening of balanced parenthesis.
                     */
                    case '(':
                        ++$depth;
                        break;

                    /* Closing of balanced parenthesis. Stop when the depth reaches zero.
                     */
                    case ')':
                        if (--$depth == 0) {
                            $string .= substr($chunk, 0, $i);
                            $parser->skipBytes($i - $chunkLength + 1);
                            break 3;
                        }
                        break;
                    }
                }
                $string .= $chunk;
            }
            $parser->skipWhiteSpace();

            /* Long lines may be continued by ending with a backslash and
             * end-of-line, which is not part of the string.
             */
            $string = preg_replace('/\\\\(\r\n|\r|\n)/', '', $string);

            /* Any unescaped end-of-line sequences in the string are all
             * interpreted as a single \n.
             */
            $string = preg_replace('/(\r\n|\r|\n)/', '\n', $string);

            /* Handle character escape sequences.
             */
            $string = preg_replace_callback(
                '/\\\\([^\d]|\d{1,3})/', 'RE_Pdf_Object_String::unescapeChar', $string);

            return new self($string);


        /* Strings may also be represented as hexidecimal characters enclosed
         * in angle brackets. This encoding is not as compact, but is easier
         * to parse.
         */
        } else if ($byte === '<') {
            $string = '';

            while (1) {
                $chunk       = $parser->readBytes(64, false);
                $chunkLength = strlen($chunk);
                if ($chunkLength < 1) {
                    break;
                }
                $length = strpos($chunk, '>');
                if ($length === false) {
                    $string .= preg_replace('/\s/', '', $chunk);
                } else {
                    $string .= preg_replace('/\s/', '', substr($chunk, 0, $length));
                    $parser->skipBytes($length - $chunkLength + 1);
                    $parser->skipWhiteSpace();
                    break;
                }
            }
            $length = strlen($string);

            /* The final trailing digit 0 of a hexidecimal string may be
             * omitted. Replace if necessary.
             */
            if (($length % 2) == 1) {
                $string .= '0';
                $length++;
            }

            for ($i = 0; $i < $length; $i += 2) {
                $string[$i / 2] = chr(hexdec(substr($string, $i, 2)));
            }
            return new self(substr($string, 0, ($length / 2)));


        } else {
            throw new RE_Pdf_Exception('Cannot read string object at parser offset '
                                         . sprintf('0x%x', ($parser->getOffset() - 1)));
        }
    }

    /**
     * Writes the string value to the specified output destination.
     *
     * @param RE_Pdf_OutputDestination_Interface $output
     */
    public function write(RE_Pdf_OutputDestination_Interface $output)
    {
        if ($this->_writeAsHex) {
            self::writeHexValue($output, $this->_value);
        } else {
            self::writeValue($output, $this->_value);
        }
    }


  /* Accessor Methods */  

    /**
     * Returns true if the string object will be written as a hexidecimal
     * string, or false if it will be written as an ordinary binary string.
     * 
     * @return boolean
     */
    public function getWriteAsHex()
    {
        return $this->_writeAsHex;
    }

    /**
     * Sets whether or not to write the string value to the output destination
     * as a hexidecimal string or an ordinary binary string. Because the
     * hexidecimal string encoding uses more space in the resulting PDF
     * document, it is used only for a few special cases.
     * 
     * @param boolean $writeAsHex
     */
    public function setWriteAsHex($writeAsHex)
    {
        $this->_writeAsHex = $writeAsHex;
    }
    
    /**
     * Sets whether or not to encrypt the string value when writing to the
     * output destination.
     * 
     * NOTE: This method exists so that certain strings in very specific
     * objects will not be encrypted (such as the file identifier in the
     * document trailer dictionary and certain keys in the document's
     * encryption dictionary). It does NOT enable selective encryption of
     * document content.
     * 
     * This method should be considered internal to the framework; you
     * should never call it directly.
     * 
     * @param boolean $usesEncryption
     */
    public function setUsesEncryption($usesEncryption)
    {
        $this->_usesEncryption = $usesEncryption;
    }
    
}
