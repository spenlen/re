<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage Core
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * RE_Pdf_Object_Array objects are ordered collections of PDF objects, which
 * may include other arrays. Array indices are always numeric.
 *
 * @package    Pdf
 * @subpackage Core
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Pdf_Object_Array extends RE_Pdf_Object implements SplObserver, SeekableIterator, Countable, ArrayAccess
{
  /**** Instance Variables ****/

    /**
     * Current array index for SeekableIterator interface.
     * @var integer
     */
    protected $currentIndex = 0;



  /**** Public Interface ****/


  /* Concrete Subclass Implementation */

    /**
     * Extracts the array object from the specified file parser at its current
     * read offset. Returns the extracted object and moves the offset past the
     * object and any trailing white space.
     *
     * @param  RE_FileParser_Pdf $parser
     * @return RE_Pdf_Object_Array
     * @throws RE_Pdf_Exception if an array object is not present at the
     *   current parser offset.
     */
    public static function extract(RE_FileParser_Pdf $parser)
    {
        if ($parser->readBytes(1) !== '[') {
            throw new RE_Pdf_Exception(sprintf('Cannot read array object at parser offset 0x%x',
                                               ($parser->getOffset() - 1)));
        }
        $parser->skipWhiteSpace();

        $object = new self();
        while (1) {
            if ($parser->peekBytes(1) === ']') {
                $parser->skipBytes(1);
                break;
            }
            $object[] = RE_Pdf_Object::extractObject($parser);
        }
        $parser->skipWhiteSpace();

        return $object;
    }

    /**
     * Writes the array object and its contents to the specified output
     * destination.
     *
     * @param RE_Pdf_OutputDestination_Interface $output
     */
    public function write(RE_Pdf_OutputDestination_Interface $output)
    {
        $output->writeBytes('[');
        foreach ($this->_value as $object) {
            $object->write($output);
        }
        $output->writeBytes(']');
    }


  /* Object Lifecycle */

    /**
     * Creates a new RE_Pdf_Object_Array, optionally setting array's initial
     * contents with the contents of the specified array.
     *
     * @param array $value
     */
    public function __construct($value = null)
    {
        if (! empty($value)) {
            $this->setValue($value);
        } else {
            $this->_value = array();
        }
    }


  /* Object Magic Methods */

    /**
     * Returns the object value as a string. Useful for debugging.
     *
     * @return string
     */
    public function __toString()
    {
        $string = '';
        foreach ($this->_value as $object) {
            $string .= $object . ', ';
        }
        return '[' . substr($string, 0, -2) . ']';
    }


  /* Accessor Methods */

    /**
     * Replaces the array's contents with the contents of the specified array.
     *
     * @param array $value
     * @throws InvalidArgumentException if the value is not an array or if any
     *   of the array values are not {@link RE_Pdf_Object}s.
     */
    public function setValue($value)
    {
        if (! is_array($value)) {
            throw new InvalidArgumentException('Array value must be an array.');
        }

        /* Cleanly unset all existing values before replacing them.
         */
        if (! empty($this->_value)) {
            foreach (array_keys($this->_value) as $key) {
                $this->offsetUnset($key);
            }
        }

        foreach ($value as $key => $object) {
            if ((! is_object($object)) || (! $object instanceof RE_Pdf_Object)) {
                throw new InvalidArgumentException("Value for key '$key' is not a RE_Pdf_Object.");
            }
            /* Slightly faster than $this[] = $object; Must call offsetSet() as
             * subclasses may require specific object types.
             */
            $this->offsetSet(null, $object);
        }
    }


  /* SplObserver Interface Methods */

    /**
     * Called whenever a member object's value has changed. Passes the change
     * notification on to this object's observer(s), if any.
     *
     * @param SplSubject $subject
     */
    public function update(SplSubject $subject)
    {
        $this->notify();
    }


  /* SeekableIterator Interface Methods */

    public function current()
    {
        return $this->_value[$this->currentIndex];
    }

    public function key()
    {
        return $this->currentIndex;
    }

    public function next()
    {
        $this->currentIndex += 1;
    }

    public function rewind()
    {
        $this->currentIndex = 0;
    }

    public function valid()
    {
        return $this->currentIndex < count($this->_value);
    }

    public function seek($index)
    {
        if (($index >= 0) && ($index < count($this->_value))) {
            $this->currentIndex = $index;
        } else {
            throw new OutOfBoundsException('Invalid seek position: ' . $index);
        }
    }


  /* Countable Interface Methods */

    public function count()
    {
        return count($this->_value);
    }


  /* ArrayAccess Interface Methods */

    public function offsetExists($offset)
    {
        return isset($this->_value[$offset]);
    }

    public function offsetGet($offset)
    {
        return $this->_value[$offset];
    }

    public function offsetSet($offset, $value)
    {
        if (! $value instanceof RE_Pdf_Object) {
            throw new InvalidArgumentException('Value must be a RE_Pdf_Object object');
        }
        if ((isset($this->_value[$offset])) && ($this->_value[$offset] === $value)) {
            return;
        }
        if (is_null($offset)) {
            $offset = count($this->_value);
        }
        $this->_value[$offset] = $value;
        $value->attach($this);  // watch for changes
        $this->notify();
    }

    public function offsetUnset($offset)
    {
        $this->_value[$offset]->detach($this);  // stop watching for changes
        unset($this->_value[$offset]);
        $this->notify();
    }

}
