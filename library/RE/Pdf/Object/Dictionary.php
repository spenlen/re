<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage Core
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * RE_Pdf_Object_Dictionary objects are unordered collections of PDF objects.
 * Each dictionary entry is comprised of a key/value pair, where the key is any
 * valid PDF name object.
 *
 * RE_Pdf_Object_Dictionary objects are the fundamental building blocks of a
 * PDF document. Virtually every complex PDF object is a specialized form of
 * dictionary.
 *
 * @package    Pdf
 * @subpackage Core
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Pdf_Object_Dictionary extends RE_Pdf_Object implements SplObserver, Iterator, Countable, ArrayAccess
{
  /**** Instance Variables ****/

    /**
     * Flag used to indicate whether the current iterator position is valid.
     * @var boolean
     */
    protected $_iteratorIsValid = true;



  /**** Public Interface ****/


  /* Concrete Subclass Implementation */

    /**
     * Extracts the dictionary object from the specified file parser at its current
     * read offset. Returns the extracted object and moves the offset past the
     * object and any trailing white space.
     *
     * @param  RE_FileParser_Pdf $parser
     * @return RE_Pdf_Object_Dictionary
     * @throws RE_Pdf_Exception if a dictionary object is not present at the
     *   current parser offset.
     */
    public static function extract(RE_FileParser_Pdf $parser)
    {
        if ($parser->readBytes(2) !== '<<') {
            throw new RE_Pdf_Exception(sprintf('Cannot read dictionary object at parser offset 0x%x',
                                               ($parser->getOffset() - 2)));
        }
        $parser->skipWhiteSpace();

        $values = array();
        while (1) {
            if ($parser->peekBytes(2) === '>>') {
                $parser->skipBytes(2);
                break;
            }

            $key = RE_Pdf_Object_Name::extract($parser);
            $values[$key->getValue()] = RE_Pdf_Object::extractObject($parser);
            $key = null;
        }
        $parser->skipWhiteSpace();

        /* Streams look like regular dictionaries until the stream keyword is
         * encountered. If found, finish parsing for and return a stream object
         * instead.
         */
        if ($parser->peekBytes(6, false) === 'stream') {
            return RE_Pdf_Object_Stream::extractStream($parser, $values);
        }

        /* Check for specialized dictionary types. Return the most appropriate
         * subclass possible.
         */
        if (isset($values['Type'])) {
            switch ($values['Type']->getValue()) {
            case 'Catalog':
                return new RE_Pdf_Document_Catalog($values);
            case 'Pages':
                return new RE_Pdf_Page_Tree($values, $parser);
            case 'Page':
                return new RE_Pdf_Page($values, $parser);
            }
        }
        
        return new self($values);
    }

    /**
     * Writes the dictionary object and its contents to the specified output
     * destination.
     *
     * @param RE_Pdf_OutputDestination_Interface $output
     */
    public function write(RE_Pdf_OutputDestination_Interface $output)
    {
        $output->writeBytes('<<');
        foreach ($this->_value as $key => $object) {
            RE_Pdf_Object_Name::writeValue($output, $key);
            $object->write($output);
        }
        $output->writeBytes('>>');
    }


  /* Object Lifecycle */

    /**
     * Creates a new RE_Pdf_Object_Dictionary, optionally setting the
     * dictionary's initial contents with the contents of the specified
     * associative array.
     *
     * @param array $value (optional)
     */
    public function __construct($value = null)
    {
        if (! empty($value)) {
            $this->setValue($value);
        } else {
            $this->_value = array();
        }
    }


  /* Object Magic Methods */

    /**
     * Returns the object value as a string. Useful for debugging.
     *
     * @return string
     */
    public function __toString()
    {
        $string = '';
        if (! empty($this->_value)) {
            foreach ($this->_value as $key => $object) {
                $string .= $key . ': ' . $object . '; ';
            }
        }
        return '<<' . substr($string, 0, -2) . '>>';
    }


  /* Accessor Methods */

    /**
     * Replaces the dictionary's contents with the contents of the specified
     * associative array.
     *
     * @param array $value
     * @throws InvalidArgumentException if the value is not an array or if any
     *   of the array values are not {@link RE_Pdf_Object}s.
     */
    public function setValue($value)
    {
        if (! is_array($value)) {
            throw new InvalidArgumentException('Dictionary value must be an associative array.');
        }

        /* Cleanly unset all existing values before replacing them.
         */
        if (! empty($this->_value)) {
            foreach (array_keys($this->_value) as $key) {
                $this->offsetUnset($key);
            }
        }

        foreach ($value as $key => $object) {
            if ((! is_object($object)) || (! $object instanceof RE_Pdf_Object)) {
                throw new InvalidArgumentException("Value for key '$key' is not a RE_Pdf_Object.");
            }
            /* Slightly faster than $this[$key] = $object; Must call offsetSet()
             * as subclasses may require specific object types for certain keys.
             */
            $this->offsetSet($key, $object);
        }
    }


  /* SplObserver Interface Methods */

    /**
     * Called whenever a member object's value has changed. Passes the change
     * notification on to this object's observer(s), if any.
     *
     * @param SplSubject $subject
     */
    public function update(SplSubject $subject)
    {
        $this->notify();
    }


  /* Iterator Interface Methods */

    public function current()
    {
        $value = current($this->_value);
        $this->_iteratorIsValid = ($value !== false);
        return $value;
    }

    public function key()
    {
        return key($this->_value);
    }

    public function next()
    {
        $value = next($this->_value);
        $this->_iteratorIsValid = ($value !== false);
    }

    public function rewind()
    {
        $value = reset($this->_value);
        $this->_iteratorIsValid = ($value !== false);
    }

    public function valid()
    {
        return $this->_iteratorIsValid;
    }


  /* Countable Interface Methods */

    public function count()
    {
        return count($this->_value);
    }


  /* ArrayAccess Interface Methods */

    public function offsetExists($offset)
    {
        return isset($this->_value[$offset]);
    }

    public function offsetGet($offset)
    {
        return $this->_value[$offset];
    }

    public function offsetSet($offset, $value)
    {
        if ((is_null($offset)) || (strlen($offset) == 0)) {
            throw new InvalidArgumentException('Dictionary keys must be valid names');
        }
        
        $objectType = $this->getValidTypeForKey($offset);
        if (is_object($value)) {
            if (! $value instanceof $objectType) {
                if ($value instanceof RE_Pdf_Object_Indirect) {
                    if (! $this->indirectObjectPermittedForKey($offset)) {
                        throw new InvalidArgumentException(
                            "Value for key $offset cannot be an indirect object; must be a $objectType object");
                    }
                } else {
                    throw new InvalidArgumentException("Value for key $offset must be a $objectType object");
                }
            }
            if (isset($this->_value[$offset])) {
                $this->offsetUnset($offset);
            }

        } else {
            /* If the value is not an object, attempt to set the new value if
             * the key already exists.
             */
            if (isset($this->_value[$offset])) {
                $this->_value[$offset]->setValue($value);
                return;
            }
            /* Otherwise, create a new object of the appropriate type from the
             * supplied value.
             */
            if ($objectType == 'RE_Pdf_Object') {  // cannot instantiate abstract base class
                throw new InvalidArgumentException('Value must be a RE_Pdf_Object object');
            }
            $value = new $objectType($value);
        }
        
        $this->_value[$offset] = $value;
        $value->attach($this);  // watch for changes
        $this->notify();
    }

    public function offsetUnset($offset)
    {
        if (! isset($this->_value[$offset])) {
            return;
        }
        $this->_value[$offset]->detach($this);  // stop watching for changes
        unset($this->_value[$offset]);
        $this->notify();
    }


  /* Dictionary Key Type Checking */
  
    /**
     * Most dictionaries require specific object types for the various keys.
     * Subclasses should override this method to return the valid class name
     * for the specified dictionary key, calling the parent method for
     * unrecognized keys. The returned value is used by {@link offsetSet()}
     * to do type checking.
     * 
     * @return string
     */
    public function getValidTypeForKey($key)
    {
        /* The only keys which are typed for every kind of dictionary are
         * listed below (see PDF 1.7 Reference, section 3.2.6). For all
         * others, any type of RE_Pdf_Object is allowed.
         */
        switch ($key) {
        case 'Type':
        case 'Subtype':
        case 'S':  // abbreviation for Subtype
            return 'RE_Pdf_Object_Name';
        }
        return 'RE_Pdf_Object';
    }

    /**
     * Indirect object references are allowed anywhere a direct object would
     * otherwise appear with very few exceptions. If a subclass requires a
     * direct object, override this method and return false for the specific
     * keys, calling the parent method for unrecognized keys.
     * 
     * The base implementation accepts indirect objects for any key. The
     * returned value is used by {@link offsetSet()} to do type checking.
     * 
     * @return boolean
     */
    public function indirectObjectPermittedForKey($key)
    {
        return true;
    }

}
