<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage Graphics
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @copyright  Copyright (c) 2005-2008 Zend Technologies USA, Inc. (http://www.zend.com)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */

/* Portions of this source file are excerpted from the Zend Framework:
 * 
 * Copyright (c) 2005-2008, Zend Technologies USA, Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 *  * Neither the name of Zend Technologies USA, Inc. nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


/**
 * Base image class.
 * 
 * Image objects themselves are normally instantiated through the factory
 * methods {@link imageWithPath()} or {@link imageWithBytes()}.
 *
 * @package    Pdf
 * @subpackage Graphics
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @copyright  Copyright (c) 2005-2008 Zend Technologies USA, Inc. (http://www.zend.com)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Pdf_Image extends RE_Pdf_Object_Stream
{
  /**** Abstract Interface ****/

    /**
     * Extracts the specific kind of image from the specified parser object.
     * 
     * NOTE: PHP does not allow for abstract definitions of static methods, so
     * this base class simply throws a BadMethodCallException exception. All
     * concrete subclasses are required to implement this method.
     * 
     * @param RE_FileParser $parser
     * @return RE_Pdf_Image
     * @throws RE_Pdf_Exception if the data source does not contain an image
     *   of the appropriate type or an error occurred.
     */
    public static function extractImage(RE_FileParser $parser)
    {
        throw new BadMethodCallException('extractImage() must be implemented by subclasses!');
    }
    


  /**** Class Methods ****/

    /**
     * Returns an image object by file path.
     *
     * The result of this method is cached, preventing unnecessary duplication
     * of image objects. Repetitive calls for the image with the same path will
     * return the same object.
     * 
     * @param string $filePath Full filesystem path to the image file.
     * @return RE_Pdf_Image
     * @throws RE_Pdf_Exception if the file path is invalid or the image type
     *   cannot be determined.
     */
    public static function imageWithPath($filePath)
    {
        /* Create a file parser data source object for this file. File path and
         * access permission checks are handled here.
         */
        require_once 'RE/FileParser/DataSource/File.php';
        $dataSource = new RE_FileParser_DataSource_File($filePath);

        /* Attempt to determine the type of image. We can't always trust file
         * extensions, but try that first since it's fastest.
         */
        $fileExtension = strtolower(pathinfo($filePath, PATHINFO_EXTENSION));

        /* If it turns out that the file is named improperly and we guess the
         * wrong type, we'll get null instead of an image object.
         */
        switch ($fileExtension) {
            case 'jpg':
            case 'jpe':
            case 'jpeg':
            case 'jp2':    // JPEG 2000
                $image = self::_extractJpegImage($dataSource);
                break;

            case 'png':
                $image = self::_extractPngImage($dataSource);
                break;

            case 'tif':
            case 'tiff':
                $image = self::_extractTiffImage($dataSource);
                break;

            case 'pdf':
            case 'ai':     // Adobe Illustrator 9.0+ file formats are actually PDF
                $image = self::_extractPdfImage($dataSource);
                break;

            default:
                /* Unrecognized extension. Try to determine the type by actually
                 * parsing it below.
                 */
                $image = null;
                break;
        }


        if (is_null($image)) {
            /* There was no match for the file extension or the extension was
             * wrong. Attempt to detect the type of image by actually parsing it.
             * We'll do the checks in order of most likely format to try to
             * reduce the detection time.
             */

            // JPEG
            if ((is_null($image)) &&
                ($fileExtension != 'jpg')  && ($fileExtension != 'jpe') &&
                ($fileExtension != 'jpeg') && ($fileExtension != 'jp2')) {
                    $image = self::_extractJpegImage($dataSource);
            }

            // PNG
            if ((is_null($image)) && ($fileExtension != 'png')) {
                $image = self::_extractPngImage($dataSource);
            }

            // TIFF
            if ((is_null($image)) &&
                ($fileExtension != 'tif') && ($fileExtension != 'tiff')) {
                    $image = self::_extractTiffImage($dataSource);
            }

            // PDF
            if ((is_null($image)) &&
                ($fileExtension != 'pdf') && ($fileExtension != 'ai')) {
                    $image = self::_extractPdfImage($dataSource);
            }
        }


        /* Done with the data source object.
         */
        $dataSource = null;

        if (is_null($image)) {
            throw new RE_Pdf_Exception("Cannot determine image type: $filePath",
                                         RE_Pdf_Exception::CANT_DETERMINE_IMAGE_TYPE);
        }

        return $image;
    }



  /**** Public Interface ****/


  /* Concrete Subclass Implementation */

    /**
     * Extracts the image object from the specified file parser at its current
     * read offset. Returns the extracted object and moves the offset past the
     * the object and any trailing white space.
     *
     * @param  RE_FileParser_Pdf $parser
     * @return RE_Pdf_Image
     * @throws RE_Pdf_Exception if an image object is not present at the
     *   current parser offset.
     */
    public static function extract(RE_FileParser_Pdf $parser)
    {
        $offset = $parser->getOffset();

        $object = RE_Pdf_Object_Dictionary::extract($parser);
        if (! $object instanceof self) {
            throw new RE_Pdf_Exception('Image object not found, found other stream object instead '
                                         . 'at parser offset ' . sprintf('0x%x', ($offset)));
        }

        return $object;
    }


  /* Object Lifecycle */

    /**
     * Object constructor.
     *
     * @param array $value (optional)
     */
    public function __construct($value = null)
    {
        parent::__construct($value);
        $this['Type']    = 'XObject';
        $this['Subtype'] = 'Image';
        
        if (! isset($this['Width'])) {
            $this['Width'] = 0;
        }
        if (! isset($this['Height'])) {
            $this['Height'] = 0;
        }
    }


  /* Accessor Methods */

    /**
     * Returns the width of the image in pixels (samples).
     *
     * @return integer
     */
    public function getPixelWidth()
    {
        return $this['Width']->getValue();
    }

    /**
     * Returns the width of the image in pixels (samples).
     *
     * @return integer
     */
    public function getPixelHeight()
    {
        return $this['Height']->getValue();
    }
    
    /**
     * Returns the image's width and height as an RE_Size object.
     * 
     * @return RE_Size
     */
    public function getSize()
    {
        return new RE_Size($this->getPixelWidth(), $this->getPixelHeight());
    }


  /* Drawing */
  
    /**
     * Draws the image on the canvas inside the specified rectangle.
     * 
     * @param RE_Pdf_Canvas_Interface $canvas
     * @param RE_Rect                 $rect
     * @param boolean                 $scaleToFit (optional)
     *   Proportionally scale the image to fit? Default is false.
     * @param string                  $horizontalAlign (optional)
     *   If scaling the image, the horizontal alignment: left, center, right
     * @param string                  $verticalAlign (optional)
     *   If scaling the image, the vertical alignment: top, middle, bottom
     */
    public function draw(RE_Pdf_Canvas_Interface $canvas, RE_Rect $rect,
                         $scaleToFit = false, $horizontalAlign = 'center', $verticalAlign = 'middle')
    {
        if ($scaleToFit) {
            $imageRect = new RE_Rect(new RE_Point(), $this->getSize());
            $rect = $imageRect->rectByScalingInsideRect($rect, $horizontalAlign, $verticalAlign);
        }
        
        $imageName = $canvas->addResource('XObject', $this);

        $canvas->saveGraphicsState();
        
        RE_Pdf_Object_Numeric::writeValue($canvas, 1);
        RE_Pdf_Object_Numeric::writeValue($canvas, 0);
        RE_Pdf_Object_Numeric::writeValue($canvas, 0);
        RE_Pdf_Object_Numeric::writeValue($canvas, 1);
        RE_Pdf_Object_Numeric::writeValue($canvas, $rect->getX());
        RE_Pdf_Object_Numeric::writeValue($canvas, $rect->getY());
        $canvas->writeBytes(" cm\n");

        RE_Pdf_Object_Numeric::writeValue($canvas, $rect->getWidth());
        RE_Pdf_Object_Numeric::writeValue($canvas, 0);
        RE_Pdf_Object_Numeric::writeValue($canvas, 0);
        RE_Pdf_Object_Numeric::writeValue($canvas, $rect->getHeight());
        RE_Pdf_Object_Numeric::writeValue($canvas, 0);
        RE_Pdf_Object_Numeric::writeValue($canvas, 0);
        $canvas->writeBytes(" cm\n");

        RE_Pdf_Object_Name::writeValue($canvas, $imageName);
        $canvas->writeBytes(" Do\n");
        
        $canvas->restoreGraphicsState();
    }

    
  /* ArrayAccess Interface Methods */

    /* Overridden to enforce specific values and/or types for certain
     * dictionary keys.
     * 
     * @throws InvalidArgumentException
     */
    public function offsetSet($offset, $value)
    {
        parent::offsetSet($offset, $value);
        
        switch ($offset) {
        case 'Type':
            if ($this[$offset]->getValue() !== 'XObject') {
                throw new InvalidArgumentException("Value for key '$offset' must be 'XObject'");
            }
            break;

        case 'Subtype':
        case 'S':
            if (! $this instanceof RE_Pdf_Image_Pdf) {
                if ($this[$offset]->getValue() !== 'Image') {
                    throw new InvalidArgumentException("Value for key '$offset' must be 'Image'");
                }
            }
            break;
            
        case 'ColorSpace':
            if (! (($this[$offset] instanceof RE_Pdf_Object_Name) ||
                   ($this[$offset] instanceof RE_Pdf_Object_Array))) {
                    throw new InvalidArgumentException(
                        "Value for key '$offset' must be a RE_Pdf_Object_Name or RE_Pdf_Object_Array object");
            }
            break;

        case 'Mask':
            if (! (($this[$offset] instanceof RE_Pdf_Object_Indirect) ||  // RE_Pdf_Object_Stream
                   ($this[$offset] instanceof RE_Pdf_Object_Array))) {
                    throw new InvalidArgumentException(
                        "Value for key '$offset' must be a RE_Pdf_Object_Stream or RE_Pdf_Object_Array object");
            }
            break;
        }
    }


  /* Dictionary Key Type Checking */

    /**
     * Returns the valid object type for important keys in this dictionary.
     *
     * @return string
     */
    public function getValidTypeForKey($key)
    {
        switch ($key) {
        case 'Intent':
            return 'RE_Pdf_Object_Name';
            
        case 'Width':
        case 'Height':
        case 'BitsPerComponent':
        case 'SMaskInData':
        case 'StructParent':
            return 'RE_Pdf_Object_Numeric_Integer';
            
        case 'Decode':
        case 'Alternates':
            return 'RE_Pdf_Object_Array';
        
        case 'ID':
            return 'RE_Pdf_Object_String';
            
        case 'OPI':
        case 'OC':
            return 'RE_Pdf_Object_Dictionary';
            
        case 'ImageMask':
        case 'Interpolate':
            return 'RE_Pdf_Object_Boolean';
            
        case 'SMask':
        case 'Metadata':
            return 'RE_Pdf_Object_Indirect';  // RE_Pdf_Object_Stream
            
        }
        return parent::getValidTypeForKey($key);
    }



  /**** Internal Methods ****/


  /* Image Extraction Methods */

    /**
     * Attempts to extract a JPEG image from the data source.
     *
     * If the image parser throws an exception that suggests the data source
     * simply doesn't contain a JPEG image, catches it and returns null. If
     * an exception is thrown that suggests the JPEG image is corrupt or
     * otherwise unusable, throws that exception. If successful, returns the
     * image object.
     *
     * @param RE_FileParser_DataSource_Interface $dataSource
     * @return RE_Pdf_Image_Jpeg Returns null if the data source does not
     *   appear to contain a JPEG image.
     * @throws RE_Pdf_Exception
     */
    protected static function _extractJpegImage(RE_FileParser_DataSource_Interface $dataSource)
    {
        try {
            require_once 'RE/FileParser/Image/Jpeg.php';
            $parser = new RE_FileParser_Image_Jpeg($dataSource);

            require_once 'RE/Pdf/Image/Jpeg.php';
            $image = RE_Pdf_Image_Jpeg::extractImage($parser);

            $parser = null;

        } catch (RE_FileParser_Exception $e) {
            /* The following exception codes suggest that this isn't really a
             * JPEG image. If we caught such an exception, simply return
             * null. For all other cases, it probably is a JPEG image but has
             * a problem; throw the exception again.
             */
            $parser = null;
            switch ($e->getCode()) {
                case RE_FileParser_Exception::NOT_AN_IMAGE_FILE:
                    return null;

                default:
                    throw $e;
            }
        }
        return $image;
    }

    /**
     * Attempts to extract a PNG image from the data source.
     *
     * If the image parser throws an exception that suggests the data source
     * simply doesn't contain a PNG image, catches it and returns null. If
     * an exception is thrown that suggests the PNG image is corrupt or
     * otherwise unusable, throws that exception. If successful, returns the
     * image object.
     *
     * @param RE_FileParser_DataSource_Interface $dataSource
     * @return RE_Pdf_Image_Png Returns null if the data source does not
     *   appear to contain a PNG image.
     * @throws RE_Pdf_Exception
     */
    protected static function _extractPngImage(RE_FileParser_DataSource_Interface $dataSource)
    {
        try {
            require_once 'RE/FileParser/Image/Png.php';
            $parser = new RE_FileParser_Image_Png($dataSource);

            require_once 'RE/Pdf/Image/Png.php';
            $image = RE_Pdf_Image_Png::extractImage($parser);

            $parser = null;

        } catch (RE_FileParser_Exception $e) {
            /* The following exception codes suggest that this isn't really a
             * PNG image. If we caught such an exception, simply return
             * null. For all other cases, it probably is a PNG image but has
             * a problem; throw the exception again.
             */
            $parser = null;
            switch ($e->getCode()) {
                case RE_FileParser_Exception::NOT_AN_IMAGE_FILE:
                    return null;

                default:
                    throw $e;
            }
        }
        return $image;
    }

    /**
     * Attempts to extract a TIFF image from the data source.
     *
     * If the image parser throws an exception that suggests the data source
     * simply doesn't contain a TIFF image, catches it and returns null. If
     * an exception is thrown that suggests the TIFF image is corrupt or
     * otherwise unusable, throws that exception. If successful, returns the
     * image object.
     *
     * @param RE_FileParser_DataSource_Interface $dataSource
     * @return RE_Pdf_Image_Tiff Return null if the data source does not
     *   appear to contain a TIFF image.
     * @throws RE_Pdf_Exception
     */
    protected static function _extractTiffImage(RE_FileParser_DataSource_Interface $dataSource)
    {
        try {
            require_once 'RE/FileParser/Image/Tiff.php';
            $parser = new RE_FileParser_Image_Tiff($dataSource);

            require_once 'RE/Pdf/Image/Tiff.php';
            $image = RE_Pdf_Image_Tiff::extractImage($parser);

            $parser = null;

        } catch (RE_FileParser_Exception $e) {
            /* The following exception codes suggest that this isn't really a
             * TIFF image. If we caught such an exception, simply return
             * null. For all other cases, it probably is a TIFF image but has
             * a problem; throw the exception again.
             * 
             * Many of these exception codes are not very telling for TIFF images
             * because there is not a lot of structure to detect; it is difficult
             * to decide whether it is a corrupted TIFF image or a different kind
             * of image altogether. For our purposes here, assume it's the wrong
             * kind of image.
             */
            $parser = null;
            switch ($e->getCode()) {
                case RE_FileParser_Exception::NOT_AN_IMAGE_FILE:
                case RE_FileParser_Exception::BAD_IMAGE_COUNT:
                case RE_FileParser_Exception::BAD_FIELD_COUNT:
                    return null;

                default:
                    throw $e;
            }
        }
        return $image;
    }

    /**
     * Attempts to extract a PDF image from the data source.
     *
     * If the image parser throws an exception that suggests the data source
     * simply doesn't contain a PDF document, catches it and returns null. If
     * an exception is thrown that suggests the PDF document is corrupt or
     * otherwise unusable, throws that exception. If successful, returns the
     * image object.
     *
     * @param RE_FileParser_DataSource_Interface $dataSource
     * @return RE_Pdf_Image_Pdf Return null if the data source does not
     *   appear to contain a PDF document.
     * @throws RE_Pdf_Exception
     */
    protected static function _extractPdfImage(RE_FileParser_DataSource_Interface $dataSource)
    {
        try {
            require_once 'RE/FileParser/Pdf.php';
            $parser = new RE_FileParser_Pdf($dataSource);

            require_once 'RE/Pdf/Image/Pdf.php';
            $image = RE_Pdf_Image_Pdf::extractImage($parser);

            $parser = null;

        } catch (RE_FileParser_Exception $e) {
            /**
             * @todo Add exception codes to PDF file parser. For now, assume
             *   it is not a PDF document and return null.
             */
            return null;
        }
        return $image;
    }
    
}
