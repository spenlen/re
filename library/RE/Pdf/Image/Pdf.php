<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage Graphics
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * PDF image.
 *
 * @package    Pdf
 * @subpackage Graphics
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Pdf_Image_Pdf extends RE_Pdf_Image
{
  /**** Instance Variables ****/
  
    /**
     * The file parser object containing the source PDF document.
     * @var RE_FileParser_Pdf
     */
    protected $_parser = null;
    
    /**
     * Currently selected page object.
     * @var RE_Pdf_Page
     */
    protected $_page = null;
    
    /**
     * Width of the PDF image.
     * @var float
     */
    protected $_width = 0;
    
    /**
     * Height of the PDF image.
     * @var float
     */
    protected $_height = 0;
  
  
  
  /**** Public Interface ****/
  

  /* Concrete Subclass Implementation */

    /**
     * Creates a PDF image using the specified parser object. Returns the
     * new image object.
     *
     * @param RE_FileParser $parser
     * @return RE_Pdf_Image_Pdf
     * @throws RE_Pdf_Exception if the the parser's data source does not
     *   contain a PDF document or an error occurred.
     */
    public static function extractImage(RE_FileParser $parser)
    {
        $parser->parse();
        
        $image = new self();
        
        $image->_parser = $parser;
        $image->selectPage(1);

        return $image;
    }

    /**
     * Writes the PDF image to the specified output destination, which must
     * be a {@link RE_Pdf_Document} object.
     * 
     * Overridden because we must import the page's resource dictionary and
     * its content streams specially.
     *
     * @param RE_Pdf_OutputDestination_Interface $output
     * @throws InvalidArgumentException if the output destination is not a
     *   {@link RE_Pdf_Document} object.
     */
    public function write(RE_Pdf_OutputDestination_Interface $output)
    {
        if (! $output instanceof RE_Pdf_Document) {
            throw new InvalidArgumentException(
                'Output destination must be a RE_Pdf_Document object; instance of ' . get_class($object) . ' given.');
        }
        
        /* Import the source page's resource dictionary and its contents as
         * a new indirect object.
         */
        $resources = $this->_page['Resources']->getObject();
        $this['Resources'] = $output->addObject($resources);
        $this['Resources']->finalize();

        $contents = $this->_page['Contents']->getObject();
        
        if ($contents instanceof RE_Pdf_Object_Stream) {
            if (isset($contents['Filter'])) {
                $this['Filter'] = $contents['Filter'];
            }
            $this->setDataProvider($contents->getDataProvider());
            
        } else {  // array of streams
            $dataProvider = new RE_Pdf_Object_StreamDataProvider_Buffer();
            foreach ($contents as $stream) {
                $dataProvider->appendBytes($stream->getObject()->getDataProvider()->getAllBytes());
            }
            $this->setDataProvider($dataProvider);
        }

        parent::write($output);
    }


  /* Object Lifecycle */

    /**
     * Object constructor.
     *
     * @param array $value (optional)
     */
    public function __construct($value = null)
    {
        parent::__construct($value);

        $this['Subtype']  = 'Form';
        $this['FormType'] = 1;
        
        unset($this['Width']);
        unset($this['Height']);
    }


  /* Page Selection */

    /**
     * Selects the specified page from the source PDF document.
     * 
     * @param integer $pageNumber
     * @throws RangeException if the page number is out of range.
     */
    public function selectPage($pageNumber)
    {
        $catalog  = $this->_parser->getCatalog();
        $pageTree = $catalog['Pages']->getObject();
        
        $page = $pageTree->getpage($pageNumber);
        if (! $page) {
            throw new RangeException("Invalid page number: $pageNumber");
        }

        $this->_page = $page;
        $this->selectBoundingBox('ArtBox');
    }
    
    /**
     * Specifies which of the page's bounding rectangles should be used to
     * crop the page in creating the image. Valid options:
     *  - MediaBox
     *  - CropBox
     *  - BleedBox
     *  - TrimBox
     *  - ArtBox
     * 
     * If you wish to use a different bounding box, you may specify your
     * own by calling {@link setBBox()} with the desired rectangle.
     * 
     * @param string $boundingBox
     */
    public function selectBoundingBox($boundingBox)
    {
        switch ($boundingBox) {
        case 'MediaBox':
            $this->setBBox($this->_page->getMediaBox());
            return;

        case 'CropBox':
            $this->setBBox($this->_page->getCropBox());
            return;

        case 'BleedBox':
            $this->setBBox($this->_page->getBleedBox());
            return;

        case 'TrimBox':
            $this->setBBox($this->_page->getTrimBox());
            return;

        }
        
        /* Default to 'ArtBox'.
         */
        $this->setBBox($this->_page->getArtBox());
    }
    
    /**
     * Sets the bounding box, defining the region of the page that is used
     * as the image content.
     * 
     * Also adjusts the form matrix to provide predictible scaling results.
     * 
     * @param RE_Rect $rect
     */
    public function setBBox(RE_Rect $rect)
    {
        $this['BBox'] = new RE_Pdf_Object_Rectangle($rect);
        
        $this->_width  = $rect->getWidth();
        $this->_height = $rect->getHeight();
        
        /**
         * @todo Correctly deal with pages that are rotated (that have a Rotate
         *   entry in the page dictionary). The height and width may need to
         *   be inverted and the transformation matrix needs to be calculated
         *   differently.
         */
        
        /* Set the form matrix so that the PDF image effectively has an
         * origin of {0, 0}.
         */
        $mediaBox = $this->_page->getMediaBox();
        $this['Matrix'] = new RE_Pdf_Object_Array(array(
            new RE_Pdf_Object_Numeric(1),
            new RE_Pdf_Object_Numeric(0),
            new RE_Pdf_Object_Numeric(0),
            new RE_Pdf_Object_Numeric(1),
            new RE_Pdf_Object_Numeric(-$rect->getX()),
            new RE_Pdf_Object_Numeric(-$rect->getY())
            ));
    }
    

  /* Accessor Methods */

    /**
     * Convenience method to return the width of the image in pixels.
     *
     * @return number
     */
    public function getPixelWidth()
    {
        return $this->_width;
    }


    /**
     * Convenience method to return the width of the image in pixels.
     *
     * @return number
     */
    public function getPixelHeight()
    {
        return $this->_height;
    }


  /* Drawing */
  
    /**
     * Overridden to adjust how the scale factor is applied to the
     * transformation matrix since PDF images do not use a 1.0 x 1.0
     * coordinate space.
     * 
     * @param RE_Pdf_Canvas_Interface $canvas
     * @param RE_Rect                 $rect
     * @param boolean                 $scaleToFit (optional)
     * @param string                  $horizontalAlign (optional)
     * @param string                  $verticalAlign (optional)
     */
    public function draw(RE_Pdf_Canvas_Interface $canvas, RE_Rect $rect,
                         $scaleToFit = false, $horizontalAlign = 'center', $verticalAlign = 'middle')
    {
        if ($scaleToFit) {
            $imageRect = new RE_Rect(new RE_Point(), $this->getSize());
            $rect = $imageRect->rectByScalingInsideRect($rect, $horizontalAlign, $verticalAlign);
        }
        
        $imageName = $canvas->addResource('XObject', $this);
    
        $canvas->saveGraphicsState();
        
        RE_Pdf_Object_Numeric::writeValue($canvas, 1);
        RE_Pdf_Object_Numeric::writeValue($canvas, 0);
        RE_Pdf_Object_Numeric::writeValue($canvas, 0);
        RE_Pdf_Object_Numeric::writeValue($canvas, 1);
        RE_Pdf_Object_Numeric::writeValue($canvas, $rect->getX());
        RE_Pdf_Object_Numeric::writeValue($canvas, $rect->getY());
        $canvas->writeBytes(" cm\n");
        
        $bbox = $this['BBox']->getValue();
        RE_Pdf_Object_Numeric::writeValue($canvas, $rect->getWidth() / $bbox->getWidth());
        RE_Pdf_Object_Numeric::writeValue($canvas, 0);
        RE_Pdf_Object_Numeric::writeValue($canvas, 0);
        RE_Pdf_Object_Numeric::writeValue($canvas, $rect->getHeight() / $bbox->getHeight());
        RE_Pdf_Object_Numeric::writeValue($canvas, 0);
        RE_Pdf_Object_Numeric::writeValue($canvas, 0);
        $canvas->writeBytes(" cm\n");
    
        RE_Pdf_Object_Name::writeValue($canvas, $imageName);
        $canvas->writeBytes(" Do\n");
        
        $canvas->restoreGraphicsState();
    }

    
  /* ArrayAccess Interface Methods */

    /* Overridden to enforce specific values and/or types for certain
     * dictionary keys.
     * 
     * @throws InvalidArgumentException
     */
    public function offsetSet($offset, $value)
    {
        parent::offsetSet($offset, $value);
        
        switch ($offset) {
        case 'FormType':
            if ($this[$offset]->getValue() !== 1) {
                throw new InvalidArgumentException("Value for key '$offset' must be 1");
            }
            break;

        }
    }


  /* Dictionary Key Type Checking */

    /**
     * Returns the valid object type for important keys in this dictionary.
     *
     * @return string
     */
    public function getValidTypeForKey($key)
    {
        switch ($key) {
        case 'FormType':
        case 'StructParent':
        case 'StructParents':
            return 'RE_Pdf_Object_Numeric_Integer';
            
        case 'Matrix':
            return 'RE_Pdf_Object_Array';
        
        case 'Resources':
        case 'Group':
        case 'Ref':
        case 'PieceInfo':
            return 'RE_Pdf_Object_Dictionary';
            
        case 'BBox':
            return 'RE_Pdf_Object_Rectangle';
            
        }
        return parent::getValidTypeForKey($key);
    }

}
