<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage Graphics
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @copyright  Copyright (c) 2005-2008 Zend Technologies USA, Inc. (http://www.zend.com)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */

/* Portions of this source file are excerpted from the Zend Framework:
 * 
 * Copyright (c) 2005-2008, Zend Technologies USA, Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 *  * Neither the name of Zend Technologies USA, Inc. nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


/**
 * JPEG image
 *
 * @package    Pdf
 * @subpackage Graphics
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @copyright  Copyright (c) 2005-2008 Zend Technologies USA, Inc. (http://www.zend.com)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Pdf_Image_Jpeg extends RE_Pdf_Image
{
  /**** Public Interface ****/
  

  /* Concrete Subclass Implementation */

    /**
     * Extracts the JPEG image using the specified parser object. Returns the
     * extracted image object.
     *
     * @param RE_FileParser $parser
     * @return RE_Pdf_Image_Jpeg
     * @throws RE_Pdf_Exception if the the parser's data source does not
     *   contain a JPEG image or an error occurred.
     */
    public static function extractImage(RE_FileParser $parser)
    {
        $parser->parse();
        
        $image = new self();
        
        $image['Width']            = $parser->getPixelWidth();
        $image['Height']           = $parser->getPixelHeight();
        $image['ColorSpace']       = new RE_Pdf_Object_Name($parser->getColorSpace());
        $image['BitsPerComponent'] = $parser->getBitsPerComponent();

        if ($parser->getImageType() == IMAGETYPE_JPEG2000) {
            $image['Filter'] = new RE_Pdf_Object_Name('JPXDecode');
        } else {  // IMAGETYPE_JPEG
            $image['Filter'] = new RE_Pdf_Object_Name('DCTDecode');
        }
        
        $dataProvider = new RE_Pdf_Object_StreamDataProvider_FileParser(
            $parser, 0, $parser->getSize(), new RE_Pdf_Object_Array(array($image['Filter'])));

        $image->setDataProvider($dataProvider);

        return $image;
    }

}
