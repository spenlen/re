<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage Graphics
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @copyright  Copyright (c) 2005-2008 Zend Technologies USA, Inc. (http://www.zend.com)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */

/* Portions of this source file are excerpted from the Zend Framework:
 * 
 * Copyright (c) 2005-2008, Zend Technologies USA, Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 *  * Neither the name of Zend Technologies USA, Inc. nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


/**
 * PNG image
 *
 * @package    Pdf
 * @subpackage Graphics
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @copyright  Copyright (c) 2005-2008 Zend Technologies USA, Inc. (http://www.zend.com)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Pdf_Image_Png extends RE_Pdf_Image
{
  /**** Instance Variables ****/
  
    /**
     * The file parser object containing the source PNG image.
     * @var RE_FileParser_Image_Png
     */
    protected $_parser = null;
    
    /**
     * Stream object containing the shadow mask for the image's alpha channel.
     * @var RE_Pdf_Object_Stream
     */
    protected $_smaskStream = null;
  
  
  
  /**** Public Interface ****/
  

  /* Concrete Subclass Implementation */

    /**
     * Extracts the PNG image using the specified parser object. Returns the
     * extracted image object.
     *
     * @param RE_FileParser $parser
     * @return RE_Pdf_Image_Png
     * @throws RE_Pdf_Exception if the the parser's data source does not
     *   contain a PNG image or an error occurred.
     */
    public static function extractImage(RE_FileParser $parser)
    {
        $parser->parse();
        
        $image = new self();
        $image->_parser = $parser;
        
        $image->_createImageDictionary();
        
        return $image;
    }

    /**
     * Writes the PNG image to the specified output destination, which must
     * be a {@link RE_Pdf_Document} object.
     * 
     * Overridden because we may need to include an additional shadow mask
     * stream in the destination file.
     * 
     * @param RE_Pdf_OutputDestination_Interface $output
     * @throws InvalidArgumentException if the output destination is not a
     *   {@link RE_Pdf_Document} object.
     */
    public function write(RE_Pdf_OutputDestination_Interface $output)
    {
        if (! $output instanceof RE_Pdf_Document) {
            throw new InvalidArgumentException(
                'Output destination must be a RE_Pdf_Document object; instance of ' . get_class($object) . ' given.');
        }

        if (! is_null($this->_smaskStream)) {
            $smaskObject = $output->addObject($this->_smaskStream);
            $smaskObject->finalize();
            $this['SMask'] = $smaskObject;
        }
        
        parent::write($output);
    }
    
    

  /**** Internal Interface ****/
  
    /**
     * Creates or replaces the image dictionary based on the currently selected
     * image. Throws an exception if the image selected is incompatible.
     * 
     * @todo Replace the buffer data providers with fileparser providers where possible.
     * @todo Recompress image data streams.
     *
     * @throws RE_Pdf_Exception
     */
    protected function _createImageDictionary()
    {
        $this['Width']            = $this->_parser->getPixelWidth();
        $this['Height']           = $this->_parser->getPixelHeight();
        $this['BitsPerComponent'] = $this->_parser->getBitDepth();
        $this['Filter']           = new RE_Pdf_Object_Name('FlateDecode');

        $this['DecodeParms'] = new RE_Pdf_Object_Dictionary(array(
            'Predictor'        => new RE_Pdf_Object_Numeric(15),
            'Columns'          => new RE_Pdf_Object_Numeric($this->_parser->getPixelWidth()),
            'BitsPerComponent' => new RE_Pdf_Object_Numeric($this->_parser->getBitDepth())
            ));
        
        
        if ($this->_parser->getInterlaceMethod() !== RE_FileParser_Image_Png::INTERLACE_METHOD_NONE) {
            throw new RE_Pdf_Exception(
                'Interlaced PNG images are currently unsupported.',
                RE_Pdf_Exception::NOT_IMPLEMENTED);
        }

        $compressionMethod = $this->_parser->getCompressionMethod();
        if ($compressionMethod !== RE_FileParser_Image_Png::COMPRESSION_METHOD_DEFLATE) {
            throw new RE_Pdf_Exception(
                'Unsupported image compression method: ' . $compressionMethod,
                RE_Pdf_Exception::NOT_IMPLEMENTED);
        }

        $imageFilter = new RE_Pdf_Object_Name('FlateDecode');
        $imageData   = $this->_parser->getImageData();
        
        
        /* Image color space.
         */
        $colorType = $this->_parser->getColorType();
        switch ($colorType) {
        
        case RE_FileParser_Image_Png::COLOR_TYPE_GRAYSCALE:
        case RE_FileParser_Image_Png::COLOR_TYPE_GRAYSCALE_ALPHA:
            $this['ColorSpace'] = new RE_Pdf_Object_Name('DeviceGray');
            $this['DecodeParms']['Colors'] = new RE_Pdf_Object_Numeric(1);
            break;


        case RE_FileParser_Image_Png::COLOR_TYPE_TRUECOLOR:
        case RE_FileParser_Image_Png::COLOR_TYPE_TRUECOLOR_ALPHA:
            $this['ColorSpace'] = new RE_Pdf_Object_Name('DeviceRGB');
            $this['DecodeParms']['Colors'] = new RE_Pdf_Object_Numeric(3);
            break;
        
        
        case RE_FileParser_Image_Png::COLOR_TYPE_INDEXED:
            $paletteData = $this->_parser->getPaletteData();
            if (empty($paletteData)) {
                throw new RE_Pdf_Exception('Image color palette data is missing',
                                           RE_Pdf_Exception::IMAGE_FILE_CORRUPT);
            }
            /**
             * @todo Make the palette string an indirect object.
             */
            $paletteString = new RE_Pdf_Object_String($paletteData);
            $paletteString->setWriteAsHex(true);
            $this['ColorSpace'] = new RE_Pdf_Object_Array(array(
                new RE_Pdf_Object_Name('Indexed'),
                new RE_Pdf_Object_Name('DeviceRGB'),
                new RE_Pdf_Object_Numeric((strlen($paletteData) / 3) - 1),
                $paletteString
                ));
            $this['DecodeParms']['Colors'] = new RE_Pdf_Object_Numeric(1);
            break;
        
        
        default:
            throw new RE_Pdf_Exception('Image color space was not recognized',
                                       RE_Pdf_Exception::IMAGE_FILE_CORRUPT);
        }
        
        
        /* Image transparency.
         */
        if (($colorType == RE_FileParser_Image_Png::COLOR_TYPE_GRAYSCALE) ||
            ($colorType == RE_FileParser_Image_Png::COLOR_TYPE_TRUECOLOR) ||
            ($colorType == RE_FileParser_Image_Png::COLOR_TYPE_INDEXED)) {
                /* If there is transparency, it is specified in a tRNS block.
                 */
                $transparency = $this->_parser->getTransparency();
                if (strlen($transparency) > 0) {
                    switch ($colorType) {
                    case RE_FileParser_Image_Png::COLOR_TYPE_GRAYSCALE:
                        $this['Mask'] = new RE_Pdf_Object_Array(array(
                            new RE_Pdf_Object_Numeric(ord($transparency[1])),
                            new RE_Pdf_Object_Numeric(ord($transparency[1]))
                            ));
                        break;

                    case RE_FileParser_Image_Png::COLOR_TYPE_TRUECOLOR:
                        $this['Mask'] = new RE_Pdf_Object_Array(array(
                            new RE_Pdf_Object_Numeric(ord($transparency[1])),
                            new RE_Pdf_Object_Numeric(ord($transparency[1])),
                            new RE_Pdf_Object_Numeric(ord($transparency[3])),
                            new RE_Pdf_Object_Numeric(ord($transparency[3])),
                            new RE_Pdf_Object_Numeric(ord($transparency[5])),
                            new RE_Pdf_Object_Numeric(ord($transparency[5]))
                            ));
                        break;

                    case RE_FileParser_Image_Png::COLOR_TYPE_INDEXED:
                        /* Find the first transparent color in the index, we will
                         * mask that. (This is a bit of a hack. This should be a
                         * SMask and mask all entries values).
                         */
                        /** @todo Create SMask here instead. */
                        if (($trnsIdx = strpos($transparency, chr(0))) !== false) {
                            $this['Mask'] = new RE_Pdf_Object_Array(array(
                                new RE_Pdf_Object_Numeric($trnsIdx),
                                new RE_Pdf_Object_Numeric($trnsIdx)
                                ));
                        }
                        break;
                    }
                }

        } else {  // *_ALPHA
            /* The image data includes alpha values which must be extracted. We
             * will be left with two images: the original without the alpha values
             * and a new alpha-only image which will be used as the shadow mask.
             */
            if ($this->_parser->getBitDepth() > 8) {
                throw new RE_Pdf_Exception(
                    $this->_parser->getBitDepth() . ' bit images with alpha channel are currently unsupported',
                    RE_Pdf_Exception::NOT_IMPLEMENTED);
            }

            unset($imageFilter);
            unset($this['DecodeParms']);

            $decodeParams = array(
                'Predictor'        => 15,
                'Columns'          => $this->_parser->getPixelWidth(),
                'BitsPerComponent' => $this->_parser->getBitDepth()
                );
            if ($colorType == RE_FileParser_Image_Png::COLOR_TYPE_GRAYSCALE_ALPHA) {
                $decodeParams['Colors'] = 2;
            } else {
                $decodeParams['Colors'] = 4;
            }
            
            $data    = RE_Pdf_Filter::decode('FlateDecode', $imageData, $decodeParams);
            $samples = strlen($data);

            $imageData = '';
            $smaskData = '';
            if ($colorType == RE_FileParser_Image_Png::COLOR_TYPE_GRAYSCALE_ALPHA) {
                for ($i = 0; $i < $samples;) {
                    $imageData .= $data[$i++];
                    $smaskData .= $data[$i++];
                }
            } else {
                for ($i = 0; $i < $samples;) {
                    $imageData .= $data[$i++] . $data[$i++] . $data[$i++];
                    $smaskData .= $data[$i++];
                }
            }
            
            $this->_smaskStream = new RE_Pdf_Object_Stream(array(
                'Type'             => new RE_Pdf_Object_Name('XObject'),
                'Subtype'          => new RE_Pdf_Object_Name('Image'),
                'Width'            => new RE_Pdf_Object_Numeric($this->_parser->getPixelWidth()),
                'Height'           => new RE_Pdf_Object_Numeric($this->_parser->getPixelHeight()),
                'ColorSpace'       => new RE_Pdf_Object_Name('DeviceGray'),
                'BitsPerComponent' => new RE_Pdf_Object_Numeric($this->_parser->getBitDepth())
                ));
            $this->_smaskStream->setDataProvider(
                new RE_Pdf_Object_StreamDataProvider_Buffer($smaskData, new RE_Pdf_Object_Array(array()))
                );
        }


        if (isset($imageFilter)) {
            $dataProvider = new RE_Pdf_Object_StreamDataProvider_Buffer(
                $imageData, new RE_Pdf_Object_Array(array($imageFilter)));
        } else {
            $dataProvider = new RE_Pdf_Object_StreamDataProvider_Buffer(
                $imageData, new RE_Pdf_Object_Array(array()));            
        }

        $this->setDataProvider($dataProvider);
    }

}
