<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage Graphics
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @copyright  Copyright (c) 2005-2008 Zend Technologies USA, Inc. (http://www.zend.com)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */

/* Portions of this source file are excerpted from the Zend Framework:
 * 
 * Copyright (c) 2005-2008, Zend Technologies USA, Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 *  * Neither the name of Zend Technologies USA, Inc. nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


/**
 * TIFF image
 *
 * @package    Pdf
 * @subpackage Graphics
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @copyright  Copyright (c) 2005-2008 Zend Technologies USA, Inc. (http://www.zend.com)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Pdf_Image_Tiff extends RE_Pdf_Image
{
  /**** Instance Variables ****/
  
    /**
     * The file parser object containing the source TIFF image.
     * @var RE_FileParser_Image_Tiff
     */
    protected $_parser = null;
  
  
  
  /**** Public Interface ****/
  

  /* Concrete Subclass Implementation */

    /**
     * Extracts the TIFF image using the specified parser object. Returns the
     * extracted image object.
     *
     * @param RE_FileParser $parser
     * @return RE_Pdf_Image_Tiff
     * @throws RE_Pdf_Exception if the the parser's data source does not
     *   contain a TIFF image or an error occurred.
     */
    public static function extractImage(RE_FileParser $parser)
    {
        $parser->parse();
        
        $image = new self();
        
        $image->_parser = $parser;
        $image->selectImage(1);
        
        return $image;
    }
    
    
  /* Image Selection */

    /**
     * Returns the number of top-level images present in the TIFF file.
     * 
     * @return integer
     */
    public function getImageCount()
    {
        return $this->_parser->getImageCount();
    }
    
    /**
     * Selects the specified top-level image in the TIFF file.
     * 
     * @param integer $imageNumber
     * @throws RangeException if the image number is out of range.
     */
    public function selectImage($imageNumber)
    {
        $this->_parser->selectImage($imageNumber);
        $this->_createImageDictionary();
    }
    
    /**
     * Returns the number of child images (alternative resolutions, thumbnails,
     * etc.) available for the currently selected image or zero if the currently
     * selected image has no children.
     * 
     * @return integer
     */
    public function getChildImageCount()
    {
        return $this->_parser->getChildImageCount();
    }

    /**
     * Selects the specified child image for the currently selected image.
     * 
     * @throws RangeException if the child image number is out of range or the
     *   currently selected image has no children.
     */
    public function selectChildImage($imageNumber)
    {
        $this->_parser->selectChildImage($imageNumber);
        $this->_createImageDictionary();
    }
    
    /**
     * Selects the next image in the TIFF file. If the currently selected image
     * is a child, selects the next sibling. Returns true if a new image was
     * selected or false if there are no additional images.
     * 
     * @return boolean
     */
    public function selectNextImage()
    {
        if (! $this->_parser->selectNextImage()) {
            return false;
        }
        $this->_createImageDictionary();
        return true;
    }

        
  /* Drawing */
  
    /**
     * Draws the image on the canvas inside the specified rectangle.
     * 
     * Overridden to apply the image's clipping path if one is present.
     * 
     * @param RE_Pdf_Canvas_Interface $canvas
     * @param RE_Rect                 $rect
     * @param boolean                 $scaleToFit (optional)
     *   Proportionally scale the image to fit? Default is false.
     * @param string                  $horizontalAlign (optional)
     *   If scaling the image, the horizontal alignment: left, center, right
     * @param string                  $verticalAlign (optional)
     *   If scaling the image, the vertical alignment: top, middle, bottom
     */
    public function draw(RE_Pdf_Canvas_Interface $canvas, RE_Rect $rect,
                         $scaleToFit = false, $horizontalAlign = 'center', $verticalAlign = 'middle')
    {
        if ($scaleToFit) {
            $imageRect = new RE_Rect(new RE_Point(), $this->getSize());
            $rect = $imageRect->rectByScalingInsideRect($rect, $horizontalAlign, $verticalAlign);
        }
        
        $imageName = $canvas->addResource('XObject', $this);

        $canvas->saveGraphicsState();
        
        RE_Pdf_Object_Numeric::writeValue($canvas, 1);
        RE_Pdf_Object_Numeric::writeValue($canvas, 0);
        RE_Pdf_Object_Numeric::writeValue($canvas, 0);
        RE_Pdf_Object_Numeric::writeValue($canvas, 1);
        RE_Pdf_Object_Numeric::writeValue($canvas, $rect->getX());
        RE_Pdf_Object_Numeric::writeValue($canvas, $rect->getY());
        $canvas->writeBytes(" cm\n");

        RE_Pdf_Object_Numeric::writeValue($canvas, $rect->getWidth());
        RE_Pdf_Object_Numeric::writeValue($canvas, 0);
        RE_Pdf_Object_Numeric::writeValue($canvas, 0);
        RE_Pdf_Object_Numeric::writeValue($canvas, $rect->getHeight());
        RE_Pdf_Object_Numeric::writeValue($canvas, 0);
        RE_Pdf_Object_Numeric::writeValue($canvas, 0);
        $canvas->writeBytes(" cm\n");

        $clippingPath = $this->_parser->getClippingPath();
        if ($clippingPath) {
            $canvas->saveGraphicsState();

            $clippingPath->clip($canvas);

            RE_Pdf_Object_Name::writeValue($canvas, $imageName);
            $canvas->writeBytes(" Do\n");
            
            $canvas->restoreGraphicsState();

        } else {
            RE_Pdf_Object_Name::writeValue($canvas, $imageName);
            $canvas->writeBytes(" Do\n");
        }
        
        $canvas->restoreGraphicsState();
    }
  

  /**** Internal Interface ****/
  
    /**
     * Creates or replaces the image dictionary based on the currently selected
     * image. Throws an exception if the image selected is incompatible.
     * 
     * @throws RE_Pdf_Exception
     */
    protected function _createImageDictionary()
    {
        $this['Width']            = $this->_parser->getPixelWidth();
        $this['Height']           = $this->_parser->getPixelHeight();
        $this['BitsPerComponent'] = $this->_parser->getBitsPerComponent();

        unset($this['Decode']);
        
        
        /* Image color space.
         */
        switch ($this->_parser->getColorSpace()) {
        
        case RE_FileParser_Image_Tiff::COLOR_SPACE_WHITE_IS_ZERO:
            $this['ColorSpace'] = new RE_Pdf_Object_Name('DeviceGray');
            $this['Decode']     = new RE_Pdf_Object_Array(array(new RE_Pdf_Object_Numeric(1),
                                                                new RE_Pdf_Object_Numeric(0)));
            break;


        case RE_FileParser_Image_Tiff::COLOR_SPACE_BLACK_IS_ZERO:
            $this['ColorSpace'] = new RE_Pdf_Object_Name('DeviceGray');
            break;
        
        
        case RE_FileParser_Image_Tiff::COLOR_SPACE_RGB:
            /**
             * @todo Check samples per pixel to look for RGBA images. May need
             *   to strip the alpha component before including in the PDF document.
             */
            $this['ColorSpace'] = new RE_Pdf_Object_Name('DeviceRGB');
            break;
        
        
        case RE_FileParser_Image_Tiff::COLOR_SPACE_SEPARATED:
            /* This usually means CMYK. Check the samples per pixel to be sure.
             */
            if ($this->_parser->getFieldValue(RE_FileParser_Image_Tiff::FIELD_SAMPLES_PER_PIXEL) != 4) {
                throw new RE_Pdf_Exception(
                    'Separated color spaces except CMYK are currently unsupported',
                     RE_Pdf_Exception::NOT_IMPLEMENTED);
            }
            $this['ColorSpace'] = new RE_Pdf_Object_Name('DeviceCMYK');
            break;
        
        
        case RE_FileParser_Image_Tiff::COLOR_SPACE_RGB_PALETTE:
            // need to create additional supporting objects for indexed color...
        case RE_FileParser_Image_Tiff::COLOR_SPACE_CIELAB:
            // need to create additional supporting objects for L*a*b color...
        case RE_FileParser_Image_Tiff::COLOR_SPACE_YCBCR:
            // need to convert Y-Cb-Cr samples to RGB samples...
        case RE_FileParser_Image_Tiff::COLOR_SPACE_ICCLAB:
            // need to embed ICC profile...
            throw new RE_Pdf_Exception('Image color space is currently unsupported',
                                         RE_Pdf_Exception::NOT_IMPLEMENTED);
        
        
        case RE_FileParser_Image_Tiff::COLOR_SPACE_TRANSPARENCY_MASK:
            throw new RE_Pdf_Exception('Transparency mask color space is unsupported',
                                         RE_Pdf_Exception::NOT_IMPLEMENTED);

        default:
            throw new RE_Pdf_Exception('Image color space was not recognized',
                                         RE_Pdf_Exception::IMAGE_FILE_CORRUPT);
        }
        
        
        /* Raw image data compression scheme.
         */
        switch ($this->_parser->getFieldValue(RE_FileParser_Image_Tiff::FIELD_COMPRESSION)) {

        case RE_FileParser_Image_Tiff::COMPRESSION_UNCOMPRESSED:
            unset($this['Filter']);
            break;

        case RE_FileParser_Image_Tiff::COMPRESSION_PACKBITS:
            $this['Filter'] = new RE_Pdf_Object_Name('RunLengthDecode');
            break;

        case RE_FileParser_Image_Tiff::COMPRESSION_CCITT:
        case RE_FileParser_Image_Tiff::COMPRESSION_CCITT_T4:
        case RE_FileParser_Image_Tiff::COMPRESSION_CCITT_T6:
        case RE_FileParser_Image_Tiff::COMPRESSION_LZW:
        case RE_FileParser_Image_Tiff::COMPRESSION_JPEG:
        case RE_FileParser_Image_Tiff::COMPRESSION_DEFLATE:
        case RE_FileParser_Image_Tiff::COMPRESSION_DEFLATE_OBSOLETE:
            throw new RE_Pdf_Exception('Image compression scheme is currently unsupported',
                                         RE_Pdf_Exception::NOT_IMPLEMENTED);
        default:
            throw new RE_Pdf_Exception('Image compression scheme was not recognized',
                                         RE_Pdf_Exception::IMAGE_FILE_CORRUPT);
        }


        /**
         * @todo Replace the buffer data provider with a fileparser provider.
         */
        if (isset($this['Filter'])) {
            $dataProvider = new RE_Pdf_Object_StreamDataProvider_Buffer(
                null, new RE_Pdf_Object_Array(array($this['Filter'])));
        } else {
            $dataProvider = new RE_Pdf_Object_StreamDataProvider_Buffer(
                null, new RE_Pdf_Object_Array(array()));            
        }
        
        /**
         * @todo Check for tiled image data. If found, decode it and reassemble
         *   into strips for the PDF doc.
         */
        
        $imageDataOffsets = $this->_parser->getFieldValue(RE_FileParser_Image_Tiff::FIELD_STRIP_OFFSETS);
        $imageDataLengths = $this->_parser->getFieldValue(RE_FileParser_Image_Tiff::FIELD_STRIP_BYTE_COUNTS);
        
        if (count($imageDataOffsets) != count($imageDataLengths)) {
            throw new RE_Pdf_Exception('Image data offset and length arrays do not agree',
                                         RE_Pdf_Exception::IMAGE_FILE_CORRUPT);
        }
        
        if (count($imageDataOffsets) == 1) {
            $this->_parser->moveToOffset($imageDataOffsets);
            $dataProvider->setBytes($this->_parser->readBytes($imageDataLengths));            
        } else {
            foreach ($imageDataOffsets as $key => $offset) {
                $this->_parser->moveToOffset($offset);
                $dataProvider->appendBytes($this->_parser->readBytes($imageDataLengths[$key]));            
            }
        }

        $this->setDataProvider($dataProvider);
    }

}
