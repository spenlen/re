<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage DocumentStructure
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * RE_Pdf_OutputDestination_Interface defines the interface which must be
 * implemented in order to be used as an output destination for any kind of
 * {@link RE_Pdf_Object} subclass via its {@link RE_Pdf_Object::write()}
 * method call.
 *
 * @package    Pdf
 * @subpackage DocumentStructure
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
interface RE_Pdf_OutputDestination_Interface
{
    /**
     * Writes the specified bytes to the output destination.
     *
     * @param string $bytes
     * @throws RE_Pdf_Exception if an error occurs while attempting to write
     *   to the output destination.
     */
    public function writeBytes($bytes);

    /**
     * Writes the entire source file to the output destination.
     *
     * @param string|resource $file Filesystem path or file pointer resource.
     * @throws InvalidArgumentException if the file is not a valid filesystem
     *   path or file pointer resource.
     * @throws RE_Pdf_Exception if an error occurs while attempting to copy
     *   the source file to the output destination.
     */
    public function writeFile($file);

}
