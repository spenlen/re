<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage DocumentStructure
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * RE_Pdf_Page_Tree...
 * 
 * @package    Pdf
 * @subpackage DocumentStructure
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Pdf_Page_Tree extends RE_Pdf_Object_Dictionary
{
  /**** Instance Variables ****/
  
    /**
     * Source PDF document; page trees belong to exactly one document.
     * @var RE_Pdf_Object_Extraction_Interface
     */
    protected $_sourcePDF = null;
    
    /**
     * The indirect object enclosing this object in the source PDF document.
     * @var RE_Pdf_Object_Indirect
     */
    protected $_indirectObject = null;

  
  
  /**** Public Interface ****/


  /* Concrete Subclass Implementation */

    /**
     * Extracts the page tree object from the specified file parser at
     * its current read offset. Returns the extracted object and moves the
     * offset past the object and any trailing white space.
     *
     * @param  RE_FileParser_Pdf $parser
     * @return RE_Pdf_Page_Tree
     * @throws RE_Pdf_Exception if a page tree object is not present
     *   at the current parser offset.
     */
    public static function extract(RE_FileParser_Pdf $parser)
    {
        $offset = $parser->getOffset();

        $object = RE_Pdf_Object_Dictionary::extract($parser);
        if (! $object instanceof self) {
            throw new RE_Pdf_Exception('Page tree object not found, found other dictionary object instead '
                                         . 'at parser offset ' . sprintf('0x%x', ($offset)));
        }

        return $object;
    }


  /* Object Lifecycle */

    /**
     * Creates a new RE_Pdf_Document_Catalog, setting the source PDF.
     * Optionally sets the dictionary's initial contents with the contents
     * of the specified associative array.
     *
     * @param array $value (optional)
     * @param RE_Pdf_Object_Extraction_Interface $sourcePDF
     */
    public function __construct($value = null, RE_Pdf_Object_Extraction_Interface $sourcePDF)
    {
        parent::__construct($value);
        $this['Type'] = 'Pages';
        
        if (! isset($this['Kids'])) {
            $this['Kids'] = new RE_Pdf_Object_Array();
        }

        if (! isset($this['Count'])) {
            $this['Count'] = new RE_Pdf_Object_Numeric(0);
        }
        
        if ($sourcePDF instanceof RE_FileParser_Pdf) {
            $this->_sourcePDF = $sourcePDF->getOwningDocument();
        } else {
            $this->_sourcePDF = $sourcePDF;
        }
    }


  /* Page Management */
  
    /**
     * Creates and returns a new page object, appending it to the end of the
     * page tree.
     * 
     * @return RE_Pdf_Page
     * @throws BadMethodCallException if {@link setIndirectObject()} has not
     *   been called yet.
     */
    public function newPage()
    {
        $page = new RE_Pdf_Page(null, $this->_sourcePDF);
        
        if (empty($this->_indirectObject)) {
            throw new BadMethodCallException('This page tree node is not properly initialized.');
        }
        $page['Parent'] = $this->_indirectObject;
        
        /** @todo Maintain B-tree for new pages. */
        $this['Kids']->getObject()->offsetSet(null, $this->_sourcePDF->addObject($page));
        
        /** @todo Also increment count for parent tree nodes. */
        $this['Count']->setValue($this['Count']->getValue() + 1);
        
        return $page;
    }
    
    /**
     * Returns the specified numbered page (ordinal page number starting at 1)
     * from the page tree or null if the page does not exist.
     * 
     * NOTE: There is no concept of absolute page numbers in a PDF document. If
     * you are looking for page 65 in the document itself, make sure you call
     * this method on the document's root page tree node, otherwise, you will
     * get the 65th page in this subtree only. In most cases, you will want to
     * use {@link RE_Pdf_Document::getPage()} instead of calling this method
     * directly.
     * 
     * @param integer $pageNumber
     * @return RE_Pdf_Page Returns null if the page number does not exist.
     */
    public function getPage($pageNumber)
    {
        if (($pageNumber < 1) || ($pageNumber > $this['Count']->getValue())) {
            return null;            
        }
        
        $count = 0;
        
        foreach ($this['Kids']->getObject() as $obj) {
            $node = $obj->getObject();

            if ($node instanceof RE_Pdf_Page) {
                if (++$count == $pageNumber) {
                    return $node;
                }

            } else {  // RE_Pdf_Page_Tree
                $count += $node['Count']->getValue();
                if ($pageNumber <= $count) {
                    return $node->getPage($count - $pageNumber + 1);
                }
            }
        }
    }
    

  /* Page Tree Management */
  
    /**
     * Sets the indirect object tracking this page tree node in the source PDF
     * document. Necessary because child page nodes require a 'Parent' entry in
     * the page dictionary which must be a reference to this object.
     * 
     * @todo This is probably not the most intuitive way to handle this. Come
     *   back to this with a better implementation or a better explanation.
     * 
     * @param RE_Pdf_Object_Indirect $indirectObject
     */
    public function setIndirectObject(RE_Pdf_Object_Indirect $indirectObject)
    {
        $this->_indirectObject = $indirectObject;
    }
    
    /**
     * Flattens this tree node by removing all intermediate nodes, moving each
     * leaf node as a child of this tree.
     * 
     * @todo Write this code.
     */
    public function flatten()
    {}
    
    /**
     * Rearranges the child nodes as a balanced tree.
     * 
     * @todo Write this code.
     */
    public function balance()
    {}
    
    /**
     * Reverses the positions of all of the nodes in this tree, descending into
     * intermediate nodes as necessary.
     * 
     * @todo Write this code.
     */
    public function reverse()
    {}
  
  
  /* Accessor Methods */
  
    /**
     * Returns the source PDF document.
     *
     * @return RE_Pdf_Object_Extraction_Interface
     */
    public function getSourcePDF()
    {
        return $this->_sourcePDF;
    }


  /* Page Boundaries */
  
    /**
     * PDF allows for inheritance of an individual page's MediaBox from an
     * ancestor page tree node, the idea being that you could set or change
     * the value at the top of the tree and the value cascades to all child
     * page nodes, unless a specific page overrides it with its own MediaBox
     * entry.
     * 
     * This is a great idea, but it causes several implementation challenges
     * because the relationship between a page node and its entire ancestry
     * must be maintained in memory at all times. In fact, inheritance is
     * prohibited in Linearized PDFs for this very reason.
     * 
     * This method exists to return the MediaBox entry for this page tree
     * node if present, or its parent node if not. If this node or its parent
     * does not have a MediaBox entry, returns null.
     * 
     * Due to the implementation challenges of inheritance, and its prohibition
     * in Linearized PDF, RE_Pdf does not support setting a MediaBox on a
     * page tree node. Use {@link RE_Pdf_Page::setMediaBox()} on each of the
     * individual pages instead.
     * 
     * @return RE_Pdf_Array|null
     */
    public function getMediaBox()
    {
        if (! isset($this['MediaBox'])) {
            /* This node does not contain a MediaBox entry. Check the parent
             * parent node. If there is no parent, return null.
             */
            if (isset($this['Parent'])) {
                return $this['Parent']->getObject()->getMediaBox();
            }
            return null;
        }

        return $this['MediaBox']->getObject();
    }
  
    /**
     * PDF allows for inheritance of an individual page's CropBox from an
     * ancestor page tree node, the idea being that you could set or change
     * the value at the top of the tree and the value cascades to all child
     * page nodes, unless a specific page overrides it with its own CropBox
     * entry.
     * 
     * This is a great idea, but it causes several implementation challenges
     * because the relationship between a page node and its entire ancestry
     * must be maintained in memory at all times. In fact, inheritance is
     * prohibited in Linearized PDFs for this very reason.
     * 
     * This method exists to return the CropBox entry for this page tree
     * node if present, or its parent node if not. If this node or its parent
     * does not have a CropBox entry, returns null.
     * 
     * Due to the implementation challenges of inheritance, and its prohibition
     * in Linearized PDF, RE_Pdf does not support setting a CropBox on a
     * page tree node. Use {@link RE_Pdf_Page::setCropBox()} on each of the
     * individual pages instead.
     * 
     * @return RE_Pdf_Array|null
     */
    public function getCropBox()
    {
        if (! isset($this['CropBox'])) {
            /* This node does not contain a CropBox entry. Check the parent
             * parent node. If there is no parent, return null.
             */
            if (isset($this['Parent'])) {
                return $this['Parent']->getObject()->getCropBox();
            }
            return null;
        }

        return $this['CropBox']->getObject();
    }
  

  /* ArrayAccess Interface Methods */

    /* Overridden to enforce specific values and/or types for certain
     * dictionary keys.
     * 
     * @throws InvalidArgumentException
     */
    public function offsetSet($offset, $value)
    {
        parent::offsetSet($offset, $value);
        
        switch ($offset) {
        case 'Type':
            if ($this['Type']->getValue() !== 'Pages') {
                throw new InvalidArgumentException("Value for key 'Type' must be 'Pages'");
            }
            break;
        }
    }


  /* Dictionary Key Type Checking */

    /**
     * Returns the valid object type for important keys in this dictionary.
     *
     * @return string
     */
    public function getValidTypeForKey($key)
    {
        switch ($key) {
        case 'Parent':
            return 'RE_Pdf_Object_Indirect';
            
        case 'Kids':
            return 'RE_Pdf_Object_Array';
            
        case 'Count':
            return 'RE_Pdf_Object_Numeric';
        }
        return parent::getValidTypeForKey($key);
    }

}
