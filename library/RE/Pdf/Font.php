<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage TextLayout
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * Abstract base font class.
 * 
 * Defines the public interface and creates shared storage for concrete
 * subclasses which are responsible for generating the font's information
 * dictionaries, mapping characters to glyphs, and providing both overall font
 * and glyph-specific metric data.
 *
 * Font objects themselves are normally instantiated through the factory methods
 * {@link fontWithName()} or {@link fontWithPath()}.
 *
 * @package    Pdf
 * @subpackage TextLayout
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
abstract class RE_Pdf_Font extends RE_Pdf_Object_Dictionary
{
  /**** Class Constants ****/


  /* Names of the Standard 14 PDF Fonts */

    /**
     * Name of the standard PDF font Courier.
     */
    const FONT_COURIER = 'Courier';

    /**
     * Name of the bold style of the standard PDF font Courier.
     */
    const FONT_COURIER_BOLD = 'Courier-Bold';

    /**
     * Name of the italic style of the standard PDF font Courier.
     */
    const FONT_COURIER_OBLIQUE = 'Courier-Oblique';

    /**
     * Convenience constant for a common misspelling of
     * {@link FONT_COURIER_OBLIQUE}.
     */
    const FONT_COURIER_ITALIC = 'Courier-Oblique';

    /**
     * Name of the bold and italic style of the standard PDF font Courier.
     */
    const FONT_COURIER_BOLD_OBLIQUE = 'Courier-BoldOblique';

    /**
     * Convenience constant for a common misspelling of
     * {@link FONT_COURIER_BOLD_OBLIQUE}.
     */
    const FONT_COURIER_BOLD_ITALIC = 'Courier-BoldOblique';

    /**
     * Name of the standard PDF font Helvetica.
     */
    const FONT_HELVETICA = 'Helvetica';

    /**
     * Name of the bold style of the standard PDF font Helvetica.
     */
    const FONT_HELVETICA_BOLD = 'Helvetica-Bold';

    /**
     * Name of the italic style of the standard PDF font Helvetica.
     */
    const FONT_HELVETICA_OBLIQUE = 'Helvetica-Oblique';

    /**
     * Convenience constant for a common misspelling of
     * {@link FONT_HELVETICA_OBLIQUE}.
     */
    const FONT_HELVETICA_ITALIC = 'Helvetica-Oblique';

    /**
     * Name of the bold and italic style of the standard PDF font Helvetica.
     */
    const FONT_HELVETICA_BOLD_OBLIQUE = 'Helvetica-BoldOblique';

    /**
     * Convenience constant for a common misspelling of
     * {@link FONT_HELVETICA_BOLD_OBLIQUE}.
     */
    const FONT_HELVETICA_BOLD_ITALIC = 'Helvetica-BoldOblique';

    /**
     * Name of the standard PDF font Symbol.
     */
    const FONT_SYMBOL = 'Symbol';

    /**
     * Name of the standard PDF font Times.
     */
    const FONT_TIMES_ROMAN = 'Times-Roman';

    /**
     * Convenience constant for a common misspelling of
     * {@link FONT_TIMES_ROMAN}.
     */
    const FONT_TIMES = 'Times-Roman';

    /**
     * Name of the bold style of the standard PDF font Times.
     */
    const FONT_TIMES_BOLD = 'Times-Bold';

    /**
     * Name of the italic style of the standard PDF font Times.
     */
    const FONT_TIMES_ITALIC = 'Times-Italic';

    /**
     * Name of the bold and italic style of the standard PDF font Times.
     */
    const FONT_TIMES_BOLD_ITALIC = 'Times-BoldItalic';

    /**
     * Name of the standard PDF font Zapf Dingbats.
     */
    const FONT_ZAPFDINGBATS = 'ZapfDingbats';


  /* Font Name String Types */

    /**
     * Full copyright notice for the font.
     */
    const NAME_COPYRIGHT =  0;

    /**
     * Font family name. Used to group similar styles of fonts together.
     */
    const NAME_FAMILY =  1;

    /**
     * Font style within the font family. Examples: Regular, Italic, Bold, etc.
     */
    const NAME_STYLE =  2;

    /**
     * Unique font identifier.
     */
    const NAME_ID =  3;

    /**
     * Full font name. Usually a combination of the {@link NAME_FAMILY} and
     * {@link NAME_STYLE} strings.
     */
    const NAME_FULL =  4;

    /**
     * Version number of the font.
     */
    const NAME_VERSION =  5;

    /**
     * PostScript name for the font. This is the name used to identify fonts
     * internally and within the PDF file.
     */
    const NAME_POSTSCRIPT =  6;

    /**
     * Font trademark notice. This is distinct from the {@link NAME_COPYRIGHT}.
     */
    const NAME_TRADEMARK =  7;

    /**
     * Name of the font manufacturer.
     */
    const NAME_MANUFACTURER =  8;

    /**
     * Name of the designer of the font.
     */
    const NAME_DESIGNER =  9;

    /**
     * Description of the font. May contain revision information, usage
     * recommendations, features, etc.
     */
    const NAME_DESCRIPTION = 10;

    /**
     * URL of the font vendor. Some fonts may contain a unique serial number
     * embedded in this URL, which is used for licensing.
     */
    const NAME_VENDOR_URL = 11;

    /**
     * URL of the font designer ({@link NAME_DESIGNER}).
     */
    const NAME_DESIGNER_URL = 12;

    /**
     * Plain language licensing terms for the font.
     */
    const NAME_LICENSE = 13;

    /**
     * URL of more detailed licensing information for the font.
     */
    const NAME_LICENSE_URL = 14;

    /**
     * Preferred font family. Used by some fonts to work around a Microsoft
     * Windows limitation where only four fonts styles can share the same
     * {@link NAME_FAMILY} value.
     */
    const NAME_PREFERRED_FAMILY = 16;

    /**
     * Preferred font style. A more descriptive string than {@link NAME_STYLE}.
     */
    const NAME_PREFERRED_STYLE = 17;

    /**
     * Suggested text to use as a representative sample of the font.
     */
    const NAME_SAMPLE_TEXT = 19;

    /**
     * PostScript CID findfont name.
     */
    const NAME_CID_NAME = 20;


  /* Font Weights */

    /**
     * Thin font weight.
     */
    const WEIGHT_THIN = 100;

    /**
     * Extra-light (Ultra-light) font weight.
     */
    const WEIGHT_EXTRA_LIGHT = 200;

    /**
     * Light font weight.
     */
    const WEIGHT_LIGHT = 300;

    /**
     * Normal (Regular) font weight.
     */
    const WEIGHT_NORMAL = 400;

    /**
     * Medium font weight.
     */
    const WEIGHT_MEDIUM = 500;

    /**
     * Semi-bold (Demi-bold) font weight.
     */
    const WEIGHT_SEMI_BOLD = 600;

    /**
     * Bold font weight.
     */
    const WEIGHT_BOLD = 700;

    /**
     * Extra-bold (Ultra-bold) font weight.
     */
    const WEIGHT_EXTRA_BOLD = 800;

    /**
     * Black (Heavy) font weight.
     */
    const WEIGHT_BLACK = 900;


  /* Font Widths */

    /**
     * Ultra-condensed font width. Typically 50% of normal.
     */
    const WIDTH_ULTRA_CONDENSED = 1;

    /**
     * Extra-condensed font width. Typically 62.5% of normal.
     */
    const WIDTH_EXTRA_CONDENSED = 2;

    /**
     * Condensed font width. Typically 75% of normal.
     */
    const WIDTH_CONDENSED = 3;

    /**
     * Semi-condensed font width. Typically 87.5% of normal.
     */
    const WIDTH_SEMI_CONDENSED = 4;

    /**
     * Normal (Medium) font width.
     */
    const WIDTH_NORMAL = 5;

    /**
     * Semi-expanded font width. Typically 112.5% of normal.
     */
    const WIDTH_SEMI_EXPANDED = 6;

    /**
     * Expanded font width. Typically 125% of normal.
     */
    const WIDTH_EXPANDED = 7;

    /**
     * Extra-expanded font width. Typically 150% of normal.
     */
    const WIDTH_EXTRA_EXPANDED = 8;

    /**
     * Ultra-expanded font width. Typically 200% of normal.
     */
    const WIDTH_ULTRA_EXPANDED = 9;



  /**** Class Variables ****/

    /**
     * Array whose keys are the unique PostScript names of instantiated fonts.
     * The values are the font objects themselves.
     * @var array
     */
    protected static $_fontCacheNames = array();

    /**
     * Array whose keys are the md5 hash of the full paths on disk for parsed
     * fonts. The values are the font objects themselves.
     * @var array
     */
    protected static $_fontCachePaths = array();



  /**** Instance Variables ****/

    /**
     * Object representing the font's cmap (character to glyph map).
     * @var RE_Font_Cmap
     */
    public $cmap = null;

    /**
     * Array containing descriptive names for the font. See {@link fontName()}.
     * @var array
     */
    protected $_fontNames = array();

    /**
     * Flag indicating whether or not this font is bold.
     * @var boolean
     */
    protected $_isBold = false;

    /**
     * Flag indicating whether or not this font is italic.
     * @var boolean
     */
    protected $_isItalic = false;

    /**
     * Flag indicating whether or not this font is monospaced.
     * @var boolean
     */
    protected $_isMonospace = false;

    /**
     * The position below the text baseline of the underline (in glyph units).
     * @var integer
     */
    protected $_underlinePosition = 0;

    /**
     * The thickness of the underline (in glyph units).
     * @var integer
     */
    protected $_underlineThickness = 0;

    /**
     * The position above the text baseline of the strikethrough (in glyph units).
     * @var integer
     */
    protected $_strikePosition = 0;

    /**
     * The thickness of the strikethrough (in glyph units).
     * @var integer
     */
    protected $_strikeThickness = 0;

    /**
     * Number of glyph units per em. See {@link getUnitsPerEm()}.
     * @var integer
     */
    protected $_unitsPerEm = 0;

    /**
     * Typographical ascent. See {@link getAscent()}.
     * @var integer
     */
    protected $_ascent = 0;

    /**
     * Typographical descent. See {@link getDescent()}.
     * @var integer
     */
    protected $_descent = 0;

    /**
     * Typographical line gap. See {@link getLineGap()}.
     * @var integer
     */
    protected $_lineGap = 0;

    /**
     * Array containing the widths of each of the glyphs contained in the font.
     * @var array
     */
    protected $_glyphWidths = null;



  /**** Class Methods ****/

    /**
     * Returns a font object by full name. This is the preferred method to
     * obtain one of the standard 14 PDF fonts.
     *
     * The result of this method is cached, preventing unnecessary duplication
     * of font objects. Repetitive calls for a font with the same name will
     * return the same object.
     *
     * @param string $name Full PostScript name of font.
     * @return RE_Pdf_Font
     * @throws RE_Pdf_Exception if the font name is not one of the 14 standard
     *   PDF fonts and does not match the name of a previously created object.
     */
    public static function fontWithName($name)
    {
        /* First check the cache. Don't duplicate font objects.
         */
        if (isset(self::$_fontCacheNames[$name])) {
            return self::$_fontCacheNames[$name];
        }

        /**
         * @todo It would be helpful to be able to have a mapping of font names
         *   to file paths in a configuration file for frequently used custom
         *   fonts. This would allow a user to use custom fonts without having
         *   to hard-code file paths all over the place.
         */

        /* Not an existing font and no mapping in the config file. Check to see
         * if this is one of the standard 14 PDF fonts.
         */
        switch ($name) {
            case RE_Pdf_Font::FONT_COURIER:
                require_once 'RE/Pdf/Font/Standard/Courier.php';
                $font = new RE_Pdf_Font_Standard_Courier();
                break;

            case RE_Pdf_Font::FONT_COURIER_BOLD:
                require_once 'RE/Pdf/Font/Standard/CourierBold.php';
                $font = new RE_Pdf_Font_Standard_CourierBold();
                break;

            case RE_Pdf_Font::FONT_COURIER_OBLIQUE:
                require_once 'RE/Pdf/Font/Standard/CourierOblique.php';
                $font = new RE_Pdf_Font_Standard_CourierOblique();
                break;

            case RE_Pdf_Font::FONT_COURIER_BOLD_OBLIQUE:
                require_once 'RE/Pdf/Font/Standard/CourierBoldOblique.php';
                $font = new RE_Pdf_Font_Standard_CourierBoldOblique();
                break;

            case RE_Pdf_Font::FONT_HELVETICA:
                require_once 'RE/Pdf/Font/Standard/Helvetica.php';
                $font = new RE_Pdf_Font_Standard_Helvetica();
                break;

            case RE_Pdf_Font::FONT_HELVETICA_BOLD:
                require_once 'RE/Pdf/Font/Standard/HelveticaBold.php';
                $font = new RE_Pdf_Font_Standard_HelveticaBold();
                break;

            case RE_Pdf_Font::FONT_HELVETICA_OBLIQUE:
                require_once 'RE/Pdf/Font/Standard/HelveticaOblique.php';
                $font = new RE_Pdf_Font_Standard_HelveticaOblique();
                break;

            case RE_Pdf_Font::FONT_HELVETICA_BOLD_OBLIQUE:
                require_once 'RE/Pdf/Font/Standard/HelveticaBoldOblique.php';
                $font = new RE_Pdf_Font_Standard_HelveticaBoldOblique();
                break;

            case RE_Pdf_Font::FONT_SYMBOL:
                require_once 'RE/Pdf/Font/Standard/Symbol.php';
                $font = new RE_Pdf_Font_Standard_Symbol();
                break;

            case RE_Pdf_Font::FONT_TIMES_ROMAN:
                require_once 'RE/Pdf/Font/Standard/TimesRoman.php';
                $font = new RE_Pdf_Font_Standard_TimesRoman();
                break;

            case RE_Pdf_Font::FONT_TIMES_BOLD:
                require_once 'RE/Pdf/Font/Standard/TimesBold.php';
                $font = new RE_Pdf_Font_Standard_TimesBold();
                break;

            case RE_Pdf_Font::FONT_TIMES_ITALIC:
                require_once 'RE/Pdf/Font/Standard/TimesItalic.php';
                $font = new RE_Pdf_Font_Standard_TimesItalic();
                break;

            case RE_Pdf_Font::FONT_TIMES_BOLD_ITALIC:
                require_once 'RE/Pdf/Font/Standard/TimesBoldItalic.php';
                $font = new RE_Pdf_Font_Standard_TimesBoldItalic();
                break;

            case RE_Pdf_Font::FONT_ZAPFDINGBATS:
                require_once 'RE/Pdf/Font/Standard/ZapfDingbats.php';
                $font = new RE_Pdf_Font_Standard_ZapfDingbats();
                break;

            default:
                throw new RE_Pdf_Exception("Unknown font name: $name",
                                             RE_Pdf_Exception::BAD_FONT_NAME);
        }

        /* Add this new font to the cache array and return it for use.
         */
        self::$_fontCacheNames[$name] = $font;
        return $font;
    }

    /**
     * Returns a font object by file path.
     *
     * The result of this method is cached, preventing unnecessary duplication
     * of font objects. Repetitive calls for the font with the same path will
     * return the same object.
     *
     * @param string $filePath Full filesystem path to the font file.
     * @return RE_Pdf_Font
     * @throws RE_Pdf_Exception if the file path is invalid or the font type
     *   cannot be determined.
     */
    public static function fontWithPath($filePath)
    {
        /* First check the cache. Don't duplicate font objects.
         */
        $filePathKey = md5($filePath);
        if (isset(self::$_fontCachePaths[$filePathKey])) {
            return self::$_fontCachePaths[$filePathKey];
        }

        /* Create a file parser data source object for this file. File path and
         * access permission checks are handled here.
         */
        require_once 'RE/FileParser/DataSource/File.php';
        $dataSource = new RE_FileParser_DataSource_File($filePath);

        /* Attempt to determine the type of font. We can't always trust file
         * extensions, but try that first since it's fastest.
         */
        $fileExtension = strtolower(pathinfo($filePath, PATHINFO_EXTENSION));

        /* If it turns out that the file is named improperly and we guess the
         * wrong type, we'll get null instead of a font object.
         */
        switch ($fileExtension) {
            case 'ttf':
                $font = self::_extractTrueTypeFont($dataSource);
                break;

            case 'otf':
                $font = self::_extractOpenTypeCFFFont($dataSource);
                break;

            default:
                /* Unrecognized extension. Try to determine the type by actually
                 * parsing it below.
                 */
                $font = null;
                break;
        }


        if (is_null($font)) {
            /* There was no match for the file extension or the extension was
             * wrong. Attempt to detect the type of font by actually parsing it.
             * We'll do the checks in order of most likely format to try to
             * reduce the detection time.
             */

            // TrueType
            if ((is_null($font)) && ($fileExtension != 'ttf')) {
                $font = self::_extractTrueTypeFont($dataSource);
            }

            // OpenType
            if ((is_null($font)) && ($fileExtension != 'otf')) {
                $font = self::_extractOpenTypeCFFFont($dataSource);
            }

            // Type 1 PostScript

            // Mac OS X dfont

            // others?
        }


        /* Done with the data source object.
         */
        $dataSource = null;

        if (! is_null($font)) {
            /* Parsing was successful. Add this font instance to the cache arrays
             * and return it for use.
             */
            $fontName = $font->getFontName(RE_Pdf_Font::NAME_POSTSCRIPT, '', '');
            self::$_fontCacheNames[$fontName] = $font;
            
            $filePathKey = md5($filePath);
            self::$_fontCachePaths[$filePathKey] = $font;
            
            return $font;

        } else {
            /* The type of font could not be determined. Give up.
             */
            throw new RE_Pdf_Exception("Cannot determine font type: $filePath",
                                         RE_Pdf_Exception::CANT_DETERMINE_FONT_TYPE);
         }

    }



  /**** Public Interface ****/


  /* Concrete Subclass Implementation */

    /**
     * Extracts the font object from the specified file parser at its current
     * read offset. Returns the extracted object and moves the offset past the
     * the object and any trailing white space.
     *
     * @param  RE_FileParser_Pdf $parser
     * @return RE_Pdf_Font
     * @throws RE_Pdf_Exception if a font object is not present at the
     *   current parser offset.
     */
    public static function extract(RE_FileParser_Pdf $parser)
    {
        $offset = $parser->getOffset();

        $object = RE_Pdf_Object_Dictionary::extract($parser);
        if (! $object instanceof self) {
            throw new RE_Pdf_Exception('Font object not found, found other dictionary object instead '
                                         . 'at parser offset ' . sprintf('0x%x', ($offset)));
        }

        /** @todo Extract font metric information so accessor methods work. */

        return $object;
    }


  /* Object Lifecycle */

    /**
     * Object constructor.
     *
     * @param array $value (optional)
     */
    public function __construct($value = null)
    {
        parent::__construct($value);
        $this['Type'] = 'Font';
    }


  /* Object Magic Methods */

    /**
     * Returns the PostScript name of the font as a UTF-8 string.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getFontName(RE_Pdf_Font::NAME_POSTSCRIPT, 'en', 'UTF-8');
    }


  /* Accessor Methods */

    /**
     * Returns the specified descriptive name for the font.
     *
     * The font name type is usually one of the following:
     *  - {@link RE_Pdf_Font::NAME_FULL}
     *  - {@link RE_Pdf_Font::NAME_FAMILY}
     *  - {@link RE_Pdf_Font::NAME_PREFERRED_FAMILY}
     *  - {@link RE_Pdf_Font::NAME_STYLE}
     *  - {@link RE_Pdf_Font::NAME_PREFERRED_STYLE}
     *  - {@link RE_Pdf_Font::NAME_DESCRIPTION}
     *  - {@link RE_Pdf_Font::NAME_SAMPLE_TEXT}
     *  - {@link RE_Pdf_Font::NAME_ID}
     *  - {@link RE_Pdf_Font::NAME_VERSION}
     *  - {@link RE_Pdf_Font::NAME_POSTSCRIPT}
     *  - {@link RE_Pdf_Font::NAME_CID_NAME}
     *  - {@link RE_Pdf_Font::NAME_DESIGNER}
     *  - {@link RE_Pdf_Font::NAME_DESIGNER_URL}
     *  - {@link RE_Pdf_Font::NAME_MANUFACTURER}
     *  - {@link RE_Pdf_Font::NAME_VENDOR_URL}
     *  - {@link RE_Pdf_Font::NAME_COPYRIGHT}
     *  - {@link RE_Pdf_Font::NAME_TRADEMARK}
     *  - {@link RE_Pdf_Font::NAME_LICENSE}
     *  - {@link RE_Pdf_Font::NAME_LICENSE_URL}
     *
     * Note that not all names are available for all fonts. In addition, some
     * fonts may contain additional names, whose indicies are in the range
     * 256 to 32767 inclusive, which are used for certain font layout features.
     *
     * If the preferred language translation is not available, uses the first
     * available translation for the name, which is usually English.
     *
     * If the requested name does not exist, returns null.
     *
     * All names are stored internally as Unicode strings, using UTF-16BE
     * encoding. You may optionally supply a different resulting character set.
     *
     * @param integer $nameType Type of name requested.
     * @param mixed $language Preferred language (string) or array of languages
     *   in preferred order. Use the ISO 639 standard 2-letter language codes.
     * @param string $characterSet (optional) Desired resulting character set.
     *   You may use any character set supported by {@link iconv()};
     * @return string
     */
    public function getFontName($nameType, $language, $characterSet = 'UTF-16BE')
    {
        if (! isset($this->_fontNames[$nameType])) {
            return null;
        }
        $name = null;
        if (is_array($language)) {
            foreach ($language as $code) {
                if (isset($this->_fontNames[$nameType][$code])) {
                    $name = $this->_fontNames[$nameType][$code];
                    break;
                }
            }
        } else {
            if (isset($this->_fontNames[$nameType][$language])) {
                $name = $this->_fontNames[$nameType][$language];
            }
        }
        /* If the preferred language could not be found, use whatever is first.
         */
        if (is_null($name)) {
            $name = reset($this->_fontNames[$nameType]);
        }
        /* Convert the character set if requested.
         */
        if ($characterSet != 'UTF-16BE') {
            $name = iconv('UTF-16BE', $characterSet, $name);
        }
        return $name;
    }

    /**
     * Returns the suggested position below the text baseline of the underline
     * in glyph units.
     *
     * This value is usually negative.
     *
     * @return integer
     */
    public function getUnderlinePosition()
    {
        return $this->_underlinePosition;
    }

    /**
     * Returns the suggested line thickness of the underline in glyph units.
     *
     * @return integer
     */
    public function getUnderlineThickness()
    {
        return $this->_underlineThickness;
    }

    /**
     * Returns the suggested position above the text baseline of the
     * strikethrough in glyph units.
     *
     * @return integer
     */
    public function getStrikePosition()
    {
        return $this->_strikePosition;
    }

    /**
     * Returns the suggested line thickness of the strikethrough in glyph units.
     *
     * @return integer
     */
    public function getStrikeThickness()
    {
        return $this->_strikeThickness;
    }

    /**
     * Returns the number of glyph units per em.
     *
     * Used to convert glyph space to user space. Frequently used in conjunction
     * with {@link widthsForGlyphs()} to calculate the with of a run of text.
     *
     * @return integer
     */
    public function getUnitsPerEm()
    {
        return $this->_unitsPerEm;
    }

    /**
     * Returns the typographic ascent in font glyph units.
     *
     * The typographic ascent is the distance from the font's baseline to the
     * top of the text frame. It is frequently used to locate the initial
     * baseline for a paragraph of text inside a given rectangle.
     *
     * @return integer
     */
    public function getAscent()
    {
        return $this->_ascent;
    }

    /**
     * Returns the typographic descent in font glyph units.
     *
     * The typographic descent is the distance below the font's baseline to the
     * bottom of the text frame. It is always negative.
     *
     * @return integer
     */
    public function getDescent()
    {
        return $this->_descent;
    }

    /**
     * Returns the typographic line gap in font glyph units.
     *
     * The typographic line gap is the distance between the bottom of the text
     * frame of one line to the top of the text frame of the next. It is
     * typically combined with the typographical ascent and descent to determine
     * the font's total line height (or leading).
     *
     * @return integer
     */
    public function getLineGap()
    {
        return $this->_lineGap;
    }

    /**
     * Returns the suggested line height (or leading) in font glyph units.
     *
     * This value is determined by adding together the values of the typographic
     * ascent, descent, and line gap. This value yields the suggested line
     * spacing as determined by the font developer.
     *
     * It should be noted that this is only a guideline; layout engines will
     * frequently modify this value to achieve special effects such as double-
     * spacing.
     *
     * @return integer
     */
    public function getLineHeight()
    {
        return $this->_ascent - $this->_descent + $this->_lineGap;
    }


  /* Information and Conversion Methods */

    /**
     * Returns a number between 0 and 1 inclusive that indicates the percentage
     * of characters in the string which are covered by glyphs in this font.
     *
     * Since no one font will contain glyphs for the entire Unicode character
     * range, this method can be used to help locate a suitable font when the
     * actual contents of the string are not known.
     *
     * Note that some fonts lie about the characters they support. Additionally,
     * fonts don't usually contain glyphs for control characters such as tabs
     * and line breaks, so it is rare that you will get back a full 1.0 score.
     * The resulting value should be considered informational only.
     *
     * @param string $string
     * @param string $charEncoding (optional) Character encoding of source text.
     *   If omitted, uses 'current locale'.
     * @return float
     */
    public function getCoveredPercentage($string, $charEncoding = '')
    {
        /* Convert the string to UTF-16BE encoding so we can match the string's
         * character codes to those found in the cmap.
         */
        if ($charEncoding != 'UTF-16BE') {
            $string = iconv($charEncoding, 'UTF-16BE', $string);
        }

        $charCount = iconv_strlen($string, 'UTF-16BE');
        if ($charCount == 0) {
            return 0;
        }

        /* Fetch the covered character code list from the font's cmap.
         */
        $coveredCharacters = $this->cmap->getCoveredCharacters();

        /* Calculate the score by doing a lookup for each character.
         */
        $score = 0;
        $maxIndex = strlen($string);
        for ($i = 0; $i < $maxIndex; $i++) {
            /**
             * @todo Properly handle characters encoded as surrogate pairs.
             */
            $charCode = (ord($string[$i]) << 8) | ord($string[++$i]);
            /* This could probably be optimized a bit with a binary search...
             */
            if (in_array($charCode, $coveredCharacters)) {
                $score++;
            }
        }
        return $score / $charCount;
    }

    /**
     * Returns the widths of the glyphs.
     *
     * The widths are expressed in the font's glyph space. You are responsible
     * for converting to user space as necessary. See {@link unitsPerEm()}.
     *
     * See also {@link widthForGlyph()}.
     *
     * @param array $glyphNumbers Array of glyph numbers.
     * @return array Array of glyph widths (integers).
     * @throws RE_Pdf_Exception if the glyph number is out of range.
     */
    public function widthsForGlyphs($glyphNumbers)
    {
        $widths = array();
        foreach ($glyphNumbers as $key => $glyphNumber) {
            if (! isset($this->_glyphWidths[$glyphNumber])) {
                if (is_null($glyphNumber)) {
                    $widths[$key] = 0;
                } else {
                    throw new RE_Pdf_Exception("Glyph number is out of range: $glyphNumber",
                                               RE_Pdf_Exception::GLYPH_OUT_OF_RANGE);
                }
            }
            $widths[$key] = $this->_glyphWidths[$glyphNumber];
        }
        return $widths;
    }

    /**
     * Returns the width of the glyph.
     *
     * The width is expressed in the font's glyph space. You are responsible
     * for converting to user space as necessary. See {@link unitsPerEm()}.
     *
     * See also {@link widthForGlyph()}.
     *
     * @param integer $glyphNumber
     * @return integer
     * @throws RE_Pdf_Exception if the glyph number is out of range.
     */
    public function widthForGlyph($glyphNumber)
    {
        if (! isset($this->_glyphWidths[$glyphNumber])) {
            if (is_null($glyphNumber)) {
                $widths[$key] = 0;
            } else {
                throw new RE_Pdf_Exception("Glyph number is out of range: $glyphNumber",
                                           RE_Pdf_Exception::GLYPH_OUT_OF_RANGE);
            }
        }
        return $this->_glyphWidths[$glyphNumber];
    }
    
    
  /* PDF Drawing */
  
    /**
     * Sets the current font and font size for the specified canvas.
     * 
     * @param RE_Pdf_Canvas_Interface $canvas
     * @param float                   $fontSize
     */
    public function setFontAndSize(RE_Pdf_Canvas_Interface $canvas, $fontSize)
    {
        $fontName = $canvas->addResource('Font', $this);
        
        RE_Pdf_Object_Name::writeValue($canvas, $fontName);
        RE_Pdf_Object_Numeric::writeValue($canvas, $fontSize);
        $canvas->writeBytes(" Tf\n");
    }

    /**
     * Shows the nominally spaced glyphs on the canvas at the current
     * location in the canvas' text space.
     * 
     * @param RE_Pdf_Canvas_Interface $canvas
     * @param array                   $glyphs
     */
    public function showGlyphs(RE_Pdf_Canvas_Interface $canvas, array $glyphs)
    {
        $packedGlyphs = '';
        foreach ($glyphs as $glyph) {
            if ($glyph > 0xff) {
                $packedGlyphs .= chr(($glyph >> 8) & 0xff) . chr($glyph & 0xff);
            } else {
                $packedGlyphs .= "\x00" . chr($glyph);
            }
        }

        RE_Pdf_Object_String::writeValue($canvas, $packedGlyphs);
        $canvas->writeBytes(" Tj\n");
    }


  /* ArrayAccess Interface Methods */

    /* Overridden to enforce specific values and/or types for certain
     * dictionary keys.
     * 
     * @throws InvalidArgumentException
     */
    public function offsetSet($offset, $value)
    {
        parent::offsetSet($offset, $value);
        
        switch ($offset) {
        case 'Type':
            if ($this[$offset]->getValue() !== 'Font') {
                throw new InvalidArgumentException("Value for key '$offset' must be 'Font'");
            }
            break;
            
        case 'Encoding':
            if (! (($this[$offset] instanceof RE_Pdf_Object_Name) ||
                   ($this[$offset] instanceof RE_Pdf_Object_Dictionary))) {
                    throw new InvalidArgumentException(
                        "Value for key '$offset' must be a RE_Pdf_Object_Name or RE_Pdf_Object_Dictionary object");
            }
            break;
        }
    }


  /* Dictionary Key Type Checking */

    /**
     * Returns the valid object type for important keys in this dictionary.
     *
     * @return string
     */
    public function getValidTypeForKey($key)
    {
        switch ($key) {
        case 'BaseFont':
            return 'RE_Pdf_Object_Name';
            
        case 'FirstChar':
        case 'LastChar':
            return 'RE_Pdf_Object_Numeric_Integer';
            
        case 'Widths':
            return 'RE_Pdf_Object_Array';
        
        case 'FontDescriptor':
            return 'RE_Pdf_Object_Dictionary';
            
        case 'ToUnicode':
            return 'RE_Pdf_Object_Indirect';  // RE_Pdf_Object_Stream
            
        }
        return parent::getValidTypeForKey($key);
    }



  /**** Internal Interface ****/


  /* Font Extraction Methods */

    /**
     * Attempts to extract a TrueType font from the data source.
     *
     * If the font parser throws an exception that suggests the data source
     * simply doesn't contain a TrueType font, catches it and returns null. If
     * an exception is thrown that suggests the TrueType font is corrupt or
     * otherwise unusable, throws that exception. If successful, returns the
     * font object.
     *
     * @param RE_FileParser_DataSource_Interface $dataSource
     * @return RE_Pdf_Font_OpenType_TrueType May also return null if
     *   the data source does not appear to contain a TrueType font.
     * @throws RE_Pdf_Exception
     */
    protected static function _extractTrueTypeFont(RE_FileParser_DataSource_Interface $dataSource)
    {
        try {
            require_once 'RE/FileParser/Font/OpenType/TrueType.php';
            $parser = new RE_FileParser_Font_OpenType_TrueType($dataSource);
            $parser->parse();

            require_once 'RE/Pdf/Font/OpenType/TrueType.php';
            $font = new RE_Pdf_Font_OpenType_TrueType();
            $font->setFontParser($parser);

            $parser = null;

        } catch (RE_FileParser_Exception $e) {
            /* The following exception codes suggest that this isn't really a
             * TrueType font. If we caught such an exception, simply return
             * null. For all other cases, it probably is a TrueType font but has
             * a problem; throw the exception again.
             */
            $parser = null;
            switch ($e->getCode()) {
                case RE_FileParser_Exception::NOT_A_FONT_FILE:
                case RE_FileParser_Exception::BAD_TABLE_COUNT:
                case RE_FileParser_Exception::BAD_MAGIC_NUMBER:
                    return null;

                default:
                    throw $e;
            }
        }
        return $font;
    }
    
    /**
     * Attempts to extract an OpenType font that uses the Compact Font Format
     * (CFF) from the data source.
     *
     * If the font parser throws an exception that suggests the data source
     * simply doesn't contain a OpenType CFF font, catches it and returns null.
     * If an exception is thrown that suggests the OpenType CFF font is corrupt
     * or otherwise unusable, throws that exception. If successful, returns the
     * font object.
     *
     * @param RE_FileParser_DataSource_Interface $dataSource
     * @return RE_Pdf_Font_OpenType_CFF May also return null if the data source
     *   does not appear to contain an OpenType CFF font.
     * @throws RE_Pdf_Exception
     */
    protected static function _extractOpenTypeCFFFont(RE_FileParser_DataSource_Interface $dataSource)
    {
        try {
            require_once 'RE/FileParser/Font/OpenType/CFF.php';
            $parser = new RE_FileParser_Font_OpenType_CFF($dataSource);
            $parser->parse();

            require_once 'RE/Pdf/Font/OpenType/CFF.php';
            $font = new RE_Pdf_Font_OpenType_CFF();
            $font->setFontParser($parser);

            $parser = null;

        } catch (RE_FileParser_Exception $e) {
            /* The following exception codes suggest that this isn't really a
             * OpenType CFF font. If we caught such an exception, simply return
             * null. For all other cases, it probably is a OpenType CFF font but
             * has a problem; throw the exception again.
             */
            $parser = null;
            switch ($e->getCode()) {
                case RE_FileParser_Exception::NOT_A_FONT_FILE:
                case RE_FileParser_Exception::BAD_TABLE_COUNT:
                case RE_FileParser_Exception::BAD_MAGIC_NUMBER:
                    return null;

                default:
                    throw $e;
            }
        }
        return $font;
    }
    
    
  /* Font Metrics */

    /**
     * If the font's glyph space is not 1000 units per em, converts the value.
     *
     * @param integer $value
     * @return integer
     */
    protected function _toEmSpace($value)
    {
        if ($this->_unitsPerEm == 1000) {
            return $value;
        }
        return ceil(($value / $this->_unitsPerEm) * 1000);    // always round up
    }
    
    /**
     * Builds and returns the glyph widths array for use in the CID descendent
     * font dictionary.
     * 
     * @return RE_Pdf_Object_Array
     */
    protected function _buildCIDWidthsArray()
    {
        $widths = new RE_Pdf_Object_Array();
        foreach ($this->_glyphWidths as $width) {
            $widths[] = new RE_Pdf_Object_Numeric_Integer($this->_toEmSpace($width));
        }
        return new RE_Pdf_Object_Array(array(new RE_Pdf_Object_Numeric_Integer(0), $widths));
    }

}
