<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage DocumentStructure
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * The RE_Pdf_Document_Catalog object is the root of a PDF document's object
 * hierarchy. It contains references to all of the objects that define the
 * document's contents, outline, named destinations, etc. It also contains
 * presentation intent settings.
 * 
 * Every {@link RE_Pdf_Document} has exactly one document catalog object,
 * which is created automatically; you should never need to create a
 * RE_Pdf_Document_Catalog object directly.
 *
 * @package    Pdf
 * @subpackage DocumentStructure
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Pdf_Document_Catalog extends RE_Pdf_Object_Dictionary
{
  /**** Public Interface ****/


  /* Concrete Subclass Implementation */

    /**
     * Extracts the document catalog object from the specified file parser at
     * its current read offset. Returns the extracted object and moves the
     * offset past the object and any trailing white space.
     *
     * @param  RE_FileParser_Pdf $parser
     * @return RE_Pdf_Document_Catalog
     * @throws RE_Pdf_Exception if a document catalog object is not present
     *   at the current parser offset.
     */
    public static function extract(RE_FileParser_Pdf $parser)
    {
        $offset = $parser->getOffset();

        $object = RE_Pdf_Object_Dictionary::extract($parser);
        if (! $object instanceof self) {
            throw new RE_Pdf_Exception('Document catalog object not found, found other dictionary object instead '
                                         . 'at parser offset ' . sprintf('0x%x', ($offset)));
        }

        return $object;
    }


  /* Object Lifecycle */

    /**
     * Creates a new RE_Pdf_Document_Catalog, optionally setting the
     * dictionary's initial contents with the contents of the specified
     * associative array.
     *
     * @param array $value
     */
    public function __construct($value = null)
    {
        parent::__construct($value);
        $this['Type'] = 'Catalog';
    }


  /* ArrayAccess Interface Methods */

    /**
     * Overridden to enforce specific values and/or types for certain
     * dictionary keys.
     * 
     * @throws InvalidArgumentException
     */
    public function offsetSet($offset, $value)
    {
        parent::offsetSet($offset, $value);
        
        switch ($offset) {
        case 'Type':
            if ($this['Type']->getValue() !== 'Catalog') {
                throw new InvalidArgumentException("Value for key 'Type' must be 'Catalog'");
            }
            break;
            
        case 'OpenAction':
            if (! (($this['OpenAction'] instanceof RE_Pdf_Object_Indirect) ||
                   ($this['OpenAction'] instanceof RE_Pdf_Object_Array)    ||
                   ($this['OpenAction'] instanceof RE_Pdf_Object_Dictionary))) {
                    throw new InvalidArgumentException(
                        "Value for key 'OpenAction' must be a RE_Pdf_Object_Array or RE_Pdf_Object_Dictionary object");
            }
            break;
        }
    }


  /* Dictionary Key Type Checking */

    /**
     * Returns the valid object type for important keys in this dictionary.
     *
     * @return string
     */
    public function getValidTypeForKey($key)
    {
        switch ($key) {
        case 'Version':
        case 'PageLayout':
        case 'PageMode':
            return 'RE_Pdf_Object_Name';
            
        case 'Pages':
        case 'Dests':
        case 'Outlines':
        case 'Threads':
        case 'Metadata':
            return 'RE_Pdf_Object_Indirect';
            
        case 'Names':
        case 'ViewerPreferences':
        case 'AA':
        case 'URI':
        case 'AcroForm':
        case 'StructTreeRoot':
        case 'MarkInfo':
        case 'SpiderInfo':
        case 'PieceInfo':
        case 'OCProperties':
        case 'Perms':
        case 'Legal':
        case 'Collection':
            return 'RE_Pdf_Object_Dictionary';
        }
        return parent::getValidTypeForKey($key);
    }

}
