<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage DocumentStructure
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * The RE_Pdf_Document_Info object contains metadata that describes the
 * document itself. In addition to the standard keys in the dictionary, you
 * may add your own information -- this dictionary is commonly indexed by
 * search engines.
 * 
 * Every {@link RE_Pdf_Document} has exactly one document info object,
 * which is created automatically; you should never need to create a
 * RE_Pdf_Document_Info object directly.
 *
 * @package    Pdf
 * @subpackage DocumentStructure
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Pdf_Document_Info extends RE_Pdf_Object_Dictionary
{
  /**** Public Interface ****/


  /* ArrayAccess Interface Methods */

    /**
     * Overridden to remove empty values from the dictionary. See PDF 1.7
     * Reference, Section 10.2.1.
     * 
     * @throws InvalidArgumentException
     */
    public function offsetSet($offset, $value)
    {
        if (! is_object($value)) {
            if ((is_null($value)) || (strlen($value) < 1)) {
                $this->offsetUnset($offset);
                return;
            }
        }

        parent::offsetSet($offset, $value);
        
        if ($this[$offset]->getValue() == '') {
            $this->offsetUnset($offset);
        }
    }


  /* Dictionary Key Type Checking */

    /**
     * Returns the valid object type for important keys in this dictionary.
     *
     * @return string
     */
    public function getValidTypeForKey($key)
    {
        switch ($key) {
        case 'CreationDate':
        case 'ModDate':
            return 'RE_Pdf_Object_String';  // date
            
        case 'Trapped':
            return 'RE_Pdf_Object_Name';
        }

        /* All other keys must always be strings.
         */
        return 'RE_Pdf_Object_String';
    }

}
