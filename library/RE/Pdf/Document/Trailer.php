<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage DocumentStructure
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * The RE_Pdf_Document_Trailer object is the first object read when opening
 * a PDF document. It stores the basic information necessary to read data from
 * the document, including the reference to the document catalog dictionary,
 * the encryption dictionary (if present), the document ID, etc.
 * 
 * Each generation of a {@link RE_Pdf_Document} has a document trailer object,
 * which is created automatically; you should never need to create a
 * RE_Pdf_Document_Trailer object directly.
 *
 * @package    Pdf
 * @subpackage DocumentStructure
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Pdf_Document_Trailer extends RE_Pdf_Object_Dictionary
{
  /**** Public Interface ****/


  /* Concrete Subclass Implementation */

    /**
     * Extracts the file trailer object from the specified file parser at
     * its current read offset. Returns the extracted object and moves the
     * offset past the object and any trailing white space.
     *
     * @param  RE_FileParser_Pdf $parser
     * @return RE_Pdf_Document_Trailer
     * @throws RE_Pdf_Exception if a document catalog object is not present
     *   at the current parser offset.
     */
    public static function extract(RE_FileParser_Pdf $parser)
    {
        $object = RE_Pdf_Object_Dictionary::extract($parser);
        
        /* The string objects in the ID array must be written as hexidecimal strings.
         */
        if (isset($object['ID'])) {
            foreach ($object['ID'] as $string) {
                $string->setWriteAsHex(true);
            }
        }

        return $object;
    }


  /* Dictionary Key Type Checking */

    /**
     * Returns the valid object type for important keys in this dictionary.
     *
     * @return string
     */
    public function getValidTypeForKey($key)
    {
        switch ($key) {
        case 'Size':
        case 'Prev':
            return 'RE_Pdf_Object_Numeric';
            
        case 'ID':
            return 'RE_Pdf_Object_Array';
            
        case 'Root':
            return 'RE_Pdf_Document_Catalog';
            
        case 'Info':
            return 'RE_Pdf_Document_Info';

        case 'Encrypt':
            return 'RE_Pdf_Object_Dictionary';
        }
        return parent::getValidTypeForKey($key);
    }

    /**
     * Returns false if the value for the specified dictionary key may not
     * be an indirect object.
     * 
     * @return boolean
     */
    public function indirectObjectPermittedForKey($key)
    {
        switch ($key) {
        case 'Size':
        case 'Prev':
            return false;
        }
        return parent::indirectObjectPermittedForKey($key);
    }

}
