<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage DocumentStructure
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * PDF document.
 *
 * @package    Pdf
 * @subpackage DocumentStructure
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Pdf_Document implements SplObserver, RE_Pdf_OutputDestination_Interface, RE_Pdf_Object_Extraction_Interface
{
  /**** Class Constants ****/


  /* Output Destinations */

    /**
     * Write the PDF document to an in-memory buffer. See
     * also {@link setMaximumInMemorySize()}.
     */
    const DESTINATION_MEMORY = 0;

    /**
     * Write the PDF document to a file stream, typically a file on disk.
     */
    const DESTINATION_FILE = 1;

    /**
     * Write the PDF document directly to the current output buffer
     * using {@link echo()}.
     */
    const DESTINATION_ECHO = 2;



  /**** Instance Variables ****/

    /**
     * Major version number of the PDF document.
     * @var string
     */
    protected $_pdfMajorVersion = 1;

    /**
     * Minor version number of the PDF document.
     * @var integer
     */
    protected $_pdfMinorVersion = 4;

    /**
     * If the document was created from an exsiting PDF, the file parser for
     * the original document.
     * @var RE_FileParser_Pdf
     */
    protected $_sourcePDF = null;
    
    /**
     * The document catalog object.
     * @var RE_Pdf_Document_Catalog
     */
    protected $_catalog = null;
    
    /**
     * The document info object.
     * @var RE_Pdf_Document_Info
     */
    protected $_info = null;
    
    /**
     * The file trailer object.
     * @var RE_Pdf_Document_Trailer
     */
    protected $_trailer = null;
    
    /**
     * Permanent portion of the document's file identifier.
     * @var string
     */
    protected $_fileID = null;
    
    /**
     * Root page tree node for the document.
     * @var RE_Pdf_Page_Tree
     */
    protected $_pageTree = null;
    
    /**
     * Current output destination.
     * @var integer
     */
    protected $_destination = RE_Pdf_Document::DESTINATION_MEMORY;

    /**
     * If the current output destination is {@link DESTINATION_MEMORY}, the
     * in-memory buffer.
     * @var string
     */
    protected $_inMemoryBuffer = '';

    /**
     * If the current output destination is {@link DESTINATION_MEMORY}, the
     * maximum size of the in-memory buffer. If this size is exceeded, the PDF
     * document will automatically be flushed to a temporary file on disk, and
     * the output destination will change to {@link DESTINATION_FILE}.
     * @var integer
     */
    protected $_maxInMemorySize = 0x400000;  // 4 MB

    /**
     * If the current output destination is {@link DESTINATION_FILE}, the
     * file pointer resource for the output file.
     * @var resource
     */
    protected $_destinationFile = null;
    
    /**
     * Flag indicating that the document's file header has already been written.
     * @var boolean
     */
    protected $_headerWritten = false;
    
    /**
     * Base byte offset used for calculating object locations for the document's
     * cross-reference table.
     * @var number
     */
    protected $_baseOffset = 0;
    
    /**
     * Current byte offset within the output destination for writing new data.
     * Used to calculate object locations for the document's cross-reference table.
     * @var number
     */
    protected $_destinationOffset = 0;
    
    /**
     * The next free object number.
     * @var integer
     */
    protected $_nextObjectNumber = 1;
    
    /**
     * Cross-reference table for the latest generation of the document.
     * @var array
     */
    protected $_xrefTable = array();

    /**
     * Array of changed indirect objects for the document.
     * @var array
     */
    protected $_updatedObjects = array();
    
    /**
     * Used to track objects imported from other PDF documents to prevent
     * duplication. See {@link importObject()}.
     * @var array
     */
    protected $_importedObjects = array();
    


  /**** Class Methods ****/
  
    /**
     * Screens the document at the specified filesystem path and returns
     * true if it appears to be a valid PDF document, false if not.
     * 
     * For speed, the screening validates the base document structure, but
     * does not analyze the contents of the document. The returned answer
     * is more accurate than checking the filename's extension or looking
     * for the PDF header token alone, but the document may still contain
     * syntax or other errors.
     * 
     * @param string $filePath
     * @return boolean
     */
    public static function isPDFDocument($filePath)
    {
        require_once 'RE/FileParser/DataSource/File.php';
        require_once 'RE/FileParser/Pdf.php';

        try {
            $dataSource = new RE_FileParser_DataSource_File($filePath);
            $parser     = new RE_FileParser_Pdf($dataSource);
            $parser->screen();
        } catch (Exception $e) {
            return false;
        }
        
        return true;
    }

    /**
     * Creates and returns a new RE_Pdf_Document based on the contents of
     * the PDF document located at the specified filesystem path, setting the
     * desired output destination.
     * 
     * @param string          $filePath
     * @param integer         $destination     (optional) If omitted, uses
     *   {@link RE_Pdf_Document::DESTINATION_MEMORY}.
     * @param string|resource $destinationFile (optional) If the destination is
     *   {@link RE_Pdf_Document::DESTINATION_FILE}, the full filesystem path
     *   or file pointer resource for the output file.
     * @return RE_Pdf_Document
     * @throws RE_Pdf_Exception if the PDF document could not be read.
     */
    public static function load(
        $filePath, $destination = RE_Pdf_Document::DESTINATION_MEMORY, $destinationFile = null)
    {
        require_once 'RE/FileParser/DataSource/File.php';
        $dataSource = new RE_FileParser_DataSource_File($filePath);
        return self::_parseAndCloneDocument($dataSource, $destination, $destinationFile);
    }
    
    /**
     * Creates and returns a new RE_Pdf_Document based on the contents of
     * the PDF document passed as a binary string, setting the desired output
     * destination.
     * 
     * @param string          $pdfData
     * @param integer         $destination     (optional) If omitted, uses
     *   {@link RE_Pdf_Document::DESTINATION_MEMORY}.
     * @param string|resource $destinationFile (optional) If the destination is
     *   {@link RE_Pdf_Document::DESTINATION_FILE}, the full filesystem path
     *   or file pointer resource for the output file.
     * @return RE_Pdf_Document
     * @throws RE_Pdf_Exception if the PDF document could not be read.
     */
    public static function parse(
        $pdfData, $destination = RE_Pdf_Document::DESTINATION_MEMORY, $destinationFile = null)
    {
        require_once 'RE/FileParser/DataSource/String.php';
        $dataSource = new RE_FileParser_DataSource_String($pdfData);
        return self::_parseAndCloneDocument($dataSource, $destination, $destinationFile);
    }
  
  
  
  /**** Public Interface ****/


  /* Object Lifecycle */

    /**
     * Creates a new RE_Pdf_Document, setting the desired output destination.
     *
     * @param integer         $destination     (optional) If omitted, uses
     *   {@link RE_Pdf_Document::DESTINATION_MEMORY}.
     * @param string|resource $destinationFile (optional) If the destination is
     *   {@link RE_Pdf_Document::DESTINATION_FILE}, the full filesystem path
     *   or file pointer resource for the output file.
     */
    public function __construct($destination = RE_Pdf_Document::DESTINATION_MEMORY, $destinationFile = null)
    {
        switch ($destination) {
        case RE_Pdf_Document::DESTINATION_FILE:
            $this->_setDestinationFile($destinationFile);
            break;
        case RE_Pdf_Document::DESTINATION_MEMORY:
        case RE_Pdf_Document::DESTINATION_ECHO:
            $this->_destination = $destination;
            break;
        default:
            throw new InvalidArgumentException("Invalid output destination: $destination");
        }
        
        /* Create the base structural objects.
         */
        $this->_xrefTable[0] = array(0, 65535, 'f');
        $this->_trailer = new RE_Pdf_Document_Trailer();
        
        $this->_info    = new RE_Pdf_Document_Info();
        $this->_catalog = new RE_Pdf_Document_Catalog();

        $this->_trailer['Info'] = $this->addObject($this->_info);
        $this->_trailer['Root'] = $this->addObject($this->_catalog);

        $this->_updateFileIdentifier();
        
        $this->_pageTree = new RE_Pdf_Page_Tree(null, $this);
        $this->_catalog['Pages'] = $this->addObject($this->_pageTree);
        $this->_pageTree->setIndirectObject($this->_catalog['Pages']);
    }


  /* Accessor Methods */

    /**
     * Returns the PDF document's catalog object.
     *
     * @return RE_Pdf_Document_Catalog
     */
    public function getCatalog()
    {
        return $this->_catalog;
    }
  
    /**
     * Returns the PDF document's info object.
     *
     * @return RE_Pdf_Document_Info
     */
    public function getInfo()
    {
        return $this->_info;
    }
  
    /**
     * Returns the specified file identifier for the PDF document as a binary
     * string.
     * 
     * The file identifier provides a more reliable alternative to filenames
     * for identifying PDF documents. It is split into two parts: the first
     * part is a permanent identifier that is generated when the document is
     * created and never changes; the second part changes each time a new
     * revision of the document is saved, allowing you to detect if you are
     * looking at a different revision of the same base document.
     * 
     * @param string $part (optional) 'permanent' (default) or 'revision'
     * @return string Returns a binary string, which may need to be hex encoded
     *   for display or storage.
     */
    public function getFileID($part = 'permanent')
    {
        if ($part == 'revision') {
            return $this->_trailer['ID'][1]->getValue();
        } else {  // 'permanent'
            return $this->_trailer['ID'][0]->getValue();
        }
    }
    
  
  /* SplObserver Interface Methods */

    /**
     * Received by {@link RE_Pdf_Object_Indirect} objects when their direct
     * object's value changes. Guarantees that the indirect object will be
     * written to the output destination when the PDF document is finalized.
     *
     * @param SplSubject $subject A RE_Pdf_Object_Indirect object.
     * @throws InvalidArgumentException if the $subject is not a
     *   RE_Pdf_Object_Indirect object.
     * @throws RuntimeException if the indirect object is owned by another
     *   PDF document.
     */
    public function update(SplSubject $subject)
    {
        if (! $subject instanceof RE_Pdf_Object_Indirect) {
            throw new InvalidArgumentException(
                'Observed object is a ' . get_class($subject) . '; must be a RE_Pdf_Object_Indirect');
        }
        if ($subject->getSourcePDF() !== $this) {
            throw new RuntimeException('Indirect object is not owned by this PDF document.');
        }
        $this->_updatedObjects[$subject->getObjectNumber()] = $subject;
    }


  /* Page Management */
  
    /**
     * Creates and returns a new page object, appending it to the end of the
     * document's page tree.
     * 
     * @return RE_Pdf_Page
     */
    public function newPage()
    {
        return $this->_pageTree->newPage();
    }
    
    /**
     * Returns the specified numbered page (ordinal page number starting at 1)
     * from the document's page tree or null if the page does not exist.
     * 
     * @param integer $pageNumber
     * @return RE_Pdf_Page Returns null if the page number does not exist.
     */
    public function getPage($pageNumber)
    {
        return $this->_pageTree->getPage($pageNumber);
    }
    
    /**
     * Appends the specified page to the end of the document's page tree.
     * Returns the page's ordinal page number. Used to add independently-
     * created page objects to the document.
     * 
     * @todo Write this code
     * 
     * @param RE_Pdf_Page $page
     * @return integer
     */
    public function appendPage(RE_Pdf_Page $page)
    {
        throw new Exception('Not implemented yet');
    }
    
    /**
     * Returns the root page tree node for the document.
     * 
     * You should only use this method if you need to iterate or manipulate
     * the page tree directly. Otherwise, use the document object's convenience
     * methods {@link newPage()} and {@link getPage()}.
     * 
     * @return RE_Pdf_Page_Tree
     */
    public function getPageTree()
    {
        return $this->_pageTree;
    }
  
  
  /* RE_Pdf_Object_Extraction_Interface Methods */
  
    /**
     * Extracts the specified object from the PDF document.
     * 
     * The returned object is independent from the document--changes to it are
     * not tracked--which is useful for duplicating content or importing into
     * other PDF documents. If you intend to modify the object in-place and
     * want those changes saved automatically, use {@link getObject()} instead.
     * 
     * If the specified object does not exist, {@link RE_Pdf_Object_Null} is
     * returned (see PDF 1.7 Reference, section 3.2.9).
     *
     * @param integer $objectNumber
     * @param integer $generationNumber
     * @return RE_Pdf_Object
     * @throws RE_Pdf_Exception
     */
    public function extractObject($objectNumber, $generationNumber)
    {
        /* First check to see if the target object is already in our
         * update cache.
         */
        if ((isset($this->_updatedObjects[$objectNumber])) &&
            ($this->_updatedObjects[$objectNumber]->getGenerationNumber() == $generationNumber)) {
                return $this->_updatedObjects[$objectNumber]->getObject();                
        }
        
        /* Next see if it can be extracted from the original PDF document.
         */
        /**
         * @todo This needs to check to ensure the object number actually
         *   existed in the source PDF first. 
         */
        if (! empty($this->_sourcePDF)) {
            return $this->_sourcePDF->extractObject($objectNumber, $generationNumber);
        }
        
        /** @todo Write extraction code for memory and file destinations. */
        throw new Exception('Cannot yet extract objects that have already been finalized.');
        
        /* The target object could not be found. Return null.
         */
        return new RE_Pdf_Object_Null();
    }

  
  /* PDF Objects */
  
    /**
     * Like {@link extractObject()}, but tracks changes to the returned object,
     * ensuring those changes are included in the next generation of the
     * document the next time {@link save()} is called.
     *
     * @param integer $objectNumber
     * @param integer $generationNumber
     * @return RE_Pdf_Object
     * @throws RE_Pdf_Exception
     */
    public function getObject($objectNumber, $generationNumber)
    {
        $object = $this->extractObject($objectNumber, $generationNumber);
        
        /* Create an indirect object to watch for changes to the extracted object.
         */
        $indirectObject = new RE_Pdf_Object_Indirect($objectNumber, $generationNumber, $this);
        $object->attach($indirectObject);
        
        return $object;
    }
    
    /**
     * Adds the {@link RE_Pdf_Object} to the PDF document, assigning it
     * a new object number in the latest generation of the file. Returns a new
     * {@link RE_Pdf_Object_Indirect} object which can be used as a proxy.
     * 
     * The indirect object itself is not written to the output destination
     * until its {@link finalize()} method is called. This allows you to refer
     * to and use an object throughout the document before its final value is
     * known.
     * 
     * If the object is an indirect object that belongs to another PDF
     * document, or is a collection object (array or dictionary) that contains
     * indirect objects from another PDF document, the indirect object and ALL
     * DEPENDENCIES are imported into this document, and new indirect objects
     * are created for this document.
     * 
     * WARNING: Be careful when importing content from other PDF documents!!
     * Because all dependent objects must also be imported, you may end up
     * with many more objects that you expect!
     * 
     * For example, if you import an annotation object, you will get not only
     * the annotation, but its target page, that page's content stream and all
     * resources (fonts, images, etc.), all of the other annotations for the
     * page, plus the page's page tree, which will ultimately include *every
     * other page in the document* because of the dependencies in the page
     * tree itself.
     * 
     * In the vast majority of cases, it is better to simply create a new
     * object in this document, then copy the object values from the other
     * document, instead of importing the object directly. In addition, most
     * complex object types (pages, annotations, forms, etc.) have specialized
     * import methods that intelligently import only what is necessary instead
     * of the entire dependency tree; those methods should be used whenever
     * possible.
     * 
     * @param  RE_Pdf_Object          $object
     * @return RE_Pdf_Object_Indirect
     */
    public function addObject(RE_Pdf_Object $object)
    {
        if ($object instanceof RE_Pdf_Object_Indirect) {
            return $this->importObject($object);
        }
        
        /* First check to see if this object has already been added to this
         * PDF document; don't duplicate objects.
         */
        if (isset($this->_fileID)) {
            $objectNumber = $object->findObjectNumber($this->_fileID);
            if ($objectNumber) {
                return new RE_Pdf_Object_Indirect($objectNumber, 0, $this, $object);
            }
        }
        
        /* The generation number is always zero for new objects. We follow
         * Acrobat's latest behavior of never reusing object numbers, which
         * greatly simplifies the implementation. See PDF 1.7 Reference,
         * Appendix H, implementation note #16.
         */
        $objectNumber = $this->_nextObjectNumber++;
        $indirectObject = new RE_Pdf_Object_Indirect($objectNumber, 0, $this, $object);
        
        if (isset($this->_fileID)) {
            $object->recordObjectNumber($this->_fileID, $objectNumber);
        }
        
        /* New objects are always marked as changed so they are guaranteed
         * to be written to the output destination when the PDF document is
         * finalized.
         */
        $this->_updatedObjects[$objectNumber] = $indirectObject;

        $this->_importIndirectChildren($object);

        return $indirectObject;
    }
    
    /**
     * Imports the indirect object into the PDF document. If it already belongs
     * to this document, it is returned. If it belongs to another document, the
     * object and ALL DEPENDENCIES are copied from the source PDF and added to
     * this document. A new indirect object for this document will be returned.
     * 
     * WARNING: Be careful when importing content from other PDF documents!!
     * See explanatory text in the documentation for {@link addObject()}.
     * 
     * @param  RE_Pdf_Object_Indirect $object
     * @return RE_Pdf_Object_Indirect
     */
    public function importObject(RE_Pdf_Object_Indirect $object)
    {
        $sourcePDF = $object->getSourcePDF();
        if ($sourcePDF === $this) {
            return $object;
        }
        
        $fileID           = $sourcePDF->getFileID();
        $objectNumber     = $object->getObjectNumber();
        $generationNumber = $object->getGenerationNumber();
        
        /* If the object has already been imported, return an appropriate
         * indirect object; don't import it again.
         */
        if (isset($this->_importedObjects[$fileID][$objectNumber][$generationNumber])) {
            return new RE_Pdf_Object_Indirect(
                $this->_importedObjects[$fileID][$objectNumber][$generationNumber],
                0, $this);
        }
        
        $newObjectNumber = $this->_nextObjectNumber++;
        $this->_importedObjects[$fileID][$objectNumber][$generationNumber] = $newObjectNumber;

        $directObject = $object->getObject();
        $importedObject = new RE_Pdf_Object_Indirect($newObjectNumber, 0, $this, $directObject);
        $this->_updatedObjects[$newObjectNumber] = $importedObject;

        $this->_importIndirectChildren($directObject);
                
        return $importedObject;
    }
    
    /**
     * Writes a new generation of the indirect object to the output
     * destination.
     * 
     * @param RE_Pdf_Object_Indirect $object
     */
    public function finalizeObject(RE_Pdf_Object_Indirect $object, $debug = false)
    {
        static $finalizationQueue = array();
        static $finalizationCount = 0;

        $finalizationQueue[] = $object;
        $finalizationCount++;
        
        if ($finalizationCount > 1) {
            /* Objects are already being written to the output destination.
             * Exit here; the queue will be cleared completely below.
             */
             return;
        }
        
        /* Write the file header if it has not already been written.
         */
        if (! $this->_headerWritten) {
            $this->_writeHeader();
        }
        
        while (count($finalizationQueue)) {

            $object = array_shift($finalizationQueue);
            $objectNumber = $object->getObjectNumber();

            $this->_xrefTable[$objectNumber] = array(
                $this->_destinationOffset, $object->getGenerationNumber(), 'n'
                );
                
            $object->writeObject($this);
            
            unset($this->_updatedObjects[$objectNumber]);

            $finalizationCount--;
        }
    }
  
    
  /* PDF Version */
  
    /**
     * Returns major version number of the PDF document.
     *
     * @return integer
     */
    public function getMajorVersion()
    {
        return $this->_pdfMajorVersion;
    }

    /**
     * Returns minor version number of the PDF document.
     *
     * @return integer
     */
    public function getMinorVersion()
    {
        return $this->_pdfMinorVersion;
    }

    /**
     * Increases the minimum PDF version of the document. Called automatically
     * by certain complex objects that utilize PDF functionality introduced in
     * specific versions of the PDF specification.
     * 
     * @param integer $majorVersion
     * @param integer $minorVersion
     */
    public function setMinimumPDFVersion($majorVersion, $minorVersion)
    {
        if ($this->_pdfMajorVersion > $majorVersion) {
            return;
        }
        
        if ($this->_pdfMajorVersion == $majorVersion) {
            if ($this->_pdfMinorVersion >= $minorVersion) {
                return;
            }
            $this->_pdfMinorVersion = $minorVersion;
        } else {
            $this->_pdfMajorVersion = $majorVersion;
            $this->_pdfMinorVersion = $minorVersion;
        }
        
        $this->_catalog['Version'] = $this->_pdfMajorVersion . '.' . $this->_pdfMinorVersion;
    }
  
  
  /* PDF Generation */
  
    /**
     * Saves a new generation of the PDF document. Writes all changed objects
     * and the document's structural elements (catalog, trailer, etc.) to the
     * output destination.
     */
    public function save()
    {
        /* Update the document info dictionary. This is a method call so
         * subclasses can override the default behavior if desired.
         */
        $this->_updateDocumentInfo();
        
        /* Write the file header, followed by all unfinalized objects, followed
         * by the file trailer to the output destination.
         */
        $this->_writeHeader();
        $this->_finalizeObjects();
        $this->_writeTrailer();
        
        /* Clear the object update caches and xref table. The document can
         * actually be saved again if it's modified later, which will cause
         * a new generation to be written.
         */
        $this->_updatedObjects = array();
        $this->_xrefTable      = array();
        
        /* Each time the document is saved, the file identifier changes.
         */
        $this->_updateFileIdentifier();
    }
    
    /**
     * Saves a new generation of the PDF document and returns the entire
     * document as a binary string.
     * 
     * NOTE: Be careful when using this method as large PDF documents may
     * not fit completely in memory. If you are merely going to send the PDF
     * document to a web browser, use {@link send()} instead.
     * 
     * If you will be saving the document to a file, it is better to set the
     * output destination to {@link RE_Pdf_Document::DESTINATION_FILE} when
     * creating the new document, then calling {@link save()} when done.
     * 
     * @return string
     * @throws RE_Pdf_Exception if the current output destination is
     *   RE_Pdf_Document::DESTINATION_ECHO.
     */
    public function render()
    {
        switch ($this->_destination) {
        case RE_Pdf_Document::DESTINATION_FILE:
            $this->save();
            return file_get_contents($this->_destinationFile);

        case RE_Pdf_Document::DESTINATION_MEMORY:
            $this->save();
            return $this->_inMemoryBuffer;

        case RE_Pdf_Document::DESTINATION_ECHO:
            throw new RE_Pdf_Exception('Cannot use render() for DESTINATION_ECHO; use save() or send() instead.');
        }
    }
    
    /**
     * Saves a new generation of the PDF document and writes the entire
     * document to PHP's current output buffer (the same destination as
     * echo() or print() calls).
     * 
     * Optionally sets the Content-Type and Content-Length HTTP response
     * headers to the correct values.
     * 
     * @param boolean|object $sendHeaders Flag indicating whether or not to
     *   send headers using PHP's {@link header()} function; or a response
     *   object that implements a setHeader($name, $value) method.
     * @throws InvalidArgumentException if $sendHeaders is an object and it
     *   does not implement a setHeader() method.
     */
    public function send($sendHeaders = false)
    {
        $this->save();
        
        if (is_object($sendHeaders)) {
            if (! method_exists($sendHeaders, 'setHeader')) {
                throw InvalidArgumentException('Object must implement setHeader()');
            }
            $sendHeaders->setHeader('Content-Type',   'application/pdf');
            $sendHeaders->setHeader('Content-Length', $this->_destinationOffset);
        } else if ($sendHeaders) {
            header('Content-Type: application/pdf');
            header('Content-Length: ' . $this->_destinationOffset);
        }
        
        switch ($this->_destination) {
        case RE_Pdf_Document::DESTINATION_FILE:
            rewind($this->_destinationFile);
            fpassthru($this->_destinationFile);
            break;

        case RE_Pdf_Document::DESTINATION_MEMORY:
            echo $this->_inMemoryBuffer;
            break;

        case RE_Pdf_Document::DESTINATION_ECHO:
            /* The document has already been written to the output buffer.
             * Nothing else to do.
             */
            break;
        }
    }
    

  /* RE_Pdf_OutputDestination_Interface Methods */

    /**
     * Writes the bytes to the PDF document's output destination.
     * 
     * @param string $bytes
     * @throws RE_Pdf_Exception if an error occurs while attempting to write
     *   to the output destination.
     */
    public function writeBytes($bytes)
    {
        $length = strlen($bytes);
        if ($length < 1) {
            return;
        }

        switch ($this->_destination) {
        case RE_Pdf_Document::DESTINATION_FILE:
            /* $length is required to prevent magic_quotes_runtime from interfering.
             */
            $byteCount = fwrite($this->_destinationFile, $bytes, $length);
            if ($byteCount === false) {
                throw new RE_Pdf_Exception('Unknown error when writing to output destination file.');
            } else if ($byteCount !== $length) {
                throw new RE_Pdf_Exception("Only wrote $byteCount of expected $length bytes to output destination file.");
            }
            break;

        case RE_Pdf_Document::DESTINATION_MEMORY:
            if (($this->_destinationOffset + $length) > $this->_maxInMemorySize) {
                $this->_switchToTemporaryFile();
                $this->writeBytes($bytes);
                return;
            }
            $this->_inMemoryBuffer .= $bytes;
            break;

        case RE_Pdf_Document::DESTINATION_ECHO:
            echo $bytes;
            break;
        }

        $this->_destinationOffset += $length;
    }

    /**
     * Writes the entire source file to the PDF document's output destination.
     * 
     * @param string|resource $file Filesystem path or file pointer resource.
     * @throws InvalidArgumentException if the file is not a valid filesystem
     *   path or file pointer resource.
     * @throws RE_Pdf_Exception if an error occurs while attempting to copy
     *   the source file to the output destination.
     */
    public function writeFile($file)
    {
        if (is_resource($file)) {
            if (get_resource_type($file) != 'stream') {
                throw new InvalidArgumentException('Source file is not a stream resource.');
            }
            
            $info = fstat($file);
            $length = $info['size'];
            if ($length < 1) {
                return;
            }
            
            switch ($this->_destination) {
            case RE_Pdf_Document::DESTINATION_FILE:
                $currentOffset = ftell($file);
                $byteCount = stream_copy_to_stream($file, $this->_destinationFile, $length, 0);
                fseek($file, $currentOffset, SEEK_SET);
                if ($byteCount === false) {
                    throw new RE_Pdf_Exception('Unknown error when copying source file to output destination file.');
                } else if ($byteCount != $length) {
                    throw new RE_Pdf_Exception("Only copied $byteCount of expected $length bytes from source file to output destination file.");
                }
                break;

            case RE_Pdf_Document::DESTINATION_MEMORY:
                if (($this->_destinationOffset + $length) > $this->_maxInMemorySize) {
                    $this->_switchToTemporaryFile();
                    $this->writeFile($file);
                    return;
                }
                $currentOffset = ftell($file);
                $contents = stream_get_contents($file, $length, 0);
                fseek($file, $currentOffset, SEEK_SET);
                if ($contents === false) {
                    throw new RE_Pdf_Exception('Unknown error when reading from source file.');
                }
                $this->_inMemoryBuffer .= $contents;
                unset($contents);
                break;

            case RE_Pdf_Document::DESTINATION_ECHO:
                $currentOffset = ftell($file);
                rewind($file);
                $byteCount = fpassthru($file);
                fseek($file, $currentOffset, SEEK_SET);
                if ($byteCount === false) {
                    throw new RE_Pdf_Exception('Unknown error when reading from source file.');
                } else if ($byteCount != $length) {
                    throw new RE_Pdf_Exception("Only read $byteCount of expected $length bytes from source file.");
                }
                break;
            }
                        
            $this->_destinationOffset += $length;

        } else if (is_dir($file)) {
            throw new InvalidArgumentException("Source file may not be a directory: $file");

        } else if ((is_file($file)) || (is_link($file))) {
            if (! is_readable($file)) {
                throw new InvalidArgumentException("Source file is not readable: $file");
            }

            $length = filesize($file);
            if ($length < 1) {
                return;
            }

            switch ($this->_destination) {
            case RE_Pdf_Document::DESTINATION_FILE:
                $fileResource = @fopen($file, 'rb');
                if ($fileResource === false) {
                    throw new RE_Pdf_Exception("Cannot open source file for reading: $file");
                }
                $byteCount = stream_copy_to_stream($fileResource, $this->_destinationFile);
                fclose($fileResource);
                if ($byteCount === false) {
                    throw new RE_Pdf_Exception('Unknown error when copying source file to output destination file.');
                } else if ($byteCount != $length) {
                    throw new RE_Pdf_Exception("Only copied $byteCount of expected $length bytes from source file to output destination file.");
                }
                break;

            case RE_Pdf_Document::DESTINATION_MEMORY:
                if (($this->_destinationOffset + $length) > $this->_maxInMemorySize) {
                    $this->_switchToTemporaryFile();
                    $this->writeFile($file);
                    return;
                }
                $contents = file_get_contents($file);
                if ($contents === false) {
                    throw new RE_Pdf_Exception('Unknown error when reading from source file.');
                }
                $this->_inMemoryBuffer .= $contents;
                unset($contents);
                break;

            case RE_Pdf_Document::DESTINATION_ECHO:
                $byteCount = @readfile($file);
                if ($byteCount === false) {
                    throw new RE_Pdf_Exception('Unknown error when reading from source file.');
                } else if ($byteCount != $length) {
                    throw new RE_Pdf_Exception("Only read $byteCount of expected $length bytes from source file.");
                }
                break;
            }

            $this->_destinationOffset += $length;
            
        } else {
            throw new InvalidArgumentException("Source file does not exist: $file");
        }
    }



  /**** Internal Interface ****/


  /* Document Import */
  
    /**
     * Parses the PDF document contained in the specified data source object
     * and creates a new RE_Pdf_Document based on its contents.
     * 
     * @param RE_FileParser_DataSource_Interface $dataSource
     * @param integer                            $destination
     * @param string|resource                    $destinationFile
     * @return RE_Pdf_Document
     * @throws RE_Pdf_Exception if the PDF document could not be read.
     */
    protected static function _parseAndCloneDocument(
        RE_FileParser_DataSource_Interface $dataSource, $destination, $destinationFile)
    {
        /* Create the new PDF document and parser, link them, and parse the
         * source document.
         */
        $pdf = new self($destination, $destinationFile);

        require_once 'RE/FileParser/Pdf.php';
        $parser = new RE_FileParser_Pdf($dataSource);

        $pdf->_sourcePDF = $parser;
        $parser->setOwningDocument($pdf);

        $parser->parse();
        
        /* Reset the state of the new PDF document to match the parsed PDF.
         */
        $pdf->_updatedObjects = array();
        
        $pdf->_pdfMajorVersion = $parser->getMajorVersion();
        $pdf->_pdfMinorVersion = $parser->getMinorVersion();
        
        /* Ensure that the source document has a file identifier. This call
         * will generate an identifier if it is missing.
         */
        $pdf->_fileID = $parser->getFileID();
        
        $pdf->_trailer  = $parser->getTrailer();
        $pdf->_info     = $parser->getInfo();
        $pdf->_catalog  = $parser->getCatalog();
        $pdf->_pageTree = $pdf->_catalog['Pages']->getObject();
        $pdf->_pageTree->setIndirectObject($pdf->_catalog['Pages']);

        $pdf->_nextObjectNumber = $pdf->_trailer['Size']->getValue();
        $pdf->_xrefTable        = array();

        /* The returned document info object is unlinked from the indirect
         * object in the file trailer (see note in the $parser->getTrailer()
         * method). Create a new indirect object to watch for changes.
         */
        if (empty($pdf->_trailer['Info'])) {
            $pdf->_trailer['Info'] = $pdf->addObject($pdf->_info);
        } else {
            $indirectObject = new RE_Pdf_Object_Indirect(
                $pdf->_trailer['Info']->getObjectNumber(), $pdf->_trailer['Info']->getGenerationNumber(), $pdf);
            $pdf->_info->attach($indirectObject);
        }

        /* Be sure there are no lingering changes after the state manipulation.
         */
        $pdf->_updatedObjects = array();

        return $pdf;
    }

  
  /* PDF Objects */
  
    /**
     * Helper method for {@link addObject()}. Recursively walks collection
     * objects looking for indirect objects that need to be imported.
     * 
     * @param RE_Pdf_Object $object
     */
    protected function _importIndirectChildren($object)
    {
        if ((! $object instanceof RE_Pdf_Object_Array) &&
            (! $object instanceof RE_Pdf_Object_Dictionary)) {
                return;
        }
        
        /* Because of the recursive nature of this method, the object may be
         * revisited and its iterator may be reset in the middle of the loop.
         * To ensure we visit every value exactly once, obtain the keys first
         * and iterate the keys array instead.
         */
        $keys = array();
        foreach ($object as $key => $value) {
            $keys[] = $key;
        }
        
        foreach ($keys as $key) {
            $value = $object[$key];
            if ($value instanceof RE_Pdf_Object_Indirect) {
                $object[$key] = $this->importObject($value);
            } else if (($value instanceof RE_Pdf_Object_Array) ||
                       ($value instanceof RE_Pdf_Object_Dictionary)) {
                $this->_importIndirectChildren($value);
            }
        }
    }
    

  /* PDF Generation */
  
    /**
     * Updates the entries in the document's info dictionary. Subclasses may
     * override this method to change the default behavior if desired.
     */
    protected function _updateDocumentInfo()
    {
        /** @todo Make this a RE_Pdf_Object_Date object instead. */
        $this->_info['ModDate'] = substr_replace(date('\D\:YmdHisO'), "'", -2, 0) . "'";
        
        if (empty($this->_info['CreationDate'])) {
            $this->_info['CreationDate'] = $this->_info['ModDate']->getValue();
        }
        
        if (empty($this->_info['Producer'])) {
            $this->_info['Producer'] = 'RE Framework';
        }
    }

    /**
     * Updates the file identifier for the PDF document.
     */
    protected function _updateFileIdentifier()
    {
        /* The file identifier is an MD5 digest of the current time, the source
         * document's location, the file size in bytes, and the full contents
         * of the information dictionary as described in PDF Reference 1.7,
         * Section 10.3.
         */
        $fileID = md5(microtime(true), true);
        
        $fileID = md5($fileID . '|' . $this->_destination, true);
        
        $fileID = md5($fileID . '|' . $this->_destinationOffset, true);
        
        if (! empty($this->_info)) {
            foreach ($this->_info as $key => $object) {
                $fileID = md5($fileID . '|' . $key . '|' . $object->getValue(), true);
            }
        }
        
        $fileIDObject = new RE_Pdf_Object_String($fileID);
        $fileIDObject->setWriteAsHex(true);
        $fileIDObject->setUsesEncryption(false);

        /* If this is the first time a file identifier has been created for
         * this document, set both strings to the same value. Otherwise, only
         * update the second string.
         */
        if (! isset($this->_trailer['ID'])) {
            $this->_trailer['ID'] = new RE_Pdf_Object_Array(array($fileIDObject, $fileIDObject));
        } else {
            $this->_trailer['ID'][1] = $fileIDObject;
        }
        
        $this->_fileID = $fileID;
    } 

    /**
     * Writes the file header to the output destination. If this PDF document
     * was loaded from an existing document, writes the contents of that source
     * document as well.
     * 
     * The base PDF version is always 1.4 to maximize backwards-compatibility.
     * If PDF features are used that require a higher PDF version, the newer
     * version number will be written to the document catalog object (see
     * {@link setMinimumPDFVersion()}).
     */
    protected function _writeHeader()
    {
        if ($this->_headerWritten) {
            return;
        }
        $this->_headerWritten = true;
        
        /* The comment on the second line contains four binary characters,
         * which serve as a hint to older systems that this file should be
         * treated as a binary file, not a text file. See PDF 1.7 Reference,
         * section 3.4.1.
         */
        $headerString = "%PDF-1.4\n%\xb1\xd7\xf7\xbf\n";
        
        if (empty($this->_sourcePDF)) {
            $this->writeBytes($headerString);
            return;
        }

        /* Check for a higher base PDF version in the source document. If not
         * at least 1.4, write our own version string. Otherwise, leave the
         * existing version string alone.
         */
        if (($this->_sourcePDF->getHeaderMajorVersion() <= 1) &&
            ($this->_sourcePDF->getHeaderMinorVersion() <  4)) {
                $this->writeBytes($headerString);
        }

        /* Copy the complete contents of the original document to the output
         * destination; subsequent changes will be included as part of a new
         * generation of the document.
         */
        $dataSource = $this->_sourcePDF->getDataSource();
        if ($dataSource instanceof RE_FileParser_DataSource_File) {
            $this->writeFile($dataSource->getFilePath());
        } else {
            $this->writeBytes($dataSource->getAllBytes());
        }
    }
    
    /**
     * Writes a new generation of all unfinalized objects to the output
     * destination.
     * 
     * @throws RE_Pdf_Exception if any objects were updated during the
     *   finalization process.
     */
    protected function _finalizeObjects()
    {
        $objects = $this->_updatedObjects;
        $this->_updatedObjects = array();
        
        foreach ($objects as $object) {
            $this->finalizeObject($object);
        }
        
        if (empty($this->_updatedObjects)) {
            return;
        }

        /* Some complex object types may need to create ancillary objects
         * during finalization. Make one final pass to finalize these new
         * objects as well.
         */
        $objects = $this->_updatedObjects;
        $this->_updatedObjects = array();
        
        foreach ($objects as $object) {
            $this->finalizeObject($object);
        }

        /* If there are newly-created objects again, something went wrong.
         */
        if (! empty($this->_updatedObjects)) {
            throw new RE_Pdf_Exception('An object was updated during finalization');
        }
    }
  
    /**
     * Writes the cross-reference table and the file trailer dictionary to the
     * output destination.
     */
    protected function _writeTrailer()
    {
        /* If the cross-reference table is empty, there are no changed objects.
         * Do not write an empty file trailer.
         */
        if (empty($this->_xrefTable)) {
            return;
        }
        
        $xrefOffset = $this->_destinationOffset;
        $this->writeBytes("xref\n");
        
        /* Objects may be finalized in any order, but the cross-reference table
         * entries must be sorted in ascending numerical order.
         */
        $objectNumbers = array_keys($this->_xrefTable);
        sort($objectNumbers, SORT_NUMERIC);
        
        /* Break the full list into subtables of contiguous object number blocks.
         */
        $firstEntry = reset($objectNumbers);
        $entryCount = 0;
        $subsection = '';
        
        foreach ($objectNumbers as $objectNumber) {
            if (($firstEntry + $entryCount) != $objectNumber) {
                $this->writeBytes($firstEntry . ' ' . $entryCount . "\n");
                $this->writeBytes($subsection);
                $firstEntry = $objectNumber;
                $entryCount = 0;
                $subsection = '';
            }

            $subsection .= sprintf("%010d %05d %s \n", $this->_xrefTable[$objectNumber][0],
                                   $this->_xrefTable[$objectNumber][1], $this->_xrefTable[$objectNumber][2]);
            $entryCount++;
        }
        $this->writeBytes($firstEntry . ' ' . $entryCount . "\n");
        $this->writeBytes($subsection);
        
        /* Write the file trailer dictionary.
         */
        $this->writeBytes("trailer\n");
        $this->_trailer['Size'] = $this->_nextObjectNumber;
        $this->_trailer->write($this);
        
        /* Write the file trailer itself. In case the document is saved again,
         * update the Prev entry in the file trailer dictionary to record the
         * offset of the just-written xref table.
         */
        $this->writeBytes("\nstartxref\n" . $xrefOffset . "\n%%EOF\n");
        $this->_trailer['Prev'] = $xrefOffset;
    }
    

  /* Output Destination */
  
    /**
     * Sets the output destination to the specified file, which may either be
     * a full filesystem path or a file pointer resource.
     * 
     * If you supply a filesystem path and the file exists, IT WILL BE
     * OVERWRITTEN. Otherwise a new file will be created.
     *
     * If you supply a file pointer resource, the PDF document will be written
     * starting at the current file position.
     * 
     * @param string|resource $file Filesystem path or file pointer resource.
     * @throws BadMethodCallException if the previous output destination was
     *   {@link RE_Pdf_Document::DESTINATION_ECHO} and any data has already
     *   been written.
     * @throws InvalidArgumentException if the file is not a valid filesystem
     *   path or file pointer resource.
     * @throws RE_Pdf_Exception if an error occurs while attempting to read
     *   from or write to the file.
     */
    protected function _setDestinationFile($file)
    {
        if (($this->_destination == RE_Pdf_Document::DESTINATION_ECHO) &&
            ($this->_destinationOffset > 0)) {
                throw new BadMethodCallException('Cannot change output destination from DESTINATION_ECHO.');
        }
        
        if (is_resource($file)) {
            if (get_resource_type($file) != 'stream') {
                throw new InvalidArgumentException('File output destination is not a stream resource.');
            }
            $this->_baseOffset = ftell($file);
            $this->_destinationFile = $file;

        } else if (is_dir($file)) {
            throw new InvalidArgumentException("File output destination may not be a directory: $file");

        } else if ((is_file($file)) || (is_link($file))) {
            if (! is_writable($file)) {
                throw new InvalidArgumentException("File output destination is not writable: $file");
            }
            $this->_destinationFile = @fopen($file, 'w+b');
            if ($this->_destinationFile === false) {
                throw new RE_Pdf_Exception("Cannot open file output destination for writing: $file");
            }

        } else {  // create file
            $this->_destinationFile = @fopen($file, 'w+b');
            if ($this->_destinationFile === false) {
                throw new RE_Pdf_Exception("Cannot create file output destination for writing: $file");
            }
        }

        $this->_destination = RE_Pdf_Document::DESTINATION_FILE;
    }

    /**
     * Redirects the output destination to a temporary file. Used when the
     * in-memory buffer exceeds the threshold set by {@link $_maxInMemorySize}.
     * 
     * @throws BadMethodCallException if the previous output destination was
     *   not {@link RE_Pdf_Document::DESTINATION_MEMORY}.
     * @throws RE_Pdf_Exception if an error occurs while attempting to create
     *   or write to the temporary file.
     */
    protected function _switchToTemporaryFile()
    {
        if ($this->_destination != RE_Pdf_Document::DESTINATION_MEMORY) {
           throw new BadMethodCallException('Output destination was not DESTINATION_MEMORY.');
        }
        
        $file = tmpfile();
        if ($file === false) {
            throw new RE_Pdf_Exception('Unable to create temporary file.');
        }
        
        $this->_setDestinationFile($file);
        
        /* Flush the in-memory buffer to the temporary file. $length is
         * required to prevent magic_quotes_runtime from interfering.
         */
        $length = $this->_destinationOffset;
        $byteCount = fwrite($file, $this->_inMemoryBuffer, $length);
        if ($byteCount === false) {
            throw new RE_Pdf_Exception('Unknown error when flushing in-memory buffer to temporary file.');
        } else if ($byteCount !== $length) {
            throw new RE_Pdf_Exception("Only flushed $byteCount of expected $length bytes from in-memory buffer to temporary file.");
        }

        $this->_inMemoryBuffer = '';
    }

}
