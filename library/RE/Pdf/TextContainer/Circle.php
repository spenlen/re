<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage TextLayout
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * Defines a solid circular region.
 *
 * @package    Pdf
 * @subpackage TextLayout
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Pdf_TextContainer_Circle extends RE_Pdf_TextContainer
{
  /**** Public Interface ****/


  /* Concrete Subclass Implementation */
  
    /**
     * Constrains the proposed line fragment rectangle to the internal region
     * of the largest circle that fits within the text container's bounding
     * rectangle.
     *
     * @param RE_Rect $proposedRect
     * @param integer $sweepDirection
     * @param integer $movementDirection
     * @param RE_Rect $remainingRect
     * @return RE_Rect
     */
    public function lineFragmentRectForProposedRect(
        RE_Rect $proposedRect, $sweepDirection, $movementDirection, RE_Rect $remainingRect)
    {
        /* For a solid circle, there will never be a remainder.
         */
        $remainingRect->setWidth(0);
        $remainingRect->setHeight(0);

        $lineFragmentRect = $this->_constrainProposedRectToBoundingRect($proposedRect, $sweepDirection, $movementDirection);
        
        /* Adjust the line fragment's origin and width to fit within the circle.
         */
        $radius = min($this->_boundingRect->getWidth(), $this->_boundingRect->getHeight()) / 2.0;
        
        $verticalPosition = abs(($lineFragmentRect->getY() + ($lineFragmentRect->getHeight() / 2.0)) - $radius);
        if ($verticalPosition >= $radius) {
            return new RE_Rect();
        }
        
        $newWidth = sqrt(($radius * $radius) - ($verticalPosition * $verticalPosition)) * 2.0;
        
        $lineFragmentRect->setX($lineFragmentRect->getX() + $radius - ($newWidth / 2.0));
        $lineFragmentRect->setWidth($newWidth);
        
        return $lineFragmentRect;
    }


  /* Text Layout */

    /**
     * Overridden to return false since this text container shape is not a
     * rectangle.
     * 
     * @return boolean
     */
    public function isSimpleRectangularTextContainer()
    {
        return false;
    }

}
