<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage TextLayout
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * Defines a non-rotated, solid rectangular region that may contain one or more
 * rectangular holes where text cannot be laid out.
 *
 * @package    Pdf
 * @subpackage TextLayout
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Pdf_TextContainer_Rect extends RE_Pdf_TextContainer
{
  /**** Instance Variables ****/

    /**
     * Array of RE_Rect objects defining holes where text cannot be laid out.
     * @var array
     */
    var $_holes = array();



  /**** Public Interface ****/


  /* Concrete Subclass Implementation */

    /**
     * Constrains the proposed line fragment rectangle to the text container's
     * bounding rectangle, avoiding all holes.
     *
     * @param RE_Rect $proposedRect
     * @param integer $sweepDirection
     * @param integer $movementDirection
     * @param RE_Rect $remainingRect
     * @return RE_Rect
     */
    public function lineFragmentRectForProposedRect(
        RE_Rect $proposedRect, $sweepDirection, $movementDirection, RE_Rect $remainingRect)
    {
        /* In most cases, there will not be a remainder.
         */
        $remainingRect->setWidth(0);
        $remainingRect->setHeight(0);

        /* First make the proposed rect fit completely within the bounds.
         */
        $lineFragmentRect = $this->_constrainProposedRectToBoundingRect($proposedRect, $sweepDirection, $movementDirection);

        if (($lineFragmentRect->isEmpty()) || (empty($this->_holes))) {
            return $lineFragmentRect;
        }

        /* Look for intersections with holes.
         */
        $intersections = array();
        foreach ($this->_holes as $hole) {
            if ($lineFragmentRect->intersectsRect($hole)) {
                $intersections[] = $lineFragmentRect->rectByIntersectingRect($hole);
            }
        }

        /* If there are no intersections, simply return the constrained rect.
         */
        if (empty($intersections)) {
            return $lineFragmentRect;
        }

        /* Find the closest intersection to the proposed rect's origin in the
         * sweep direction.
         */
        $rect = reset($intersections);
        for ($i = 1; $i < count($intersections); $i++) {
            switch ($sweepDirection) {
                case RE_Pdf_TextContainer::LINE_SWEEP_RIGHT:
                    if ($intersections[$i]->minX() < $rect->minX()) {
                        $rect = $intersections[$i];
                    }
                    break;
                case RE_Pdf_TextContainer::LINE_SWEEP_LEFT:
                    if ($intersections[$i]->maxX() > $rect->maxX()) {
                        $rect = $intersections[$i];
                    }
                    break;
                case RE_Pdf_TextContainer::LINE_SWEEP_DOWN:
                    if ($intersections[$i]->minY() < $rect->minY()) {
                        $rect = $intersections[$i];
                    }
                    break;
                case RE_Pdf_TextContainer::LINE_SWEEP_UP:
                    if ($intersections[$i]->maxY() > $rect->maxY()) {
                        $rect = $intersections[$i];
                    }
                    break;
            }
        }

        /* Trim the line fragment rect to terminate at the intersection point
         * and calculate the remaining rect.
         */
        $remainingRect->setOrigin($lineFragmentRect->getOrigin());
        $remainingRect->setSize($lineFragmentRect->getSize());

        switch ($sweepDirection) {
            case RE_Pdf_TextContainer::LINE_SWEEP_RIGHT:
                $lineFragmentRect->setWidth($rect->minX() - $lineFragmentRect->minX());
                $delta = $rect->maxX() - $lineFragmentRect->getX();
                $remainingRect->setX($remainingRect->getX() + $delta);
                $remainingRect->setWidth($remainingRect->getWidth() - $delta);
                break;
            case RE_Pdf_TextContainer::LINE_SWEEP_LEFT:
                $lineFragmentRect->setWidth($lineFragmentRect->maxX() - $rect->maxX());
                $lineFragmentRect->setX($lineFragmentRect->getX() + $rect->maxX());
                $remainingRect->setWidth($rect->minX());
                break;
            case RE_Pdf_TextContainer::LINE_SWEEP_DOWN:
            case RE_Pdf_TextContainer::LINE_SWEEP_UP:
                /** @todo Implement vertical line sweep. */
                throw new Exception('Not implemented yet');
                break;
        }

        return $lineFragmentRect;
    }


  /* Layout Holes */

    /**
     * Adds a hole to the text container. Holes are defined in the text
     * container's coordinate system, which starts at {0, 0} in the upper-
     * left corner.
     *
     * @param RE_Rect $hole
     */
    public function addHole(RE_Rect $hole)
    {
        $this->_holes[] = clone $hole;
    }


  /* Text Layout */

    /**
     * Overridden to return false if the text container has holes.
     *
     * @return boolean
     */
    public function isSimpleRectangularTextContainer()
    {
        return empty($this->_holes);
    }


  /* Debugging */

    /**
     * Overridden to additionally draw debug rectangles for the container's
     * hole rects.
     */
    public function drawDebug()
    {
        parent::drawDebug();

        $path = new RE_Path();
        foreach ($this->_holes as $hole) {
            $path->appendRect($hole);
        }
        $path->setLineDashPattern(array(4, 4));
        $path->stroke($this->getCanvas());
    }

}
