<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage DocumentStructure
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @copyright  Copyright (c) 2005-2008 Zend Technologies USA, Inc. (http://www.zend.com)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */

/* Portions of this source file are excerpted from the Zend Framework:
 * 
 * Copyright (c) 2005-2008, Zend Technologies USA, Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 *  * Neither the name of Zend Technologies USA, Inc. nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


/**
 * RE_Pdf_Page objects represent the imagable pages that comprise a PDF
 * document. Page objects are primarily responsible for maintaining one or
 * more content streams which describe the visual appearance of the page,
 * as well as the resources (images, fonts, etc.), annotations, and other
 * specialty objects that appear on the page.
 * 
 * Page objects are typically obtained through the document object's
 * convenience methods {@link RE_Pdf_Document::newPage()} and
 * {@link RE_Pdf_Document::getPage()}. You will rarely need to create
 * them directly.
 * 
 * @package    Pdf
 * @subpackage DocumentStructure
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @copyright  Copyright (c) 2005-2008 Zend Technologies USA, Inc. (http://www.zend.com)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Pdf_Page extends RE_Pdf_Object_Dictionary implements RE_Pdf_Canvas_Interface
{
  /**** Class Constants ****/


  /* Standard Page Sizes */

    /**
     * Size representing a US Letter page in portrait (tall) orientation.
     */
    const SIZE_LETTER = '{612, 792}';

    /**
     * Size representing an A4 page in portrait (tall) orientation.
     */
    const SIZE_A4     = '{595, 842}';



  /**** Class Variables ****/
  
    /**
     * Default page size used for newly-created page objects.
     * @var RE_Size
     */
    protected static $_defaultPageSize = null;



  /**** Instance Variables ****/
  
    /**
     * Source PDF document; pages belong to exactly one document.
     * @var RE_Pdf_Object_Extraction_Interface
     */
    protected $_sourcePDF = null;

    /**
     * The page's current content stream object.
     * @var RE_Pdf_ContentStream
     */
    protected $_contentStream = null;

    /**
     * Indirect object for the page's current content stream object.
     * @var RE_Pdf_Object_Indirect
     */
    protected $_contentStreamObject = null;

    /**
     * Array of procedure set names used by all content streams on the page.
     * @var array
     */
    protected $_procSets = array();
    
    /**
     * Array of recently-added entries to the resources dictionary.
     * @var array
     */
    protected $_resources = array();



  /**** Class Methods ****/
  
    /**
     * Returns an array of standard page sizes suitable for use in HTML
     * SELECT elements.
     * 
     * @return array
     */
    public static function getPageSizesArray()
    {
        return array(
            '{612, 792}'   => '8½ × 11″ (US Letter)',
            '{612, 1008}'  => '8½ × 14″ (US Legal)',
            '{792, 1224}'  => '11 × 17″ (US Tabloid/Ledger)',
            '{297, 684}'   => '4⅛ × 9½″ (Envelope #10)',
            '{298, 420}'   => '105 × 148 mm (A6)',
            '{420, 595}'   => '148 × 210 mm (A5)',
            '{595, 842}'   => '210 × 297 mm (A4)',
            '{842, 1190}'  => '297 × 420 mm (A3)',
            '{1190, 1684}' => '420 × 594 mm (A2)',
            '{323, 459}'   => '114 × 162 mm (Envelope C6)',
            '{459, 649}'   => '162 × 229 mm (Envelope C5)',
            '{649, 919}'   => '229 × 324 mm (Envelope C4)',
            );
    }

    /**
     * Returns the default page size for newly-created page objects.
     * 
     * If not changed by {@link setDefaultPageSize()}, the default page size
     * is US Letter, portrait orientation (8.5 x 11 inches).
     * 
     * @return RE_Size
     */
    public static function getDefaultPageSize()
    {
        if (is_null(self::$_defaultPageSize)) {
            self::setDefaultPageSize(RE_Pdf_Page::SIZE_LETTER);
        }
        return clone self::$_defaultPageSize;
    }

    /**
     * Sets the default page size for newly-created page objects.
     * 
     * @param RE_Size|string $size
     */
    public static function setDefaultPageSize($size)
    {
        if ((is_object($size)) && ($size instanceof RE_Size)) {
            self::$_defaultPageSize = clone $size;
        } else {
            self::$_defaultPageSize = RE_Size::fromString($size);
        }
    }


  
  /**** Public Interface ****/


  /* Concrete Subclass Implementation */

    /**
     * Extracts the page object from the specified file parser at its current
     * read offset. Returns the extracted object and moves the offset past the
     * the object and any trailing white space.
     *
     * @param  RE_FileParser_Pdf $parser
     * @return RE_Pdf_Page
     * @throws RE_Pdf_Exception if a page object is not present at the
     *   current parser offset.
     */
    public static function extract(RE_FileParser_Pdf $parser)
    {
        $offset = $parser->getOffset();

        $object = RE_Pdf_Object_Dictionary::extract($parser);
        if (! $object instanceof self) {
            throw new RE_Pdf_Exception('Page object not found, found other dictionary object instead '
                                         . 'at parser offset ' . sprintf('0x%x', ($offset)));
        }
        
        if (! empty($object['Resources']['ProcSet'])) {
            foreach ($object['Resources']['ProcSet'] as $procSet) {
                $object->_procSets[$procSet->getValue()] = true;
            }
        }
        
        return $object;
    }


  /* Object Lifecycle */

    /**
     * Creates a new RE_Pdf_Page, establishing the association with the
     * source PDF document, and optionally setting the dictionary's initial
     * contents with the contents of the specified associative array.
     *
     * Page objects are typically obtained through the document object's
     * convenience methods {@link RE_Pdf_Document::newPage()} and
     * {@link RE_Pdf_Document::getPage()}. You will rarely need to create
     * them directly.
     * 
     * @param array $value (optional)
     * @param RE_Pdf_Object_Extraction_Interface $sourcePDF
     */
    public function __construct($value = null, RE_Pdf_Object_Extraction_Interface $sourcePDF)
    {
        parent::__construct($value);
        
        if (! isset($this['Type'])) {
            $this['Type'] = 'Page';
        }

        /* If a page dictionary is provided to the constructor, it is assumed
         * that all of the required entries will be present. Only create the
         * defaults for new page objects.
         */
        if (is_null($value)) {
            
            $this['Resources'] = new RE_Pdf_Object_Dictionary();
        
            /* Procedure sets are obsolete as of PDF 1.4 (see PDF 1.7 Reference,
             * Section 10.1). To simplify our implementation, always add the two
             * most common ProcSet entries to the stream's resource dictionary.
             * Modern PDF viewers always ignore these entries.
             */
            $this->addProcSet('PDF');
            $this->addProcSet('Text');

            $this->setPageSize(self::getDefaultPageSize());
        }
        
        if ($sourcePDF instanceof RE_FileParser_Pdf) {
            $this->_sourcePDF = $sourcePDF->getOwningDocument();
        } else {
            $this->_sourcePDF = $sourcePDF;
        }
    }


  /* Accessor Methods */
  
    /**
     * Returns the source PDF document.
     *
     * @return RE_Pdf_Object_Extraction_Interface
     */
    public function getSourcePDF()
    {
        return $this->_sourcePDF;
    }


  /* Page Boundaries */
  
    /**
     * Convenience method which sets the size of the page's media box (the
     * unfinished paper size) and sets its orgin at {0, 0}.
     * 
     * @param RE_Size|string $size
     */
    public function setPageSize($size)
    {
        if (! ((is_object($size)) && ($size instanceof RE_Size))) {
            $size = RE_Size::fromString($size);
        }
        $this->setMediaBox(new RE_Rect(new RE_Point(), $size));
    }
  
    /**
     * Returns size of the page's media box (the unfinished paper size).
     * 
     * NOTE: The returned size may not accurately describe the page's
     * imagable area. See the additional discussion in {@link setCropBox()},
     * {@link setBleedBox()}, {@link setTrimBox()}, and {@link setArtBox()}.
     * 
     * If you are adding content to an existing page, you should call
     * {@link getCropBox()} instead to obtain the origin and size of the
     * visible region and ensure your drawing is constrained to that box.
     * 
     * @return RE_Size
     */
    public function getPageSize()
    {
        $mediaBox = $this->getMediaBox();
        return clone $mediaBox->getSize();
    }
    
    /**
     * Sets the media box, the boundary of the physical medium (the unfinished
     * paper size) of the page.
     * 
     * This region may include content not intended for the final finished page
     * such as registration, crop, and other printing marks. This content is
     * typically masked by the page's bleed and/or trim boxes.
     * 
     * @param RE_Rect $mediaBox
     */
    public function setMediaBox(RE_Rect $mediaBox)
    {
        $this['MediaBox'] = new RE_Pdf_Object_Rectangle($mediaBox);
    }

    /**
     * Returns the media box, the boundary of the physical medium (the
     * unfinished paper size) of the page.
     * 
     * @return RE_Rect
     */
    public function getMediaBox()
    {
        if (isset($this['MediaBox'])) {
            $box = $this['MediaBox']->getObject();
        } else {
            /* If the page dictionary does not contain a MediaBox entry, it is
             * inherited from an ancestor page tree node. Fetch the MediaBox
             * from the closest ancestor.
             */
            $box = $this['Parent']->getObject()->getMediaBox();
        }

        /* There is no default for the MediaBox. If one cannot be found, throw
         * an exception.
         */
        if (! $box) {
            throw new RE_Pdf_Exception('MediaBox not found in page dictionary or ancestor page tree node.');
        }
        
        if (! $box instanceof RE_Pdf_Object_Rectangle) {
            $box = new RE_Pdf_Object_Rectangle($box);
        }
        return clone $box->getValue();
    }
  
    /**
     * Sets the crop box, the default visible region of the page. Content
     * appearing outside this region is generally not displayed or printed.
     * 
     * @param RE_Rect|null $cropBox Set to null to remove.
     */
    public function setCropBox(RE_Rect $cropBox = null)
    {
        if (is_null($cropBox)) {
            unset($this['CropBox']);
            return;
        }
        $this['CropBox'] = new RE_Pdf_Object_Rectangle($cropBox);
    }

    /**
     * Returns the crop box, the default visible region of the page. If the
     * page does not have a crop box defined, returns the media box.
     * 
     * @return RE_Rect
     */
    public function getCropBox()
    {
        if (isset($this['CropBox'])) {
            $box = $this['CropBox']->getObject();
        } else {
            /* If the page dictionary does not contain a CropBox entry, it may
             * be inherited from an ancestor page tree node. Fetch the CropBox
             * from the closest ancestor.
             */
            $box = $this['Parent']->getObject()->getCropBox();
        }

        if (! $box) {
            return $this->getMediaBox();
        }
        
        if (! $box instanceof RE_Pdf_Object_Rectangle) {
            $box = new RE_Pdf_Object_Rectangle($box);
        }
        return clone $box->getValue();
    }
  
    /**
     * Sets the bleed box, region of the page that should be output during
     * printing, which may include additional area to accommodate the pysical
     * limitations of finishing (cutting, folding, trimming, etc.) equipment.
     * The actual printed area may be larger than this box to include printers
     * marks, but should be no smaller.
     * 
     * @param RE_Rect|null $bleedBox Set to null to remove.
     */
    public function setBleedBox(RE_Rect $bleedBox = null)
    {
        if (is_null($bleedBox)) {
            unset($this['BleedBox']);
            return;
        }
        $this['BleedBox'] = new RE_Pdf_Object_Rectangle($bleedBox);
    }

    /**
     * Returns the bleed box, the region of the page that should be output
     * during printing. If the page does not have a bleed box defined, returns
     * the crop box.
     * 
     * @return RE_Rect
     */
    public function getBleedBox()
    {
        if (! isset($this['BleedBox'])) {
            /* The BleedBox cannot be inherited. If one is not found, return
             * the page's CropBox.
             */
            return $this->getCropBox();
        }

        $box = $this['BleedBox']->getObject();
        
        if (! $box instanceof RE_Pdf_Object_Rectangle) {
            $box = new RE_Pdf_Object_Rectangle($box);
        }
        return clone $box->getValue();
    }
  
    /**
     * Sets the trim box, the intended finished region of the page after
     * cutting, folding, trimming, etc.
     * 
     * @param RE_Rect|null $trimBox Set to null to remove.
     */
    public function setTrimBox(RE_Rect $trimBox = null)
    {
        if (is_null($trimBox)) {
            unset($this['TrimBox']);
            return;
        }
        $this['TrimBox'] = new RE_Pdf_Object_Rectangle($trimBox);
    }

    /**
     * Returns the trim box, the intended finished region of the page after
     * cutting, folding, trimming, etc. If the page does not have a trim box
     * defined, returns the crop box.
     * 
     * @return RE_Rect
     */
    public function getTrimBox()
    {
        if (! isset($this['TrimBox'])) {
            /* The TrimBox cannot be inherited. If one is not found, return
             * the page's CropBox.
             */
            return $this->getCropBox();
        }

        $box = $this['TrimBox']->getObject();
        
        if (! $box instanceof RE_Pdf_Object_Rectangle) {
            $box = new RE_Pdf_Object_Rectangle($box);
        }
        return clone $box->getValue();
    }

    /**
     * Sets the art box, the region of the page containing all of its
     * meaningful content.
     * 
     * @param RE_Rect|null $artBox Set to null to remove.
     */
    public function setArtBox(RE_Rect $artBox = null)
    {
        if (is_null($artBox)) {
            unset($this['ArtBox']);
            return;
        }
        $this['ArtBox'] = new RE_Pdf_Object_Rectangle($artBox);
    }

    /**
     * Returns the art box, the region of the page containing all of its
     * meaningful content. If the page does not have a art box defined,
     * returns the crop box.
     * 
     * @return RE_Rect
     */
    public function getArtBox()
    {
        if (! isset($this['ArtBox'])) {
            /* The ArtBox cannot be inherited. If one is not found, return
             * the page's CropBox.
             */
            return $this->getCropBox();
        }

        $box = $this['ArtBox']->getObject();
        
        if (! $box instanceof RE_Pdf_Object_Rectangle) {
            $box = new RE_Pdf_Object_Rectangle($box);
        }
        return clone $box->getValue();
    }
  
  
  /* Content Stream */
  
    /**
     * Returns the page's current content stream object. If there is no current
     * content stream, a new one is created.
     * 
     * Existing content streams may not be modified. If necessary, you can
     * obtain read-only references to existing content streams, and may remove
     * or reorder them, by interacting directly with the page's Contents entry.
     * 
     * @return RE_Pdf_ContentStream
     */
    public function getCurrentContentStream()
    {
        if (isset($this->_contentStream)) {
            return $this->_contentStream;
        }
        return $this->addContentStream();
    }
    
    /**
     * Adds a new content stream to the page, finalizing the current content
     * stream if one exists. Returns the new content stream object.
     * 
     * @return RE_Pdf_ContentStream
     */
    public function addContentStream()
    {
        $this->finalizeContentStream();
        $this->_contentStream = new RE_Pdf_ContentStream();

        $this->_contentStreamObject = $this->_sourcePDF->addObject($this->_contentStream);
        
        if (empty($this['Contents'])) {
            $this['Contents'] = $this->_contentStreamObject;
            
        } else if ($this['Contents'] instanceof RE_Pdf_Object_Indirect) {
            $this['Contents'] = new RE_Pdf_Object_Array(
                array($this['Contents'], $this->_contentStreamObject));
            
        } else {
            $this['Contents'][] = $this->_contentStreamObject;
            
        }        
        
        return $this->_contentStream;
    }
    
    /**
     * Finalizes the current content stream, writing its contents to the parent
     * PDF document's output destination
     */
    public function finalizeContentStream()
    {
        if (! isset($this->_contentStream)) {
            return;
        }
        $this->_contentStream = null;
        
        $this->_contentStreamObject->finalize();
        $this->_contentStreamObject = null;
    }
  
  
  /* RE_Pdf_Canvas_Interface Methods */
  
    /**
     * Forwarded to the {@link RE_Pdf_ContentStream::writeBytes()} method
     * for the page's current content stream. If there is no current content
     * stream, one is created.
     *
     * @param string $bytes
     */
    public function writeBytes($bytes)
    {
        if (! isset($this->_contentStream)) {
            $this->addContentStream();
        }
        $this->_contentStream->writeBytes($bytes);
    }

    /**
     * Forwarded to the {@link RE_Pdf_ContentStream::writeFile()} method
     * for the page's current content stream. If there is no current content
     * stream, one is created.
     *
     * @param string|resource $file
     * @throws InvalidArgumentException
     * @throws RE_Pdf_Exception
     */
    public function writeFile($file)
    {
        if (! isset($this->_contentStream)) {
            $this->addContentStream();
        }
        $this->_contentStream->writeFile($file);
    }

    /**
     * Forwarded to the {@link RE_Pdf_ContentStream::saveGraphicsState()} method
     * for the page's current content stream. If there is no current content
     * stream, one is created.
     *
     * @throws BadMethodCallException
     */
    public function saveGraphicsState()
    {
        if (! isset($this->_contentStream)) {
            $this->addContentStream();
        }
        $this->_contentStream->saveGraphicsState();
    }
    
    /**
     * Forwarded to the {@link RE_Pdf_ContentStream::restoreGraphicsState()} method
     * for the page's current content stream. If there is no current content
     * stream, one is created.
     * 
     * @throws BadMethodCallException
     */
    public function restoreGraphicsState()
    {
        if (! isset($this->_contentStream)) {
            $this->addContentStream();
        }
        $this->_contentStream->restoreGraphicsState();
    }
    
    /**
     * Forwarded to the {@link RE_Pdf_ContentStream::setStrokeColor()} method
     * for the page's current content stream. If there is no current content
     * stream, one is created.
     * 
     * @param RE_Color $color
     */
    public function setStrokeColor(RE_Color $color)
    {
        if (! isset($this->_contentStream)) {
            $this->addContentStream();
        }
        $this->_contentStream->setStrokeColor($color);
    }
    
    /**
     * Forwarded to the {@link RE_Pdf_ContentStream::setFillColor()} method
     * for the page's current content stream. If there is no current content
     * stream, one is created.
     * 
     * @param RE_Color $color
     */
    public function setFillColor(RE_Color $color)
    {
        if (! isset($this->_contentStream)) {
            $this->addContentStream();
        }
        $this->_contentStream->setFillColor($color);
    }

    /**
     * Adds the specified procedure set to the page's resource dictionary.
     * 
     * @param string $setName
     */
    public function addProcSet($setName)
    {
        if (isset($this->_procSets[$setName])) {
            return;
        }
        $this->_procSets[$setName] = true;
        
        $resources = $this['Resources']->getObject();
        
        if (! isset($resources['ProcSet'])) {
            $resources['ProcSet'] = new RE_Pdf_Object_Array();
        } else {
            foreach ($resources['ProcSet'] as $procSet) {
                if ($procSet->getValue() == $setName) {
                    return;
                }
            }
        }
        
        $resources['ProcSet'][] = new RE_Pdf_Object_Name($setName);
    }

    /**
     * Adds the specified resource to the page's resource dictionary. Returns
     * the unique resource name.
     *
     * @param string        $resourceType
     * @param RE_Pdf_Object $resource
     * @return string
     */
    public function addResource($resourceType, RE_Pdf_Object $resource)
    {
        /**
         * @todo This may still duplicate objects if an existing resource is
         *   reused in a document being modified.
         */
        if (isset($this->_resources[$resourceType])) {
            foreach ($this->_resources[$resourceType] as $resourceName => $res) {
                if ($res === $resource) {
                    /* The object already exists in the resource dictionary. Don't
                     * add it again.
                     */
                    return $resourceName;
                }
            }
        }
        
        $resources = $this['Resources']->getObject();

        if (! isset($resources[$resourceType])) {
            $resources[$resourceType] = new RE_Pdf_Object_Dictionary();
        }
        
        /* Obtain a unique name for this resource.
         */
        switch ($resourceType) {
        case 'ExtGState':
            $resourceName = 'GS'; break;
        case 'ColorSpace':
            $resourceName = 'CS'; break;
        case 'Pattern':
            $resourceName = 'P';  break;
        case 'Shading':
            $resourceName = 'Sh'; break;
        case 'XObject':
            $resourceName = 'O';  break;
        case 'Font':
            $resourceName = 'F';  break;
        case 'Properties':
            $resourceName = 'PR'; break;
        default:
            $resourceName = strtoupper(substr($resourceType, 0, 3));
        }
        
        $i = 1;
        for ( ; isset($resources[$resourceType][$resourceName . $i]); $i++);
        $resourceName .= $i;
        
        $this->_resources[$resourceType][$resourceName]  = $resource;
        $resources[$resourceType][$resourceName] = $this->_sourcePDF->addObject($resource);
        
        return $resourceName;
    }







    // public function setStyle(Zend_Pdf_Style $style)
    // {
    //     $this->_style = $style;
    // 
    //     $this->_addProcSet('Text');
    //     $this->_addProcSet('PDF');
    //     if ($style->getFont() !== null) {
    //         $this->setFont($style->getFont(), $style->getFontSize());
    //     }
    //     $this->_contents .= $style->instructions($this->_pageDictionary->Resources);
    // }
    // 
    // public function  drawCircle($x, $y, $radius, $param4 = null, $param5 = null, $param6 = null)
    // {
    //     $this->drawEllipse($x - $radius, $y - $radius,
    //                        $x + $radius, $y + $radius,
    //                        $param4, $param5, $param6);
    // }
    // 
    // public function drawEllipse($x1, $y1, $x2, $y2, $param5 = null, $param6 = null, $param7 = null)
    // {
    //     if ($param5 === null) {
    //         // drawEllipse($x1, $y1, $x2, $y2);
    //         $startAngle = null;
    //         $fillType = Zend_Pdf_Page::SHAPE_DRAW_FILL_AND_STROKE;
    //     } else if ($param6 === null) {
    //         // drawEllipse($x1, $y1, $x2, $y2, $fillType);
    //         $startAngle = null;
    //         $fillType = $param5;
    //     } else {
    //         // drawEllipse($x1, $y1, $x2, $y2, $startAngle, $endAngle);
    //         // drawEllipse($x1, $y1, $x2, $y2, $startAngle, $endAngle, $fillType);
    //         $startAngle = $param5;
    //         $endAngle   = $param6;
    // 
    //         if ($param7 === null) {
    //             $fillType = Zend_Pdf_Page::SHAPE_DRAW_FILL_AND_STROKE;
    //         } else {
    //             $fillType = $param7;
    //         }
    //     }
    // 
    //     $this->_addProcSet('PDF');
    // 
    //     if ($x2 < $x1) {
    //         $temp = $x1;
    //         $x1   = $x2;
    //         $x2   = $temp;
    //     }
    //     if ($y2 < $y1) {
    //         $temp = $y1;
    //         $y1   = $y2;
    //         $y2   = $temp;
    //     }
    // 
    //     $x = ($x1 + $x2)/2.;
    //     $y = ($y1 + $y2)/2.;
    // 
    //     $xC = new Zend_Pdf_Element_Numeric($x);
    //     $yC = new Zend_Pdf_Element_Numeric($y);
    // 
    //     if ($startAngle !== null) {
    //         if ($startAngle != 0) { $startAngle = fmod($startAngle, M_PI*2); }
    //         if ($endAngle   != 0) { $endAngle   = fmod($endAngle,   M_PI*2); }
    // 
    //         if ($startAngle > $endAngle) {
    //             $endAngle += M_PI*2;
    //         }
    // 
    //         $clipPath    = $xC->toString() . ' ' . $yC->toString() . " m\n";
    //         $clipSectors = (int)ceil(($endAngle - $startAngle)/M_PI_4);
    //         $clipRadius  = max($x2 - $x1, $y2 - $y1);
    // 
    //         for($count = 0; $count <= $clipSectors; $count++) {
    //             $pAngle = $startAngle + ($endAngle - $startAngle)*$count/(float)$clipSectors;
    // 
    //             $pX = new Zend_Pdf_Element_Numeric($x + cos($pAngle)*$clipRadius);
    //             $pY = new Zend_Pdf_Element_Numeric($y + sin($pAngle)*$clipRadius);
    //             $clipPath .= $pX->toString() . ' ' . $pY->toString() . " l\n";
    //         }
    // 
    //         $this->_contents .= "q\n" . $clipPath . "h\nW\nn\n";
    //     }
    // 
    //     $xLeft  = new Zend_Pdf_Element_Numeric($x1);
    //     $xRight = new Zend_Pdf_Element_Numeric($x2);
    //     $yUp    = new Zend_Pdf_Element_Numeric($y2);
    //     $yDown  = new Zend_Pdf_Element_Numeric($y1);
    // 
    //     $xDelta  = 2*(M_SQRT2 - 1)*($x2 - $x1)/3.;
    //     $yDelta  = 2*(M_SQRT2 - 1)*($y2 - $y1)/3.;
    //     $xr = new Zend_Pdf_Element_Numeric($x + $xDelta);
    //     $xl = new Zend_Pdf_Element_Numeric($x - $xDelta);
    //     $yu = new Zend_Pdf_Element_Numeric($y + $yDelta);
    //     $yd = new Zend_Pdf_Element_Numeric($y - $yDelta);
    // 
    //     $this->_contents .= $xC->toString() . ' ' . $yUp->toString() . " m\n"
    //                      .  $xr->toString() . ' ' . $yUp->toString() . ' '
    //                      .    $xRight->toString() . ' ' . $yu->toString() . ' '
    //                      .      $xRight->toString() . ' ' . $yC->toString() . " c\n"
    //                      .  $xRight->toString() . ' ' . $yd->toString() . ' '
    //                      .    $xr->toString() . ' ' . $yDown->toString() . ' '
    //                      .      $xC->toString() . ' ' . $yDown->toString() . " c\n"
    //                      .  $xl->toString() . ' ' . $yDown->toString() . ' '
    //                      .    $xLeft->toString() . ' ' . $yd->toString() . ' '
    //                      .      $xLeft->toString() . ' ' . $yC->toString() . " c\n"
    //                      .  $xLeft->toString() . ' ' . $yu->toString() . ' '
    //                      .    $xl->toString() . ' ' . $yUp->toString() . ' '
    //                      .      $xC->toString() . ' ' . $yUp->toString() . " c\n";
    // 
    //     switch ($fillType) {
    //         case Zend_Pdf_Page::SHAPE_DRAW_FILL_AND_STROKE:
    //             $this->_contents .= " B*\n";
    //             break;
    //         case Zend_Pdf_Page::SHAPE_DRAW_FILL:
    //             $this->_contents .= " f*\n";
    //             break;
    //         case Zend_Pdf_Page::SHAPE_DRAW_STROKE:
    //             $this->_contents .= " S\n";
    //             break;
    //     }
    // 
    //     if ($startAngle !== null) {
    //         $this->_contents .= "Q\n";
    //     }
    // }
    // 
    // public function rotate($x, $y, $angle)
    // {
    //     $cos  = new Zend_Pdf_Element_Numeric(cos($angle));
    //     $sin  = new Zend_Pdf_Element_Numeric(sin($angle));
    //     $mSin = new Zend_Pdf_Element_Numeric(-$sin->value);
    // 
    //     $xObj = new Zend_Pdf_Element_Numeric($x);
    //     $yObj = new Zend_Pdf_Element_Numeric($y);
    // 
    //     $mXObj = new Zend_Pdf_Element_Numeric(-$x);
    //     $mYObj = new Zend_Pdf_Element_Numeric(-$y);
    // 
    // 
    //     $this->_addProcSet('PDF');
    //     $this->_contents .= '1 0 0 1 ' . $xObj->toString() . ' ' . $yObj->toString() . " cm\n"
    //                      . $cos->toString() . ' ' . $sin->toString()
    //                      . ' ' . $mSin->toString() . ' ' . $cos->toString() . " 0 0 cm\n"
    //                      .'1 0 0 1 ' . $mXObj->toString() . ' ' . $mYObj->toString() . " cm\n";
    // }

}
