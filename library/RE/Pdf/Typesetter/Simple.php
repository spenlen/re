<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage TextLayout
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */

require_once 'RE/Unicode/GeneralCategory.php';


/**
 * The RE_Pdf_Typesetter_Simple lays out glyphs in a left-to-right horizontal
 * line with vertical line movement only. This is usually sufficient for most
 * Latin-based scripts or for very short runs of text that do not require any
 * advanced typographical features.
 *
 * In particular, the simple typesetter lacks support for:
 *
 *  - Right-to-left or vertical line sweep, required for CJK and many
 *     Middle-Eastern scripts.
 *  - Bi-directional text
 *  - Pairwise kerning from the font's kerning table or via character attributes
 *  - Ligatures, both for Latin-based and other scripts
 *  - Text attachment attributes
 *  - Text lists
 *  - Text tables
 *  - Hyphenation
 *  - Glyph substitution for missing glyphs
 *
 * Despite its lack of functionality, RE_Pdf_Typesetter_Simple is currently
 * the default typesetter used by {@link RE_Pdf_LayoutManager} objects in an
 * attempt to ship at least a partially-working implementation in the framework
 * as soon as possible.
 *
 * Ultimately, an advanced typesetter will be added that addresses these
 * shortcomings. At that time, it will replace the simple typesetter as the
 * default. However, this typesetter will remain available for backwards-
 * compatibility (as the advanced typesetter's more accurate line width
 * calculations will likely change existing layouts) or as an optimization
 * option for simplified use cases for Latin-based scripts.
 *
 * @package    Pdf
 * @subpackage TextLayout
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Pdf_Typesetter_Simple extends RE_Pdf_Typesetter
{
  /**** Instance Variables ****/

    /**
     * Working array of glyphs for the current paragraph glyph range.
     * @var array
     */
    protected $_glyphs = array();

    /**
     * Working array of character indexes corresponding to each of the glyphs
     * in the current paragraph glyph range.
     * @var array
     */
    protected $_glyphCharacterIndexes = array();

    /**
     * Working array of {@link RE_Font} objects corresponding to each of the
     * glyphs in the current paragraph glyph range.
     * @var array
     */
    protected $_glyphFonts = array();

    /**
     * Working array of font sizes correspondding to each of the glyphs in the
     * current paragraph glyph range.
     * @var array
     */
    protected $_glyphFontSizes = array();

    /**
     * Working array of nominal widths corresponding to each of the glyphs in
     * the current paragraph glyph range.
     * @var array
     */
    protected $_glyphWidths = array();

    /**
     */
    protected $_characters = array();
    protected $_generalCategories = array();

    /**
     */
    protected $_lineFragments = array();

    protected $_nominalGlyphRuns = array();



  /**** Public Interface ****/


  /* Concrete Subclass Implementation */

    /**
     * Lays out the paragraph in the current glyph range inside the current
     * text container, using the specified line fragment origin, until the
     * start of the next paragraph is reached or the text container is full.
     *
     * When complete, the origin is set to the proposed origin for the next
     * line fragment rectangle, and the index of the next glyph to be laid out
     * is returned.
     *
     * @param RE_Point $lineFragmentOrigin
     * @return integer
     */
    public function layoutParagraph(RE_Point $lineFragmentOrigin)
    {
        $layoutManager = $this->getLayoutManager();

        $textContainer = $this->getCurrentTextContainer();
        $containerSize = $textContainer->getContainerSize();

        $boundingRect  = new RE_Rect($lineFragmentOrigin,
            new RE_Size($containerSize->getWidth()  - $lineFragmentOrigin->getX(),
                        $containerSize->getHeight() - $lineFragmentOrigin->getY())
            );

        $proposedRect  = clone $boundingRect;

        $lineFragmentRect          = new RE_Rect();
        $usedRect                  = new RE_Rect();
        $lineFragmentRemainingRect = new RE_Rect();
        $remainingRect             = new RE_Rect();

        /**
         * @todo Update to work with line sweeps other than left-to-right and
         *   text movements other than top-to-bottom.
         */
        $lineSweep    = RE_Pdf_TextContainer::LINE_SWEEP_RIGHT;
        $lineMovement = RE_Pdf_TextContainer::LINE_MOVEMENT_DOWN;

        $startGlyphIndex = $this->getParagraphSeparatorGlyphRange()->getLocation();

        $this->beginParagraph();
        $this->beginLine($startGlyphIndex);

        while (($startGlyphIndex < $this->getParagraphSeparatorGlyphRange()->maxRange()) && (! $proposedRect->isEmpty())) {

            $lineSpacing            = $this->getLineSpacingAfterGlyphIndex($startGlyphIndex, $proposedRect);
            $paragraphSpacingBefore = $this->getParagraphSpacingBeforeGlyphIndex($startGlyphIndex, $proposedRect);
            $paragraphSpacingAfter  = $this->getParagraphSpacingAfterGlyphIndex($startGlyphIndex, $proposedRect);

            $remainingRect->setHeight(0);

            /* Optimistically lay out a single line fragment that spans the full
             * width of the proposed rectangle. This identifies the largest rect
             * and glyph range that can possibly fit, and in the most common case
             * of a simple rectangular text container with no holes, all the work
             * that needs to be done.
             */
            $this->layoutLineFragment($lineFragmentRect, $usedRect, $lineFragmentRemainingRect,
                $startGlyphIndex, $proposedRect, $lineSpacing, $paragraphSpacingBefore, $paragraphSpacingAfter);

            if ($lineFragmentRect->isEmpty()) {

                if ($textContainer->isSimpleRectangularTextContainer()) {
                    /* The text container is full. Stop here.
                     */
                    break;
                }

                /* No glyphs could be laid out in the proposed rectangle in the
                 * non-rectangular text container. See if there is another usable
                 * rectangle on this same line.
                 */
                $constrainedLineFragmentRect = $textContainer->lineFragmentRectForProposedRect(
                    $proposedRect, $lineSweep, $lineMovement, $remainingRect);

            } else {

                /* Ask the text container to constrain our optimistic line fragment
                 * rectangle if necessary (to account for non-rectangular geometry,
                 * holes in the container, etc.).
                 */
                $constrainedLineFragmentRect = $textContainer->lineFragmentRectForProposedRect(
                    $lineFragmentRect, $lineSweep, $lineMovement, $remainingRect);

                if ($constrainedLineFragmentRect->isEmpty()) {
                    /* No portion of the line fragment rectangle can be made to fit at
                     * the proposed location due to text container geometry.
                     */
                    $this->_nominalGlyphRuns = array();

                } else if (! $constrainedLineFragmentRect->isEqualToRect($lineFragmentRect)) {
                    /* The text container modified our line fragment rectangle. Lay out a
                     * new line fragment, as the number of glyphs that will fit in the new
                     * rect is almost guaranteed to be fewer.
                     */
                    $this->layoutLineFragment($lineFragmentRect, $usedRect, $lineFragmentRemainingRect,
                        $startGlyphIndex, $constrainedLineFragmentRect, $lineSpacing, $paragraphSpacingBefore, $paragraphSpacingAfter);

                }

                if (! empty($this->_nominalGlyphRuns)) {
                    $this->_lineFragments[] = array(
                        $this->_nominalGlyphRuns,
                        clone $lineFragmentRect,
                        clone $usedRect,
                        );
                    foreach ($this->_nominalGlyphRuns as $glyphRun) {
                        $startGlyphIndex = $glyphRun[0]->maxRange();
                    }
                }

            }

            if (! $remainingRect->isEmpty()) {
                /* The text container returned a remaining rect for the line fragment.
                 * Fill it before moving to the next full line.
                 */
                $proposedRect->setOrigin($remainingRect->getOrigin());
                $proposedRect->setSize($remainingRect->getSize());
                continue;
            }

            $lineHeight   = 1.0;
            $lineBaseline = 0.0;

            if (! empty($this->_lineFragments)) {
                $glyphRange = clone $this->_lineFragments[0][0][0][0];
                $maxGlyphIndex = 0;
                foreach ($this->_lineFragments as $lineFragment) {
                    $lineHeight = max($lineHeight, $lineFragment[1]->getHeight());
                    foreach ($lineFragment[0] as $glyphRun) {
                        $maxGlyphIndex = max($maxGlyphIndex, $glyphRun[0]->maxRange());
                        $lineBaseline  = max($lineBaseline,  $glyphRun[1]->getY());
                    }
                }
                $glyphRange->setLength($maxGlyphIndex - $glyphRange->getLocation());

                /* Adjust the origins to line up the baseline for all fragments.
                 */
                foreach ($this->_lineFragments as $lineFragment) {
                    $delta = $lineHeight - $lineFragment[1]->getHeight();

                    $lineFragmentRect = $lineFragment[1];
                    $lineFragmentRect->setHeight($lineFragmentRect->getHeight() + $delta);

                    $baseline = 0.0;
                    foreach ($lineFragment[0] as $glyphRun) {
                        $baseline = max($baseline, $glyphRun[1]->getY());
                    }
                    $delta = $lineBaseline - $baseline;

                    $usedRect = $lineFragment[2];
                    $usedRect->setY($usedRect->getY() + $delta);

                    foreach ($lineFragment[0] as $glyphRun) {
                        $origin = $glyphRun[1];
                        $origin->setY($origin->getY() + $delta);
                        $layoutManager->setLocationForStartOfGlyphRange($glyphRun[0], $origin);
                        $layoutManager->setTextContainerForGlyphRange($glyphRun[0], $textContainer);
                        $layoutManager->setLineFragmentRectForGlyphRange($glyphRun[0], $lineFragmentRect, $usedRect);
                    }
                }

                $this->endLine($glyphRange);

                $startGlyphIndex = $glyphRange->maxRange();
                if ($startGlyphIndex < $this->getParagraphSeparatorGlyphRange()->maxRange()) {
                    $this->beginLine($startGlyphIndex);
                }
            }

            switch ($lineMovement) {
                case RE_Pdf_TextContainer::LINE_MOVEMENT_DOWN:
                    $lineFragmentOrigin->setY($lineFragmentOrigin->getY() + $lineHeight);
                    break;
                case RE_Pdf_TextContainer::LINE_MOVEMENT_UP:
                    $lineFragmentOrigin->setY($lineFragmentOrigin->getY() - $lineHeight);
                    break;
            }

            $boundingRect->setY($lineFragmentOrigin->getY());
            $boundingRect->setHeight($boundingRect->getHeight() - $lineHeight);

            $proposedRect->setOrigin($boundingRect->getOrigin());
            $proposedRect->setSize($boundingRect->getSize());
        }

        if ($startGlyphIndex == $this->getParagraphSeparatorGlyphRange()->maxRange()) {
            switch ($this->actionForControlCharacterAtIndex($this->_glyphCharacterIndexes[$startGlyphIndex - 1])) {
                case RE_Pdf_Typesetter::CONTROL_ACTION_LINE_BREAK:
                case RE_Pdf_Typesetter::CONTROL_ACTION_PARAGRAPH_BREAK:
                case RE_Pdf_Typesetter::CONTROL_ACTION_CONTAINER_BREAK:
                    $layoutManager->replaceGlyph($startGlyphIndex - 1, null);
                    break;
            }
        }

        $this->endParagraph();

        return $startGlyphIndex;
    }


  /* Layout Phase Notification */

    /**
     * Obtains the glyphs and character indexes for the paragraph from the
     * layout manager, then determines the fonts and nominal widths for each
     * of those glyphs.
     */
    public function beginParagraph()
    {
        parent::beginParagraph();

        $paragraphSeparatorGlyphRange = $this->getParagraphSeparatorGlyphRange();
        if ($paragraphSeparatorGlyphRange->isEmpty()) {
            return;
        }


        $string = $this->getString();

        $startGlyphIndex = $paragraphSeparatorGlyphRange->getLocation();
        $maxGlyphIndex   = $paragraphSeparatorGlyphRange->maxRange();


        /* Obtain glyphs in bulk.
         */
        $glyphCount = $this->getLayoutManager()->getGlyphsInRangeForLayout(
            $paragraphSeparatorGlyphRange, $this->_glyphs, $this->_glyphCharacterIndexes);


        $effectiveRange = new RE_Range();

        /* Obtain glyph metrics.
         */
        $glyphIndex = $startGlyphIndex;
        while ($glyphIndex < $maxGlyphIndex) {

            $font = RE_Pdf_Font::fontWithName($string->getAttributeAtIndex(
                RE_AttributedString::ATTRIBUTE_FONT, $this->_glyphCharacterIndexes[$glyphIndex], $effectiveRange));

            $unitsPerEm = $font->getUnitsPerEm();

            $firstGlyphIndex   = $glyphIndex;
            $maxCharacterIndex = $effectiveRange->maxRange();

            for (; (($glyphIndex < $maxGlyphIndex) && ($this->_glyphCharacterIndexes[$glyphIndex] < $maxCharacterIndex)); $glyphIndex++);

            $sliceLocation = $firstGlyphIndex - $startGlyphIndex;
            $sliceLength   = $glyphIndex - $firstGlyphIndex;
            $runGlyphs = array_slice($this->_glyphs, $sliceLocation, $sliceLength, true);

            $widths = $font->widthsForGlyphs($runGlyphs);
            foreach ($widths as $index => $width) {
                $this->_glyphFonts[$index]  = $font;
                $this->_glyphWidths[$index] = $width / $unitsPerEm;
            }

        }

        $glyphIndex = $startGlyphIndex;
        while ($glyphIndex < $maxGlyphIndex) {

            $fontSize = (float)$string->getAttributeAtIndex(
                RE_AttributedString::ATTRIBUTE_FONT_SIZE, $this->_glyphCharacterIndexes[$glyphIndex], $effectiveRange);

            $maxCharacterIndex = $effectiveRange->maxRange();
            for (; (($glyphIndex < $maxGlyphIndex) && ($this->_glyphCharacterIndexes[$glyphIndex] < $maxCharacterIndex)); $glyphIndex++) {
                $this->_glyphFontSizes[$glyphIndex] = $fontSize;
            }

        }


        /* Obtain the actual characters and Unicode general categories.
         */
        $this->_characters = $string->getCharactersInRange(
            new RE_Range($this->_glyphCharacterIndexes[$startGlyphIndex],
                         ($this->_glyphCharacterIndexes[$maxGlyphIndex - 1] - $this->_glyphCharacterIndexes[$startGlyphIndex] + 1))
            );

        $this->_generalCategories = RE_Unicode_GeneralCategory::getGeneralCategories($this->_characters);


        /** @todo Implement ligatures */

    }

    /**
     * Sets up layout parameters at the beginning of each line of text.
     *
     * @param integer $glyphIndex The first glyph to be laid out in the line.
     */
    public function beginLine($glyphIndex)
    {
        parent::beginLine($glyphIndex);

        $this->_lineFragments = array();
    }

    /**
     * Sets up layout parameters at the end of each line of text.
     *
     * @param RE_Range $glyphRange The range of glyphs laid out in the line.
     */
    public function endLine(RE_Range $glyphRange)
    {
        $this->_lineFragments = array();

        parent::endLine($glyphRange);
    }

    /**
     * Cleans up after the paragraph has been laid out.
     */
    public function endParagraph()
    {
        $this->_glyphs                = array();
        $this->_glyphCharacterIndexes = array();
        $this->_glyphFonts            = array();
        $this->_glyphFontSizes        = array();
        $this->_glyphWidths           = array();

        $this->_characters        = array();
        $this->_generalCategories = array();

        parent::endParagraph();
    }


  /* Line Fragment Generation */

    /**
     * Generates a line fragment rectangle inside the proposed rect starting
     * with the specified glyph.
     *
     * @param RE_Rect $lineFragmentRect
     * @param RE_Rect $usedRect
     * @param RE_Rect $remainingRect,
     * @param integer $startGlyphIndex
     * @param RE_Rect $proposedRect,
     * @param float   $lineSpacing
     * @param float   $paragraphSpacingBefore
     * @param float   $paragraphSpacingAfter
     */
    public function layoutLineFragment(
        RE_Rect $lineFragmentRect, RE_Rect $usedRect, RE_Rect $remainingRect,
        $startGlyphIndex, RE_Rect $proposedRect, $lineSpacing, $paragraphSpacingBefore, $paragraphSpacingAfter)
    {
        $this->_nominalGlyphRuns = array();

        $layoutManager  = $this->getLayoutManager();
        $paragraphStyle = $this->getCurrentParagraphStyle();


        $lineFragmentPadding = $this->getLineFragmentPadding();

        $initialGlyphLocation = $lineFragmentPadding + $paragraphStyle->getHeadMargin();

        $maxGlyphLocation = $proposedRect->getWidth() - $lineFragmentPadding;

        $tailMargin = $paragraphStyle->getTailMargin();
        if ($tailMargin > 0) {
            $maxGlyphLocation = min($tailMargin, $maxGlyphLocation);
        } else {
            $maxGlyphLocation -= $tailMargin;
        }

        $maxWidth = $maxGlyphLocation - $initialGlyphLocation;
        if ($maxWidth <= 0) {
            /* The proposed rectangle is too narrow to accommodate the margins.
             */
            $lineFragmentRect->setWidth(0);
            $lineFragmentRect->setHeight(0);
            return;
        }


        $lineBreakMode = $paragraphStyle->getLineBreakMode();

        $stopOnOverflow = true;
        switch ($lineBreakMode) {
            case RE_ParagraphStyle::LINE_BREAK_TRUNCATE_HEAD:
            case RE_ParagraphStyle::LINE_BREAK_TRUNCATE_MIDDLE:
                // $stopOnOverflow = false;
        }

        $runWidth = 0;
        $naturalLineBreak = false;
        $runTabs  = array();

        $glyphIndex    = $startGlyphIndex;
        $maxGlyphIndex = $this->getParagraphSeparatorGlyphRange()->maxRange();
        for (; $glyphIndex < $maxGlyphIndex; $glyphIndex++) {

            if (! isset($this->_glyphs[$glyphIndex])) {
                continue; // skip null glyphs
            }

            /* Check for control characters.
             */
            if (($this->_generalCategories[$this->_glyphCharacterIndexes[$glyphIndex]][0] === 'C') ||
                ($this->_generalCategories[$this->_glyphCharacterIndexes[$glyphIndex]]    === 'Zl') ||
                ($this->_generalCategories[$this->_glyphCharacterIndexes[$glyphIndex]]    === 'Zp')) {
                switch ($this->actionForControlCharacterAtIndex($this->_glyphCharacterIndexes[$glyphIndex])) {

                case RE_Pdf_Typesetter::CONTROL_ACTION_WHITESPACE:
                    $this->_generalCategories[$this->_glyphCharacterIndexes[$glyphIndex]] = 'Zs';
                    break;

                case RE_Pdf_Typesetter::CONTROL_ACTION_HORIZONTAL_TAB:
                    $glyphLocation = $initialGlyphLocation + $runWidth;
                    $tabStop = $this->textTabForGlyphLocationWritingDirectionMaxLocation(
                        $glyphLocation, RE_Pdf_Text::WRITING_DIRECTION_LEFT_TO_RIGHT, $maxGlyphLocation);
                    if (! is_null($tabStop)) {
                        switch ($tabStop->getType()) {

                        case RE_TabStop::TYPE_LEFT:
                            $runTabs[$glyphIndex] = array($tabStop->getLocation() - $glyphLocation, $tabStop);
                            break;

                        case RE_TabStop::TYPE_CENTER:
                        case RE_TabStop::TYPE_RIGHT:
                        case RE_TabStop::TYPE_DECIMAL:

                        }
                    }
                    break;

                case RE_Pdf_Typesetter::CONTROL_ACTION_LINE_BREAK:
                case RE_Pdf_Typesetter::CONTROL_ACTION_PARAGRAPH_BREAK:
                case RE_Pdf_Typesetter::CONTROL_ACTION_CONTAINER_BREAK:
                    $naturalLineBreak = true;
                    break(2);

                case RE_Pdf_Typesetter::CONTROL_ACTION_ZERO_ADVANCEMENT:
                    $layoutManager->replaceGlyph($glyphIndex, null);
                    $this->_glyphs[$glyphIndex] = null;
                    continue(2);
                }
            }

            $runWidth += $this->_glyphWidths[$glyphIndex] * $this->_glyphFontSizes[$glyphIndex];
            if (($stopOnOverflow) && ($runWidth > $maxWidth)) {
                break;
            }
        }

        // if (! empty($runTabs)) {var_dump($runTabs);die;}

        /* Trim trailing whitespace from the width calculation of the run.
         */
        for (; (($glyphIndex > $startGlyphIndex) && ($glyphIndex < $maxGlyphIndex) &&
                ($this->_generalCategories[$this->_glyphCharacterIndexes[$glyphIndex]] == 'Zs')); $glyphIndex--) {
                    $runWidth -= $this->_glyphWidths[$glyphIndex] * $this->_glyphFontSizes[$glyphIndex];
        }

        if ($runWidth > $maxWidth) {

            while ($runWidth > $maxWidth) {
                $runWidth -= $this->_glyphWidths[$glyphIndex] * $this->_glyphFontSizes[$glyphIndex--];
            }

            if ($glyphIndex >= $startGlyphIndex) {
                switch ($lineBreakMode) {
                    case RE_ParagraphStyle::LINE_BREAK_WORD:
                        $breakGlyphIndex = $this->_getWordLineBreakGlyphIndex($startGlyphIndex, $glyphIndex);
                        if ($breakGlyphIndex < $startGlyphIndex) {
                            // could not find a suitable word break opportunity, break by character instead
                        } else {
                            for (; $glyphIndex > $breakGlyphIndex; $glyphIndex--) {
                                $runWidth -= $this->_glyphWidths[$glyphIndex] * $this->_glyphFontSizes[$glyphIndex];
                            }
                        }
                        break;

                    case RE_ParagraphStyle::LINE_BREAK_CHARACTER:
                    case RE_ParagraphStyle::LINE_BREAK_TRUNCATE_HEAD:
                    case RE_ParagraphStyle::LINE_BREAK_TRUNCATE_MIDDLE:
                    case RE_ParagraphStyle::LINE_BREAK_TRUNCATE_TAIL:
                        // no further action necessary
                        break;

                    case RE_ParagraphStyle::LINE_BREAK_CLIP:
                        // delete the remaining glyphs for the paragraph
                        $this->_deleteGlyphs(new RE_Range($glyphIndex + 1, ($maxGlyphIndex - $glyphIndex - 1)));
                        $maxGlyphIndex = $glyphIndex;
                        break;

                    }
            }

        }

        if ($glyphIndex < $startGlyphIndex) {
            /* The proposed rectangle is too narrow to accommodate any glyphs
             * using the specified line break mode.
             */
            $lineFragmentRect->setWidth(0);
            $lineFragmentRect->setHeight(0);
            return;
        }

        /* Trim trailing whitespace from the width calculation of the run.
         */
        for (; (($glyphIndex > $startGlyphIndex) && ($glyphIndex < $maxGlyphIndex) &&
                ($this->_generalCategories[$this->_glyphCharacterIndexes[$glyphIndex]] == 'Zs')); $glyphIndex--) {
                    $runWidth -= $this->_glyphWidths[$glyphIndex] * $this->_glyphFontSizes[$glyphIndex];
        }

        /* If the run was not terminated by a line break character, fold
         * trailing whitespace onto this line.
         */
        $glyphIndex++;
        if (! $naturalLineBreak) {
            for (; (($glyphIndex < $maxGlyphIndex) &&
                    ($this->_generalCategories[$this->_glyphCharacterIndexes[$glyphIndex]] == 'Zs')); $glyphIndex++);
        }
        $maxGlyphIndex = $glyphIndex - 1;


        /* Now that the final width of the run is known, adjust the initial
         * glyph location to accommodate for text alignment.
         */
        $textAlignment = $paragraphStyle->getAlignment();
        switch ($textAlignment) {
            case RE_ParagraphStyle::ALIGN_NATURAL:
            case RE_ParagraphStyle::ALIGN_LEFT:
                break;  // no adjustment

            case RE_ParagraphStyle::ALIGN_JUSTIFIED:
                /** @todo Implement justification */
                break;

            case RE_ParagraphStyle::ALIGN_CENTER:
                $initialGlyphLocation += ($maxWidth - $runWidth) / 2;
                break;

            case RE_ParagraphStyle::ALIGN_RIGHT:
                $initialGlyphLocation += $maxWidth - $runWidth;
                break;
        }


        /* Determine each of the nominally spaced glyph runs, and calculate
         * the typographical line height.
         */
        $glyphRunIndex = -1;
        $glyphLocation = $initialGlyphLocation;

        $lastFont     = null;
        $lastFontSize = null;
        $fontAscent   = 0;
        $fontDescent  = 0;
        $fontLineGap  = 0;

        $lineAscent   = 0;
        $lineDescent  = 0;
        $lineLineGap  = 0;

        for ($i = 0, $glyphIndex = $startGlyphIndex; $glyphIndex <= $maxGlyphIndex; $glyphIndex++) {

            if (! isset($this->_glyphs[$glyphIndex])) {
                continue;  // skip null glyphs
            }

            if ($this->_glyphFonts[$glyphIndex] !== $lastFont) {
                $unitsPerEm = $this->_glyphFonts[$glyphIndex]->getUnitsPerEm();

                $fontAscent  = $this->_glyphFonts[$glyphIndex]->getAscent()  / $unitsPerEm;
                $fontDescent = abs($this->_glyphFonts[$glyphIndex]->getDescent()) / $unitsPerEm;
                $fontLineGap = $this->_glyphFonts[$glyphIndex]->getLineGap() / $unitsPerEm;

                $lastFont     = $this->_glyphFonts[$glyphIndex];
                $lastFontSize = null;
            }

            if ($this->_glyphFontSizes[$glyphIndex] !== $lastFontSize) {
                $this->_nominalGlyphRuns[++$glyphRunIndex] =
                    array(new RE_Range($glyphIndex, 0), new RE_Point($glyphLocation, 0));

                // check for attributes that affect baseline: subscript, superscript, baselineshift?

                $lineAscent  = max($lineAscent,  ($fontAscent  * $this->_glyphFontSizes[$glyphIndex]));
                $lineDescent = max($lineDescent, ($fontDescent * $this->_glyphFontSizes[$glyphIndex]));
                $lineLineGap = max($lineLineGap, ($fontLineGap * $this->_glyphFontSizes[$glyphIndex]));

                $lastFontSize = $this->_glyphFontSizes[$glyphIndex];
            }

            $width = $this->_glyphWidths[$glyphIndex] * $this->_glyphFontSizes[$glyphIndex];

            $this->_nominalGlyphRuns[$glyphRunIndex][0]->length++;
            $glyphLocation += $width;
        }

        /* Determine the final line height and baseline position.
         */
        $lineHeight = $lineAscent + $lineDescent + $lineLineGap + $lineSpacing;

        $lineHeightMultiple = $paragraphStyle->getLineHeightMultiple();
        if ($lineHeightMultiple > 0) {
            $lineHeight *= $lineHeightMultiple;
        }

        $minimumHeight = $paragraphStyle->getMinimumLineHeight();
        if (($minimumHeight > 0) && ($lineHeight < $minimumHeight)) {
            $delta = $minimumHeight - $lineHeight;
            $lineHeight += $delta;
            $lineAscent += $delta;
        }

        $maximumHeight = $paragraphStyle->getMaximumLineHeight();
        if (($maximumHeight > 0) && ($lineHeight > $maximumHeight)) {
            $delta = $lineHeight - $maximumHeight;
            $lineHeight -= $delta;
            $lineAscent -= $delta;
        }

        $lineFragmentSpacingBefore = 0.0;
        $lineFragmentSpacingAfter  = 0.0;
        if ($glyphRunIndex >= 0) {
            if (($this->getParagraphSeparatorGlyphRange()->getLocation() == $startGlyphIndex) &&
                (! $this->getCurrentTextContainer()->getGlyphRange()->isEmpty())) {
                   $lineFragmentSpacingBefore = $paragraphSpacingBefore;
            }
            if ($this->getParagraphSeparatorGlyphRange()->maxRange() == $this->_nominalGlyphRuns[$glyphRunIndex][0]->maxRange()) {
                $lineFragmentSpacingAfter = $paragraphSpacingAfter;
            }
        }

        foreach ($this->_nominalGlyphRuns as $glyphRun) {
            $glyphRun[1]->y += $lineAscent + $lineFragmentSpacingBefore;
        }

        /* Calculate the line fragment and remaining rectangles.
         */
        $usedRect->setX($proposedRect->getX() + $initialGlyphLocation - $lineFragmentPadding);
        $usedRect->setY($proposedRect->getY() + $lineFragmentSpacingBefore);
        $usedRect->setWidth($runWidth + $lineFragmentPadding + $lineFragmentPadding);
        $usedRect->setHeight($lineHeight);

        $lineFragmentRect->setOrigin($proposedRect->getOrigin());
        $lineFragmentRect->setWidth($proposedRect->getWidth());
        $lineFragmentRect->setHeight($usedRect->getHeight() + $lineFragmentSpacingBefore + $lineFragmentSpacingAfter);

        $remainingRect->setX($proposedRect->getX());
        $remainingRect->setY($lineFragmentRect->maxY());
        $remainingRect->setWidth($proposedRect->getWidth());
        $remainingRect->setHeight($proposedRect->getHeight() - $lineFragmentRect->getHeight());
    }



  /**** Internal Interface ****/

    /**
     *
     * @param integer $minGlyphIndex
     * @param integer $glyphIndex
     * @return integer
     */
    protected function _getWordLineBreakGlyphIndex($minGlyphIndex, $glyphIndex)
    {
        $minCharacterIndex = $this->_glyphCharacterIndexes[$minGlyphIndex];
        $characterIndex    = $this->_glyphCharacterIndexes[$glyphIndex];

        $characterRange = new RE_Range($minCharacterIndex, ($characterIndex - $minCharacterIndex + 1));

        $string = $this->getString();
        while ((! is_null($characterIndex)) && ($characterIndex > $minCharacterIndex)) {

            $characterIndex = $string->lineBreakBeforeIndex($characterIndex, $characterRange);

            if ((! is_null($characterIndex)) && ($this->shouldBreakLineByWordBeforeCharacter($characterIndex))) {
                break;
            }
        }

        if (is_null($characterIndex)) {
            return $minCharacterIndex - 1;
        }

        for (; (($glyphIndex >= 0) && ($this->_glyphCharacterIndexes[$glyphIndex] >= $characterIndex)); $glyphIndex--);
        return $glyphIndex;
    }

    /**
     * Deletes the glyphs in the specified range from both the internal working
     * caches and the layout manager.
     *
     * @param RE_Range $glyphRange
     * @throws RangeException if any part of the range is invalid.
     */
    protected function _deleteGlyphs(RE_Range $glyphRange)
    {
        $paragraphGlyphRange          = $this->getParagraphGlyphRange();
        $paragraphSeparatorGlyphRange = $this->getParagraphSeparatorGlyphRange();

        if (($glyphRange->getLocation() < $paragraphSeparatorGlyphRange->getLocation()) ||
            ($glyphRange->maxRange() > $paragraphSeparatorGlyphRange->maxRange())) {
                throw new RangeException('Glyph range is invalid');
        }

        $this->getLayoutManager()->deleteGlyphs($glyphRange);

        $location = $glyphRange->getLocation();
        $length   = $glyphRange->getLength();

        $firstHalfEnd    = $location - $paragraphSeparatorGlyphRange->getLocation();
        $secondHalfStart = $firstHalfEnd + $length;

        $keyStart = $location;
        $keyEnd   = $paragraphSeparatorGlyphRange->maxRange() - $length - 2;
        $keys = ($keyEnd >= $keyStart) ? range($keyStart, $keyEnd) : array();

        $array = array_slice($this->_glyphs, 0, $firstHalfEnd, true) + array_combine($keys, array_slice($this->_glyphs, $secondHalfStart));
        $this->_glyphs = $array;

        $array = array_slice($this->_glyphCharacterIndexes, 0, $firstHalfEnd, true) + array_combine($keys, array_slice($this->_glyphCharacterIndexes, $secondHalfStart));
        $this->_glyphCharacterIndexes = $array;

        $array = array_slice($this->_glyphFonts, 0, $firstHalfEnd, true) + array_combine($keys, array_slice($this->_glyphFonts, $secondHalfStart));
        $this->_glyphFonts = $array;

        $array = array_slice($this->_glyphFontSizes, 0, $firstHalfEnd, true) + array_combine($keys, array_slice($this->_glyphFontSizes, $secondHalfStart));
        $this->_glyphFontSizes = $array;

        $array = array_slice($this->_glyphWidths, 0, $firstHalfEnd, true) + array_combine($keys, array_slice($this->_glyphWidths, $secondHalfStart));
        $this->_glyphWidths = $array;

        $paragraphGlyphRange->setLength($paragraphGlyphRange->getLength() - $length);
        $paragraphSeparatorGlyphRange->setLength($paragraphSeparatorGlyphRange->getLength() - $length);
    }






/* EVERYTHING BELOW THIS LINE IS DEPRECATED AND WILL BE REMOVED IN THE NEXT UPDATE */





    /**
     * Draws the text completely within the drawing rectangle. Wraps long lines
     * to fit only at space or tab characters. Also wraps lines if a carriage
     * return or line feed character is encountered.
     *
     * @private This method is a stop-gap until the full layout manager and
     *   typesetter implementations are complete. You should not call it
     *   directly as it will be removed without notice in a future revision.
     *   Use RE_Pdf_LayoutManager::drawText() instead.
     *
     * @param RE_Pdf_Text $text Source text.
     * @param RE_Pdf_Page $page Page in which to draw the text.
     * @param RE_Rect     $rect Bounding rectangle.
     * @param RE_Rect     $usedRect (optional) If provided, will be set to the
     *   actual rectangle used by the text.
     * @return integer Index of the last character that was drawn.
     */
    public function drawTextOnPageInRect(RE_Pdf_Text $text, RE_Pdf_Page $page, RE_Rect $rect, RE_Rect $usedRect = null)
    {
        $characters = $text->getCharacters();
        if (count($characters) < 1) {
            return 0;
        }

        $startChar = 0;
        $endChar   = 0;
        $lastChar  = count($characters) - 1;

        /* Identify the paragraphs. Font, size, and color can currently only
         * be changed at paragraph boundaries. This limitation will be lifted
         * when the remainder of the typesetter code is completed.
         */
        $paragraphs = array();
        for ($endChar = 0; $endChar < $lastChar; $endChar++) {
            switch ($characters[$endChar]) {
            case 13:  // CR
                if ((($endChar + 1) < $lastChar) && ($characters[$endChar + 1] == 10)) {  // check for CRLF
                    $paragraphs[] = array($startChar, $endChar - 1);
                    $startChar = ++$endChar + 1;
                    break;
                }
                // break intentionally omitted

            case 10:  // LF
                $paragraphs[] = array($startChar, $endChar - 1);
                $startChar = $endChar + 1;

            }
        }
        $paragraphs[] = array($startChar, $endChar);

        $lastFont      = null;
        $lastFontSize  = null;
        $lastFontColor = null;

        /* Keep track of the mixinum and maximum X coordinates. Used to return
         * the actual drawn rectangle below.
         */
        $minX = $rect->maxX();
        $maxX = $rect->minX();

        $y = $rect->maxY();

        foreach ($paragraphs as $paragraph) {

            $startChar = $paragraph[0];
            $endChar   = 0;
            $lastChar  = $paragraph[1];

            $fontName = $text->getAttributeAtIndex(RE_AttributedString::ATTRIBUTE_FONT, $startChar);
            $font = RE_Pdf_Font::fontWithName($fontName);

            $fontSize  = $text->getAttributeAtIndex(RE_AttributedString::ATTRIBUTE_FONT_SIZE, $startChar);
            $fontColor = $text->getAttributeAtIndex(RE_AttributedString::ATTRIBUTE_COLOR, $startChar);

            if (($font !== $lastFont) || ($fontSize !== $lastFontSize)) {
                $font->setFontAndSize($page, $fontSize);
                $lastFont     = $font;
                $lastFontSize = $fontSize;
            }
            if ($fontColor !== $lastFontColor) {
                $page->setFillColor($fontColor);
                $lastFontColor = $fontColor;
            }

            $workingCharacters = array_slice($characters, $startChar, ($lastChar - $startChar + 1), true);

            $glyphs = $font->cmap->glyphNumbersForCharacters($workingCharacters);
            $widths = $font->widthsForGlyphs($glyphs);


            // available width is in glyph units; everything else is in page units
            $availableWidth = $rect->getWidth() * $font->getUnitsPerEm() / $fontSize;

            $toBaseline = $font->getAscent() / $font->getUnitsPerEm() * $fontSize;
            $descent    = -($font->getDescent() / $font->getUnitsPerEm() * $fontSize);
            $lineGap    = ($font->getLineGap() / $font->getUnitsPerEm() * $fontSize);
            $lineHeight = $font->getLineHeight() / $font->getUnitsPerEm() * $fontSize;

            $y += $lineHeight - $toBaseline;

            while ($startChar <= $lastChar) {

                $y -= $lineHeight;

                $runWidth = 0;
                for ($endChar = $startChar; $endChar <= $lastChar; $endChar++) {
                    $runWidth += $widths[$endChar];
                    if ($runWidth >= $availableWidth) {
                        break;
                    }
                }

                if ($runWidth > $availableWidth) {
                    // the run is too long. working backwards, look for a break point
                    $newEndChar = $endChar;
                    $newRunWidth = $runWidth;
                    for (; $newEndChar >= $startChar; $newEndChar--) {
                        $newRunWidth -= $widths[$newEndChar];
                        if (($characters[$newEndChar] == 32) ||
                            ($characters[$newEndChar] ==  9)) {
                            break;
                        }
                    }
                    if ($newRunWidth != 0) {
                        $endChar = $newEndChar;
                        $runWidth = $newRunWidth;
                    } else {
                        // could not find an good break character. force-wrap the line
                        // by shaving off the last character.
                        $runWidth -= $widths[$endChar--];
                    }
                }

                // if there is a run of spaces after the line break character, fold them
                // onto this line as well. they will not be included in the total width
                // calculation of this line.
                if ($endChar < $lastChar) {
                    if ($characters[$endChar + 1] == 32) {
                        for (; (($endChar < $lastChar) && ($characters[$endChar + 1] == 32)); $endChar++);
                    }
                }

                $textWidth = $runWidth * $fontSize / $font->getUnitsPerEm();

                $x = $rect->minX();

                // adjust horizontal position for paragraph alignment
                switch ($text->getAttributeAtIndex(RE_AttributedString::ATTRIBUTE_PARAGRAPH_STYLE, $startChar)->getAlignment()) {
                case RE_ParagraphStyle::ALIGN_NATURAL:
                    /** @todo Detect right-to-left and adjust accordingly. For now, act as left-aligned. */
                case RE_ParagraphStyle::ALIGN_LEFT:
                    // already at the left margin. do nothing.
                    break;
                case RE_ParagraphStyle::ALIGN_CENTER:
                    $x += ($rect->getWidth() - $textWidth) / 2;
                    break;
                case RE_ParagraphStyle::ALIGN_RIGHT:
                    $x += $rect->getWidth() - $textWidth;
                    break;
                case RE_ParagraphStyle::ALIGN_JUSTIFIED:
                    /** @todo Adjust character and word spacing for justification. For now, do nothing. */
                    break;
                }

                if ($x < $minX) {
                    $minX = $x;
                }
                if (($x + $textWidth) > $maxX) {
                    $maxX = $x + $textWidth;
                }

                // $textRun   = $text->getSubstring(new RE_Range($startChar, ($endChar - $startChar - 1)), 'UTF-16BE');
                // $page->drawText($textRun, $x, $y, 'UTF-16BE');

                $page->writeBytes(" BT\n");

                $page->writeBytes(' 1 0 0 1');
                RE_Pdf_Object_Numeric::writeValue($page, $x);
                RE_Pdf_Object_Numeric::writeValue($page, $y);
                $page->writeBytes(" Tm\n");

                $textRunGlyphs = array();
                for ($i = $startChar; $i < $endChar; $i++) {
                    $textRunGlyphs[] = $glyphs[$i];
                }
                $font->showGlyphs($page, $textRunGlyphs);

                $page->writeBytes(" ET\n");


                $startChar = $endChar + 1;

                if (($y - $lineHeight - $descent) < $rect->minY()) {
                    // filled up all available vertical space
                    break;
                }
            }

            $y -= $descent + $lineGap;
            if (($y - $lineHeight) < $rect->minY()) {
                // filled up all available vertical space
                break;
            }

        }
        $y += $lineGap;

        if ($usedRect) {
            $usedRect->setX($minX);
            $usedRect->setY($y);
            $usedRect->setWidth($maxX - $minX);
            $usedRect->setHeight($rect->maxY() - $y);
        }

        return $startChar - 2;
    }

}
