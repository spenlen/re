<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage Core
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * Abstract base class used for modeling the primitive PDF data types. All PDF
 * objects, from content streams and pages to fonts and images, extend these
 * primitive types.
 *
 * @package    Pdf
 * @subpackage Core
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
abstract class RE_Pdf_Object implements SplSubject
{
  /**** Instance Variables ****/

    /**
     * Object value as a primitive PHP data type.
     * @var mixed
     */
    protected $_value = null;
    
    /**
     * Array of objects that receive change notifications.
     * @var array
     */
    protected $_observers = array();
    
    /**
     * Array used to record object number assignments from {@link RE_Pdf_Document}.
     * @var array
     */
    protected $_objectNumbers = array();
    


  /**** Abstract Interface ****/

    /**
     * Extracts the specific kind of PDF object from the specified file parser
     * at its current read offset. Returns the extracted object and moves the
     * offset past the object and any trailing white space.
     * 
     * Used by {@link RE_FileParser_Pdf} to read and parse existing PDF documents.
     * 
     * NOTE: PHP does not allow for abstract definitions of static methods, so
     * this base class simply throws a BadMethodCallException exception. All
     * concrete subclasses are required to implement this method.
     * 
     * @param  RE_FileParser_Pdf $parser
     * @return RE_Pdf_Object
     * @throws RE_Pdf_Exception if an object of the specific type is not present
     *   at the current parser offset.
     */
    public static function extract(RE_FileParser_Pdf $parser)
    {
        throw new BadMethodCallException('extract() must be implemented by concrete subclasses!');
    }
    
    /**
     * Writes the object's value to the specified output destination.
     *
     * @param RE_Pdf_OutputDestination_Interface $output
     */
    abstract public function write(RE_Pdf_OutputDestination_Interface $output);
  
  
  
  /**** Class Methods ****/
  
    /**
     * Extracts a PDF object from the specified file parser at its current
     * read offset. Returns the extracted object and moves the offset past
     * the object and any trailing white space.
     * 
     * Used by {@link RE_FileParser_Pdf} when the expected object type is
     * unknown. If the expected type is known, call the appropriate subclass'
     * {@link extract()} method as it will be faster.
     * 
     * @param  RE_FileParser_Pdf $parser
     * @return RE_Pdf_Object
     * @throws RE_Pdf_Exception if a PDF object could not be read at the
     *   current parser offset.
     */
    public static function extractObject(RE_FileParser_Pdf $parser)
    {
        /* The type of the object can generally be detected just by looking at
         * the first two bytes.
         */
        $bytes = $parser->peekBytes(2);
        switch ($bytes[0]) {

        case '[':
            return RE_Pdf_Object_Array::extract($parser);

        case '<':
            if ($bytes === '<<') {
                return RE_Pdf_Object_Dictionary::extract($parser);
            } else {
                return RE_Pdf_Object_String::extract($parser);                
            }

        case '(':
            return RE_Pdf_Object_String::extract($parser);                
        
        case '/':
            return RE_Pdf_Object_Name::extract($parser);

        case 't':
        case 'f':
            return RE_Pdf_Object_Boolean::extract($parser);

        case 'n':
            return RE_Pdf_Object_Null::extract($parser);

        case '+':
        case '-':
        case '.':
            return RE_Pdf_Object_Numeric::extract($parser);

        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            $bytes = $parser->peekBytes(24, false);
            if (preg_match('/^\d+ \d+ R/', $bytes)) {
                return RE_Pdf_Object_Indirect::extract($parser);
            } else {
                return RE_Pdf_Object_Numeric::extract($parser);
            }
        
        default:
            throw new RE_Pdf_Exception(sprintf('Cannot read PDF object at parser offset 0x%x',
                                               ($parser->getOffset() - 1)));
        }
    }
  
    /**
     * In some cases it may be more convenient to write a primitive value
     * directly to an output destination without creating and destroying an
     * ephemeral object to store that value.
     * 
     * If a subclass can reasonably implement this method, it should do so.
     * This base implementation simply throws a BadMethodCallException.
     * 
     * @param RE_Pdf_OutputDestination_Interface $output
     * @param mixed $value
     */
    public static function writeValue(RE_Pdf_OutputDestination_Interface $output, $value)
    {
        throw new BadMethodCallException('writeValue() is not implemented by this subclass.');
    }
  
  
  /**** Public Interface ****/


  /* Object Lifecycle */

    /**
     * Creates a new RE_Pdf_Object, setting object's initial value as a
     * primitive PHP data type.
     * 
     * If the object value cannot naturally map to a PHP type, subclasses
     * should override this method and call their appropriate accessor method(s).
     * 
     * @param mixed $value
     */
    public function __construct($value = null)
    {
        $this->setValue($value);
    }
    
    
  /* Object Magic Methods */
  
    /**
     * Returns the object value as a string. Useful for debugging.
     * 
     * Complex object types should override this method to return a reasonable
     * string approximation.
     * 
     * @return string
     */
    public function __toString()
    {
        return (string)$this->_value;
    }


  /* SplSubject Interface Methods */
  
    /**
     * Adds the object to the list of observers that receive change
     * notifications.
     * 
     * @param SplObserver $observer
     */
    public function attach(SplObserver $observer)
    {
        foreach ($this->_observers as $value) {
            if ($value === $observer) {
                return;
            }
        }
        $this->_observers[] = $observer;
    }

    /**
     * Removes the object from the list of observers that receive change
     * notifications.
     * 
     * @param SplObserver $observer
     */
    public function detach(SplObserver $observer)
    {
        foreach ($this->_observers as $key => $value) {
            if ($value === $observer) {
                unset($this->_observers[$key]);
                break;
            }
        }
    }

    /**
     * Notifies all observers that the object's value has changed.
     * 
     * NOTE: This method MUST be called every time the object value changes as
     * it is the only means by which the parent PDF document tracks changes to
     * its objects.
     */
    public function notify()
    {
        foreach ($this->_observers as $observer) {
            $observer->update($this);
        }
    }
  

  /* Accessor Methods */  

    /**
     * Returns the object's value as an appropriate primitive PHP data type.
     *
     * If the object value cannot naturally map to a PHP type, subclasses
     * should provide alternative accessor methods, then override this method
     * and throw a BadMethodCallException.
     * 
     * @return mixed
     */
    public function getValue()
    {
        return $this->_value;
    }

    /**
     * Sets the object's value to the primitive PHP data type.
     * 
     * Type checking and/or coercion is not normally needed here because it
     * will be done by {@link write()} at serialization time. This allows
     * RE_Pdf_Objects to act similarly to PHP variables, accepting a wide
     * variety of value sources (scalars or even other objects).
     * 
     * If the object value cannot naturally map to a PHP type, subclasses
     * should provide alternative accessor methods, then override this method
     * and throw a BadMethodCallException.
     * 
     * Be sure to call {@link $notify()} as appropriate in additional accessor
     * methods.
     * 
     * @param mixed $value
     */
    public function setValue($value)
    {
        if ($this->_value === $value) {
            return;
        }
        $this->_value = $value;
        $this->notify();
    }
    
    /**
     * Returns the object.
     * 
     * See {@see RE_Pdf_Object_Indirect::getObject()}.
     * 
     * @return RE_Pdf_Object
     */
    public function getObject()
    {
        return $this;
    }

    /**
     * Returns the object's type (its RE_Pdf class name).
     *
     * See {@see RE_Pdf_Object_Indirect::getObjectType()}.
     * 
     * @return string
     */
    public function getObjectType()
    {
        return get_class($this);
    }
    

  /* */
  
    /**
     * Called by {@link RE_Pdf_Document} to record the object number assigned
     * to this object in the latest generation of the document.
     * 
     * @param string  $fileID
     * @param integer $objectNumber
     */
    public function recordObjectNumber($fileID, $objectNumber)
    {
        $this->_objectNumbers[$fileID] = $objectNumber;
    }

    /**
     * Called by {@link RE_Pdf_Document} to see if it has already added this
     * object to the latest generation of the document.
     * 
     * @return integer|null
     */
    public function findObjectNumber($fileID)
    {
        if (isset($this->_objectNumbers[$fileID])) {
            return $this->_objectNumbers[$fileID];
        }
        return null;
    }

}
