<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage TextLayout
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * TrueType fonts implementation
 *
 * Font objects should be normally be obtained from the factory methods
 * {@link RE_Pdf_Font::fontWithName()} and {@link RE_Pdf_Font::fontWithPath()}.
 *
 * @package    Pdf
 * @subpackage TextLayout
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Pdf_Font_OpenType_TrueType extends RE_Pdf_Font_OpenType
{
  /**** Public Interface ****/


  /* Concrete Subclass Implementation */

    /**
     * Writes the font object to the specified output destination, which must
     * be a {@link RE_Pdf_Document} object.
     * 
     * Overridden to create the font descriptor dictionary and embeded font
     * program stream objects.
     * 
     * @param RE_Pdf_OutputDestination_Interface $output
     * @throws InvalidArgumentException if the output destination is not a
     *   {@link RE_Pdf_Document} object.
     */
    public function write(RE_Pdf_OutputDestination_Interface $output)
    {
        if (! $output instanceof RE_Pdf_Document) {
            throw new InvalidArgumentException(
                'Output destination must be a RE_Pdf_Document object; instance of ' . get_class($object) . ' given.');
        }

        $fontDescriptor = $this->_buildFontDescriptorDictionary();

        /* Eventually, this method will do additional processing on the font
         * program itself to create a subset using only the glyphs which
         * appear in the specified file. The space savings can be dramatic.
         */
        $fontFileStream = $this->_buildFontFileStream();
        if (! empty($fontFileStream)) {
            $fontFile = $output->addObject($fontFileStream);
            $fontFile->finalize();
            $fontDescriptor['FontFile2'] = $fontFile;
        }
                
        $descendantFont = new RE_Pdf_Object_Dictionary(array(
            'Type'           => new RE_Pdf_Object_Name('Font'),
            'Subtype'        => new RE_Pdf_Object_Name('CIDFontType2'),
            'BaseFont'       => $this['BaseFont'],
            'CIDSystemInfo'  => new RE_Pdf_Object_Dictionary(array(
                                    'Registry'   => new RE_Pdf_Object_String('Adobe'),
                                    'Ordering'   => new RE_Pdf_Object_String('Identity'),
                                    'Supplement' => new RE_Pdf_Object_Numeric_Integer(0),
                                    )),
            'FontDescriptor' => $output->addObject($fontDescriptor),
            'W'              => $this->_buildCIDWidthsArray(),
            ));
        
        if (! empty($fontFileStream)) {
            $descendantFont['CIDToGIDMap'] = new RE_Pdf_Object_Name('Identity');
        }
        
        $this['DescendantFonts'] = new RE_Pdf_Object_Array(array(
            $output->addObject($descendantFont)
            ));
        
        parent::write($output);
    }



  /**** Internal Interface ****/

    /**
     * Builds and returns the font descriptor object, which contains the
     * metrics and other attributes for the font as a whole. It may also
     * contain the embedded font program.
     * 
     * @return RE_Pdf_Object_Dictionary
     */
    protected function _buildFontDescriptorDictionary()
    {
        /** @todo Create a RE_Pdf_Font_Descriptor object instead. */
        $fontDescriptor = new RE_Pdf_Object_Dictionary();

        $fontDescriptor['Type']     = new RE_Pdf_Object_Name('FontDescriptor');
        $fontDescriptor['FontName'] = $this['BaseFont'];

        /* The font flags value is a bitfield that describes the stylistic
         * attributes of the font. We will set as many of the bits as can be
         * determined from the font parser.
         */
        $flags = 0;
        if ($this->_isMonospaced) {    // bit 1: FixedPitch
            $flags |= 1 << 0;
        }
        if ($this->_fontParser->isSerifFont) {    // bit 2: Serif
            $flags |= 1 << 1;
        }

        $flags |= 1 << 2;    // bit 3: Symbolic - always set

        if ($this->_fontParser->isScriptFont) {    // bit 4: Script
            $flags |= 1 << 3;
        }

        // bit 6: Nonsymbolic - never set

        if ($this->_isItalic) {    // bit 7: Italic
            $flags |= 1 << 6;
        }
        // bits 17-19: AllCap, SmallCap, ForceBold; not available
        $fontDescriptor['Flags'] = new RE_Pdf_Object_Numeric($flags);
        
        $fontDescriptor['FontBBox'] = new RE_Pdf_Object_Array(array(
            new RE_Pdf_Object_Numeric($this->_toEmSpace($this->_fontParser->xMin)),
            new RE_Pdf_Object_Numeric($this->_toEmSpace($this->_fontParser->yMin)),
            new RE_Pdf_Object_Numeric($this->_toEmSpace($this->_fontParser->xMax)),
            new RE_Pdf_Object_Numeric($this->_toEmSpace($this->_fontParser->yMax))
            ));

        $fontDescriptor['ItalicAngle'] = new RE_Pdf_Object_Numeric($this->_fontParser->italicAngle);

        $fontDescriptor['Ascent']  = new RE_Pdf_Object_Numeric($this->_toEmSpace($this->_ascent));
        $fontDescriptor['Descent'] = new RE_Pdf_Object_Numeric($this->_toEmSpace($this->_descent));

        $fontDescriptor['CapHeight'] = new RE_Pdf_Object_Numeric($this->_fontParser->capitalHeight);
        /* The vertical stem width is not yet extracted from the OpenType font
         * file. For now, record zero which is interpreted as 'unknown'.
         */
        /** @todo Calculate value for StemV. */
        $fontDescriptor['StemV'] = new RE_Pdf_Object_Numeric(0);

        return $fontDescriptor;
    }
    
    /**
     * Builds and returns the stream object which embeds the actual font
     * program within the PDF document. Returns null if the font program
     * cannot be embedded.
     * 
     * @return RE_Pdf_Object_Stream|null
     */
    protected function _buildFontFileStream()
    {
        $fileSize = $this->_fontParser->getSize();
        
        $fontFile = new RE_Pdf_Object_Stream();

        $fontFile['Filter']  = new RE_Pdf_Object_Name('FlateDecode');
        $fontFile['Length1'] = new RE_Pdf_Object_Numeric($fileSize);
        
        $dataProvider = new RE_Pdf_Object_StreamDataProvider_FileParser($this->_fontParser, 0, $fileSize);
        $fontFile->setDataProvider($dataProvider);

        return $fontFile;
    }

}
