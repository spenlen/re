<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage TextLayout
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */

/** RE_Pdf_Font_Standard */
require_once 'RE/Pdf/Font/Standard.php';


/**
 * Implementation for the standard PDF font Symbol.
 *
 * This class was generated automatically using the font information and metric
 * data contained in the Adobe Font Metric (AFM) files, available here:
 * {@link http://partners.adobe.com/public/developer/en/pdf/Core14_AFMs.zip}
 *
 * IMPORTANT: This class file was generated automatically by the
 * processCoreAFM.php script, located in the /tools/pdf directory of the
 * framework distribution. If you need to make changes to this class, you must
 * modify that script and regenerate the class instead of changing this file
 * by hand.
 *
 * @package    Pdf
 * @subpackage TextLayout
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Pdf_Font_Standard_Symbol extends RE_Pdf_Font_Standard
{
  /**** Instance Variables ****/


    /**
     * Array for conversion from special font encoding to Unicode encoding.
     * See {@link toUnicode()}.
     * @var array
     */
    protected $_fromFontEncoding = array(
            0x20 => "\x00\x20",   0x21 => "\x00\x21",   0x22 => "\x22\x00",
            0x23 => "\x00\x23",   0x24 => "\x22\x03",   0x25 => "\x00\x25",
            0x26 => "\x00\x26",   0x27 => "\x22\x0b",   0x28 => "\x00\x28",
            0x29 => "\x00\x29",   0x2a => "\x22\x17",   0x2b => "\x00\x2b",
            0x2c => "\x00\x2c",   0x2d => "\x22\x12",   0x2e => "\x00\x2e",
            0x2f => "\x00\x2f",   0x30 => "\x00\x30",   0x31 => "\x00\x31",
            0x32 => "\x00\x32",   0x33 => "\x00\x33",   0x34 => "\x00\x34",
            0x35 => "\x00\x35",   0x36 => "\x00\x36",   0x37 => "\x00\x37",
            0x38 => "\x00\x38",   0x39 => "\x00\x39",   0x3a => "\x00\x3a",
            0x3b => "\x00\x3b",   0x3c => "\x00\x3c",   0x3d => "\x00\x3d",
            0x3e => "\x00\x3e",   0x3f => "\x00\x3f",   0x40 => "\x22\x45",
            0x41 => "\x03\x91",   0x42 => "\x03\x92",   0x43 => "\x03\xa7",
            0x44 => "\x22\x06",   0x45 => "\x03\x95",   0x46 => "\x03\xa6",
            0x47 => "\x03\x93",   0x48 => "\x03\x97",   0x49 => "\x03\x99",
            0x4a => "\x03\xd1",   0x4b => "\x03\x9a",   0x4c => "\x03\x9b",
            0x4d => "\x03\x9c",   0x4e => "\x03\x9d",   0x4f => "\x03\x9f",
            0x50 => "\x03\xa0",   0x51 => "\x03\x98",   0x52 => "\x03\xa1",
            0x53 => "\x03\xa3",   0x54 => "\x03\xa4",   0x55 => "\x03\xa5",
            0x56 => "\x03\xc2",   0x57 => "\x21\x26",   0x58 => "\x03\x9e",
            0x59 => "\x03\xa8",   0x5a => "\x03\x96",   0x5b => "\x00\x5b",
            0x5c => "\x22\x34",   0x5d => "\x00\x5d",   0x5e => "\x22\xa5",
            0x5f => "\x00\x5f",   0x60 => "\xf8\xe5",   0x61 => "\x03\xb1",
            0x62 => "\x03\xb2",   0x63 => "\x03\xc7",   0x64 => "\x03\xb4",
            0x65 => "\x03\xb5",   0x66 => "\x03\xc6",   0x67 => "\x03\xb3",
            0x68 => "\x03\xb7",   0x69 => "\x03\xb9",   0x6a => "\x03\xd5",
            0x6b => "\x03\xba",   0x6c => "\x03\xbb",   0x6d => "\x00\xb5",
            0x6e => "\x03\xbd",   0x6f => "\x03\xbf",   0x70 => "\x03\xc0",
            0x71 => "\x03\xb8",   0x72 => "\x03\xc1",   0x73 => "\x03\xc3",
            0x74 => "\x03\xc4",   0x75 => "\x03\xc5",   0x76 => "\x03\xd6",
            0x77 => "\x03\xc9",   0x78 => "\x03\xbe",   0x79 => "\x03\xc8",
            0x7a => "\x03\xb6",   0x7b => "\x00\x7b",   0x7c => "\x00\x7c",
            0x7d => "\x00\x7d",   0x7e => "\x22\x3c",   0xa0 => "\x20\xac",
            0xa1 => "\x03\xd2",   0xa2 => "\x20\x32",   0xa3 => "\x22\x64",
            0xa4 => "\x20\x44",   0xa5 => "\x22\x1e",   0xa6 => "\x01\x92",
            0xa7 => "\x26\x63",   0xa8 => "\x26\x66",   0xa9 => "\x26\x65",
            0xaa => "\x26\x60",   0xab => "\x21\x94",   0xac => "\x21\x90",
            0xad => "\x21\x91",   0xae => "\x21\x92",   0xaf => "\x21\x93",
            0xb0 => "\x00\xb0",   0xb1 => "\x00\xb1",   0xb2 => "\x20\x33",
            0xb3 => "\x22\x65",   0xb4 => "\x00\xd7",   0xb5 => "\x22\x1d",
            0xb6 => "\x22\x02",   0xb7 => "\x20\x22",   0xb8 => "\x00\xf7",
            0xb9 => "\x22\x60",   0xba => "\x22\x61",   0xbb => "\x22\x48",
            0xbc => "\x20\x26",   0xbd => "\xf8\xe6",   0xbe => "\xf8\xe7",
            0xbf => "\x21\xb5",   0xc0 => "\x21\x35",   0xc1 => "\x21\x11",
            0xc2 => "\x21\x1c",   0xc3 => "\x21\x18",   0xc4 => "\x22\x97",
            0xc5 => "\x22\x95",   0xc6 => "\x22\x05",   0xc7 => "\x22\x29",
            0xc8 => "\x22\x2a",   0xc9 => "\x22\x83",   0xca => "\x22\x87",
            0xcb => "\x22\x84",   0xcc => "\x22\x82",   0xcd => "\x22\x86",
            0xce => "\x22\x08",   0xcf => "\x22\x09",   0xd0 => "\x22\x20",
            0xd1 => "\x22\x07",   0xd2 => "\xf6\xda",   0xd3 => "\xf6\xd9",
            0xd4 => "\xf6\xdb",   0xd5 => "\x22\x0f",   0xd6 => "\x22\x1a",
            0xd7 => "\x22\xc5",   0xd8 => "\x00\xac",   0xd9 => "\x22\x27",
            0xda => "\x22\x28",   0xdb => "\x21\xd4",   0xdc => "\x21\xd0",
            0xdd => "\x21\xd1",   0xde => "\x21\xd2",   0xdf => "\x21\xd3",
            0xe0 => "\x25\xca",   0xe1 => "\x23\x29",   0xe2 => "\xf8\xe8",
            0xe3 => "\xf8\xe9",   0xe4 => "\xf8\xea",   0xe5 => "\x22\x11",
            0xe6 => "\xf8\xeb",   0xe7 => "\xf8\xec",   0xe8 => "\xf8\xed",
            0xe9 => "\xf8\xee",   0xea => "\xf8\xef",   0xeb => "\xf8\xf0",
            0xec => "\xf8\xf1",   0xed => "\xf8\xf2",   0xee => "\xf8\xf3",
            0xef => "\xf8\xf4",   0xf1 => "\x23\x2a",   0xf2 => "\x22\x2b",
            0xf3 => "\x23\x20",   0xf4 => "\xf8\xf5",   0xf5 => "\x23\x21",
            0xf6 => "\xf8\xf6",   0xf7 => "\xf8\xf7",   0xf8 => "\xf8\xf8",
            0xf9 => "\xf8\xf9",   0xfa => "\xf8\xfa",   0xfb => "\xf8\xfb",
            0xfc => "\xf8\xfc",   0xfd => "\xf8\xfd",   0xfe => "\xf8\xfe",
        );



  /**** Public Interface ****/


  /* Object Lifecycle */

    /**
     * Object constructor
     * 
     * @param array $value (optional)
     */
    public function __construct($value = null)
    {
        parent::__construct($value);


        /* Object properties */

        /* The font names are stored internally as Unicode UTF-16BE-encoded
         * strings. Since this information is static, save unnecessary trips
         * through iconv() and just use pre-encoded hexidecimal strings.
         */
        $this->_fontNames[RE_Pdf_Font::NAME_COPYRIGHT]['en'] =
          "\x00\x43\x00\x6f\x00\x70\x00\x79\x00\x72\x00\x69\x00\x67\x00\x68\x00"
          . "\x74\x00\x20\x00\x28\x00\x63\x00\x29\x00\x20\x00\x31\x00\x39\x00"
          . "\x38\x00\x35\x00\x2c\x00\x20\x00\x31\x00\x39\x00\x38\x00\x37\x00"
          . "\x2c\x00\x20\x00\x31\x00\x39\x00\x38\x00\x39\x00\x2c\x00\x20\x00"
          . "\x31\x00\x39\x00\x39\x00\x30\x00\x2c\x00\x20\x00\x31\x00\x39\x00"
          . "\x39\x00\x37\x00\x20\x00\x41\x00\x64\x00\x6f\x00\x62\x00\x65\x00"
          . "\x20\x00\x53\x00\x79\x00\x73\x00\x74\x00\x65\x00\x6d\x00\x73\x00"
          . "\x20\x00\x49\x00\x6e\x00\x63\x00\x6f\x00\x72\x00\x70\x00\x6f\x00"
          . "\x72\x00\x61\x00\x74\x00\x65\x00\x64\x00\x2e\x00\x20\x00\x41\x00"
          . "\x6c\x00\x6c\x00\x20\x00\x72\x00\x69\x00\x67\x00\x68\x00\x74\x00"
          . "\x73\x00\x20\x00\x72\x00\x65\x00\x73\x00\x65\x00\x72\x00\x76\x00"
          . "\x65\x00\x64\x00\x2e";
        $this->_fontNames[RE_Pdf_Font::NAME_FAMILY]['en'] =
          "\x00\x53\x00\x79\x00\x6d\x00\x62\x00\x6f\x00\x6c";
        $this->_fontNames[RE_Pdf_Font::NAME_STYLE]['en'] =
          "\x00\x4d\x00\x65\x00\x64\x00\x69\x00\x75\x00\x6d";
        $this->_fontNames[RE_Pdf_Font::NAME_ID]['en'] =
          "\x00\x34\x00\x33\x00\x30\x00\x36\x00\x34";
        $this->_fontNames[RE_Pdf_Font::NAME_FULL]['en'] =
          "\x00\x53\x00\x79\x00\x6d\x00\x62\x00\x6f\x00\x6c\x00\x20\x00\x4d\x00"
          . "\x65\x00\x64\x00\x69\x00\x75\x00\x6d";
        $this->_fontNames[RE_Pdf_Font::NAME_VERSION]['en'] =
          "\x00\x30\x00\x30\x00\x31\x00\x2e\x00\x30\x00\x30\x00\x38";
        $this->_fontNames[RE_Pdf_Font::NAME_POSTSCRIPT]['en'] =
          "\x00\x53\x00\x79\x00\x6d\x00\x62\x00\x6f\x00\x6c";

        $this->_isBold = false;
        $this->_isItalic = false;
        $this->_isMonospaced = false;

        $this->_underlinePosition = -100;
        $this->_underlineThickness = 50;
        $this->_strikePosition = 225;
        $this->_strikeThickness = 50;

        $this->_unitsPerEm = 1000;

        $this->_ascent  = 1000;
        $this->_descent = 0;
        $this->_lineGap = 200;

        $this->_glyphWidths = array(
            0x00 => 0x01f4,   0x20 =>   0xfa,   0x21 => 0x014d,   0x22 => 0x02c9,
            0x23 => 0x01f4,   0x24 => 0x0225,   0x25 => 0x0341,   0x26 => 0x030a,
            0x27 => 0x01b7,   0x28 => 0x014d,   0x29 => 0x014d,   0x2a => 0x01f4,
            0x2b => 0x0225,   0x2c =>   0xfa,   0x2d => 0x0225,   0x2e =>   0xfa,
            0x2f => 0x0116,   0x30 => 0x01f4,   0x31 => 0x01f4,   0x32 => 0x01f4,
            0x33 => 0x01f4,   0x34 => 0x01f4,   0x35 => 0x01f4,   0x36 => 0x01f4,
            0x37 => 0x01f4,   0x38 => 0x01f4,   0x39 => 0x01f4,   0x3a => 0x0116,
            0x3b => 0x0116,   0x3c => 0x0225,   0x3d => 0x0225,   0x3e => 0x0225,
            0x3f => 0x01bc,   0x40 => 0x0225,   0x41 => 0x02d2,   0x42 => 0x029b,
            0x43 => 0x02d2,   0x44 => 0x0264,   0x45 => 0x0263,   0x46 => 0x02fb,
            0x47 => 0x025b,   0x48 => 0x02d2,   0x49 => 0x014d,   0x4a => 0x0277,
            0x4b => 0x02d2,   0x4c => 0x02ae,   0x4d => 0x0379,   0x4e => 0x02d2,
            0x4f => 0x02d2,   0x50 => 0x0300,   0x51 => 0x02e5,   0x52 => 0x022c,
            0x53 => 0x0250,   0x54 => 0x0263,   0x55 => 0x02b2,   0x56 => 0x01b7,
            0x57 => 0x0300,   0x58 => 0x0285,   0x59 => 0x031b,   0x5a => 0x0263,
            0x5b => 0x014d,   0x5c => 0x035f,   0x5d => 0x014d,   0x5e => 0x0292,
            0x5f => 0x01f4,   0x60 => 0x01f4,   0x61 => 0x0277,   0x62 => 0x0225,
            0x63 => 0x0225,   0x64 => 0x01ee,   0x65 => 0x01b7,   0x66 => 0x0209,
            0x67 => 0x019b,   0x68 => 0x025b,   0x69 => 0x0149,   0x6a => 0x025b,
            0x6b => 0x0225,   0x6c => 0x0225,   0x6d => 0x0240,   0x6e => 0x0209,
            0x6f => 0x0225,   0x70 => 0x0225,   0x71 => 0x0209,   0x72 => 0x0225,
            0x73 => 0x025b,   0x74 => 0x01b7,   0x75 => 0x0240,   0x76 => 0x02c9,
            0x77 => 0x02ae,   0x78 => 0x01ed,   0x79 => 0x02ae,   0x7a => 0x01ee,
            0x7b => 0x01e0,   0x7c =>   0xc8,   0x7d => 0x01e0,   0x7e => 0x0225,
            0xa0 => 0x02ee,   0xa1 => 0x026c,   0xa2 =>   0xf7,   0xa3 => 0x0225,
            0xa4 =>   0xa7,   0xa5 => 0x02c9,   0xa6 => 0x01f4,   0xa7 => 0x02f1,
            0xa8 => 0x02f1,   0xa9 => 0x02f1,   0xaa => 0x02f1,   0xab => 0x0412,
            0xac => 0x03db,   0xad => 0x025b,   0xae => 0x03db,   0xaf => 0x025b,
            0xb0 => 0x0190,   0xb1 => 0x0225,   0xb2 => 0x019b,   0xb3 => 0x0225,
            0xb4 => 0x0225,   0xb5 => 0x02c9,   0xb6 => 0x01ee,   0xb7 => 0x01cc,
            0xb8 => 0x0225,   0xb9 => 0x0225,   0xba => 0x0225,   0xbb => 0x0225,
            0xbc => 0x03e8,   0xbd => 0x025b,   0xbe => 0x03e8,   0xbf => 0x0292,
            0xc0 => 0x0337,   0xc1 => 0x02ae,   0xc2 => 0x031b,   0xc3 => 0x03db,
            0xc4 => 0x0300,   0xc5 => 0x0300,   0xc6 => 0x0337,   0xc7 => 0x0300,
            0xc8 => 0x0300,   0xc9 => 0x02c9,   0xca => 0x02c9,   0xcb => 0x02c9,
            0xcc => 0x02c9,   0xcd => 0x02c9,   0xce => 0x02c9,   0xcf => 0x02c9,
            0xd0 => 0x0300,   0xd1 => 0x02c9,   0xd2 => 0x0316,   0xd3 => 0x0316,
            0xd4 => 0x037a,   0xd5 => 0x0337,   0xd6 => 0x0225,   0xd7 =>   0xfa,
            0xd8 => 0x02c9,   0xd9 => 0x025b,   0xda => 0x025b,   0xdb => 0x0412,
            0xdc => 0x03db,   0xdd => 0x025b,   0xde => 0x03db,   0xdf => 0x025b,
            0xe0 => 0x01ee,   0xe1 => 0x0149,   0xe2 => 0x0316,   0xe3 => 0x0316,
            0xe4 => 0x0312,   0xe5 => 0x02c9,   0xe6 => 0x0180,   0xe7 => 0x0180,
            0xe8 => 0x0180,   0xe9 => 0x0180,   0xea => 0x0180,   0xeb => 0x0180,
            0xec => 0x01ee,   0xed => 0x01ee,   0xee => 0x01ee,   0xef => 0x01ee,
            0xf1 => 0x0149,   0xf2 => 0x0112,   0xf3 => 0x02ae,   0xf4 => 0x02ae,
            0xf5 => 0x02ae,   0xf6 => 0x0180,   0xf7 => 0x0180,   0xf8 => 0x0180,
            0xf9 => 0x0180,   0xfa => 0x0180,   0xfb => 0x0180,   0xfc => 0x01ee,
            0xfd => 0x01ee,   0xfe => 0x01ee);

        $cmapData = array(
            0x20 =>   0x20,   0x21 =>   0x21,   0x23 =>   0x23,   0x25 =>   0x25,
            0x26 =>   0x26,   0x28 =>   0x28,   0x29 =>   0x29,   0x2b =>   0x2b,
            0x2c =>   0x2c,   0x2e =>   0x2e,   0x2f =>   0x2f,   0x30 =>   0x30,
            0x31 =>   0x31,   0x32 =>   0x32,   0x33 =>   0x33,   0x34 =>   0x34,
            0x35 =>   0x35,   0x36 =>   0x36,   0x37 =>   0x37,   0x38 =>   0x38,
            0x39 =>   0x39,   0x3a =>   0x3a,   0x3b =>   0x3b,   0x3c =>   0x3c,
            0x3d =>   0x3d,   0x3e =>   0x3e,   0x3f =>   0x3f,   0x5b =>   0x5b,
            0x5d =>   0x5d,   0x5f =>   0x5f,   0x7b =>   0x7b,   0x7c =>   0x7c,
            0x7d =>   0x7d,   0xac =>   0xd8,   0xb0 =>   0xb0,   0xb1 =>   0xb1,
            0xb5 =>   0x6d,   0xd7 =>   0xb4,   0xf7 =>   0xb8, 0x0192 =>   0xa6,
          0x0391 =>   0x41, 0x0392 =>   0x42, 0x0393 =>   0x47, 0x0395 =>   0x45,
          0x0396 =>   0x5a, 0x0397 =>   0x48, 0x0398 =>   0x51, 0x0399 =>   0x49,
          0x039a =>   0x4b, 0x039b =>   0x4c, 0x039c =>   0x4d, 0x039d =>   0x4e,
          0x039e =>   0x58, 0x039f =>   0x4f, 0x03a0 =>   0x50, 0x03a1 =>   0x52,
          0x03a3 =>   0x53, 0x03a4 =>   0x54, 0x03a5 =>   0x55, 0x03a6 =>   0x46,
          0x03a7 =>   0x43, 0x03a8 =>   0x59, 0x03b1 =>   0x61, 0x03b2 =>   0x62,
          0x03b3 =>   0x67, 0x03b4 =>   0x64, 0x03b5 =>   0x65, 0x03b6 =>   0x7a,
          0x03b7 =>   0x68, 0x03b8 =>   0x71, 0x03b9 =>   0x69, 0x03ba =>   0x6b,
          0x03bb =>   0x6c, 0x03bd =>   0x6e, 0x03be =>   0x78, 0x03bf =>   0x6f,
          0x03c0 =>   0x70, 0x03c1 =>   0x72, 0x03c2 =>   0x56, 0x03c3 =>   0x73,
          0x03c4 =>   0x74, 0x03c5 =>   0x75, 0x03c6 =>   0x66, 0x03c7 =>   0x63,
          0x03c8 =>   0x79, 0x03c9 =>   0x77, 0x03d1 =>   0x4a, 0x03d2 =>   0xa1,
          0x03d5 =>   0x6a, 0x03d6 =>   0x76, 0x2022 =>   0xb7, 0x2026 =>   0xbc,
          0x2032 =>   0xa2, 0x2033 =>   0xb2, 0x2044 =>   0xa4, 0x20ac =>   0xa0,
          0x2111 =>   0xc1, 0x2118 =>   0xc3, 0x211c =>   0xc2, 0x2126 =>   0x57,
          0x2135 =>   0xc0, 0x2190 =>   0xac, 0x2191 =>   0xad, 0x2192 =>   0xae,
          0x2193 =>   0xaf, 0x2194 =>   0xab, 0x21b5 =>   0xbf, 0x21d0 =>   0xdc,
          0x21d1 =>   0xdd, 0x21d2 =>   0xde, 0x21d3 =>   0xdf, 0x21d4 =>   0xdb,
          0x2200 =>   0x22, 0x2202 =>   0xb6, 0x2203 =>   0x24, 0x2205 =>   0xc6,
          0x2206 =>   0x44, 0x2207 =>   0xd1, 0x2208 =>   0xce, 0x2209 =>   0xcf,
          0x220b =>   0x27, 0x220f =>   0xd5, 0x2211 =>   0xe5, 0x2212 =>   0x2d,
          0x2217 =>   0x2a, 0x221a =>   0xd6, 0x221d =>   0xb5, 0x221e =>   0xa5,
          0x2220 =>   0xd0, 0x2227 =>   0xd9, 0x2228 =>   0xda, 0x2229 =>   0xc7,
          0x222a =>   0xc8, 0x222b =>   0xf2, 0x2234 =>   0x5c, 0x223c =>   0x7e,
          0x2245 =>   0x40, 0x2248 =>   0xbb, 0x2260 =>   0xb9, 0x2261 =>   0xba,
          0x2264 =>   0xa3, 0x2265 =>   0xb3, 0x2282 =>   0xcc, 0x2283 =>   0xc9,
          0x2284 =>   0xcb, 0x2286 =>   0xcd, 0x2287 =>   0xca, 0x2295 =>   0xc5,
          0x2297 =>   0xc4, 0x22a5 =>   0x5e, 0x22c5 =>   0xd7, 0x2320 =>   0xf3,
          0x2321 =>   0xf5, 0x2329 =>   0xe1, 0x232a =>   0xf1, 0x25ca =>   0xe0,
          0x2660 =>   0xaa, 0x2663 =>   0xa7, 0x2665 =>   0xa9, 0x2666 =>   0xa8,
          0xf6d9 =>   0xd3, 0xf6da =>   0xd2, 0xf6db =>   0xd4, 0xf8e5 =>   0x60,
          0xf8e6 =>   0xbd, 0xf8e7 =>   0xbe, 0xf8e8 =>   0xe2, 0xf8e9 =>   0xe3,
          0xf8ea =>   0xe4, 0xf8eb =>   0xe6, 0xf8ec =>   0xe7, 0xf8ed =>   0xe8,
          0xf8ee =>   0xe9, 0xf8ef =>   0xea, 0xf8f0 =>   0xeb, 0xf8f1 =>   0xec,
          0xf8f2 =>   0xed, 0xf8f3 =>   0xee, 0xf8f4 =>   0xef, 0xf8f5 =>   0xf4,
          0xf8f6 =>   0xf6, 0xf8f7 =>   0xf7, 0xf8f8 =>   0xf8, 0xf8f9 =>   0xf9,
          0xf8fa =>   0xfa, 0xf8fb =>   0xfb, 0xf8fc =>   0xfc, 0xf8fd =>   0xfd,
          0xf8fe =>   0xfe);
        $this->cmap = RE_Font_Cmap::cmapWithTypeData(
          RE_Font_Cmap::TYPE_BYTE_ENCODING_STATIC, $cmapData);


        /* Resource dictionary */

        $this['BaseFont'] = 'Symbol';

        /* This font has a built-in custom character encoding method. Don't
         * override with WinAnsi like the other built-in fonts or else it will
         * not work as expected.
         */
        unset($this['Encoding']);
    }


  /* Information and Conversion Methods */

    /**
     * Converts a Latin-encoded string that fakes the font's internal encoding
     * to the proper Unicode characters, in UTF-8 encoding.
     *
     * Used to maintain backwards compatibility with the 20 year-old legacy
     * method of using this font, which is still employed by recent versions of
     * some popular word processors.
     *
     * Note that using this method adds overhead due to the additional
     * character conversion. Don't use this for new code; it is more efficient
     * to use the appropriate Unicode characters directly.
     *
     * @param string $string
     * @param string $charEncoding (optional) Character encoding of source
     *   string. Defaults to current locale.
     * @return string
     */
    public function toUnicode($string, $charEncoding = '')
    {
        /* When using these faked strings, the closest match to the font's
         * internal encoding is ISO-8859-1.
         */
        if ($charEncoding != 'ISO-8859-1') {
            $string = iconv($charEncoding, 'ISO-8859-1', $string);
        }
        $decodedString = '';
        for ($i = 0; $i < strlen($string); $i++) {
            $characterCode = ord($string[$i]);
            if (isset($this->_fromFontEncoding[$characterCode])) {
                $decodedString .= $this->_fromFontEncoding[$characterCode];
            } else {
                /* Unknown characters are removed completely.
                 */
                /** @todo Consider inserting the Unicode substitution character (U+FFFD). */
            }
        }
        return iconv('UTF-16BE', 'UTF-8', $decodedString);
    }

}
