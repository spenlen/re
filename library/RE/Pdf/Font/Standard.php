<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage TextLayout
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * Abstract class definition for the standard 14 Type 1 PDF fonts, which are
 * guaranteed to be availble in any PDF viewer implementation.
 *
 * The standard fonts and the corresponding subclasses that manage them:
 *  - Courier               - {@link RE_Pdf_Font_Standard_Courier}
 *  - Courier-Bold          - {@link RE_Pdf_Font_Standard_CourierBold}
 *  - Courier-Oblique       - {@link RE_Pdf_Font_Standard_CourierOblique}
 *  - Courier-BoldOblique   - {@link RE_Pdf_Font_Standard_CourierBoldOblique}
 *  - Helvetica             - {@link RE_Pdf_Font_Standard_Helvetica}
 *  - Helvetica-Bold        - {@link RE_Pdf_Font_Standard_HelveticaBold}
 *  - Helvetica-Oblique     - {@link RE_Pdf_Font_Standard_HelveticaOblique}
 *  - Helvetica-BoldOblique - {@link RE_Pdf_Font_Standard_HelveticaBoldOblique}
 *  - Symbol                - {@link RE_Pdf_Font_Standard_Symbol}
 *  - Times                 - {@link RE_Pdf_Font_Standard_Times}
 *  - Times-Bold            - {@link RE_Pdf_Font_Standard_TimesBold}
 *  - Times-Italic          - {@link RE_Pdf_Font_Standard_TimesItalic}
 *  - Times-BoldItalic      - {@link RE_Pdf_Font_Standard_TimesBoldItalic}
 *  - ZapfDingbats          - {@link RE_Pdf_Font_Standard_ZapfDingbats}
 *
 * Font objects should be normally be obtained from the factory methods
 * {@link RE_Pdf_Font::fontWithName} and {@link RE_Pdf_Font::fontWithPath}.
 *
 * @package    Pdf
 * @subpackage TextLayout
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
abstract class RE_Pdf_Font_Standard extends RE_Pdf_Font
{
  /**** Public Interface ****/


  /* Object Lifecycle */

    /**
     * Object constructor
     * 
     * @param array $value (optional)
     */
    public function __construct($value = null)
    {
        parent::__construct($value);
        
        $this['Subtype']  = 'Type1';
        
        $this['Encoding'] = new RE_Pdf_Object_Dictionary(array(
            'Type'         => new RE_Pdf_Object_Name('Encoding'),
            'BaseEncoding' => new RE_Pdf_Object_Name('WinAnsiEncoding'),
            'Differences'  => new RE_Pdf_Object_Array(array(
                new RE_Pdf_Object_Numeric(8),
                new RE_Pdf_Object_Name('breve'),
                new RE_Pdf_Object_Name('caron'),
                new RE_Pdf_Object_Name('commaaccent'),
                new RE_Pdf_Object_Name('Dcroat'),
                new RE_Pdf_Object_Name('dcroat'),
                new RE_Pdf_Object_Name('Delta'),
                new RE_Pdf_Object_Name('dotaccent'),
                new RE_Pdf_Object_Name('dotlessi'),
                new RE_Pdf_Object_Name('fi'),
                new RE_Pdf_Object_Name('fl'),
                new RE_Pdf_Object_Name('fraction'),
                new RE_Pdf_Object_Name('greaterequal'),
                new RE_Pdf_Object_Name('hungarumlaut'),
                new RE_Pdf_Object_Name('lessequal'),
                new RE_Pdf_Object_Name('lozenge'),
                new RE_Pdf_Object_Name('Lslash'),
                new RE_Pdf_Object_Name('lslash'),
                new RE_Pdf_Object_Name('minus'),
                new RE_Pdf_Object_Name('notequal'),
                new RE_Pdf_Object_Name('ogonek'),
                new RE_Pdf_Object_Name('partialdiff'),
                new RE_Pdf_Object_Name('radical'),
                new RE_Pdf_Object_Name('ring'),
                new RE_Pdf_Object_Name('summation'),
                ))
            ));
    }


  /* PDF Drawing */
  
    /**
     * Overridden because the standard PDF fonts use a 1-byte glyph encoding.
     * 
     * @param RE_Pdf_Canvas_Interface $canvas
     * @param array                   $glyphs
     */
    public function showGlyphs(RE_Pdf_Canvas_Interface $canvas, array $glyphs)
    {
        $packedGlyphs = '';
        foreach ($glyphs as $glyph) {
            $packedGlyphs .= chr($glyph);
        }

        RE_Pdf_Object_String::writeValue($canvas, $packedGlyphs);
        $canvas->writeBytes(" Tj\n");
    }


  /* ArrayAccess Interface Methods */

    /* Overridden to enforce specific values and/or types for certain
     * dictionary keys.
     * 
     * @throws InvalidArgumentException
     */
    public function offsetSet($offset, $value)
    {
        parent::offsetSet($offset, $value);
        
        switch ($offset) {
        case 'Subtype':
            if ($this['Subtype']->getValue() !== 'Type1') {
                throw new InvalidArgumentException("Value for key 'Subtype' must be 'Type1'");
            }
            break;
        }
    }

}
