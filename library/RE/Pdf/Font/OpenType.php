<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage TextLayout
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * OpenType fonts implementation
 *
 * OpenType fonts can contain either TrueType or PostScript Type 1 outlines. The
 * code in this class is common to both types. However, you will only deal
 * directly with subclasses.
 *
 * Font objects should be normally be obtained from the factory methods
 * {@link RE_Pdf_Font::fontWithName} and {@link RE_Pdf_Font::fontWithPath}.
 *
 * @todo Reintroduce better handling of embedding permission flags.
 * 
 * @package    Pdf
 * @subpackage TextLayout
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
abstract class RE_Pdf_Font_OpenType extends RE_Pdf_Font
{
  /**** Instance Variables ****/
  
    /**
     * The file parser object for the font file.
     * @var RE_FileParser_Font_OpenType
     */
    protected $_fontParser = null;
  
  
  
  /**** Public Interface ****/


  /* Accessor Methods */

    /**
     * Extracts the common layout and metrics information from the font parser
     * and sets up the font resource dictionary. Should be called immediately
     * after instantiating a new font object.
     *
     * @param RE_FileParser_Font_OpenType $fontParser Font parser object
     *   containing OpenType file.
     * @throws RE_Pdf_Exception
     */
    public function setFontParser(RE_FileParser_Font_OpenType $fontParser)
    {
        $this->_fontParser = $fontParser;


        /* Object properties */

        $this->_fontNames = $fontParser->names;

        $this->_isBold = $fontParser->isBold;
        $this->_isItalic = $fontParser->isItalic;
        $this->_isMonospaced = $fontParser->isMonospaced;

        $this->_underlinePosition = $fontParser->underlinePosition;
        $this->_underlineThickness = $fontParser->underlineThickness;
        $this->_strikePosition = $fontParser->strikePosition;
        $this->_strikeThickness = $fontParser->strikeThickness;

        $this->_unitsPerEm = $fontParser->unitsPerEm;

        $this->_ascent  = $fontParser->ascent;
        $this->_descent = $fontParser->descent;
        $this->_lineGap = $fontParser->lineGap;

        $this->_glyphWidths = $fontParser->glyphWidths;

        $this->cmap = $fontParser->cmap;


        /* Resource dictionary */

        $this['Subtype']  = 'Type0';
        $this['BaseFont'] = $this->getFontName(RE_Pdf_Font::NAME_POSTSCRIPT, 'en', 'UTF-8');
        $this['Encoding'] = new RE_Pdf_Object_Name('Identity-H');
    }

}
