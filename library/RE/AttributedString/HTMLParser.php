<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Text
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */

/**
 * Helper class for {@link RE_AttributedString} that attempts to parse a chunk
 * of HTML markup and add the string content to the attributed string, setting
 * as many attributes as possible.
 *
 * @package    Text
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_AttributedString_HTMLParser
{
  /**** Public Interface ****/


  /* Parsing */

    /**
     * Appends the text contained in the HTML to the end of the string using
     * the specified initial character attributes. Modifies the attributes
     * as appropriate during processing of supported HTML tags and CSS style
     * property values.
     *
     * The HTML should include a <meta http-equiv="Content-Type"> tag in the <head>
     * element to define the character encoding used by the HTML. If the <head> or
     * <meta> elements are missing, they will be added and UTF-8 encoding assumed.
     *
     * If the HTML string could not be parsed, appends a string containing the
     * libXML error message.
     *
     * @param RE_AttributedString $string
     * @param string              $html
     * @param array               $attributes Array of initial attribute name/value pairs.
     */
    public function parseAndAppendToString(RE_AttributedString $string, $html, array $attributes)
    {
        if (strlen($html) < 1) {
            return;
        }

        $useErrors = libxml_use_internal_errors(true);

        $dom = new DOMDocument();
        $dom->loadHTML($this->_checkForHTMLStructure($html));

        $this->_appendDOMNodeToString($string, $dom->documentElement, $this->_checkForRequiredAttributes($attributes));

        foreach (libxml_get_errors() as $error) {
            if ($error->level == LIBXML_ERR_FATAL) {
                $string->appendString("ERROR parsing HTML on line " . $error->line . ": " . trim($error->message) . "\n", $attributes, 'UTF-8');
            }
        }

        libxml_clear_errors();
        libxml_use_internal_errors($useErrors);
    }



  /**** Internal Interface ****/


  /* Parsing */

    /**
     * Ensures that the minimal attributes required for parsing and processing
     * the HTML are present in the array. Returns the modified array.
     *
     * @param array $attributes
     * @return array
     */
    protected function _checkForRequiredAttributes(array $attributes)
    {
        if (! isset($attributes[RE_AttributedString::ATTRIBUTE_FONT])) {
            $attributes[RE_AttributedString::ATTRIBUTE_FONT] = 'Helvetica';
        }
        if (! isset($attributes[RE_AttributedString::ATTRIBUTE_FONT_SIZE])) {
            $attributes[RE_AttributedString::ATTRIBUTE_FONT_SIZE] = 12.0;
        }
        if (! isset($attributes[RE_AttributedString::ATTRIBUTE_COLOR])) {
            $attributes[RE_AttributedString::ATTRIBUTE_COLOR] = RE_Color::blackColor();
        }
        if (! isset($attributes[RE_AttributedString::ATTRIBUTE_PARAGRAPH_STYLE])) {
            $attributes[RE_AttributedString::ATTRIBUTE_PARAGRAPH_STYLE] = RE_ParagraphStyle::defaultParagraphStyle();
        }
        return $attributes;
    }

    /**
     * Ensures that the essential HTML structural elements -- html, head, and
     * body -- are present in the string and adds them if necessary. Also looks
     * for the Content-Type meta element and adds one specifying the UTF-8
     * character set if missing.
     *
     * @param string $html
     * @return string
     */
    protected function _checkForHTMLStructure($html)
    {
        if (stripos($html, '<body') === false) {
            $html = '<body>' . $html;
        }
        if (stripos($html, '</body>') === false) {
            $html .= '</body>';
        }

        if (stripos($html, '<head') === false) {
            $html = '<head></head>' . $html;
        }

        if (stripos($html, '<html') === false) {
            $html = '<html>' . $html;
        }
        if (stripos($html, '</html>') === false) {
            $html .= '</html>';
        }

        if (! preg_match('/<meta .+?(Content-Type.+?charset=|charset=.+?Content-Type).+?>/i', $html)) {
            $html = preg_replace('/(<head[^>]*>)/i', '$1<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>', $html);
        }

        return $html;
    }

    /**
     * Recursively appends the content in the DOMNode to the string using
     * the specified character attributes.
     *
     * @param RE_AttributedString $string
     * @param DOMNode             $domNode
     * @param array               $attributes
     * @param array               $state      (optional) for internal use
     * @return array
     */
    protected function _appendDOMNodeToString(RE_AttributedString $string, DOMNode $domNode, array $attributes, $state = array())
    {
        foreach ($domNode->childNodes as $node) {
            if ($node instanceof DOMElement) {

                $childAttributes = $attributes;

                $style = $node->getAttribute('style');
                switch ($node->tagName) {
                    case 'strong':
                    case 'b':
                        $style = 'font-weight:bold;' . $style;
                        break;
                    case 'em':
                    case 'i':
                        $style = 'font-style:italic;' . $style;
                        break;
                    case 's':
                        $style = 'text-decoration:line-through;' . $style;
                        break;
                    case 'u':
                        $style = 'text-decoration:underline;' . $style;
                        break;
                    case 'h1':
                    case 'h2':
                    case 'h3':
                    case 'h4':
                    case 'h5':
                    case 'h6':
                        $headingSize = '1em';
                        switch ($node->tagName) {
                            case 'h1': $headingSize = '2em'; break;
                            case 'h2': $headingSize = '1.5em'; break;
                            case 'h3': $headingSize = '1.3em'; break;
                            case 'h4': break;
                            case 'h5': $headingSize = '0.9em'; break;
                            case 'h6': $headingSize = '0.8em'; break;
                        }
                        $style = 'font-size:' . $headingSize . ';font-weight:bold;' . $style;
                        break;
                    case 'blockquote':
                        $delta = $this->_cssLengthToSize('1.5em', $childAttributes[RE_AttributedString::ATTRIBUTE_FONT_SIZE]);
                        $paragraphStyle = clone $childAttributes[RE_AttributedString::ATTRIBUTE_PARAGRAPH_STYLE];
                        $paragraphStyle->setHeadMargin($paragraphStyle->getHeadMargin() + $delta);
                        $paragraphStyle->setTailMargin($paragraphStyle->getTailMargin() - $delta);
                        $childAttributes[RE_AttributedString::ATTRIBUTE_PARAGRAPH_STYLE] = $paragraphStyle;
                        break;
                    case 'pre':
                        $paragraphStyle = clone $childAttributes[RE_AttributedString::ATTRIBUTE_PARAGRAPH_STYLE];
                        $paragraphStyle->setParagraphSpacingAfter(0);
                        $childAttributes[RE_AttributedString::ATTRIBUTE_PARAGRAPH_STYLE] = $paragraphStyle;
                        $state['preserveNewlines'] = true;
                        $style = 'line-height:1.1em;' . $style;
                        // break intentionally omitted
                    case 'code':
                        $style = 'font-family:monospace;' . $style;
                        break;
                    case 'sub':
                        $style = 'font-size:smaller;baseline-shift:sub;' . $style;
                        break;
                    case 'super':
                        $style = 'font-size:smaller;baseline-shift:super;' . $style;
                        break;
                    case 'center':
                        $style = 'text-align:center;' . $style;
                        break;
                    case 'big':
                        $style = 'font-size:larger;' . $style;
                        break;
                    case 'small':
                        $style = 'font-size:smaller;' . $style;
                        break;
                    case 'ol':
                    case 'ul':
                        $state['listType']  = $node->tagName;
                        $state['listIndex'] = 1;
                        $delta = $this->_cssLengthToSize('0.5em', $childAttributes[RE_AttributedString::ATTRIBUTE_FONT_SIZE]);
                        $paragraphStyle = clone $childAttributes[RE_AttributedString::ATTRIBUTE_PARAGRAPH_STYLE];
                        $paragraphStyle->setHeadMargin($paragraphStyle->getHeadMargin() + $delta);
                        $childAttributes[RE_AttributedString::ATTRIBUTE_PARAGRAPH_STYLE] = $paragraphStyle;
                        break;
                    case 'li':
                        if ($state['listType'] == 'ol') {
                            $string->appendString($state['listIndex'] . '. ', $childAttributes, 'UTF-8');
                            $state['listIndex']++;
                        } else {
                            $string->appendString("\x20\x22\x00\x20", $childAttributes, 'UTF-16BE');  // U+2022 BULLET
                        }
                }

                if (! empty($style)) {
                    $childAttributes = $this->_parseCSSStyle($childAttributes, $style);
                }
                if ($node->hasChildNodes()) {
                    $this->_appendDOMNodeToString($string, $node, $childAttributes, $state);
                }

                switch ($node->tagName) {
                    case 'br':
                        $string->appendString("\x20\x28", $childAttributes, 'UTF-16BE');  // U+2028 LINE SEPARATOR
                        break;
                    case 'p':
                    case 'h1':
                    case 'h2':
                    case 'h3':
                    case 'h4':
                    case 'h5':
                    case 'h6':
                    case 'li':
                    case 'pre':
                    case 'blockquote':
                        $string->appendString("\x20\x29", $childAttributes, 'UTF-16BE');  // U+2029 PARAGRAPH SEPARATOR
                        break;
                    case 'tab':
                        $string->appendString("\x00\x09", $childAttributes, 'UTF-16BE');  // U+0009 CHARACTER TABULATION
                        break;
                    case 'pagebreak':
                        $string->appendString("\x00\x0c", $childAttributes, 'UTF-16BE');  // U+000C FORM FEED (FF)
                        break;
                }

            } else if ($node instanceof DOMText) {
                $text = (! empty($state['preserveNewlines'])) ? str_replace("\n", "\xe2\x80\xa8", $node->data) : trim($node->data, "\r\n");
                if (strlen($text) > 0) {
                    $string->appendString($text, $attributes, 'UTF-8');
                }
            }
        }
    }


  /* CSS Styles */

    /**
     * Parses the CSS style properties and sets the corresponding character
     * attributes in the specified array. Returns the modified array.
     *
     * @param array  $attributes
     * @param string $style
     */
    protected function _parseCSSStyle(array $attributes, $style)
    {
        foreach (explode(';', $style) as $property) {
            $pos = strpos($property, ':');
            if ($pos < 3) {
                continue;
            }
            $name  = trim(substr($property, 0, $pos));
            $value = trim(substr($property, $pos + 1));
            if (strlen($value) < 1) {
                continue;
            }
            switch ($name) {
                case 'font':
                    $attributes = $this->_parseCSSFont($attributes, $value);
                    break;
                case 'font-family':
                    $attributes = $this->_parseCSSFontFamily($attributes, $value);
                    break;
                case 'font-size':
                    $attributes = $this->_parseCSSFontSize($attributes, $value);
                    break;
                case 'line-height':
                    $attributes = $this->_parseCSSLineHeight($attributes, $value);
                    break;
                case 'font-weight':
                    $attributes = $this->_parseCSSFontWeight($attributes, $value);
                    break;
                case 'font-style':
                    $attributes = $this->_parseCSSFontStyle($attributes, $value);
                    break;
                case 'text-align':
                    $attributes = $this->_parseCSSTextAlign($attributes, $value);
                    break;
                case 'color':
                    $attributes = $this->_parseCSSColor($attributes, $value);
                    break;
                case 'white-space':
                    $attributes = $this->_parseCSSWhiteSpace($attributes, $value);
                    break;
            }
        }
        return $attributes;
    }

    /**
     * Attempts to parse the CSS length data type. Uses the specified size
     * as the base for relative sizes. Returns the calculated size in points
     * or the base size if the length cannot be parsed.
     *
     * @param string $length
     * @param float  $size
     * @return float
     */
    protected function _cssLengthToSize($length, $size)
    {
        if (! preg_match('/^(-?[0-9\.]+)(px|mm|cm|in|pt|pc|%|em)$/', $length, $matches)) {
            return $size;
        }
        $value = floatval($matches[1]);

        switch ($matches[2]) {
            case 'px':
                $size = round(($value * 0.75), 6); break;
            case 'mm':
                $size = round(($value * 2.83464567), 6); break;
            case 'cm':
                $size = round(($value * 28.3464567), 6); break;
            case 'in':
                $size = round(($value * 72), 6); break;
            case 'pt':
                $size = round($value, 6); break;
            case 'pc':
                $size = round(($value * 12), 6); break;

            case '%':
                $size = round(($size * ($value / 100)), 6); break;
            case 'em':
                $size = round(($size * $value), 6); break;
        }

        return $size;
    }

    /**
     * Finds the most appropriate font based on the name and style flags, then
     * sets the font character attribute. Returns the modified array.
     *
     * @param array   $attributes
     * @param string  $fontName
     * @param boolean $isBold
     * @param boolean $isItalic
     * @return array
     */
    protected function _setFontAttribute(array $attributes, $fontName, $isBold, $isItalic)
    {
        $currentFont = $attributes[RE_AttributedString::ATTRIBUTE_FONT];

        if (! is_null($isBold)) {
            switch ($currentFont) {
                case 'Helvetica':
                case 'Helvetica-Bold':
                    $fontName = $isBold ? 'Helvetica-Bold' : 'Helvetica';
                    break;
                case 'Helvetica-Oblique':
                case 'Helvetica-BoldOblique':
                    $fontName = $isBold ? 'Helvetica-BoldOblique' : 'Helvetica-Oblique';
                    break;
            }
        }

        if (! is_null($isItalic)) {
            switch ($currentFont) {
                case 'Helvetica':
                case 'Helvetica-Oblique':
                    $fontName = $isItalic ? 'Helvetica-Oblique' : 'Helvetica';
                    break;
                case 'Helvetica-Bold':
                case 'Helvetica-BoldOblique':
                    $fontName = $isItalic ? 'Helvetica-BoldOblique' : 'Helvetica-Bold';
                    break;
            }
        }

        if (! is_null($fontName)) {
            $attributes[RE_AttributedString::ATTRIBUTE_FONT] = $fontName;
        }

        return $attributes;
    }


  /* CSS Properties */

    /**
     * Attempts to parse the CSS "font" shorthand property value. If successful,
     * sets the appropriate character attributes and returns the modified array.
     *
     * @param array  $attributes
     * @param string $value
     * @return array
     */
    protected function _parseCSSFont(array $attributes, $value)
    {
        $regex = '/^'
               . '(?=(?:(?:[-a-z]+\s*){0,2}(italic|oblique))?)'
               . '(?=(?:(?:[-a-z]+\s*){0,2}(small-caps))?)'
               . '(?=(?:(?:[-a-z]+\s*){0,2}(bold(?:er)?|lighter|[1-9]00))?)'
               . '(?:(?:normal|\1|\2|\3)\s*){0,3}((?:xx?-)?(?:small|large)|medium|smaller|larger|[.\d]+(?:\%|in|[cem]m|ex|p[ctx]))'
               . '(?:\s*\/\s*(normal|[.\d]+(?:\%|in|[cem]m|ex|p[ctx])))?'
               . '\s*([-,\"\sa-z]+?)'
               . '$/i';
        if (preg_match($regex, $value, $matches)) {
            $attributes = $this->_parseCSSFontFamily( $attributes, empty($matches[6]) ? 'Helvetica' : $matches[6]);
            $attributes = $this->_parseCSSFontStyle(  $attributes, empty($matches[1]) ? 'normal'    : $matches[1]);
            // $attributes = $this->_parseCSSFontVariant($attributes, empty($matches[2]) ? 'normal'    : $matches[2]);
            $attributes = $this->_parseCSSFontWeight( $attributes, empty($matches[3]) ? 'normal'    : $matches[3]);
            $attributes = $this->_parseCSSFontSize(   $attributes, empty($matches[4]) ? 'medium'    : $matches[4]);
            $attributes = $this->_parseCSSLineHeight( $attributes, empty($matches[5]) ? 'normal'    : $matches[5]);
        }
        return $attributes;
    }

    /**
     * Attempts to parse the CSS "font-family" property value. If successful,
     * sets the appropriate character attributes and returns the modified array.
     *
     * @param array  $attributes
     * @param string $value
     * @return array
     */
    protected function _parseCSSFontFamily(array $attributes, $value)
    {
        $currentFont = $attributes[RE_AttributedString::ATTRIBUTE_FONT];

        $isBold = false;
        if (stripos($currentFont, 'bold') !== false) {
            $isBold = true;
        }

        $isItalic = false;
        if ((stripos($currentFont, 'italic')  !== false) ||
            (stripos($currentFont, 'oblique') !== false)) {
                $isItalic = true;
        }

        switch ($value) {
            case 'serif':
                $value = 'Times-Roman'; break;
            case 'sans-serif':
                $value = 'Helvetica'; break;
            case 'monospace':
                $value = 'Courier'; break;
        }
        return $this->_setFontAttribute($attributes, $value, null, null);
    }

    /**
     * Attempts to parse the CSS "font-size" property value. If successful,
     * sets the appropriate character attributes and returns the modified array.
     *
     * @param array  $attributes
     * @param string $value
     * @return array
     */
    protected function _parseCSSFontSize(array $attributes, $value)
    {
        $size = $attributes[RE_AttributedString::ATTRIBUTE_FONT_SIZE];

        switch ($value) {
            case 'inherit':
                break;

            case 'xx-small':
                $size = 6.75; break;
            case 'x-small':
                $size = 7.50; break;
            case 'small':
                $size = 9.75; break;
            case 'medium':
                $size = 12.0; break;
            case 'large':
                $size = 13.5; break;
            case 'x-large':
                $size = 18.0; break;
            case 'xx-large':
                $size = 24.0; break;

            case 'larger':
                $size = round(($size * 1.2), 6); break;
            case 'smaller':
                $size = round(($size / 1.2), 6); break;

            default:
                $size = max($this->_cssLengthToSize($value, $size), 0);
        }

        $attributes[RE_AttributedString::ATTRIBUTE_FONT_SIZE] = $size;
        return $attributes;
    }

    /**
     * Attempts to parse the CSS "line-height" property value. If successful,
     * sets the appropriate character attributes and returns the modified array.
     *
     * @param array  $attributes
     * @param string $value
     * @return array
     */
    protected function _parseCSSLineHeight(array $attributes, $value)
    {
        $lineHeightMultiple = null;
        switch ($value) {
            case 'inherit':
                break;
            case 'normal':
                $lineHeightMultiple = 0.0; break;
            default:
                $fontSize = $attributes[RE_AttributedString::ATTRIBUTE_FONT_SIZE];
                if (is_numeric($value)) {
                    $lineHeightMultiple = round((max(floatval($value), 0) * $fontSize), 6);
                } else {
                    $value = max($this->_cssLengthToSize($value, $fontSize), 0);
                    $lineHeightMultiple = round(($value / $fontSize), 6);
                }
        }
        if (! is_null($lineHeightMultiple)) {
            $paragraphStyle = clone $attributes[RE_AttributedString::ATTRIBUTE_PARAGRAPH_STYLE];
            $paragraphStyle->setLineHeightMultiple($lineHeightMultiple);
            $attributes[RE_AttributedString::ATTRIBUTE_PARAGRAPH_STYLE] = $paragraphStyle;
        }
        return $attributes;
    }

    /**
     * Attempts to parse the CSS "font-weight" property value. If successful,
     * sets the appropriate character attributes and returns the modified array.
     *
     * @param array  $attributes
     * @param string $value
     * @return array
     */
    protected function _parseCSSFontWeight(array $attributes, $value)
    {
        switch ($value) {
            case 'bold':
                return $this->_setFontAttribute($attributes, null, true, null);
            case 'normal':
                return $this->_setFontAttribute($attributes, null, false, null);
        }
        return $attributes;
    }

    /**
     * Attempts to parse the CSS "font-style" property value. If successful,
     * sets the appropriate character attributes and returns the modified array.
     *
     * @param array  $attributes
     * @param string $value
     * @return array
     */
    protected function _parseCSSFontStyle(array $attributes, $value)
    {
        switch ($value) {
            case 'italic':
            case 'oblique':
                return $this->_setFontAttribute($attributes, null, null, true);
            case 'normal':
                return $this->_setFontAttribute($attributes, null, null, false);
        }
        return $attributes;
    }

    /**
     * Attempts to parse the CSS "text-align" property value. If successful,
     * sets the appropriate character attributes and returns the modified array.
     *
     * @param array  $attributes
     * @param string $value
     * @return array
     */
    protected function _parseCSSTextAlign(array $attributes, $value)
    {
        $alignment = null;
        switch ($value) {
            case 'inherit':
                break;
            case 'left':
                $alignment = RE_ParagraphStyle::ALIGN_LEFT; break;
            case 'right':
                $alignment = RE_ParagraphStyle::ALIGN_RIGHT; break;
            case 'center':
                $alignment = RE_ParagraphStyle::ALIGN_CENTER; break;
            case 'justify':
                $alignment = RE_ParagraphStyle::ALIGN_JUSTIFIED; break;
            case 'start':
                $alignment = RE_ParagraphStyle::ALIGN_NATURAL; break;
        }
        if (! is_null($alignment)) {
            $paragraphStyle = clone $attributes[RE_AttributedString::ATTRIBUTE_PARAGRAPH_STYLE];
            $paragraphStyle->setAlignment($alignment);
            $attributes[RE_AttributedString::ATTRIBUTE_PARAGRAPH_STYLE] = $paragraphStyle;
        }
        return $attributes;
    }

    /**
     * Attempts to parse the CSS "color" property value. If successful,
     * sets the appropriate character attributes and returns the modified array.
     *
     * @param array  $attributes
     * @param string $value
     * @return array
     */
    protected function _parseCSSColor(array $attributes, $value)
    {
        $attributes[RE_AttributedString::ATTRIBUTE_COLOR] = RE_Color::htmlColor($value);
        return $attributes;
    }

    /**
     * Attempts to parse the CSS "white-space" property value. If successful,
     * sets the appropriate character attributes and returns the modified array.
     *
     * @param array  $attributes
     * @param string $value
     * @return array
     */
    protected function _parseCSSWhiteSpace(array $attributes, $value)
    {
        $lineBreakMode = null;
        switch ($value) {
            case 'inherit':
                break;
            case 'normal':
                $lineBreakMode = RE_ParagraphStyle::LINE_BREAK_WORD; break;
            case 'nowrap':
                $lineBreakMode = RE_ParagraphStyle::LINE_BREAK_CLIP; break;
        }
        if (! is_null($lineBreakMode)) {
            $paragraphStyle = clone $attributes[RE_AttributedString::ATTRIBUTE_PARAGRAPH_STYLE];
            $paragraphStyle->setLineBreakMode($lineBreakMode);
            $attributes[RE_AttributedString::ATTRIBUTE_PARAGRAPH_STYLE] = $paragraphStyle;
        }
        return $attributes;
    }

}
