<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Color
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @copyright  Copyright (c) 2005-2008 Zend Technologies USA, Inc. (http://www.zend.com)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */

/* Portions of this source file are excerpted from the Zend Framework:
 *
 * Copyright (c) 2005-2008, Zend Technologies USA, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 *  * Neither the name of Zend Technologies USA, Inc. nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/** RE_Color_Gray */
require_once 'RE/Color/Gray.php';

/** RE_Color_Rgb */
require_once 'RE/Color/Rgb.php';

/** RE_Color_Cmyk */
require_once 'RE/Color/Cmyk.php';


/**
 * Abstract base class for representing color values. The various color models
 * models are described in concrete subclasses.
 *
 * Also provides a set of convenience methods for creating common colors and
 * colors from HTML representations.
 *
 * @package    Color
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @copyright  Copyright (c) 2005-2008 Zend Technologies USA, Inc. (http://www.zend.com)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
abstract class RE_Color
{
  /**** Class Methods ****/

    /**
     * Convenience method that returns a solid black color.
     *
     * @return RE_Color
     */
    public static function blackColor()
    {
        static $blackColor = null;
        if (! isset($blackColor)) {
            $blackColor = new RE_Color_Gray(0);
        }
        return $blackColor;
    }

    /**
     * Convenience method that returns a solid white color.
     *
     * @return RE_Color
     */
    public static function whiteColor()
    {
        static $whiteColor = null;
        if (! isset($whiteColor)) {
            $whiteColor = new RE_Color_Gray(1);
        }
        return $whiteColor;
    }

    /**
     * Convenience method that returns a solid red color.
     *
     * @return RE_Color
     */
    public static function redColor()
    {
        static $redColor = null;
        if (! isset($redColor)) {
            $redColor = new RE_Color_Rgb(1, 0, 0);
        }
        return $redColor;
    }

    /**
     * Convenience method that returns a solid green color.
     *
     * @return RE_Color
     */
    public static function greenColor()
    {
        static $greenColor = null;
        if (! isset($greenColor)) {
            $greenColor = new RE_Color_Rgb(0, 0.5, 0);
        }
        return $greenColor;
    }

    /**
     * Convenience method that returns a solid blue color.
     *
     * @return RE_Color
     */
    public static function blueColor()
    {
        static $blueColor = null;
        if (! isset($blueColor)) {
            $blueColor = new RE_Color_Rgb(0, 0, 1);
        }
        return $blueColor;
    }

    /**
     * Convenience method that returns a solid cyan color.
     *
     * @return RE_Color
     */
    public static function cyanColor()
    {
        static $cyanColor = null;
        if (! isset($cyanColor)) {
            $cyanColor = new RE_Color_Cmyk(1, 0, 0, 0);
        }
        return $cyanColor;
    }

    /**
     * Convenience method that returns a solid magenta color.
     *
     * @return RE_Color
     */
    public static function magentaColor()
    {
        static $magentaColor = null;
        if (! isset($magentaColor)) {
            $magentaColor = new RE_Color_Cmyk(0, 1, 0, 0);
        }
        return $magentaColor;
    }

    /**
     * Convenience method that returns a solid yellow color.
     *
     * @return RE_Color
     */
    public static function yellowColor()
    {
        static $yellowColor = null;
        if (! isset($yellowColor)) {
            $yellowColor = new RE_Color_Cmyk(0, 0, 1, 0);
        }
        return $yellowColor;
    }

    /**
     * Returns a RE_Color object from the HTML/CSS color representation: one of
     * the well-known color names (black, white, blue, etc.), a hexidecimal
     * number: "#rgb" or "#rrggbb", a rgb(), or a hsl() function.
     *
     * Returns a solid black color if the HTML color cannot be parsed.
     *
     * @param string $htmlColor
     * @return RE_Color
     */
    public static function htmlColor($htmlColor)
    {
        $rgb = null;
        if (preg_match('/^#[A-Fa-f0-9]{6}$/', $htmlColor)) {
            $rgb = array(round((hexdec(substr($htmlColor, 1, 2)) / 255), 3),
                         round((hexdec(substr($htmlColor, 3, 2)) / 255), 3),
                         round((hexdec(substr($htmlColor, 5, 2)) / 255), 3));

        } else if (preg_match('/^#[A-Fa-f0-9]{3}$/', $htmlColor)) {
            $rgb = array(round((hexdec(str_repeat(substr($htmlColor, 1, 1), 2)) / 255), 3),
                         round((hexdec(str_repeat(substr($htmlColor, 2, 1), 2)) / 255), 3),
                         round((hexdec(str_repeat(substr($htmlColor, 3, 1), 2)) / 255), 3));

        } else if (preg_match('/^rgb\(([0-9]+(\.[0-9]+)?)(%)?,\s*([0-9]+(\.[0-9]+)?)(%)?,\s*([0-9]+(\.[0-9]+)?)(%)?\)$/', $htmlColor, $matches)) {
            $r = round(empty($matches[3]) ? (min(intval($matches[1]), 255) / 255) : (min(floatval($matches[1]), 100) / 100), 3);
            $g = round(empty($matches[6]) ? (min(intval($matches[4]), 255) / 255) : (min(floatval($matches[4]), 100) / 100), 3);
            $b = round(empty($matches[9]) ? (min(intval($matches[7]), 255) / 255) : (min(floatval($matches[7]), 100) / 100), 3);
            $rgb = array($r, $g, $b);

        } else if (preg_match('/^hsl\(([0-9]+(\.[0-9]+)?)(%)?,\s*([0-9]+(\.[0-9]+)?)(%)?,\s*([0-9]+(\.[0-9]+)?)(%)?\)$/', $htmlColor, $matches)) {
            $h = round(empty($matches[3]) ? ((fmod((fmod($matches[1], 360) + 360), 360)) / 360) : (min(floatval($matches[1]), 100) / 100), 3);
            $s = round(empty($matches[6]) ? ((fmod((fmod($matches[4], 360) + 360), 360)) / 360) : (min(floatval($matches[4]), 100) / 100), 3);
            $l = round(empty($matches[9]) ? ((fmod((fmod($matches[7], 360) + 360), 360)) / 360) : (min(floatval($matches[7]), 100) / 100), 3);
            $rgb = self::_hslToRGB($h, $s, $l);

        } else {
            switch (strtolower($htmlColor)) {
                case 'aqua':
                    $rgb = array(0.0, 1.0, 1.0);   break;
                case 'black':
                    $rgb = array(0.0, 0.0, 0.0);   break;
                case 'blue':
                    $rgb = array(0.0, 0.0, 1.0);   break;
                case 'fuchsia':
                    $rgb = array(1.0, 0.0, 1.0);   break;
                case 'gray':
                case 'grey':
                    $rgb = array(0.502, 0.502, 0.502); break;
                case 'green':
                    $rgb = array(0.0, 0.502, 0.0);   break;
                case 'lime':
                    $rgb = array(0.0, 1.0, 0.0);   break;
                case 'maroon':
                    $rgb = array(0.502, 0.0, 0.0);   break;
                case 'navy':
                    $rgb = array(0.0, 0.0, 0.502); break;
                case 'olive':
                    $rgb = array(0.502, 0.502, 0.0);   break;
                case 'purple':
                    $rgb = array(0.502, 0.0, 0.502); break;
                case 'red':
                    $rgb = array(1.0, 0.0, 0.0);   break;
                case 'silver':
                    $rgb = array(0.753, 0.753, 0.753); break;
                case 'teal':
                    $rgb = array(0.0, 0.502, 0.502); break;
                case 'white':
                    $rgb = array(1.0, 1.0, 1.0);   break;
                case 'yellow':
                    $rgb = array(1.0, 1.0, 0.0);   break;

                case 'aliceblue':
                    $rgb = array(0.941, 0.973, 1.0);   break;
                case 'antiquewhite':
                    $rgb = array(0.980, 0.922, 0.843); break;
                case 'aquamarine':
                    $rgb = array(0.498, 1.0, 0.831); break;
                case 'azure':
                    $rgb = array(0.941, 1.0, 1.0);   break;
                case 'beige':
                    $rgb = array(0.961, 0.961, 0.863); break;
                case 'bisque':
                    $rgb = array(1.0, 0.894, 0.769); break;
                case 'blanchedalmond':
                    $rgb = array(1.0, 1.0, 0.804); break;
                case 'blueviolet':
                    $rgb = array(0.541, 0.169, 0.886); break;
                case 'brown':
                    $rgb = array(0.647, 0.165, 0.165); break;
                case 'burlywood':
                    $rgb = array(0.871, 0.722, 0.529); break;
                case 'cadetblue':
                    $rgb = array(0.373, 0.620, 0.627); break;
                case 'chartreuse':
                    $rgb = array(0.498, 1.0, 0.0);   break;
                case 'chocolate':
                    $rgb = array(0.824, 0.412, 0.118); break;
                case 'coral':
                    $rgb = array(1.0, 0.498, 0.314); break;
                case 'cornflowerblue':
                    $rgb = array(0.392, 0.584, 0.929); break;
                case 'cornsilk':
                    $rgb = array(1.0, 0.973, 0.863); break;
                case 'crimson':
                    $rgb = array(0.863, 0.078, 0.235); break;
                case 'cyan':
                    $rgb = array(0.0, 1.0, 1.0);   break;
                case 'darkblue':
                    $rgb = array(0.0, 0.0, 0.545); break;
                case 'darkcyan':
                    $rgb = array(0.0, 0.545, 0.545); break;
                case 'darkgoldenrod':
                    $rgb = array(0.722, 0.525, 0.043); break;
                case 'darkgray':
                case 'darkgrey':
                    $rgb = array(0.663, 0.663, 0.663); break;
                case 'darkgreen':
                    $rgb = array(0.0, 0.392, 0.0);   break;
                case 'darkkhaki':
                    $rgb = array(0.741, 0.718, 0.420); break;
                case 'darkmagenta':
                    $rgb = array(0.545, 0.0, 0.545); break;
                case 'darkolivegreen':
                    $rgb = array(0.333, 0.420, 0.184); break;
                case 'darkorange':
                    $rgb = array(1.0, 0.549, 0.0);   break;
                case 'darkorchid':
                    $rgb = array(0.6, 0.196, 0.8);   break;
                case 'darkred':
                    $rgb = array(0.545, 0.0, 0.0);   break;
                case 'darksalmon':
                    $rgb = array(0.914, 0.588, 0.478); break;
                case 'darkseagreen':
                    $rgb = array(0.561, 0.737, 0.561); break;
                case 'darkslateblue':
                    $rgb = array(0.282, 0.239, 0.545); break;
                case 'darkslategray':
                case 'darkslategrey':
                    $rgb = array(0.184, 0.310, 0.310); break;
                case 'darkturquoise':
                    $rgb = array(0.0, 0.808, 0.820); break;
                case 'darkviolet':
                    $rgb = array(0.580, 0.0, 0.827); break;
                case 'deeppink':
                    $rgb = array(1.0, 0.078, 0.576); break;
                case 'deepskyblue':
                    $rgb = array(0.0, 0.749, 1.0);   break;
                case 'dimgray':
                case 'dimgrey':
                    $rgb = array(0.412, 0.412, 0.412); break;
                case 'dodgerblue':
                    $rgb = array(0.118, 0.565, 1.0);   break;
                case 'firebrick':
                    $rgb = array(0.698, 0.133, 0.133); break;
                case 'floralwhite':
                    $rgb = array(1.0, 0.980, 0.941); break;
                case 'forestgreen':
                    $rgb = array(0.133, 0.545, 0.133); break;
                case 'gainsboro':
                    $rgb = array(0.863, 0.863, 0.863); break;
                case 'ghostwhite':
                    $rgb = array(0.973, 0.973, 1.0);   break;
                case 'gold':
                    $rgb = array(1.0, 0.843, 0.0);   break;
                case 'goldenrod':
                    $rgb = array(0.855, 0.647, 0.125); break;
                case 'greenyellow':
                    $rgb = array(0.678, 1.0, 0.184); break;
                case 'honeydew':
                    $rgb = array(0.941, 1.0, 0.941); break;
                case 'hotpink':
                    $rgb = array(1.0, 0.412, 0.706); break;
                case 'indianred':
                    $rgb = array(0.804, 0.361, 0.361); break;
                case 'indigo':
                    $rgb = array(0.294, 0.0, 0.510); break;
                case 'ivory':
                    $rgb = array(1.0, 0.941, 0.941); break;
                case 'khaki':
                    $rgb = array(0.941, 0.902, 0.549); break;
                case 'lavender':
                    $rgb = array(0.902, 0.902, 0.980); break;
                case 'lavenderblush':
                    $rgb = array(1.0, 0.941, 0.961); break;
                case 'lawngreen':
                    $rgb = array(0.486, 0.988, 0.0);   break;
                case 'lemonchiffon':
                    $rgb = array(1.0, 0.980, 0.804); break;
                case 'lightblue':
                    $rgb = array(0.678, 0.847, 0.902); break;
                case 'lightcoral':
                    $rgb = array(0.941, 0.502, 0.502); break;
                case 'lightcyan':
                    $rgb = array(0.878, 1.0, 1.0);   break;
                case 'lightgoldenrodyellow':
                    $rgb = array(0.980, 0.980, 0.824); break;
                case 'lightgreen':
                    $rgb = array(0.565, 0.933, 0.565); break;
                case 'lightgray':
                case 'lightgrey':
                    $rgb = array(0.827, 0.827, 0.827); break;
                case 'lightpink':
                    $rgb = array(1.0, 0.714, 0.757); break;
                case 'lightsalmon':
                    $rgb = array(1.0, 0.627, 0.478); break;
                case 'lightseagreen':
                    $rgb = array(0.125, 0.698, 0.667); break;
                case 'lightskyblue':
                    $rgb = array(0.529, 0.808, 0.980); break;
                case 'lightslategray':
                case 'lightslategrey':
                    $rgb = array(0.467, 0.533, 0.6);   break;
                case 'lightsteelblue':
                    $rgb = array(0.690, 0.769, 0.871); break;
                case 'lightyellow':
                    $rgb = array(1.0, 1.0, 0.878); break;
                case 'limegreen':
                    $rgb = array(0.196, 0.804, 0.196); break;
                case 'linen':
                    $rgb = array(0.980, 0.941, 0.902); break;
                case 'magenta':
                    $rgb = array(1.0, 0.0, 1.0);   break;
                case 'mediumaquamarine':
                    $rgb = array(0.4, 0.804, 0.667); break;
                case 'mediumblue':
                    $rgb = array(0.0, 0.0, 0.804); break;
                case 'mediumorchid':
                    $rgb = array(0.729, 0.333, 0.827); break;
                case 'mediumpurple':
                    $rgb = array(0.576, 0.439, 0.859); break;
                case 'mediumseagreen':
                    $rgb = array(0.235, 0.702, 0.443); break;
                case 'mediumslateblue':
                    $rgb = array(0.482, 0.408, 0.933); break;
                case 'mediumspringgreen':
                    $rgb = array(0.0, 0.980, 0.604); break;
                case 'mediumturquoise':
                    $rgb = array(0.282, 0.820, 0.8);   break;
                case 'mediumvioletred':
                    $rgb = array(0.780, 0.082, 0.522); break;
                case 'midnightblue':
                    $rgb = array(0.098, 0.098, 0.439); break;
                case 'mintcream':
                    $rgb = array(0.961, 1.0, 0.980); break;
                case 'mistyrose':
                    $rgb = array(1.0, 0.894, 0.882); break;
                case 'moccasin':
                    $rgb = array(1.0, 0.894, 0.710); break;
                case 'navajowhite':
                    $rgb = array(1.0, 0.871, 0.678); break;
                case 'oldlace':
                    $rgb = array(0.992, 0.961, 0.902); break;
                case 'olivedrab':
                    $rgb = array(0.420, 0.557, 0.137); break;
                case 'orange':
                    $rgb = array(1.0, 0.647, 0.0);   break;
                case 'orangered':
                    $rgb = array(1.0, 0.271, 0.0);   break;
                case 'orchid':
                    $rgb = array(0.855, 0.439, 0.839); break;
                case 'palegoldenrod':
                    $rgb = array(0.933, 0.910, 0.667); break;
                case 'palegreen':
                    $rgb = array(0.596, 0.984, 0.596); break;
                case 'paleturquoise':
                    $rgb = array(0.686, 0.933, 0.933); break;
                case 'palevioletred':
                    $rgb = array(0.859, 0.439, 0.576); break;
                case 'papayawhip':
                    $rgb = array(1.0, 0.937, 0.835); break;
                case 'peachpuff':
                    $rgb = array(1.0, 0.937, 0.835); break;
                case 'peru':
                    $rgb = array(0.804, 0.522, 0.247); break;
                case 'pink':
                    $rgb = array(1.0, 0.753, 0.796); break;
                case 'plum':
                    $rgb = array(0.867, 0.627, 0.867); break;
                case 'powderblue':
                    $rgb = array(0.690, 0.878, 0.902); break;
                case 'rebeccapurple':  // http://codepen.io/trezy/blog/honoring-a-great-man
                    $rgb = array(0.4, 0.2, 0.6); break;
                case 'rosybrown':
                    $rgb = array(0.737, 0.561, 0.561); break;
                case 'royalblue':
                    $rgb = array(0.255, 0.412, 0.882); break;
                case 'saddlebrown':
                    $rgb = array(0.545, 0.271, 0.075); break;
                case 'salmon':
                    $rgb = array(0.980, 0.502, 0.447); break;
                case 'sandybrown':
                    $rgb = array(0.957, 0.643, 0.376); break;
                case 'seagreen':
                    $rgb = array(0.180, 0.545, 0.341); break;
                case 'seashell':
                    $rgb = array(1.0, 0.961, 0.933); break;
                case 'sienna':
                    $rgb = array(0.627, 0.322, 0.176); break;
                case 'skyblue':
                    $rgb = array(0.529, 0.808, 0.922); break;
                case 'slateblue':
                    $rgb = array(0.416, 0.353, 0.804); break;
                case 'slategray':
                case 'slategrey':
                    $rgb = array(0.439, 0.502, 0.565); break;
                case 'snow':
                    $rgb = array(1.0, 0.980, 0.980); break;
                case 'springgreen':
                    $rgb = array(0.0, 1.0, 0.498); break;
                case 'steelblue':
                    $rgb = array(0.275, 0.510, 0.706); break;
                case 'tan':
                    $rgb = array(0.824, 0.706, 0.549); break;
                case 'thistle':
                    $rgb = array(0.847, 0.749, 0.847); break;
                case 'tomato':
                    $rgb = array(0.992, 0.388, 0.278); break;
                case 'turquoise':
                    $rgb = array(0.251, 0.878, 0.816); break;
                case 'violet':
                    $rgb = array(0.933, 0.510, 0.933); break;
                case 'wheat':
                    $rgb = array(0.961, 0.871, 0.702); break;
                case 'whitesmoke':
                    $rgb = array(0.961, 0.961, 0.961); break;
                case 'yellowgreen':
                    $rgb = array(0.604, 0.804, 0.196); break;
            }
        }

        if (empty($rgb)) {
            return self::blackColor();
        }

        if (($rgb[0] == $rgb[1]) && ($rgb[1] == $rgb[2])) {
            return new RE_Color_Gray($rgb[0]);
        }
        return new RE_Color_Rgb($rgb[0], $rgb[1], $rgb[2]);
    }



  /**** Abstract Interface ****/

    /**
     * Sets the current filling or stroking color on the PDF canvas.
     *
     * @param RE_Pdf_Canvas_Interface $canvas
     * @param boolean $useForStroking
     */
    abstract public function draw(RE_Pdf_Canvas_Interface $canvas, $useForStroking);



  /**** Internal Interface ****/

    /**
     * Converts the HSL value to an array of RGB values using the algorithm
     * specified in the CSS Level 3 specification
     * (http://www.w3.org/TR/2011/REC-css3-color-20110607/#hsl-color).
     *
     * @param float $h
     * @param float $s
     * @param float $l
     * @return float[]
     */
    protected static function _hslToRGB($h, $s, $l)
    {
        $m2 = ($l < 0.5) ? $l * ($s + 1) : $l + $s - ($l * $s);
        $m1 = ($l * 2) - $m2;
        return array(self::_hueToRGB($m1, $m2, $h + (1 / 3)),
                     self::_hueToRGB($m1, $m2, $h),
                     self::_hueToRGB($m1, $m2, $h - (1 / 3)));
    }

    /**
     * Helper function for {@link _hslToRGB()} to calculate the RGB component
     * values.
     *
     * @param float $m1
     * @param float $m2
     * @param float $h
     * @return float
     */
    protected static function _hueToRGB($m1, $m2, $h)
    {
        if ($h < 0) {
            $h += 1;
        }
        if ($h > 1) {
            $h -= 1;
        }
        if (($h * 6) < 1) {
            return $m1 + (($m2 - $m1) * $h * 6);
        }
        if (($h * 2) < 1) {
            return $m2;
        }
        if (($h * 3) < 2) {
            return $m1 + (($m2 - $m1) * ((2 / 3) - $h) * 6);
        }
        return $m1;
    }

}
