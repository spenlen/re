<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Text
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * Represents a measurement of a linear segment, such as a string of characters.
 * Comprised of two values: the location (or offset or index) and the length.
 *
 * Primarily used with {@link RE_AttributedString} to manage character attributes.
 *
 * @todo Add the following method: subtractRange()
 *
 * @package    Text
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Range
{
  /**** Instance Variables ****/

    /**
     * Stores the location (or offset) of the range. Locations are zero-based.
     * @var integer
     */
    public $location = 0;

    /**
     * Stores the length.
     * @var integer
     */
    public $length = 0;



  /**** Class Methods ****/

    /**
     * Returns a new RE_Range object from the string representation, usually
     * formatted as "{loc, len}".
     *
     * The string is scanned for two numbers, which are used as the location
     * and length, in that order. If the string does not contain two numbers,
     * any numbers found are used, and the remaining values are assumed to be
     * zero. All values are interpreted as integers.
     *
     * @param string $string
     * @return RE_Range
     */
    public static function fromString($string)
    {
        preg_match('/(\d+\.?\d*)(.+?(\d+\.?\d*))?/ms', $string, $matches);
        return new self((isset($matches[1]) ? $matches[1] : 0), (isset($matches[3]) ? $matches[3] : 0));
    }



  /**** Public Interface ****/


  /* Object Lifecycle */

    /**
     * Object constructor signatures:
     *
     *  - Initialize to an empty range:
     *      new RE_Range();
     *
     *  - Initialize to a specific location and length:
     *      new RE_Range(integer $location, integer $length);
     *
     *  - Clone an existing range:
     *      new RE_Range(RE_Range $range);
     *
     * Location and length values must be positive or zero. Negative values are
     * silently clamped to zero.
     *
     * @param mixed $param1
     * @param mixed $param2
     * @throws BadMethodCallException if the constructor arguments cannot be deduced.
     */
    public function __construct($param1 = null, $param2 = null)
    {
        if (isset($param1, $param2)) {
            /* Initialize with a specific location and length.
             */
            $this->location = max(0, (int)$param1);
            $this->length   = max(0, (int)$param2);

        } else if ((! isset($param1)) && (! isset($param2))) {
            /* Initialize to an empty range. Done.
             */

        } else if (($param1 instanceof RE_Range) &&
                   (! isset($param2))) {
            /* Clone an existing range. Copy the values.
             */
            $this->location = $param1->location;
            $this->length   = $param1->length;

        } else {
            throw new BadMethodCallException('Unrecognized arguments provided to object constructor.');
        }
    }


  /* Object Magic Methods */

    /**
     * Returns the range's length and location as a string of the form
     * "{loc, len}".
     *
     * @return string
     */
    public function __toString()
    {
        return '{' . $this->location . ', ' . $this->length . '}';
    }


  /* Accessor Methods */

    /**
     * Returns the location.
     *
     * @return integer
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Sets the location.
     *
     * Location values must be positive or zero. Negative values are silently
     * clamped to zero.
     *
     * @param integer $location
     */
    public function setLocation($location)
    {
        $this->location = max(0, (int)$location);
    }

    /**
     * Returns the length.
     *
     * @return integer
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * Sets the length.
     *
     * Length values must be positive or zero. Negative values are silently
     * clamped to zero.
     *
     * @param integer $length
     */
    public function setLength($length)
    {
        $this->length = max(0, (int)$length);
    }


  /* Calculation */

    /**
     * Returns the extent of the range (its location plus length), which is a
     * a number one greater than the last location in the range
     *
     * @return integer
     */
    public function maxRange()
    {
        return $this->location + $this->length;
    }


  /* Test and Comparison */

    /**
     * Returns true if the length is zero.
     *
     * @return boolean
     */
    public function isEmpty()
    {
        if ($this->length == 0) {
            return true;
        }
        return false;
    }

    /**
     * Returns true if this range is equal in location and length to the
     * specified range.
     *
     * @param RE_Range $range
     * @return boolean
     */
    public function isEqualToRange(RE_Range $range)
    {
        if (($this->location == $range->location) &&
            ($this->length   == $range->length)) {
                return true;
        }
        return false;
    }

    /**
     * Returns true if this range is contained completely within the
     * specified range.
     *
     * @param RE_Range $range
     * @return boolean
     */
    public function isSubrangeOfRange(RE_Range $range)
    {
        if (($this->location >= $range->location) &&
            (($this->location + $this->length) <= ($range->location + $range->length))) {
                return true;
        }
        return false;
    }

    /**
     * Returns true if the specified range intersects this range (its length
     * is greater than zero and its position is within this range).
     *
     * @param RE_Range $range
     * @return boolean
     */
    public function intersectsRange(RE_Range $range)
    {
        if (($range->length > 0) &&
            (($range->location >= $this->location) &&
             ($range->location <  ($this->location + $this->length))) ||
            (($this->location >= $range->location) && 
             ($this->location <  ($range->location + $range->length)))) {
                 return true;
        }
        return false;
    }

    /**
     * Returns true if the specified location is within this range (greater
     * than or equal to the location and less than the location plus length).
     *
     * @param integer $location
     * @return boolean
     */
    public function containsLocation($location)
    {
        if (($location >= $this->location) &&
            ($location <  ($this->location + $this->length))) {
                return true;
        }
        return false;
    }


  /* Conversion */

    /**
     * Returns a new range object that is the intersection of this range and
     * the specified range (containing only indices that are present in both
     * ranges).
     *
     * If the ranges do not intersect, a range is returned whose location and
     * length are zero.
     *
     * @param RE_Range $range
     * @return RE_Range
     */
    public function rangeByIntersectingRange(RE_Range $range)
    {
        if (($this->location == $range->location) && ($this->length == $range->length)) {
            return clone $this;
        }

        $thisMax  = $this->location  + $this->length;
        $rangeMax = $range->location + $range->length;

        if (($this->location >= $range->location) && ($this->location < $rangeMax)) {
            return new RE_Range($this->location,
                                (($rangeMax < $thisMax) ? $rangeMax - $this->location : $this->length));
        }

        if (($range->location >= $this->location) && ($range->location < $thisMax)) {
            return new RE_Range($range->location,
                                (($thisMax < $rangeMax) ? $thisMax - $range->location : $range->length));
        }

        return new RE_Range();
    }

    /**
     * Returns a new range object that is the union of this range and the
     * the specified range (containing all indices that are in and between
     * both ranges).
     *
     * @param RE_Range $range
     * @return RE_Range
     */
    public function rangeByUnioningRange(RE_Range $range)
    {
        $location = min($this->location, $range->location);
        
        $thisMax  = $this->location  + $this->length;
        $rangeMax = $range->location + $range->length;
        $length = max($thisMax, $rangeMax) - $location;

        return new RE_Range($location, $length);
    }

}
