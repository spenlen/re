<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Fonts
 * @subpackage Cmap
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */

/** RE_Font_Cmap */
require_once 'RE/Font/Cmap.php';


/**
 * Implements the "trimmed table mapping" character map (type 6).
 *
 * This table type is preferred over the {@link RE_Font_Cmap_SegmentToDelta}
 * table when the Unicode characters covered by the font fall into a single
 * contiguous range.
 *
 * @package    Fonts
 * @subpackage Cmap
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Font_Cmap_TrimmedTable extends RE_Font_Cmap
{
  /**** Instance Variables ****/

    /**
     * The starting character code covered by this table.
     * @var integer
     */
    protected $_startCode = 0;

    /**
     * The ending character code covered by this table.
     * @var integer
     */
    protected $_endCode = 0;

    /**
     * Glyph index array. Stores the actual glyph numbers.
     * @var array
     */
    protected $_glyphIndexArray = array();



  /**** Public Interface ****/


  /* Concrete Class Implementation */

    /**
     * Returns an array of glyph numbers corresponding to the Unicode characters.
     *
     * If a particular character doesn't exist in this font, the special 'missing
     * character glyph' will be substituted.
     *
     * See also {@link glyphNumberForCharacter()}.
     *
     * @param array $characterCodes Array of Unicode character codes (code points).
     * @return array Array of glyph numbers.
     */
    public function glyphNumbersForCharacters($characterCodes)
    {
        $glyphNumbers = array();
        foreach ($characterCodes as $key => $characterCode) {

            if (($characterCode < $this->_startCode) || ($characterCode > $this->_endCode)) {
                $glyphNumbers[$key] = RE_Font_Cmap::MISSING_CHARACTER_GLYPH;
                continue;
            }

            $glyphIndex = $characterCode - $this->_startCode;
            $glyphNumbers[$key] = $this->_glyphIndexArray[$glyphIndex];

        }
        return $glyphNumbers;
    }

    /**
     * Returns the glyph number corresponding to the Unicode character.
     *
     * If a particular character doesn't exist in this font, the special 'missing
     * character glyph' will be substituted.
     *
     * See also {@link glyphNumbersForCharacters()} which is optimized for bulk
     * operations.
     *
     * @param integer $characterCode Unicode character code (code point).
     * @return integer Glyph number.
     */
    public function glyphNumberForCharacter($characterCode)
    {
        if (($characterCode < $this->_startCode) || ($characterCode > $this->_endCode)) {
            return RE_Font_Cmap::MISSING_CHARACTER_GLYPH;
        }
        $glyphIndex = $characterCode - $this->_startCode;
        return $this->_glyphIndexArray[$glyphIndex];
    }

    /**
     * Returns an array containing the Unicode characters that have entries in
     * this character map.
     *
     * @return array Unicode character codes.
     */
    public function getCoveredCharacters()
    {
        $characterCodes = array();
        for ($code = $this->_startCode; $code <= $this->_endCode; $code++) {
            $characterCodes[] = $code;
        }
        return $characterCodes;
    }


  /* Object Lifecycle */

    /**
     * Parses the raw binary table data.
     *
     * @param string $cmapData Raw binary cmap table data.
     * @throws InvalidArgumentException if the table data is invalid.
     */
    public function __construct($cmapData)
    {
        /* Sanity check: The table should be at least 9 bytes in size.
         */
        $actualLength = strlen($cmapData);
        if ($actualLength < 9) {
            throw new InvalidArgumentException('Insufficient table data');
        }

        /* Sanity check: Make sure this is right data for this table type.
         */
        $type = $this->_extractUInt2($cmapData, 0);
        if ($type != RE_Font_Cmap::TYPE_TRIMMED_TABLE) {
            throw new InvalidArgumentException('Wrong cmap table type');
        }

        $length = $this->_extractUInt2($cmapData, 2);
        if ($length != $actualLength) {
            throw new InvalidArgumentException("Table length ($length) does not match actual length ($actualLength)");
        }

        /* Mapping tables should be language-independent. The font may not work
         * as expected if they are not. Unfortunately, many font files in the
         * wild incorrectly record a language ID in this field, so we can't
         * call this a failure.
         */
        $language = $this->_extractUInt2($cmapData, 4);
        if ($language != 0) {
            // Record a warning here somehow?
        }

        $this->_startCode = $this->_extractUInt2($cmapData, 6);

        $entryCount = $this->_extractUInt2($cmapData, 8);
        $expectedCount = ($length - 10) >> 1;
        if ($entryCount != $expectedCount) {
            throw new InvalidArgumentException("Entry count is wrong; expected: $expectedCount; actual: $entryCount");
        }

        $this->_endCode = $this->_startCode + $entryCount - 1;

        $offset = 10;
        for ($i = 0; $i < $entryCount; $i++, $offset += 2) {
            $this->_glyphIndexArray[] = $this->_extractUInt2($cmapData, $offset);
        }

        /* Sanity check: After reading all of the data, we should be at the end
         * of the table.
         */
        if ($offset != $length) {
            throw new InvalidArgumentException("Ending offset ($offset) does not match length ($length)");
        }
    }

}
