<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Fonts
 * @subpackage Cmap
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */

/** RE_Font_Cmap_ByteEncoding */
require_once 'RE/Font/Cmap/ByteEncoding.php';


/**
 * Custom cmap type used for the Adobe Standard 14 PDF fonts.
 *
 * Just like {@link RE_Font_Cmap_ByteEncoding} except that the constructor
 * takes a predefined array of glyph numbers and can cover any Unicode character.
 *
 * @package    Fonts
 * @subpackage Cmap
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Font_Cmap_ByteEncoding_Static extends RE_Font_Cmap_ByteEncoding
{
  /**** Public Interface ****/


  /* Object Lifecycle */

    /**
     * Object constructor
     *
     * @param array $cmapData Array whose keys are Unicode character codes and
     *   values are glyph numbers.
     * @throws InvalidArgumentException if the table data is not provided as an array.
     */
    public function __construct($cmapData)
    {
        if (! is_array($cmapData)) {
            throw new InvalidArgumentException('Constructor parameter must be an array');
        }
        $this->_glyphIndexArray = $cmapData;
    }

}
