<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Fonts
 * @subpackage Cmap
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * Defines the public interface for concrete subclasses which are responsible
 * for mapping Unicode characters to a font's glyph numbers. Also provides
 * shared utility methods.
 *
 * Cmap objects should ordinarily be obtained through the factory method
 * {@link cmapWithTypeData()}.
 *
 * The supported character map types are those found in the OpenType spec. For
 * additional detail on the internal binary format of these tables, see:
 *  - {@link http://developer.apple.com/textfonts/TTRefMan/RM06/Chap6cmap.html}
 *  - {@link http://www.microsoft.com/OpenType/OTSpec/cmap.htm}
 *  - {@link http://partners.adobe.com/public/developer/opentype/index_cmap.html}
 *
 * @todo Implement the RE_Font_Cmap_HighByteMapping, RE_Font_Cmap_MixedCoverage,
 *   RE_Font_Cmap_TrimmedArray, and RE_Font_Cmap_SegmentedCoverage classes.
 *
 * @package    Fonts
 * @subpackage Cmap
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
abstract class RE_Font_Cmap
{
  /**** Class Constants ****/


  /* Cmap Table Types */

    /**
     * Byte Encoding character map table type.
     */
    const TYPE_BYTE_ENCODING = 0x00;

    /**
     * High Byte Mapping character map table type.
     */
    const TYPE_HIGH_BYTE_MAPPING = 0x02;

    /**
     * Segment Value to Delta Mapping character map table type.
     */
    const TYPE_SEGMENT_TO_DELTA = 0x04;

    /**
     * Trimmed Table character map table type.
     */
    const TYPE_TRIMMED_TABLE = 0x06;

    /**
     * Mixed Coverage character map table type.
     */
    const TYPE_MIXED_COVERAGE = 0x08;

    /**
     * Trimmed Array character map table type.
     */
    const TYPE_TRIMMED_ARRAY = 0x0a;

    /**
     * Segmented Coverage character map table type.
     */
    const TYPE_SEGMENTED_COVERAGE = 0x0c;

    /**
     * Static Byte Encoding character map table type. Variant of
     * {@link TYPE_BYTE_ENCODING}.
     */
    const TYPE_BYTE_ENCODING_STATIC = 0xf1;

    /**
     * Unknown character map table type.
     */
    const TYPE_UNKNOWN = 0xff;


  /* Special Glyph Names */

    /**
     * Glyph representing missing characters.
     */
    const MISSING_CHARACTER_GLYPH = 0x00;



  /**** Public Interface ****/


  /* Factory Methods */

    /**
     * Instantiates the appropriate concrete subclass based on the type of cmap
     * table and returns the instance.
     *
     * The cmap type must be one of the following values:
     *  - {@link RE_Font_Cmap::TYPE_BYTE_ENCODING}
     *  - {@link RE_Font_Cmap::TYPE_BYTE_ENCODING_STATIC}
     *  - {@link RE_Font_Cmap::TYPE_HIGH_BYTE_MAPPING}
     *  - {@link RE_Font_Cmap::TYPE_SEGMENT_TO_DELTA}
     *  - {@link RE_Font_Cmap::TYPE_TRIMMED_TABLE}
     *  - {@link RE_Font_Cmap::TYPE_MIXED_COVERAGE}
     *  - {@link RE_Font_Cmap::TYPE_TRIMMED_ARRAY}
     *  - {@link RE_Font_Cmap::TYPE_SEGMENTED_COVERAGE}
     *
     * @param integer $cmapType Type of cmap.
     * @param mixed $cmapData Cmap table data. Usually a string or array.
     * @return RE_Font_Cmap
     * @throws InvalidArgumentException if the table type is invalid or the cmap
     *   table data cannot be validated.
     */
    public static function cmapWithTypeData($cmapType, $cmapData)
    {
        switch ($cmapType) {
            case RE_Font_Cmap::TYPE_BYTE_ENCODING:
                require_once 'RE/Font/Cmap/ByteEncoding.php';
                return new RE_Font_Cmap_ByteEncoding($cmapData);

            case RE_Font_Cmap::TYPE_BYTE_ENCODING_STATIC:
                require_once 'RE/Font/Cmap/ByteEncoding/Static.php';
                return new RE_Font_Cmap_ByteEncoding_Static($cmapData);

            case RE_Font_Cmap::TYPE_HIGH_BYTE_MAPPING:
                throw new InvalidArgumentException('High byte mapping cmap currently unsupported');

            case RE_Font_Cmap::TYPE_SEGMENT_TO_DELTA:
                require_once 'RE/Font/Cmap/SegmentToDelta.php';
                return new RE_Font_Cmap_SegmentToDelta($cmapData);

            case RE_Font_Cmap::TYPE_TRIMMED_TABLE:
                require_once 'RE/Font/Cmap/TrimmedTable.php';
                return new RE_Font_Cmap_TrimmedTable($cmapData);

            case RE_Font_Cmap::TYPE_MIXED_COVERAGE:
                throw new InvalidArgumentException('Mixed coverage cmap currently unsupported');

            case RE_Font_Cmap::TYPE_TRIMMED_ARRAY:
                throw new InvalidArgumentException('Trimmed array cmap currently unsupported');

            case RE_Font_Cmap::TYPE_SEGMENTED_COVERAGE:
                throw new InvalidArgumentException('Segmented coverage cmap currently unsupported');

            default:
                throw new InvalidArgumentException("Unknown cmap type: $cmapType");
        }
    }


  /* Abstract Methods */

    /**
     * Object constructor
     *
     * Parses the raw binary table data. Throws an exception if the table is
     * malformed.
     *
     * @param string $cmapData Raw binary cmap table data.
     * @throws Exception
     */
    abstract public function __construct($cmapData);

    /**
     * Returns an array of glyph numbers corresponding to the Unicode characters.
     *
     * If a particular character doesn't exist in this font, the special 'missing
     * character glyph' will be substituted.
     *
     * See also {@link glyphNumberForCharacter()}.
     *
     * @param array $characterCodes Array of Unicode character codes (code points).
     * @return array Array of glyph numbers.
     */
    abstract public function glyphNumbersForCharacters($characterCodes);

    /**
     * Returns the glyph number corresponding to the Unicode character.
     *
     * If a particular character doesn't exist in this font, the special 'missing
     * character glyph' will be substituted.
     *
     * See also {@link glyphNumbersForCharacters()} which is optimized for bulk
     * operations.
     *
     * @param integer $characterCode Unicode character code (code point).
     * @return integer Glyph number.
     */
    abstract public function glyphNumberForCharacter($characterCode);

    /**
     * Returns an array containing the Unicode characters that have entries in
     * this character map.
     *
     * @return array Unicode character codes.
     */
    abstract public function getCoveredCharacters();



  /**** Internal Methods ****/


  /* Internal Utility Methods */

    /**
     * Extracts a signed 2-byte integer from a string.
     *
     * Integers are always big-endian. Throws an exception if the index is out
     * of range.
     *
     * @param string $data
     * @param integer $index Position in string of integer.
     * @return integer
     * @throws RangeException
     */
    protected function _extractInt2($data, $index)
    {
        if (($index < 0) | (($index + 1) > strlen($data))) {
            throw new RangeException("Index out of range: $index");
        }
        $number = ord($data[$index]);
        if (($number & 0x80) == 0x80) {    // negative
            $number = ~((((~ $number) & 0xff) << 8) | ((~ ord($data[++$index])) & 0xff));
        } else {
            $number = ($number << 8) | ord($data[++$index]);
        }
        return $number;
    }

    /**
     * Extracts an unsigned 2-byte integer from a string.
     *
     * Integers are always big-endian. Throws an exception if the index is out
     * of range.
     *
     * @param string $data
     * @param integer $index Position in string of integer.
     * @return integer
     * @throws RangeException
     */
    protected function _extractUInt2($data, $index)
    {
        if (($index < 0) | (($index + 1) > strlen($data))) {
            throw new RangeException("Index out of range: $index");
        }
        $number = (ord($data[$index]) << 8) | ord($data[++$index]);
        return $number;
    }

    /**
     * Extracts an unsigned 4-byte integer from a string.
     *
     * Integers are always big-endian. Throws an exception if the index is out
     * of range.
     *
     * NOTE: If you ask for a 4-byte unsigned integer on a 32-bit machine, the
     * resulting value WILL BE SIGNED because PHP uses signed integers internally
     * for everything. To guarantee portability, be sure to use bitwise or
     * similar operators on large integers!
     *
     * @param string $data
     * @param integer $index Position in string of integer.
     * @return integer
     * @throws RangeException
     */
    protected function _extractUInt4($data, $index)
    {
        if (($index < 0) | (($index + 3) > strlen($data))) {
            throw new RangeException("Index out of range: $index");
        }
        $number = (ord($data[$index]) << 24) | (ord($data[++$index]) << 16) |
                  (ord($data[++$index]) << 8) | ord($data[++$index]);
        return $number;
    }

}
