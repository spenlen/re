<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Geometry
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */

/** RE_Point */
require_once 'RE/Point.php';

/** RE_Size */
require_once 'RE/Size.php';


/**
 * Represents a rectangle.
 *
 * A RE_Rect is comprised of a {@link RE_Point} which represents its origin
 * (the lower-left corner) and a {@link RE_Size} which represents its width
 * and height (as measured from its origin).
 *
 * @todo Add the following method: divideRect()
 *
 * @package    Geometry
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Rect
{
  /**** Class Constants ****/

    /**
     * Precision for comparing equality of float values.
     */
    const FLOAT_COMPARISON_PRECISION = 1e-4;



  /**** Instance Variables ****/

    /**
     * The {@link RE_Point} object that stores the rect's origin.
     * @var RE_Point
     */
    public $origin = null;

    /**
     * The {@link RE_Size} object that stores the rect's width and height.
     * @var RE_Size
     */
    public $size = null;



  /**** Class Methods ****/

    /**
     * Returns a new RE_Rect object from the string representation, usually
     * formatted as "{{x, y}, {w, h}}".
     *
     * The string is scanned for four numbers, which are used as the x and y
     * coordinates of the origin, and width and height of the rect, in that
     * order. If the string does not contain four numbers, any numbers found
     * are used, and the remaining values are assumed to be zero. All values
     * are interpreted as floats.
     *
     * @param string $string
     * @return RE_Rect
     */
    public static function fromString($string)
    {
        preg_match('/(-?\d+\.?\d*)(.+?(-?\d+\.?\d*)(.+?(-?\d+\.?\d*)(.+?(-?\d+\.?\d*))?)?)?/ms', $string, $matches);
        return new self((isset($matches[1]) ? $matches[1] : 0.0), (isset($matches[3]) ? $matches[3] : 0.0),
                        (isset($matches[5]) ? $matches[5] : 0.0), (isset($matches[7]) ? $matches[7] : 0.0));
    }



  /**** Public Interface ****/


  /* Object Lifecycle */

    /**
     * Object constructor signatures:
     *
     *  - Initialize an empty rect:
     *      new RE_Rect();
     *
     *  - Initialize at a specific origin with a specific size:
     *      new RE_Rect(number $x, number $y, number $width, number $height);
     *
     *  - Initialize using the {@link RE_Point}'s value as the origin and the
     *    {@link RE_Size}'s value as the size:
     *      new RE_Rect(RE_Point $origin, RE_Size $size);
     *
     *  - Initialize using two {@link RE_Point}s as the opposite corners
     *    corners of the rect:
     *      new RE_Rect(RE_Point $pointOne, RE_Point $pointTwo);
     *
     *  - Clone an existing rect:
     *      new RE_Rect(RE_Rect $rect);
     *
     * @param mixed $param1
     * @param mixed $param2
     * @param mixed $param3
     * @param mixed $param4
     * @throws BadMethodCallException if the constructor arguments cannot be deduced.
     */
    public function __construct($param1 = null, $param2 = null, $param3 = null, $param4 = null)
    {
        if (isset($param1, $param2, $param3, $param4)) {
            /* Initialize at a specific origin with a specific size.
             */
            $this->origin = new RE_Point($param1, $param2);
            $this->size   = new RE_Size($param3, $param4);

        } else if ((! isset($param1)) && (! isset($param2)) &&
                   (! isset($param3)) && (! isset($param4))) {
            /* Initialize an empty rect.
             */
            $this->origin = new RE_Point();
            $this->size   = new RE_Size();

        } else if (($param1 instanceof RE_Point) && ($param2 instanceof RE_Size) &&
                   (! isset($param3)) && (! isset($param4))) {
            /* Initialize using the point object as the origin and the size
             * object as the size. Copy the values.
             */
            $this->origin = clone $param1;
            $this->size   = clone $param2;

        } else if (($param1 instanceof RE_Point) && ($param2 instanceof RE_Point) &&
                   (! isset($param3)) && (! isset($param4))) {
            /* Initialize using two point objects as opposite corners of the rect.
             */
            $this->origin = new RE_Point(min($param1->x, $param2->x), min($param1->y, $param2->y));
            $this->size   = new RE_Size(abs($param1->x - $param2->x), abs($param1->y - $param2->y));

        } else if (($param1 instanceof RE_Rect) &&
                   (! isset($param2)) && (! isset($param3)) && (! isset($param4))) {
            /* Clone an existing size object. Copy the values.
             */
            $this->origin = clone $param1->origin;
            $this->size   = clone $param1->size;

        } else {
            throw new BadMethodCallException('Unrecognized arguments provided to object constructor.');
        }
    }


  /* Object Magic Methods */

    /**
     * Returns the rect's coordinates as a string of the form "{{x, y}, {w, h}}".
     *
     * @return string
     */
    public function __toString()
    {
        return '{' . $this->origin->__toString() . ', ' . $this->size->__toString() . '}';
    }

    /**
     * Returns a copy of the rect.
     *
     * @return RE_Rect
     */
    public function __clone()
    {
        $this->origin = clone $this->origin;
        $this->size   = clone $this->size;
    }


  /* Accessor Methods */

    /**
     * Returns the origin.
     *
     * @return RE_Point
     */
    public function getOrigin()
    {
        return $this->origin;
    }

    /**
     * Sets the origin.
     *
     * @param RE_Point $origin
     */
    public function setOrigin(RE_Point $origin)
    {
        $this->origin->x = $origin->x;
        $this->origin->y = $origin->y;
    }

    /**
     * Returns the horizontal (x) coordinate of the origin.
     *
     * @return float
     */
    public function getX()
    {
        return $this->origin->x;
    }

    /**
     * Sets the horizontal (x) coordinate of the origin.
     *
     * @param float $x
     */
    public function setX($x)
    {
        $this->origin->x = (float)$x;
    }

    /**
     * Returns the vertical (y) coordinate of the origin.
     *
     * @return float
     */
    public function getY()
    {
        return $this->origin->y;
    }

    /**
     * Sets the vertical (y) coordinate of the origin.
     *
     * @param float $y
     */
    public function setY($y)
    {
        $this->origin->y = (float)$y;
    }

    /**
     * Returns the size of the rect.
     *
     * @return RE_Size
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Sets the size of the rect.
     *
     * @param RE_Size $size
     */
    public function setSize(RE_Size $size)
    {
        $this->size->width  = $size->width;
        $this->size->height = $size->height;
    }

    /**
     * Returns the width of the rect.
     *
     * @return float
     */
    public function getWidth()
    {
        return $this->size->width;
    }

    /**
     * Sets the width of the rect.
     *
     * @param float $width
     */
    public function setWidth($width)
    {
        $this->size->width = (float)$width;
    }

    /**
     * Returns the height of the rect.
     *
     * @return float
     */
    public function getHeight()
    {
        return $this->size->height;
    }

    /**
     * Sets the height of the rect.
     *
     * @param float $height
     */
    public function setHeight($height)
    {
        $this->size->height = (float)$height;
    }


  /* Calculation */

    /**
     * Returns the minimum horizontal (x) coordinate of the rect.
     *
     * @return float
     */
    public function minX()
    {
        return $this->origin->x;
    }

    /**
     * Returns the horizontal (x) coordinate at the midpoint of the rect.
     *
     * @return float
     */
    public function midX()
    {
        return $this->origin->x + ($this->size->width / 2);
    }

    /**
     * Returns the maximum horizontal (x) coordinate of the rect.
     *
     * @return float
     */
    public function maxX()
    {
        return $this->origin->x + $this->size->width;
    }

    /**
     * Returns the minimum vertical (y) coordinate of the rect.
     *
     * @return float
     */
    public function minY()
    {
        return $this->origin->y;
    }

    /**
     * Returns the vertical (y) coordinate at the midpoint of the rect.
     *
     * @return float
     */
    public function midY()
    {
        return $this->origin->y + ($this->size->height / 2);
    }

    /**
     * Returns the maximum vertical (y) coordinate of the rect.
     *
     * @return float
     */
    public function maxY()
    {
        return $this->origin->y + $this->size->height;
    }


  /* Test and Comparison */

    /**
     * Returns true if the rect is empty, meaning it has a height or width
     * less than or equal to zero.
     *
     * @return boolean
     */
    public function isEmpty()
    {
        return $this->size->isEmpty();
    }

    /**
     * Returns true if the specified rect has the same origin and size as
     * this rect.
     *
     * @param RE_Rect $rect
     * @return boolean
     */
    public function isEqualToRect(RE_Rect $rect)
    {
        if (($this->origin->isEqualToPoint($rect->origin)) &&
            ($this->size->isEqualToSize($rect->size))) {
                return true;
        }
        return false;
    }

    /**
     * Returns true if the specified rect intersects with this rect.
     *
     * @param RE_Rect $rect
     * @return boolean
     */
    public function intersectsRect(RE_Rect $rect)
    {
        if ((! $this->size->isEmpty()) &&
            (! $rect->size->isEmpty()) &&
            ($rect->origin->x < ($this->origin->x + $this->size->width )) &&
            ($rect->origin->y < ($this->origin->y + $this->size->height)) &&
            ($this->origin->x < ($rect->origin->x + $rect->size->width )) &&
            ($this->origin->y < ($rect->origin->y + $rect->size->height))) {
                return true;
        }
        return false;
    }

    /**
     * Returns true if this rect is completely contained within the specified
     * rect's bounds.
     *
     * @param RE_Rect $rect
     * @return boolean
     */
    public function isSubrectOfRect(RE_Rect $rect)
    {
        if ((! $this->size->isEmpty()) &&
            (! $rect->size->isEmpty()) &&
            ($this->origin->x >= $rect->origin->x) &&
            ($this->origin->y >= $rect->origin->y) &&
            (($this->origin->x + $this->size->width) <=
             ($rect->origin->x + $rect->size->width)) &&
            (($this->origin->y + $this->size->height) <=
             ($rect->origin->y + $rect->size->height))) {
                 return true;
         }
        return false;
    }

    /**
     * Returns true if the specified rect is completely contained within this
     * rect's bounds.
     *
     * @param RE_Rect $rect
     * @return boolean
     */
    public function containsRect(RE_Rect $rect)
    {
        return $rect->isSubrectOfRect($this);
    }

    /**
     * Returns true if the specified point lies anywhere within the rect's
     * bounds.
     *
     * @param RE_Point $point
     * @return boolean
     */
    public function containsPoint(RE_Point $point)
    {
        if ((! $this->size->isEmpty()) &&
            ($point->x >= $this->origin->x) &&
            ($point->y >= $this->origin->y) &&
            ($point->x <= ($this->origin->x + $this->size->width )) &&
            ($point->y <= ($this->origin->y + $this->size->height))) {
                return true;
        }
        return false;
    }


  /* Conversion */

    /**
     * Returns a new rect object that is inset horizontally and vertically from
     * this rect by the specified amounts.
     *
     * @param float $horizontalInset
     * @param float $verticalInset
     * @return RE_Rect
     */
    public function rectByInsettingRect($horizontalInset, $verticalInset)
    {
        return new self(($this->origin->x + $horizontalInset),
                        ($this->origin->y + $verticalInset),
                        ($this->size->width  - $horizontalInset - $horizontalInset),
                        ($this->size->height - $verticalInset   - $verticalInset));
    }

    /**
     * Returns a new rect object that is the result of the intersection between
     * this rect and the specified rect.
     *
     * Returns an empty rect if either rect is empty or if the rects do not
     * intersect.
     *
     * @param RE_Rect $rect
     * @return RE_Rect
     */
    public function rectByIntersectingRect(RE_Rect $rect)
    {
        if ($this->size->isEmpty() || $rect->size->isEmpty()) {
            return new self();
        }

        $xMin = max($this->origin->x, $rect->origin->x);
        $xMax = min(($this->origin->x + $this->size->width), ($rect->origin->x + $rect->size->width));
        if ($xMax <= $xMin) {
            return new self();
        }

        $yMin = max($this->origin->y, $rect->origin->y);
        $yMax = min(($this->origin->y + $this->size->height), ($rect->origin->y + $rect->size->height));
        if ($yMax <= $yMin) {
            return new self();
        }

        return new self($xMin, $yMin, ($xMax - $xMin), ($yMax - $yMin));
    }

    /**
     * Returns a new rect object whose origin coordinates are the result of
     * rounding this rect's origin coordinates down to the nearest integer and
     * whose size is the result of rounding this rect's size up to the nearest
     * integer.
     *
     * The result of this operation is a rect that lies completely on integer
     * boundaries.
     *
     * Returns an empty rect if this rect is empty.
     *
     * @return RE_Rect
     */
    public function rectByMakingIntegral()
    {
        if ($this->size->isEmpty()) {
            return new self();
        }
        return new self(floor($this->origin->x),
                        floor($this->origin->y),
                        ceil($this->size->width),
                        ceil($this->size->height));
    }

    /**
     * Returns a new rect whose offset has been shifted horizontally and
     * vertically from this rect by the specified amounts. The rect's size
     * remains the same.
     *
     * @param float $horizontalOffset
     * @param float $verticalOffset
     * @return RE_Rect
     */
    public function rectByOffsettingRect($horizontalOffset, $verticalOffset)
    {
        return new self(($this->origin->x + $horizontalOffset),
                        ($this->origin->y + $verticalOffset),
                        $this->size->width,
                        $this->size->height);
    }

    /**
     * Returns a new rect that completely encloses this rect and and the
     * specified rect.
     *
     * If both rects are empty, returns an empty rect. If only one is empty,
     * returns a copy of the other rect.
     *
     * @param RE_Rect $rect
     * @return RE_Rect
     */
    public function rectByUnioningRect(RE_Rect $rect)
    {
        if ($this->size->isEmpty()) {
            if ($rect->size->isEmpty()) {
                return new self();
            } else {
                return clone $rect;
            }
        } else if ($rect->size->isEmpty()) {
            return clone $this;
        }

        $x = min($this->origin->x, $rect->origin->x);
        $y = min($this->origin->y, $rect->origin->y);

        $width  = max(($this->origin->x + $this->size->width),  ($rect->origin->x + $rect->size->width))  - $x;
        $height = max(($this->origin->y + $this->size->height), ($rect->origin->y + $rect->size->height)) - $y;

        return new self($x, $y, $width, $height);
    }

    /**
     * Returns a copy of this rect that is proportionally scaled and aligned
     * inside the specified rect.
     *
     * If either rect is empty, returns an empty rect.
     *
     * @param RE_Rect $rect
     * @param string  $horizontalAlign (optional) left [default], center, right
     * @param string  $verticalAlign   (optional) top, middle, bottom [default]
     */
    public function rectByScalingInsideRect(RE_Rect $rect, $horizontalAlign = 'left', $verticalAlign = 'bottom')
    {
        if (($this->size->isEmpty()) || ($rect->size->isEmpty())) {
            return new self();
        }

        $scaledRect = clone $rect;

        $thisRatio = $this->size->width / $this->size->height;
        $rectRatio = $rect->size->width / $rect->size->height;

        if (abs($thisRatio - $rectRatio) < RE_Rect::FLOAT_COMPARISON_PRECISION) {    // $thisRatio == $rectRatio
            $scaledRect->size->width  *= $thisRatio / $rectRatio;
            $scaledRect->size->height *= $thisRatio / $rectRatio;
        } else if ($thisRatio > $rectRatio) {
            $scaledRect->size->height = $scaledRect->size->width / $thisRatio;
        } else {    // $thisRatio < $rectRatio
            $scaledRect->size->width = $scaledRect->size->height * $thisRatio;
        }

        if ($scaledRect->size->width < $rect->size->width) {
            switch ($horizontalAlign) {
            case 'left':
                // already left-aligned. do nothing.
                break;
            case 'right':
                $scaledRect->origin->x += $rect->size->width - $scaledRect->size->width;
                break;
            default:  // 'center'
                $scaledRect->origin->x += ($rect->size->width - $scaledRect->size->width) / 2;
            }
        }

        if ($scaledRect->size->height < $rect->size->height) {
            switch ($verticalAlign) {
            case 'top':
                $scaledRect->origin->y += $rect->size->height - $scaledRect->size->height;
                break;
            case 'bottom':
                // already bottom-aligned. do nothing.
                break;
            default:  // 'middle'
                $scaledRect->origin->y += ($rect->size->height - $scaledRect->size->height) / 2;
            }
        }

        return $scaledRect;
    }

}
