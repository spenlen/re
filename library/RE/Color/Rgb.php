<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Color
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @copyright  Copyright (c) 2005-2008 Zend Technologies USA, Inc. (http://www.zend.com)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */

/* Portions of this source file are excerpted from the Zend Framework:
 * 
 * Copyright (c) 2005-2008, Zend Technologies USA, Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 *  * Neither the name of Zend Technologies USA, Inc. nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/** RE_Color */
require_once 'RE/Color.php';


/**
 * RGB color implementation
 *
 * @package    Color
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @copyright  Copyright (c) 2005-2008 Zend Technologies USA, Inc. (http://www.zend.com)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Color_Rgb extends RE_Color
{
  /**** Instance Variables ****/
  
    /**
     * Red color component.
     * @var float
     */
    protected $_red = 0.0;

    /**
     * Green color component.
     * @var float
     */
    protected $_green = 0.0;

    /**
     * Blue color component.
     * @var float
     */
    protected $_blue = 0.0;



  /**** Public Interface ****/
  
  
  /* Concrete Subclass Implementation */

    /**
     * Sets the current filling or stroking color on the canvas.
     *
     * @param RE_Pdf_Canvas_Interface $canvas
     * @param boolean $useForStroking
     */
    public function draw(RE_Pdf_Canvas_Interface $canvas, $useForStroking)
    {
        RE_Pdf_Object_Numeric::writeValue($canvas, $this->_red);
        RE_Pdf_Object_Numeric::writeValue($canvas, $this->_green);
        RE_Pdf_Object_Numeric::writeValue($canvas, $this->_blue);
        $canvas->writeBytes($useForStroking ? " RG\n" : " rg\n");
    }


  /* Object Lifecycle */
  
    /**
     * Creates a new RE_Color_Rgb object.
     * 
     * Individual red, green, and blue components are specified as float values
     * in the range of 0.0 to 1.0, where 0.0 is the complete lack of that color
     * component and 1.0 is maximum intensity. Invalid values are clamped to
     * the valid range.
     *
     * @param float $red
     * @param float $green
     * @param float $blue
     */
    public function __construct($red, $green, $blue)
    {
        /* Clamp values to legal limits.
         */
        $this->_red   = min(max((float)$red,   0), 1);
        $this->_green = min(max((float)$green, 0), 1);
        $this->_blue  = min(max((float)$blue,  0), 1);
    }

}
