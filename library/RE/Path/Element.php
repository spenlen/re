<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Graphics
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * RE_Path_Element objects model the individual control and end points used to
 * define a line segment within a {@link RE_Path}.
 * 
 * RE_Path_Element objects are created automatically by the path construction
 * methods in {@link RE_Path}; you will rarely need to create one directly.
 * However, you may wish to modify individual elements of an already-constructed
 * path.
 * 
 * The type of the element is determined by an attribute as opposed to using
 * subclasses so that it can be easily changed. For example, you can flatten
 * a complex curve by changing each element's type from {@link TYPE_CURVE} to
 * {@link TYPE_LINE}.
 * 
 * @package    Graphics
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Path_Element
{
  /**** Class Constants ****/

    /**
     * Create a new subpath by moving the current drawing point to the path
     * element's end point.
     * @var integer
     */
    const TYPE_MOVE = 0;
    
    /**
     * Append a straight line segment to the path from the current drawing
     * point to the path element's end point.
     * @var integer
     */
    const TYPE_LINE = 1;
    
    /**
     * Append a quadratic (if one control point is present) or cubic (if two
     * control points are present) curved line segment to the path from the
     * current drawing point to the path element's end point.
     * @var integer
     */
    const TYPE_CURVE = 2;
    
    /**
     * Close the current subpath.
     * @var integer
     */
    const TYPE_CLOSE = 3;



  /**** Instance Variables ****/
  
    /**
     * Array of elements comprising the path.
     * @var array
     */
    protected $_type = RE_Path_Element::TYPE_MOVE;
    
    /**
     * @var RE_Point
     */
    protected $_endPoint = null;
  
    /**
     * @var RE_Point
     */
    protected $_controlPoint1 = null;
  
    /**
     * @var RE_Point
     */
    protected $_controlPoint2 = null;
  
  
  
  /**** Public Interface ****/


  /* Object Lifecycle */
  
    /**
     * Creates a new RE_Path_Element object of the specified type, using the
     * specified end and control points.
     * 
     * The element type must be one of the following values:
     *  - {@link RE_Path_Element::TYPE_MOVE}
     *  - {@link RE_Path_Element::TYPE_LINE}
     *  - {@link RE_Path_Element::TYPE_CURVE}
     *  - {@link RE_Path_Element::TYPE_CLOSE}
     * 
     * Also verifies that at least one control point has been set for
     * {@link TYPE_CURVE}.
     *
     * @param integer  $type
     * @param RE_Point $endPoint
     * @param RE_Point $controlPoint1 (optional)
     * @param RE_Point $controlPoint2 (optional)
     * @throws InvalidArgumentException if the type is invalid or if the
     *   control points are missing but required for the type.
     */
    public function __construct($type, RE_Point $endPoint,
        RE_Point $controlPoint1 = null, RE_Point $controlPoint2 = null)
    {
        $this->setEndPoint($endPoint);
        $this->setControlPoint1($controlPoint1);
        $this->setControlPoint2($controlPoint2);
        
        /* Set the type last as it performs all of the validation.
         */
        $this->setType($type);
    }
  
  
  /* Object Magic Methods */
  
    /**
     * Returns the path element as a string of the form
     *   "{type, endPoint, controlPoint1, controlPoint2}".
     *
     * @return string
     */
    public function __toString()
    {
        switch ($this->_type) {
        case RE_Path_Element::TYPE_MOVE:
            $string = '{move, ';  break;
        case RE_Path_Element::TYPE_LINE:
            $string = '{line, ';  break;
        case RE_Path_Element::TYPE_CURVE:
            $string = '{curve, '; break;
        case RE_Path_Element::TYPE_CLOSE:
            $string = '{close, '; break;
        }
        return $string . $this->_endPoint->__toString() . ', '
             . ($this->_controlPoint1 ? $this->_controlPoint1->__toString() : 'null') . ', '
             . ($this->_controlPoint2 ? $this->_controlPoint2->__toString() : 'null') . '}';
    }
    
    /**
     * Returns a copy of the path element.
     * 
     * @return RE_Path_Element
     */
    public function __clone()
    {
        $this->_endPoint = clone $this->_endPoint;
        if (! is_null($this->_controlPoint1)) {
            $this->_controlPoint1 = clone $this->_controlPoint1;
        }
        if (! is_null($this->_controlPoint2)) {
            $this->_controlPoint2 = clone $this->_controlPoint2;
        }
    }
    

  /* Accessor Methods */
    
    /**
     * Returns the element's type.
     * 
     * @return integer One of the TYPE constants defined in this class.
     */
    public function getType()
    {
        return $this->_type;
    }
    
    /**
     * Sets the element's type.
     * 
     * The element type must be one of the following values:
     *  - {@link RE_Path_Element::TYPE_MOVE}
     *  - {@link RE_Path_Element::TYPE_LINE}
     *  - {@link RE_Path_Element::TYPE_CURVE}
     *  - {@link RE_Path_Element::TYPE_CLOSE}
     * 
     * Also verifies that at least one control point has been set for
     * {@link TYPE_CURVE}.
     * 
     * @param integer $type
     * @throws InvalidArgumentException if the type is invalid or if the
     *   control points are missing but required for the type.
     */
    public function setType($type)
    {
        switch ($type) {
        case RE_Path_Element::TYPE_MOVE:
        case RE_Path_Element::TYPE_LINE:
        case RE_Path_Element::TYPE_CLOSE:
            $this->_type = $type;
            break;
            
        case RE_Path_Element::TYPE_CURVE:
            if ((empty($this->_controlPoint1)) && (empty($this->_controlPoint2))) {
                throw new InvalidArgumentException('At least one control point is required for curve elements.');
            }
            $this->_type = $type;
            break;
            
        default:
            throw new InvalidArgumentException("Invalid path element type: $type");
        }
    }

    /**
     * Returns the element's end point.
     * 
     * @return RE_Point
     */
    public function getEndPoint()
    {
        return $this->_endPoint;
    }
    
    /**
     * Sets the element's end point.
     * 
     * @param RE_Point $endPoint
     */
    public function setEndPoint(RE_Point $endPoint)
    {
        $this->_endPoint = clone $endPoint;
    }

    /**
     * Returns the first control point, the point associated with the
     * current drawing point.
     * 
     * @return RE_Point
     */
    public function getControlPoint1()
    {
        return $this->_controlPoint1;
    }
    
    /**
     * Sets or clears the first control point, the point associated with the
     * current drawing point.
     * 
     * @param RE_Point $controlPoint (optional)
     */
    public function setControlPoint1(RE_Point $controlPoint = null)
    {
        if (is_null($controlPoint)) {
            $this->_controlPoint1 = null;
        } else {
            $this->_controlPoint1 = clone $controlPoint;
        }
    }

    /**
     * Returns the second control point, the point associated with the
     * element's end point.
     * 
     * @return RE_Point
     */
    public function getControlPoint2()
    {
        return $this->_controlPoint2;
    }
    
    /**
     * Sets or clears the second control point, the point associated with the
     * element's end point.
     * 
     * @param RE_Point $controlPoint (optional)
     */
    public function setControlPoint2(RE_Point $controlPoint = null)
    {
        if (is_null($controlPoint)) {
            $this->_controlPoint2 = null;
        } else {
            $this->_controlPoint2 = clone $controlPoint;
        }
    }



  /* PDF Drawing */
  
    /**
     * Draws the path element on the specified canvas.
     * 
     * @param RE_Pdf_Canvas_Interface $canvas
     */
    public function draw(RE_Pdf_Canvas_Interface $canvas)
    {
        switch ($this->_type) {
            
        case RE_Path_Element::TYPE_MOVE:
            RE_Pdf_Object_Numeric::writeValue($canvas, $this->_endPoint->getX());
            RE_Pdf_Object_Numeric::writeValue($canvas, $this->_endPoint->getY());
            $canvas->writeBytes(" m\n");
            break;
            
        case RE_Path_Element::TYPE_LINE:
            RE_Pdf_Object_Numeric::writeValue($canvas, $this->_endPoint->getX());
            RE_Pdf_Object_Numeric::writeValue($canvas, $this->_endPoint->getY());
            $canvas->writeBytes(" l\n");
            break;
            
        case RE_Path_Element::TYPE_CURVE:
            $operator = " c\n";
            
            if (is_null($this->_controlPoint1)) {
                $operator = " v\n";
            } else {
                RE_Pdf_Object_Numeric::writeValue($canvas, $this->_controlPoint1->getX());
                RE_Pdf_Object_Numeric::writeValue($canvas, $this->_controlPoint1->getY());
            }
            
            if (is_null($this->_controlPoint2)) {
                $operator = " y\n";
            } else {
                RE_Pdf_Object_Numeric::writeValue($canvas, $this->_controlPoint2->getX());
                RE_Pdf_Object_Numeric::writeValue($canvas, $this->_controlPoint2->getY());
            }
            
            RE_Pdf_Object_Numeric::writeValue($canvas, $this->_endPoint->getX());
            RE_Pdf_Object_Numeric::writeValue($canvas, $this->_endPoint->getY());
            $canvas->writeBytes($operator);
            break;
            
        case RE_Path_Element::TYPE_CLOSE:
            $canvas->writeBytes(" h\n");
            break;

        }
    }
    
}
