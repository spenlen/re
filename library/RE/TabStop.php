<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Text
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * Represents a tab stop on a ruler.
 *
 * Used with {@link RE_ParagraphStyle} objects.
 *
 * @package    Text
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_TabStop
{
  /**** Class Constants ****/
 
 
  /* Tab Stop Type */
  
    /**
     * Left-aligned tab.
     */
    const TYPE_LEFT = 0;
    
    /**
     * Center-aligned tab.
     */
    const TYPE_CENTER = 1;
    
    /**
     * Right-aligned tab.
     */
    const TYPE_RIGHT = 2;
    
    /**
     * Decimal-aligned tab.
     */
    const TYPE_DECIMAL = 3;


  /* Common Leader Strings */
  
    /**
     * No leader.
     */
    const LEADER_NONE = '';

    /**
     * Dot (period) -- FULL STOP U+002E.
     */
    const LEADER_DOT = '.';

    /**
     * Middle dot (bullet) -- MIDDLE DOT U+00B7.
     */
    const LEADER_MIDDLE_DOT = "\xc2\xb7";

    /**
     * Hyphens (dash) -- HYPHEN-MINUS U+002D.
     */
    const LEADER_HYPHEN = '-';

    /**
     * Underline -- LOW LINE U+005F.
     */
    const LEADER_UNDERLINE = '_';



  /**** Instance Variables ****/
  
    /**
     * Tab type. Use the TYPE_ constants defined in this class.
     * @var integer
     */
    protected $_type = RE_TabStop::TYPE_LEFT;
    
    /**
     * The distance in points from the head edge (the left edge for left-to-right
     * writing systems) of the enclosing rectangle to the location of this tab
     * stop. Must be positive or zero.
     * @var float
     */
    protected $_location = 0.0;
  
    /**
     * Leader string. Usually one of the LEADER_ constants defined in this class.
     * @var string
     */
    protected $_leader = RE_TabStop::LEADER_NONE;
    
    
    
  /**** Public Interface ****/


  /* Object Lifecycle */
  
    /**
     * Object constructor.
     *
     * @param integer $type     One of the TYPE_ constants defined in this class.
     * @param float   $location Location in points
     * @param string  $leader   (optional) Leader string. Default is no leader.
     */
    public function __construct($type, $location, $leader = RE_TabStop::LEADER_NONE)
    {
        $this->setType($type);
        $this->setLocation($location);
        $this->setLeader($leader);
    }
    
    
  /* Object Magic Methods */
  
    /**
     * Returns a string describing the tab stop's type, leader type, and location.
     *
     * @return string
     */
    public function __toString()
    {
        switch ($this->_type) {
        case RE_TabStop::TYPE_LEFT:
            $string = 'Left';
            break;
        case RE_TabStop::TYPE_CENTER:
            $string = 'Center';
            break;
        case RE_TabStop::TYPE_RIGHT:
            $string = 'Right';
            break;
        case RE_TabStop::TYPE_DECIMAL:
            $string = 'Decimal';
            break;
        }
        if (strlen($this->_leader)) {
            $string .= '(' . str_repeat($this->_leader, 3) . ')';
        }
        return $string . ': ' . $this->_location;
    }
    

  /* Accessor Methods */
  
    /**
     * Returns the tab type.
     *
     * @return integer One of the TYPE_ constants defined in this class.
     */
    public function getType()
    {
        return $this->_type;
    }
    
    /**
     * Sets the tab type.
     *
     * The tab type must be one of the following values:
     *  - {@link RE_TabStop::TYPE_LEFT}
     *  - {@link RE_TabStop::TYPE_CENTER}
     *  - {@link RE_TabStop::TYPE_RIGHT}
     *  - {@link RE_TabStop::TYPE_DECIMAL}
     *
     * @param integer $type
     * @throws InvalidArgumentException if the tab type is invalid.
     */
    public function setType($type)
    {
        switch ($type) {
        case RE_TabStop::TYPE_LEFT:
        case RE_TabStop::TYPE_CENTER:
        case RE_TabStop::TYPE_RIGHT:
        case RE_TabStop::TYPE_DECIMAL:
            $this->_type = $type;
            break;
        default:
            throw new InvalidArgumentException("Invalid tab stop type: $type");
        }
    }

    /**
     * Returns the location of the tab.
     *
     * @return float
     */
    public function getLocation()
    {
        return $this->_location;
    }
    
    /**
     * Sets the location of the tab.
     *
     * The location is the distance in points from the head edge (the left edge
     * for left-to-right writing systems) of the enclosing rectangle. It must be
     * positive or zero.
     *
     * @param float $location
     * @throws RangeException if the tab stop location is less than zero.
     */
    public function setLocation($location)
    {
        $tabLocation = (float)$location;
        if ($tabLocation < 0) {
            throw new RangeException("Tab stop location must be positive or zero: $location");
        }
        $this->_location = $tabLocation;
    }

    /**
     * Returns the tab leader string.
     *
     * @return string Will be empty if the tab has no leader.
     */
    public function getLeader()
    {
        return $this->_leader;
    }
    
    /**
     * Sets the leader string.
     *
     * You may use the following constants for common leaders:
     *  - {@link RE_TabStop::LEADER_NONE}
     *  - {@link RE_TabStop::LEADER_DOT}
     *  - {@link RE_TabStop::LEADER_MIDDLE_DOT}
     *  - {@link RE_TabStop::LEADER_HYPHEN}
     *  - {@link RE_TabStop::LEADER_UNDERLINE}
     *
     * @param string $leader Must be UTF-8 encoded.
     */
    public function setLeader($leader)
    {
        $this->_leader = (string)$leader;
    }

}
