<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    FileParser
 * @subpackage Images
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */

/** RE_FileParser */
require_once 'RE/FileParser.php';


/**
 * Abstract base class for image file parsers.
 *
 * Defines the public interface for concrete subclasses which are responsible
 * for parsing the raw binary image data. Also provides shared utility methods.
 * 
 * @package    FileParser
 * @subpackage Images
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
abstract class RE_FileParser_Image extends RE_FileParser
{
  /**** Abstract Interface ****/

    /**
     * Returns the width of the image in pixels (samples).
     *
     * @return integer
     */
    abstract public function getPixelWidth();

    /**
     * Returns the width of the image in pixels (samples).
     *
     * @return integer
     */
    abstract public function getPixelHeight();

}
