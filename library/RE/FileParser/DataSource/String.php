<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    FileParser
 * @subpackage DataSource
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */

/** RE_FileParser_DataSource_Interface */
require_once 'RE/FileParser/DataSource/Interface.php';

/** RE_FileParser_Exception */
require_once 'RE/FileParser/Exception.php';


/**
 * File parser data source that provides an interface to binary strings.
 *
 * @package    FileParser
 * @subpackage DataSource
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_FileParser_DataSource_String implements RE_FileParser_DataSource_Interface
{
  /**** Instance Variables ****/

    /**
     * The binary string to parse.
     * @var string
     */
    protected $_string = '';

    /**
     * Byte length of the binary string.
     * @var integer
     */
    protected $_size = 0;

    /**
     * Current offset within the binary string.
     * @var integer
     */
    protected $_offset = 0;



  /**** Public Interface ****/


  /* Object Lifecycle */

    /**
     * Verifies that the source binary string is not empty.
     *
     * @param string $string
     * @throws RE_FileParser_Exception if the binary string is empty.
     */
    public function __construct($string)
    {
        $this->_size = strlen($string);
        if ($this->_size < 1) {
            throw new RE_FileParser_Exception(
                'String is empty', RE_FileParser_Exception::FILE_IS_EMPTY);
        }
        $this->_string = $string;
    }


  /* Object Magic Methods */

    /**
     * Returns a string containing the binary string's byte length.
     *
     * @return string
     */
    public function __toString()
    {
        return 'String (' . $this->_size . ' bytes)';
    }


  /* RE_FileParser_DataSource_Interface Implementation */

    /**
     * Returns the size in bytes of the binary string.
     *
     * @return integer
     */
    public function getSize()
    {
        return $this->_size;
    }

    /**
     * Returns the byte offset of the current read position within the binary
     * string.
     *
     * @return integer
     */
    public function getOffset()
    {
        return $this->_offset;
    }

    /**
     * Moves the current read position to the specified byte offset.
     *
     * @param integer $offset Destination byte offset.
     * @throws RangeException if the byte offset is moved before the beginning
     *   or beyond the end of the data source.
     */
    public function moveToOffset($offset)
    {
        if ($this->_offset == $offset) {
            return;    // Not moving; do nothing.
        }
        
        if ($offset < 0) {
            throw new RangeException('Attempt to move before start of data source');
        }
        if ($offset > $this->_size) {    // Offsets are zero-based.
            throw new RangeException('Attempt to move beyond end of data source');
        }
        $this->_offset = $offset;
    }

    /**
     * Shifts the current read position within the binary string by the specified
     * number of bytes. You may move forward (positive numbers) or backward
     * (negative numbers).
     *
     * @param integer $byteCount Number of bytes to skip.
     * @throws RangeException if the new byte offset is before the beginning or
     *   beyond the end of the data source.
     */
    public function skipBytes($byteCount)
    {
        if ($byteCount == 0) {
            return;    // Not moving; do nothing.
        }
        $offset = $this->_offset + $byteCount;

        if ($offset < 0) {
            throw new RangeException('Attempt to move before start of data source');
        }
        if ($offset > $this->_size) {    // Offsets are zero-based.
            throw new RangeException('Attempt to move beyond end of data source');
        }
        $this->_offset = $offset;
    }

    /**
     * Returns the specified number of raw bytes from the binary string at the
     * byte offset of the current read position.
     *
     * Advances the read position by the number of bytes read.
     *
     * @param integer $byteCount              Number of bytes to read.
     * @param boolean $failOnInsufficientData (optional) Default is true.
     * @return string
     * @throws RangeException if $failOnInsufficientData is true and there is
     *   insufficient data to completely fulfill the request.
     */
    public function readBytes($byteCount, $failOnInsufficientData = true)
    {
        if ($byteCount < 1) {
            return '';
        }
        
        if ($failOnInsufficientData) {
            if (($this->_offset + $byteCount) > $this->_size) {
                throw new RangeException("Insufficient data to read $byteCount bytes");
            }
        } else {
            $remainingBytes = $this->_size - $this->_offset;
            if ($byteCount > $remainingBytes) {
                $byteCount = $remainingBytes;
            }
        }
        
        $bytes = substr($this->_string, $this->_offset, $byteCount);
        
        $this->_offset += $byteCount;
        return $bytes;
    }

    /**
     * Like {@link readBytes()} but does not advance the current read position.
     * If there is insufficient data to fulfill the request, returns as much as
     * possible.
     *
     * @param integer $byteCount Number of bytes to read.
     * @return string
     */
    public function peekBytes($byteCount)
    {
        if ($byteCount < 1) {
            return '';
        }
        
        return substr($this->_string, $this->_offset, $byteCount);
    }

    /**
     * Returns a string containing the raw bytes from the data source starting
     * at the current read position and continuing until the end of the data is
     * source is reached, the specified maximum number bytes is exceeded, or
     * an end-of-line sequence (which is *not* included in the return value) is
     * encountered. Returns false if there is no more data to read.
     *
     * The end-of-line sequence is usually a single LINE FEED character "\n",
     * which is the standard end-of-line on Linux, Unix, and Mac OS X platforms.
     * If you are reading data wich may use Mac OS 9 (a single CARRIAGE RETURN
     * character "\r") or Windows (a CARRIAGE RETURN-LINE FEED sequence "\r\n")
     * line endings, you may request that those endings be used as well. This
     * behavior is disabled by default as it incurs a slight performance penalty.
     *
     * Advances the read position by the number of bytes read, including the
     * end-of-line sequence.
     *
     * @param integer $maxBytes   (optional) Maximum bytes to return. Must be
     *   non-zero. If omitted, uses 8192 (8k).
     * @param boolean $checkForCR (optional) Also check for CR or CRLF end-of-line.
     *   If omitted, uses false.
     * @return string Returns false if there is no more data to read.
     * @throws RangeException if the maximum bytes is less than 1.
     */
    public function readLine($maxBytes = 8192, $checkForCR = false)
    {
        if ($maxBytes < 1) {
            throw new RangeException('Maximum bytes must be greater than zero');
        }
        if ($this->_offset > $this->_size) {
            return false;
        }

        $startOffset = $this->_offset;
        $maxOffset = $startOffset + $maxBytes;
        if ($maxOffset > $this->_size) {
            $maxOffset = $this->_size;
        }

        $length = 0;

        if ($checkForCR) {
            while ($this->_offset < $maxOffset) {

                $char = $this->_string[$this->_offset++];

                if ($char === "\n") {
                    break;
                } else if ($char === "\r") {
                    if ($this->_offset < $this->_size) {
                        if ($this->_string[$this->_offset++] !== "\n") {  // check for CRLF
                            $this->_offset--;
                        }
                    }
                    break;
                }

                $length++;

                if ($this->_offset == $maxOffset) {
                    /* At maximum returned length. Check for an end-of-line immediately
                     * after our final character.
                     */
                    if ($this->_offset < $this->_size) {
                        $char = $this->_string[$this->_offset++];
                        if ($char === "\n") {
                            // break
                        } else if ($char !== "\r") {
                            $this->_offset--;
                        } else {
                            if ($this->_offset < $this->_size) {
                                if ($this->_string[$this->_offset++] !== "\n") {
                                    $this->_offset--;
                                }
                            }
                        }
                    }
                }

            }

        } else {
            while ($this->_offset < $maxOffset) {

                if ($this->_string[$this->_offset++] === "\n") {
                    break;
                }

                $length++;

                if ($this->_offset == $maxOffset) {
                    /* At maximum returned length. Check for an end-of-line immediately
                     * after our final character.
                     */
                    if ($this->_offset < $this->_size) {
                        if ($this->_string[$this->_offset++] !== "\n") {
                            $this->_offset--;
                        }
                    }
                }

            }

        }

        return substr($this->_string, $startOffset, $length);
    }

    /**
     * Returns the entire binary string.
     *
     * Preserves the current read position.
     * 
     * @return string
     */
    public function readAllBytes()
    {
        return $this->_string;
    }

}
