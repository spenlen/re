<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    FileParser
 * @subpackage DataSource
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * The RE_FileParser_DataSource_Interface defines a set of methods which allow
 * safe and efficient random access to in-memory, filesystem, and other sources
 * of binary data.
 * 
 * Concrete implementations take care of error handling and other mundane tasks.
 *
 * @package    FileParser
 * @subpackage DataSource
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
interface RE_FileParser_DataSource_Interface
{

    /**
     * Returns the size in bytes of the data source.
     *
     * @return integer
     */
    public function getSize();

    /**
     * Returns the byte offset of the current read position within the data
     * source.
     *
     * @return integer
     */
    public function getOffset();

    /**
     * Moves the current read position to the specified byte offset.
     *
     * @param integer $offset Destination byte offset.
     * @throws RangeException if the byte offset is moved before the beginning
     *   or beyond the end of the data source.
     */
    public function moveToOffset($offset);

    /**
     * Shifts the current read position within the data source by the specified
     * number of bytes. You may move forward (positive numbers) or backward
     * (negative numbers).
     *
     * @param integer $byteCount Number of bytes to skip.
     * @throws RangeException if the new byte offset is before the beginning or
     *   beyond the end of the data source.
     */
    public function skipBytes($byteCount);

    /**
     * Returns the specified number of raw bytes from the data source starting
     * at the current read position.
     *
     * Advances the read position by the number of bytes read.
     *
     * @param integer $byteCount              Number of bytes to read.
     * @param boolean $failOnInsufficientData (optional) Default is true.
     * @return string
     * @throws RangeException if $failOnInsufficientData is true and there is
     *   insufficient data to completely fulfill the request.
     * @throws RE_FileParser_Exception if an error was encountered while reading
     *   the data.
     */
    public function readBytes($byteCount, $failOnInsufficientData = true);

    /**
     * Like {@link readBytes()} but does not advance the data source's current
     * read position. If there is insufficient data to fulfill the request,
     * returns as much as available.
     *
     * @param integer $byteCount Number of bytes to read.
     * @return string
     * @throws RE_FileParser_Exception if an error was encountered while reading
     *   the data.
     */
    public function peekBytes($byteCount);

    /**
     * Returns a string containing the raw bytes from the data source starting
     * at the current read position and continuing until the end of the data is
     * source is reached, the specified maximum number bytes is exceeded, or
     * an end-of-line sequence (which is *not* included in the return value) is
     * encountered. Returns false if there is no more data to read.
     * 
     * The end-of-line sequence is usually a single LINE FEED character "\n",
     * which is the standard end-of-line on Linux, Unix, and Mac OS X platforms.
     * If you are reading data wich may use Mac OS 9 (a single CARRIAGE RETURN
     * character "\r") or Windows (a CARRIAGE RETURN-LINE FEED sequence "\r\n")
     * line endings, you may request that those endings be used as well. This
     * behavior is disabled by default as it incurs a slight performance penalty.
     *
     * Advances the read position by the number of bytes read, including the
     * end-of-line sequence.
     *
     * @param integer $maxBytes   (optional) Maximum bytes to return. Must be
     *   non-zero. If omitted, uses 8192 (8k).
     * @param boolean $checkForCR (optional) Also check for CR or CRLF end-of-line.
     *   If omitted, uses false.
     * @return string Returns false if there is no more data to read.
     * @throws RangeException if the maximum bytes is less than 1.
     */
    public function readLine($maxBytes = 8192, $checkForCR = false);

    /**
     * Returns the entire contents of the data source as a binary string.
     *
     * This method preserves the byte offset of the current read position, and
     * so may safely be called at any time.
     *
     * @return string
     */
    public function readAllBytes();

}
