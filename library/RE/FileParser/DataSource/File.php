<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    FileParser
 * @subpackage DataSource
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */

/** RE_FileParser_DataSource_Interface */
require_once 'RE/FileParser/DataSource/Interface.php';

/** RE_FileParser_Exception */
require_once 'RE/FileParser/Exception.php';


/**
 * File parser data source that provides an interface to filesystem objects.
 *
 * Note that this class cannot be used for other sources that may be supported
 * by {@link fopen()} through URL wrappers. It can only be used for local
 * filesystem objects.
 *
 * @package    FileParser
 * @subpackage DataSource
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_FileParser_DataSource_File implements RE_FileParser_DataSource_Interface
{
  /**** Instance Variables ****/

    /**
     * Fully-qualified filesystem path to the file.
     * @var string
     */
    protected $_filePath = '';

    /**
     * File resource handle.
     * @var resource
     */
    protected $_fileResource = null;

    /**
     * Total byte size of the file.
     * @var integer
     */
    protected $_size = 0;

    /**
     * Byte offset of the current read position within the file.
     * @var integer
     */
    protected $_offset = 0;



  /**** Public Interface ****/


  /* Object Lifecycle */

    /**
     * Validates the path to the file, ensures that it is readable, then opens
     * it for reading.
     *
     * @param string $filePath Fully-qualified filesystem path to the file.
     * @throws RE_FileParser_Exception if the file is missing, cannot be opened,
     *   or is empty.
     */
    public function __construct($filePath)
    {
        if (! (is_file($filePath) || is_link($filePath))) {
            throw new RE_FileParser_Exception(
                "Invalid file path: $filePath", RE_FileParser_Exception::BAD_FILE_PATH);
        }
        if (! is_readable($filePath)) {
            throw new RE_FileParser_Exception(
                "File is not readable: $filePath", RE_FileParser_Exception::NOT_READABLE);
        }
        if (($this->_size = filesize($filePath)) === false) {
            throw new RE_FileParser_Exception(
                "Error while obtaining file size: $filePath", RE_FileParser_Exception::CANT_GET_FILE_SIZE);
        }
        if ($this->_size < 1) {
            throw new RE_FileParser_Exception(
                "File is empty: $filePath", RE_FileParser_Exception::FILE_IS_EMPTY);
        }
        if (($this->_fileResource = fopen($filePath, 'rb')) === false) {
            throw new RE_FileParser_Exception(
                "Cannot open file for reading: $filePath", RE_FileParser_Exception::CANT_OPEN_FILE);
        }
        $this->_filePath = $filePath;
    }

    /**
     * Closes the file if it had been successfully opened.
     */
    public function __destruct()
    {
        if (is_resource($this->_fileResource)) {
            fclose($this->_fileResource);
            $this->_fileResource = null;
        }
    }


 /* Object Magic Methods */
 
    /**
     * Returns a string containing the full filesystem path of the source file.
     *
     * @return string
     */
    public function __toString()
    {
        return 'File (' . $this->_filePath . ')';
    }


  /* RE_FileParser_DataSource_Interface Implementation */

    /**
     * Returns the size in bytes of the file.
     *
     * @return integer
     */
    public function getSize()
    {
        return $this->_size;
    }

    /**
     * Returns the byte offset of the current read position within the file.
     *
     * @return integer
     */
    public function getOffset()
    {
        return $this->_offset;
    }

    /**
     * Seeks the file read position to the specified byte offset.
     *
     * @param integer $offset Destination byte offset.
     * @throws RangeException if the byte offset is before the beginning or
     *   beyond the end of the data source.
     * @throws RE_FileParser_Exception if the file read position cannot be moved.
     */
    public function moveToOffset($offset)
    {
        if ($this->_offset == $offset) {
            return;    // Not moving; do nothing.
        }

        if ($offset < 0) {
            throw new RangeException('Attempt to move before start of data source');
        }
        if ($offset > $this->_size) {    // Offsets are zero-based.
            throw new RangeException('Attempt to move beyond end of data source');
        }
        $this->_offset = $offset;

        $result = fseek($this->_fileResource, $offset, SEEK_SET);
        if ($result !== 0) {
            throw new RE_FileParser_Exception(
                'Error while setting new file position', RE_FileParser_Exception::CANT_SET_FILE_POSITION);
        }
        if (feof($this->_fileResource)) {
            throw new RangeException('Moved beyond the end of the file');
        }
    }

    /**
     * Seeks the current read position within the file by the specified number
     * of bytes. You may move forward (positive numbers) or backward (negative
     * numbers).
     *
     * @param integer $byteCount Number of bytes to skip.
     * @throws RangeException if the new byte offset is before the beginning or
     *   beyond the end of the data source.
     */
    public function skipBytes($byteCount)
    {
        if ($byteCount == 0) {
            return;    // Not moving; do nothing.
        }
        $offset = $this->_offset + $byteCount;

        if ($offset < 0) {
            throw new RangeException('Attempt to move before start of data source');
        }
        if ($offset > $this->_size) {    // Offsets are zero-based.
            throw new RangeException('Attempt to move beyond end of data source');
        }
        $this->_offset = $offset;

        $result = fseek($this->_fileResource, $offset, SEEK_SET);
        if ($result !== 0) {
            throw new RE_FileParser_Exception(
                'Error while setting new file position', RE_FileParser_Exception::CANT_SET_FILE_POSITION);
        }
        if (feof($this->_fileResource)) {
            throw new RangeException('Moved beyond the end of the file');
        }
    }

    /**
     * Returns the specified number of raw bytes from the file starting at
     * the current read position.
     *
     * Advances the read position by the number of bytes read.
     *
     * @param integer $byteCount              Number of bytes to read.
     * @param boolean $failOnInsufficientData (optional) Default is true.
     * @return string
     * @throws RangeException if $failOnInsufficientData is true and there is
     *   insufficient data to completely fulfill the request.
     * @throws RE_FileParser_Exception if an error was encountered while reading
     *   the file.
     */
    public function readBytes($byteCount, $failOnInsufficientData = true)
    {
        if ($byteCount < 1) {
            return '';
        }
        
        if ($failOnInsufficientData) {
            if (($this->_offset + $byteCount) > $this->_size) {
                throw new RangeException("Insufficient data to read $byteCount bytes");
            }
        } else {
            $remainingBytes = $this->_size - $this->_offset;
            if ($byteCount > $remainingBytes) {
                $byteCount = $remainingBytes;
            }
        }
        
        $bytes = fread($this->_fileResource, $byteCount);
        if ($bytes === false) {
            throw new RE_FileParser_Exception(
                'Unexpected error while reading file', RE_FileParser_Exception::ERROR_DURING_READ);
        }
        if (strlen($bytes) != $byteCount) {
            throw new RE_FileParser_Exception(
                sprintf('Only read %d of expected %d bytes', strlen($bytes), $byteCount),
                RE_FileParser_Exception::ERROR_DURING_READ);
        }
        
        $this->_offset += $byteCount;
        return $bytes;
    }

    /**
     * Like {@link readBytes()} but does not advance the current read position.
     * If there is insufficient data to fulfill the request, returns as much as
     * possible.
     *
     * @param integer $byteCount Number of bytes to read.
     * @return string
     * @throws RE_FileParser_Exception if an error was encountered while reading
     *   the file.
     */
    public function peekBytes($byteCount)
    {
        if ($byteCount < 1) {
            return '';
        }
        
        $remainingBytes = $this->_size - $this->_offset;
        if ($byteCount > $remainingBytes) {
            $byteCount = $remainingBytes;
        }
        
        $bytes = fread($this->_fileResource, $byteCount);
        if ($bytes === false) {
            throw new RE_FileParser_Exception(
                'Unexpected error while reading file', RE_FileParser_Exception::ERROR_DURING_READ);
        }
        if (strlen($bytes) != $byteCount) {
            throw new RE_FileParser_Exception(
                sprintf('Only read %d of expected %d bytes', strlen($bytes), $byteCount),
                RE_FileParser_Exception::ERROR_DURING_READ);
        }
        
        $result = fseek($this->_fileResource, $this->_offset, SEEK_SET);
        if ($result !== 0) {
            throw new RE_FileParser_Exception(
                'Error while setting new file position', RE_FileParser_Exception::CANT_SET_FILE_POSITION);
        }

        return $bytes;
    }

    /**
     * Returns a string containing the raw bytes from the data source starting
     * at the current read position and continuing until the end of the data is
     * source is reached, the specified maximum number bytes is exceeded, or
     * an end-of-line sequence (which is *not* included in the return value) is
     * encountered. Returns false if there is no more data to read.
     *
     * The end-of-line sequence is usually a single LINE FEED character "\n",
     * which is the standard end-of-line on Linux, Unix, and Mac OS X platforms.
     * If you are reading data wich may use Mac OS 9 (a single CARRIAGE RETURN
     * character "\r") or Windows (a CARRIAGE RETURN-LINE FEED sequence "\r\n")
     * line endings, you may request that those endings be used as well. This
     * behavior is disabled by default as it incurs a slight performance penalty.
     *
     * Advances the read position by the number of bytes read, including the
     * end-of-line sequence.
     *
     * @param integer $maxBytes   (optional) Maximum bytes to return. Must be
     *   non-zero. If omitted, uses 8192 (8k).
     * @param boolean $checkForCR (optional) Also check for CR or CRLF end-of-line.
     *   If omitted, uses false.
     * @return string Returns false if there is no more data to read.
     * @throws RangeException if the maximum bytes is less than 1.
     * @throws RE_FileParser_Exception if an error was encountered while reading
     *   the file.
     */
    public function readLine($maxBytes = 8192, $checkForCR = false)
    {
        if ($maxBytes < 1) {
            throw new RangeException('Maximum bytes must be greater than zero');
        }
        if (($this->_offset > $this->_size) || (feof($this->_fileResource))) {
            return false;
        }

        $maxOffset = $this->_offset + $maxBytes;
        if ($maxOffset > $this->_size) {
            $maxOffset = $this->_size;
        }

        $bytes = '';

        if ($checkForCR) {
            while ($this->_offset < $maxOffset) {

                if (($char = fgetc($this->_fileResource)) === false) {
                    break;
                }
                $this->_offset++;

                if ($char === "\n") {
                    break;
                } else if ($char === "\r") {
                    if (($char = fgetc($this->_fileResource)) === false) {
                        break;
                    }
                    $this->_offset++;
                    if ($char !== "\n") {  // check for CRLF
                        $this->_offset--;
                        fseek($this->_fileResource, -1, SEEK_CUR);
                    }
                    break;
                }

                $bytes .= $char;

                if ($this->_offset == $maxOffset) {
                    /* At maximum returned length. Check for an end-of-line immediately
                     * after our final character.
                     */
                    if (($char = fgetc($this->_fileResource)) !== false) {
                        $this->_offset++;
                        if ($char === "\n") {
                            // break
                        } else if ($char !== "\r") {
                            $this->_offset--;
                            fseek($this->_fileResource, -1, SEEK_CUR);
                        } else {
                            if (($char = fgetc($this->_fileResource)) !== false) {
                                $this->_offset++;
                                if ($char !== "\n") {
                                    $this->_offset--;
                                    fseek($this->_fileResource, -1, SEEK_CUR);
                                }
                            }
                        }
                    }
                }

            }

        } else {
            while ($this->_offset < $maxOffset) {

                if (($char = fgetc($this->_fileResource)) === false) {
                    break;
                }
                $this->_offset++;

                if ($char === "\n") {
                    break;
                }

                $bytes .= $char;

                if ($this->_offset == $maxOffset) {
                    /* At maximum returned length. Check for an end-of-line immediately
                     * after our final character.
                     */
                    if (($char = fgetc($this->_fileResource)) !== false) {
                        $this->_offset++;
                        if ($char !== "\n") {
                            $this->_offset--;
                            fseek($this->_fileResource, -1, SEEK_CUR);
                        }
                    }
                }

            }

        }

        return $bytes;
    }

    /**
     * Returns the entire contents of the file as a string.
     *
     * Preserves the current file read position.
     *
     * @return string
     */
    public function readAllBytes()
    {
        return file_get_contents($this->_filePath);
    }


  /* Accessor Methods */

    /**
     * Returns the full filesystem path of the source file.
     *
     * @return string
     */
    public function getFilePath()
    {
        return $this->_filePath;
    }

}
