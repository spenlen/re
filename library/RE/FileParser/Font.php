<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    FileParser
 * @subpackage Fonts
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */

/** RE_FileParser */
require_once 'RE/FileParser.php';

/** RE_Font_Cmap */
require_once 'RE/Font/Cmap.php';


/**
 * Abstract base class for font file parsers.
 *
 * Defines the public interface for concrete subclasses which are responsible
 * for parsing the raw binary font data. Also provides shared utility methods.
 *
 * @package    FileParser
 * @subpackage Fonts
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
abstract class RE_FileParser_Font extends RE_FileParser
{
  /**** Instance Variables ****/

    /**
     * Array of parsed font properties. Used with {@link __get()} and
     * {@link __set()}.
     * @var array
     */
    protected $_fontProperties = array();



  /**** Public Interface ****/


  /* Accessor Methods */

    /**
     * Get handler
     *
     * @param string $property
     * @return mixed
     */
    public function __get($property)
    {
        if (isset($this->_fontProperties[$property])) {
            return $this->_fontProperties[$property];
        } else {
            return null;
        }
    }

    /**
     * Set handler
     *
     * NOTE: This method is marked as public, but only this class and its
     * subclasses should set font properties.
     *
     * @param string $property
     * @param  mixed $value
     */
    public function __set($property, $value)
    {
        if (is_null($value)) {
            unset($this->_fontProperties[$property]);
        } else {
            $this->_fontProperties[$property] = $value;
        }
    }


  /* Utility Methods */

    /**
     * Writes the entire font properties array to STDOUT. Used only for debugging.
     */
    public function writeDebug()
    {
        print_r($this->_fontProperties);
    }

}
