<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    FileParser
 * @subpackage Fonts
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */

/** RE_FileParser_Font_OpenType */
require_once 'RE/FileParser/Font/OpenType.php';


/**
 * Parses an OpenType font file containing Compact Font Format (CFF) outlines.
 *
 * @package    FileParser
 * @subpackage Fonts
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_FileParser_Font_OpenType_CFF extends RE_FileParser_Font_OpenType
{
  /**** Public Interface ****/


  /* Concrete Class Implementation */

    /**
     * Verifies that the font file actually uses CFF.
     *
     * @throws RE_FileParser_Exception
     */
    public function screen()
    {
        if ($this->_isScreened) {
            return;
        }

        parent::screen();

        switch ($this->_readScalerType()) {
            case 0x4f54544f:    // 'OTTO' - the CFF signature
                break;

            default:
                throw new RE_FileParser_Exception(
                    'Not an OpenType CFF font file', RE_FileParser_Exception::NOT_A_FONT_FILE);
        }

        $this->_isScreened = true;
    }

    /**
     * Reads and parses the OpenType CFF font data from the file on disk.
     *
     * @throws RE_FileParser_Exception
     */
    public function parse()
    {
        if ($this->_isParsed) {
            return;
        }

        parent::parse();

        /* There is nothing additional to parse for OpenType CFF fonts at this time.
         */

        $this->_isParsed = true;
    }
}
