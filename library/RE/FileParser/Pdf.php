<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    FileParser
 * @subpackage PDF
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */

/** RE_FileParser */
require_once 'RE/FileParser.php';

/** RE_Pdf */
require_once 'RE/Pdf.php';


/**
 * Parses a PDF document, providing fast, efficient access to its contents.
 *
 * For disk-based PDFs, the entire document is never read into memory at once,
 * allowing for manipulation of virtually any sized document.
 *
 * @todo Add codes to exceptions.
 * 
 * @package    FileParser
 * @subpackage PDF
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_FileParser_Pdf extends RE_FileParser implements SplObserver, RE_Pdf_Object_Extraction_Interface
{
  /**** Instance Variables ****/

    /**
     * Major version number of the PDF document.
     * @var string
     */
    protected $_pdfMajorVersion = 0;

    /**
     * Minor version number of the PDF document.
     * @var integer
     */
    protected $_pdfMinorVersion = 0;

    /**
     * Major version number of the PDF document appearing in the file header.
     * @var string
     */
    protected $_headerMajorVersion = 0;

    /**
     * Minor version number of the PDF document appearing in the file header.
     * @var integer
     */
    protected $_headerMinorVersion = 0;

    /**
     * The document catalog object.
     * @var RE_Pdf_Document_Catalog
     */
    protected $_catalog = null;
    
    /**
     * The document info object.
     * @var RE_Pdf_Document_Info
     */
    protected $_info = null;
    
    /**
     * The file trailer object.
     * @var RE_Pdf_Document_Trailer
     */
    protected $_trailer = null;
    
    /**
     * Permanent portion of the document's file identifier.
     * @var string
     */
    protected $_fileID = null;
    
    /**
     * Array of cross reference table locations for the document.
     * @var array
     */
    protected $_xrefTables = array();



  /**** Public Interface ****/


  /* Concrete Subclass Implementation */

    /**
     * Verifies that the document is a valid PDF.
     *
     * @throws RE_FileParser_Exception
     */
    public function screen()
    {
        if ($this->_isScreened) {
            return;
        }

        $this->_readFileTrailer();

        /* The header is read after the file trailer so we can obtain the PDF
         * version number from the document catalog if necessary.
         */
        $this->_readFileHeader();

        $this->_isScreened = true;
    }

    /**
     * Reads and parses the additional structural data which is skipped
     * by {@link screen()} that is required for fast access to the document's
     * contents.
     * 
     * Does not read the entire file; specific objects are retrieved on-demand
     * by the various get() methods.
     *
     * @throws RE_FileParser_Exception
     */
    public function parse()
    {
        if ($this->_isParsed) {
            return;
        }

        /* Screen the document first, if it hasn't been done yet.
         */
        $this->screen();

        if (isset($this->_trailer['Encrypt'])) {
            throw new RE_FileParser_Exception('Encrypted PDFs are currently unsupported.');
        }

        $this->_isParsed = true;
    }


  /* Object Lifecycle */

    /**
     * Creates a new RE_FileParser_Pdf object using the specified data
     * source object.
     *
     * @param RE_FileParser_DataSource_Interface $dataSource
     * @throws RE_FileParser_Exception
     */
    public function __construct(RE_FileParser_DataSource_Interface $dataSource)
    {
        parent::__construct($dataSource);
        $this->_owningDocument = $this;
    }


  /* SplObserver Interface Methods */

    /**
     * Received by {@link RE_Pdf_Object_Indirect} objects when their direct
     * object's value changes. Since file parsers are read-only machines, the
     * notification is ignored.
     *
     * @param SplSubject $subject
     */
    public function update(SplSubject $subject)
    {}


  /* RE_Pdf_Object_Extraction_Interface Methods */
  
    /**
     * Extracts the specified object from the PDF document. If the specified
     * object does not exist, {@link RE_Pdf_Object_Null} is returned (see
     * PDF 1.7 Reference, section 3.2.9).
     *
     * @param integer $objectNumber
     * @param integer $generationNumber
     * @return RE_Pdf_Object
     * @throws RE_FileParser_Exception
     */
    public function extractObject($objectNumber, $generationNumber)
    {
        $offset = $this->_getObjectOffset($objectNumber, $generationNumber);
        if (! $offset) {
            return new RE_Pdf_Object_Null();
        }
        $this->moveToOffset($offset);
        
        /* Some badly-formed PDF documents have extra whitespace before the
         * object number. Skip over it.
         */
        $this->skipWhiteSpace();
        
        $expectedString = $objectNumber . ' ' . $generationNumber . ' obj';
        $actualString   = $this->readBytes(strlen($expectedString));
        if ($actualString !== $expectedString) {
            throw new RE_FileParser_Exception(
                'Syntax error attempting to extract PDF object at offset '
              . sprintf('0x%x', $offset) . ': expected "' . $expectedString . '"; '
              . 'read "' . $actualString . '"');
        }
        $this->skipWhiteSpace();

        $object = RE_Pdf_Object::extractObject($this);
        $this->skipWhiteSpace();

        if ($this->readBytes(6) !== 'endobj') {
            throw new RE_FileParser_Exception(
                sprintf('Syntax error attempting to extract PDF object at offset 0x%x; '
                      . 'missing endobj keyword at offset 0x%x',
                        $offset, ($this->getOffset() - 6)));
        }
        
        if (! isset($this->_fileID)) {
            $this->_generateFileIdentifier();
        }
        $object->recordObjectNumber($this->_fileID, $objectNumber);

        return $object;
    }

  
  /* Accessor Methods */

    /**
     * Returns major version number of the PDF document.
     *
     * @return integer
     */
    public function getMajorVersion()
    {
        return $this->_pdfMajorVersion;
    }

    /**
     * Returns minor version number of the PDF document.
     *
     * @return integer
     */
    public function getMinorVersion()
    {
        return $this->_pdfMinorVersion;
    }

    /**
     * Returns major version number of the PDF document appearing in the file
     * header. This number may be smaller than that for the document if a
     * newer version number appears in the document catalog object.
     *
     * @return integer
     */
    public function getHeaderMajorVersion()
    {
        return $this->_headerMajorVersion;
    }

    /**
     * Returns minor version number of the PDF document appearing in the file
     * header. This number may be smaller than that for the document if a
     * newer version number appears in the document catalog object.
     *
     * @return integer
     */
    public function getHeaderMinorVersion()
    {
        return $this->_headerMinorVersion;
    }

    /**
     * Sets the owning document object (usually a {@link RE_Pdf_Document})
     * that receives notifications when the value on an extracted indirect
     * object changes.
     * 
     * @param RE_Pdf_Object_Extraction_Interface $owningDocument
     */
    public function setOwningDocument(RE_Pdf_Object_Extraction_Interface $owningDocument)
    {
        $this->_owningDocument = $owningDocument;
    }
    
    /**
     * Returns the owning document is set in {@link setOwningDocument} or
     * this parser object. Used to set the proper source PDF document value
     * in {@link RE_Pdf_Object_Indirect::extract()} and similar methods.
     * 
     * @return RE_Pdf_Object_Extraction_Interface
     */
    public function getOwningDocument()
    {
        return $this->_owningDocument;
    }
    
    /**
     * Returns the specified file identifier for the PDF document as a binary
     * string.
     * 
     * The file identifier is split into two parts: the first part is a
     * permanent identifier which never changes; the second part changes each
     * time a new revision of the document is saved.
     * 
     * @param string $part (optional) 'permanent' (default) or 'revision'
     * @return string
     */
    public function getFileID($part = 'permanent')
    {
        /* If the trailer dictionary does not contain a file identifier,
         * create one now.
         */
        if (! isset($this->_trailer['ID'])) {
            $this->_generateFileIdentifier();
        }
        if ($part == 'revision') {
            return $this->_trailer['ID'][1]->getValue();
        } else {  // 'permanent'
            return $this->_trailer['ID'][0]->getValue();
        }
    }
    
    /**
     * Returns the PDF document's file trailer object.
     *
     * @return RE_Pdf_Document_Trailer
     */
    public function getTrailer()
    {
        return $this->_trailer;
    }

    /**
     * Returns the PDF document's catalog object.
     *
     * @return RE_Pdf_Document_Catalog
     */
    public function getCatalog()
    {
        if (is_null($this->_catalog)) {
            $this->_catalog = $this->_trailer['Root']->getObject();
        }
        return $this->_catalog;
    }
    
    /**
     * Returns the PDF document's info object.
     * 
     * @return RE_Pdf_Document_Info
     */
    public function getInfo()
    {
        if (is_null($this->_info)) {
            if (empty($this->_trailer['Info'])) {
                $this->_info = new RE_Pdf_Document_Info();
            } else {
                /* The document info dictionary does not contain a Type key.
                 * Transform the generic dictionary into the proper object type.
                 */
                $dictionary = $this->_trailer['Info']->getObject();
                $this->_info = new RE_Pdf_Document_Info($dictionary->getValue());
            }
        }
        return $this->_info;
    }


  /* Parser Methods */

    /**
     * Moves the parser's data source object's read position forward past
     * any whitespace characters.
     */
    public function skipWhiteSpace()
    {
        $length = 1;
        while ($length > 0) {
            /* White space runs are generally very short. Work in small chunks.
             */
            $string = $this->readBytes(8, false);
            $length = strspn($string, "\x00\t\n\x0c\r ");
            $this->skipBytes($length - strlen($string));
        }
        /* Skip over comments.
         */
        if ($this->peekBytes(1) === '%') {
            $comment = $this->readLine(0x10000, true);
        }
    }



  /**** Internal Methods ****/


  /* Parser Methods */

    /**
     * Reads the PDF version of the document from the file header and document
     * catalog object.
     *
     * Maintains compatibility with Acrobat viewers by mimicking the behaviors
     * described the following implemenation notes from the PDF 1.7 Reference
     * (Appendix H):
     *   18. Check for %%EOF marker in last 1024 bytes of file
     *
     * @throws RE_FileParser_Exception
     */
    protected function _readFileTrailer()
    {
        $this->_debugLog('Reading file trailer');

        $size = $this->getSize();
        if ($size < 1024) {
            $this->moveToOffset(0);
            $trailerBytes = $this->readBytes($size);
        } else {
            $this->moveToOffset($size - 1024);
            $trailerBytes = $this->readBytes(1024);
            $size = 1024;
        }

        $eofPos = strrpos($trailerBytes, '%%EOF');
        if ($eofPos === false) {
            throw new RE_FileParser_Exception('Missing PDF end-of-file marker.');
        }

        $startXrefPos = strrpos($trailerBytes, 'startxref', ($eofPos - $size));
        if ($startXrefPos === false) {
            throw new RE_FileParser_Exception('Missing cross reference table offset.');
        }

        sscanf(substr($trailerBytes, $startXrefPos, ($eofPos - $startXrefPos)), 'startxref %d', $xrefOffset);
        if (! isset($xrefOffset)) {
            throw new RE_FileParser_Exception('Unable to obtain cross reference table offset.');
        }
        unset($trailerBytes);
        
        $lastXref = $xrefOffset;
        
        /* Read each cross-reference section in the document.
         */
        while ($xrefOffset) {
            $this->_readXrefTable($xrefOffset);       
            $xrefOffset = $this->_readTrailerDictionary();
        }
        $this->_trailer['Prev'] = $lastXref;

        if (! isset($this->_trailer['Size'])) {
            throw new RE_FileParser_Exception('File trailer dictionary missing Size entry.');
        }
        if (! isset($this->_trailer['Root'])) {
            throw new RE_FileParser_Exception('File trailer dictionary missing Root entry.');
        }
    }
    
    /**
     * Reads and validates the cross reference table located at the specified
     * byte offset.
     * 
     * @param integer $xrefOffset
     */
    protected function _readXrefTable($xrefOffset)
    {
        $this->_debugLog('Reading xref table at offset 0x%x', $xrefOffset);
        $this->moveToOffset($xrefOffset);

        $line = $this->readLine(4, true);
        if ($line === 'xref') {    // cross-reference table
            while (($line = $this->readLine(32, true)) !== 'trailer') {
                
                sscanf($line, '%d %d', $startObject, $objectCount);
                if (! isset($objectCount)) {
                    throw new RE_FileParser_Exception(sprintf(
                        'Cannot read range for cross reference subsection at offset 0x%X', $this->getOffset()));
                }
                $this->_debugLog('Subsection: start object: %d; number of entries: %d', $startObject, $objectCount);

                $this->_xrefTables[$this->getOffset()] = array('table', $startObject, ($startObject + $objectCount - 1));

                /* Each entry in the cross-reference subsection is exactly 20 bytes
                 * in length. Jump over the subsection and look for the next one.
                 */
                $this->skipBytes($objectCount * 20);
            }
            
        } else {    // cross-reference stream
            $this->skipBytes(-4);
            /** @todo Implement cross-reference streams. */
            throw new RE_FileParser_Exception('Cross-reference streams are currently unsupported');
        }
    }
    
    /**
     * Reads the file trailer dictionary located at the current parser offset.
     * If the trailer contains a reference to another cross reference table,
     * returns the byte offset for that table.
     * 
     * @return integer Returns null if there is no previous xref table.
     */
    protected function _readTrailerDictionary()
    {
        if (is_null($this->_trailer)) {
            $this->_trailer = new RE_Pdf_Document_Trailer();
        }

        $trailer = RE_Pdf_Document_Trailer::extract($this);
        
        /* Create the latest generation of the file trailer dictionary. Newer
         * entries overwrite older entries.
         */
        foreach ($trailer as $key => $object) {
            if (! isset($this->_trailer[$key])) {
                $this->_trailer[$key] = $object;
            }
        }
        
        if (isset($trailer['Prev'])) {
            return $trailer['Prev']->getValue();
        }
        return null;
    }

    /**
     * Reads the PDF version of the document from the file header and document
     * catalog object.
     *
     * Maintains compatibility with Acrobat viewers by mimicking the behaviors
     * described the following implemenation notes from the PDF 1.7 Reference
     * (Appendix H):
     *   13. Check for header in first 1024 bytes of file
     *   14. Allow for alternate header syntax: %!PS-Adobe-N.n PDF-M.m
     *
     * Must be read after the file trailer ({@link _readFileTrailer()}) so that
     * the document catalog object can be located.
     * 
     * @throws RE_FileParser_Exception if file header cannot be found, is invalid,
     *   or if the PDF version is unsupported.
     */
    protected function _readFileHeader()
    {
        $this->_debugLog('Reading file header');
        $this->moveToOffset(0);

        $size = $this->getSize();
        if ($size < 1024) {
            $headerBytes = $this->readBytes($size);
        } else {
            $headerBytes = $this->readBytes(1024);
        }

        if (preg_match('/^%PDF-(\d+)\.(\d+)/m', $headerBytes, $matches)) {
            $this->_headerMajorVersion = (int)$matches[1];
            $this->_headerMinorVersion = (int)$matches[2];
        } else if (preg_match('/^%!PS-Adobe-\d+\.\d+ PDF-(\d+)\.(\d+)/m', $headerBytes, $matches)) {
            $this->_headerMajorVersion = (int)$matches[1];
            $this->_headerMinorVersion = (int)$matches[2];
        } else {
            throw new RE_FileParser_Exception('Missing or invalid PDF file header.');
        }
        $this->_debugLog('Header PDF version: %d.%d', $this->_headerMajorVersion, $this->_headerMinorVersion);
        
        $catalog = $this->_trailer['Root']->getObject();
        if (empty($catalog['Version'])) {
            $this->_pdfMajorVersion = $this->_headerMajorVersion;
            $this->_pdfMinorVersion = $this->_headerMinorVersion;
            
        } else {
            $versionString = $catalog['Version']->getValue();
            $this->_debugLog('Document catalog PDF version: %s', $versionString);

            if (preg_match('/^(\d+)\.(\d+)$/', $versionString, $matches)) {
                $this->_pdfMajorVersion = (int)$matches[1];
                $this->_pdfMinorVersion = (int)$matches[2];
            } else {
                throw new RE_FileParser_Exception("Invalid PDF version in document catalog object: $versionString");
            }

        }

        if ($this->_pdfMajorVersion > 1) {
            throw new RE_FileParser_Exception('Unsupported PDF version '
                . $this->_pdfMajorVersion . '.' . $this->_pdfMinorVersion . '; must be version 1.x');
        }
    }


  /* Object Information and Extraction */
  
    /**
     * Returns the byte offset from the beginning of the PDF document of the
     * specified object.
     * 
     * @param integer $objectNumber
     * @param integer $generationNumber
     * @return integer Returns null if the object does not exist.
     */
    public function _getObjectOffset($objectNumber, $generationNumber)
    {
        foreach ($this->_xrefTables as $baseOffset => $table) {
            if (($table[1] > $objectNumber) || ($table[2] < $objectNumber)) {
                continue;
            }

            if ($table[0] === 'table') {
                $this->moveToOffset($baseOffset + (($objectNumber - $table[1]) * 20));
                $xref = $this->readBytes(20);

                if (preg_match('/^(\d{10}) (\d{5}) n/', $xref, $matches)) {
                    if ($matches[2] == $generationNumber) {
                        return $matches[1];
                    }
                }
                                    
            }
        }
        return null;
    }
  
  
  /**** Internal Interface ****/

    /**
     * Generates a new file identifier for the PDF document and adds it to
     * the trailer dictionary.
     * 
     * Called by {@link getFileID()} if the source PDF document did not already
     * contain a file identifier.
     */
    protected function _generateFileIdentifier()
    {
        /* The file identifier is an MD5 digest of the current time, the source
         * document's location, the file size in bytes, and the full contents
         * of the information dictionary as described in PDF Reference 1.7,
         * Section 10.3.
         */
        $fileID = md5(microtime(true), true);
        
        $dataSource = $this->getDataSource();
        if ($dataSource instanceof RE_FileParser_DataSource_File) {
            $fileID = md5($fileID . '|' . $dataSource->getFilePath(), true);
        } else {
            $fileID = md5($fileID . '|' . get_class($dataSource), true);
        }
        
        $fileID = md5($fileID . '|' . $dataSource->getSize(), true);
        
        if (! empty($this->_trailer['Info'])) {
            foreach ($this->_trailer['Info'] as $key => $object) {
                $fileID = md5($fileID . '|' . $key . '|' . $object->getValue(), true);
            }
        }
        
        $fileIDObject = new RE_Pdf_Object_String($fileID);
        $fileIDObject->setWriteAsHex(true);
        $fileIDObject->setUsesEncryption(false);

        /* Since this is the first time a file identifier has been created for
         * this document, both strings are the same.
         */
        $this->_trailer['ID'] = new RE_Pdf_Object_Array(array($fileIDObject, $fileIDObject));

        $this->_fileID = $fileID;
    } 

}
