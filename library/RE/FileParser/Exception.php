<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    FileParser
 * @subpackage Core
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * Exception class for RE_FileParser.
 *
 * If you expect a certain type of exception to be caught and handled by the
 * caller, create a constant for it here and include it in the object being
 * thrown. Example:
 *
 *   throw new RE_FileParser_Exception(
 *       'foo() is not yet implemented', RE_FileParser_Exception::NOT_IMPLEMENTED);
 *
 * This allows the caller to determine the specific type of exception that was
 * thrown without resorting to parsing the descriptive text.
 *
 * IMPORTANT: Do not rely on numeric values of the constants! They are grouped
 * sequentially below for organizational purposes only. The numbers may come to
 * mean something in the future, but they are subject to renumbering at any
 * time. ALWAYS use the symbolic constant names, which are guaranteed never to
 * change, in logical checks! You have been warned.
 *
 * @package    FileParser
 * @subpackage Core
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_FileParser_Exception extends Exception
{
  /**** Class Constants ****/


  /* Generic Exceptions */

    /**
     * The feature or option is planned but has not yet been implemented. It
     * should be available in a future revision of the framework.
     */
    const NOT_IMPLEMENTED = 0x0001;



  /* Filesystem I/O */

    /**
     * The file path was unusable or invalid.
     */
    const BAD_FILE_PATH = 0x0101;

    /**
     * The file is not readable by the current user.
     */
    const NOT_READABLE = 0x0102;

    /**
     * The file is not writeable by the current user.
     */
    const NOT_WRITEABLE = 0x0103;

    /**
     * The file resource has been closed unexpectedly.
     */
    const FILE_NOT_OPEN = 0x0104;

    /**
     * An error was encountered while attempting to open the file.
     */
    const CANT_OPEN_FILE = 0x0105;

    /**
     * An error was encountered while attempting to obtain the current file
     * position.
     */
    const CANT_GET_FILE_POSITION = 0x0106;

    /**
     * An error was encountered while attempting to set a new file position.
     */
    const CANT_SET_FILE_POSITION = 0x0107;

    /**
     * An attempt was made to move the current file position before the start
     * of the file.
     */
    const MOVE_BEFORE_START_OF_FILE = 0x0108;

    /**
     * An attempt was made to move the current file position beyond the end of
     * the file.
     */
    const MOVE_BEYOND_END_OF_FILE = 0x0109;

    /**
     * An error was encountered while attempting to obtain the file size.
     */
    const CANT_GET_FILE_SIZE = 0x010a;

    /**
     * An error was encountered while attempting to read data from the file.
     */
    const ERROR_DURING_READ = 0x010b;

    /**
     * An error was encountered while attempting to write data to the file.
     */
    const ERROR_DURING_WRITE = 0x010c;

    /**
     * An incompatible page size was specified for a buffered read operation.
     */
    const INVALID_PAGE_SIZE = 0x010d;

    /**
     * There is insufficient data to fulfill the read request.
     */
    const INSUFFICIENT_DATA = 0x010e;
    
    /**
     * The file is empty.
     */
    const FILE_IS_EMPTY = 0x010f;



  /* Parsing Phase */

    /**
     * The file parser data source object was invalid or improperly initialized.
     */
    const BAD_DATA_SOURCE = 0x0201;

    /**
     * An unknown byte order was specified.
     */
    const INVALID_BYTE_ORDER = 0x0202;

    /**
     * An invalid integer size was specified.
     */
    const INVALID_INTEGER_SIZE = 0x0203;

    /**
     * An invalid fixed-point number size was specified.
     */
    const BAD_FIXED_POINT_SIZE = 0x0204;

    /**
     * The string cannot be read.
     */
    const CANT_READ_STRING = 0x0205;

    /**
     * This file type must be parsed in a specific order and a parsing method
     * was called out-of-turn.
     */
    const PARSED_OUT_OF_ORDER = 0x0206;
    
    /**
     * The file contains an invalid internal byte offset.
     */
    const BAD_BYTE_OFFSET = 0x0207;

    /**
     * The file contains an invalid internal byte length.
     */
    const BAD_BYTE_LENGTH = 0x0208;



  /* Font Parsers */

    /**
     * The file does not contain a font.
     */
    const NOT_A_FONT_FILE = 0x0301;

    /**
     * The number of tables contained in the font is outside the expected range.
     */
    const BAD_TABLE_COUNT = 0x0302;

    /**
     * A required table was not present in the font.
     */
    const REQUIRED_TABLE_NOT_FOUND = 0x0303;

    /**
     * The parser does not understand this version of this table in the font.
     */
    const DONT_UNDERSTAND_TABLE_VERSION = 0x0303;

    /**
     * The magic number in the font file is incorrect.
     */
    const BAD_MAGIC_NUMBER = 0x0304;

    /**
     * Could not locate a usable character map for this font.
     */
    const CANT_FIND_GOOD_CMAP = 0x0305;



  /* Image Parsers */
  
    /**
     * The file does not contain an image.
     */
    const NOT_AN_IMAGE_FILE = 0x0401;
    
    /**
     * The number of images in the file is outside the expected range.
     */
    const BAD_IMAGE_COUNT = 0x0402;

    /**
     * The number of fields in the image directory is outside the expected range.
     */
    const BAD_FIELD_COUNT = 0x0403;
    
}
