<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    FileParser
 * @subpackage Images
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */

/** RE_FileParser_Image */
require_once 'RE/FileParser/Image.php';


/**
 * Parses a TIFF image file.
 *
 * @package    FileParser
 * @subpackage Images
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_FileParser_Image_Tiff extends RE_FileParser_Image
{
  /**** Class Constants ****/


  /* Field Data Types */
  
    /**
     * 8-bit unsigned integer.
     */
    const TYPE_BYTE = 0x01;
  
    /**
     * NULL-terminated ASCII string.
     */
    const TYPE_ASCII = 0x02;
  
    /**
     * 2-byte unsigned integer.
     */
    const TYPE_SHORT = 0x03;
  
    /**
     * 4-byte unsigned integer
     */
    const TYPE_LONG = 0x04;
  
    /**
     * Two {@link TYPE_LONG}s, the first representing the numerator of a
     * fraction, the second, the denominator.
     */
    const TYPE_RATIONAL = 0x05;
  
    /**
     * 8-bit signed (two's complement) integer.
     */
    const TYPE_SBYTE = 0x06;
  
    /**
     * Arbitrary 8-bit binary data.
     */
    const TYPE_UNDEFINED = 0x07;
  
    /**
     * 2-byte signed (two's complement) integer.
     */
    const TYPE_SSHORT = 0x08;
  
    /**
     * 4-byte signed (two's complement) integer.
     */
    const TYPE_SLONG = 0x09;
  
    /**
     * Two {@link TYPE_SLONG}s, the first representing the numerator of a
     * fraction, the second, the denominator.
     */
    const TYPE_SRATIONAL = 0x0a;
  
    /**
     * Single-precision (4-byte) IEEE floating point number.
     */
    const TYPE_FLOAT = 0x0b;
  
    /**
     * Double-precision (8-byte) IEEE floating point number.
     */
    const TYPE_DOUBLE = 0x0c;
  
    /**
     * Byte offset to child IFD (4-byte unsigned integer).
     */
    const TYPE_IFD = 0x0d;
  
  
  /* Essential Image Information Field Types */

    /**
     * Number of bits per color component.
     */
    const FIELD_BITS_PER_SAMPLE = 0x0102;

    /**
     * Compression scheme used for the image data (see the COMPRESSION_
     * constants).
     */
    const FIELD_COMPRESSION = 0x0103;

    /**
     * Height of the image in pixels.
     */
    const FIELD_IMAGE_LENGTH = 0x0101;

    /**
     * Width of the image in pixels.
     */
    const FIELD_IMAGE_WIDTH = 0x0100;

    /**
     * Indication of the type of data contained in the subfile.
     */
    const FIELD_NEW_SUBFILE_TYPE = 0x00fe;

    /**
     * Logical order of pixel data within a byte (see the FILL_ORDER_
     * constants).
     */
    const FIELD_FILL_ORDER = 0x010a;

    /**
     * Horizontal and vertical orientation of the image data.
     */
    const FIELD_ORIENTATION = 0x0112;

    /**
     * Color space of the image data (see COLOR_SPACE_ constants).
     */
    const FIELD_PHOTOMETRIC_INTERPRETATION = 0x0106;

    /**
     * Storage order for each pixel component (see PLANAR_CONFIGURATION_
     * constants).
     */
    const FIELD_PLANAR_CONFIGURATION = 0x011c;

    /**
     * Unit of measurement for {@link FIELD_X_RESOLUTION} and {@link FIELD_Y_RESOLUTION}.
     */
    const FIELD_RESOLUTION_UNIT = 0x0128;

    /**
     * Pixels per {@link FIELD_RESOLUTION_UNIT} for the image width.
     */
    const FIELD_X_RESOLUTION = 0x011a;

    /**
     * Pixels per {@link FIELD_RESOLUTION_UNIT} for the image height.
     */
    const FIELD_Y_RESOLUTION = 0x011b;


  /* Field Types */

    /**
     * Length of the dithering or halftoning matrix for dithered or halftoned
     * images.
     */
    const FIELD_CELL_LENGTH = 0x0109;

    /**
     * Width of the dithering or halftoning matrix for dithered or halftoned
     * images.
     */
    const FIELD_CELL_WIDTH = 0x0108;

    /**
     * Color map for pallete (indexed) color images.
     */
    const FIELD_COLOR_MAP = 0x0140;

    /**
     * Description of extra pixel components (see the EXTRA_SAMPLES_ constants).
     */
    const FIELD_EXTRA_SAMPLES = 0x0152;

    /**
     * Lengths of chunks of unused bytes.
     */
    const FIELD_FREE_BYTE_COUNTS = 0x0121;

    /**
     * Offsets of chunks of unused bytes.
     */
    const FIELD_FREE_OFFSETS = 0x0120;

    /**
     * Optical density of each possible pixel value for grayscale data.
     */
    const FIELD_GRAY_RESPONSE_CURVE = 0x0123;

    /**
     * Precision of the information in the {@link FIELD_GRAY_RESPONSE_CURVE}.
     */
    const FIELD_GRAY_RESPONSE_UNIT = 0x0122;

    /**
     * Range of gray levels that should retain tonal detail for halftoned
     * images.
     */
    const FIELD_HALFTONE_HINTS = 0x0141;

    /**
     * Adobe Photoshop specific image source data (layer and mask information).
     */
    const FIELD_IMAGE_SOURCE_DATA = 0x935c;

    /**
     * Indicates an indexed image in non-RGB color space.
     */
    const FIELD_INDEXED = 0x015a;

    /**
     * Maximum component value used for pixel data (statistical only).
     */
    const FIELD_MAX_SAMPLE_VALUE = 0x0119;

    /**
     * Minimum component value used for pixel data (statistical only).
     */
    const FIELD_MIN_SAMPLE_VALUE = 0x0118;

    /**
     * Adobe Photoshop specific image resource data.
     */
    const FIELD_PHOTOSHOP_IMAGE_RESOURCES = 0x8649;
    
    /**
     * Differencing predictor used for {@link COMPRESSION_LZW} and
     * {@link COMPRESSION_DEFLATE} compression schemes.
     */
    const FIELD_PREDICTOR = 0x013d;

    /**
     * Chromaticities of the primaries of the image.
     */
    const FIELD_PRIMARY_CHROMATICITIES = 0x013f;

    /**
     * Headroom and footroom reference black and white values.
     */
    const FIELD_REFERENCE_BLACK_WHITE = 0x0214;

    /**
     * Number of rows per strip of image data.
     */
    const FIELD_ROWS_PER_STRIP = 0x0116;

    /**
     * Number format for each data sample in a pixel (see the SAMPLE_FORMAT_
     * constants).
     */
    const FIELD_SAMPLE_FORMAT = 0x0153;

    /**
     * Maximum sample value from all image data.
     */
    const FIELD_SMAX_SAMPLE_VALUE = 0x0155;

    /**
     * Minimum sample value from all image data.
     */
    const FIELD_SMIN_SAMPLE_VALUE = 0x0154;

    /**
     * Number of color components per pixel.
     */
    const FIELD_SAMPLES_PER_PIXEL = 0x0115;

    /**
     * Number of bytes for each strip of image data after compression.
     */
    const FIELD_STRIP_BYTE_COUNTS = 0x0117;

    /**
     * Byte offsets for each strip of image data.
     */
    const FIELD_STRIP_OFFSETS = 0x0111;

    /**
     * Indication of the type of data contained in the subfile (deprecated,
     * see {@link FIELD_NEW_SUBFILE_TYPE}).
     */
    const FIELD_SUBFILE_TYPE = 0x00ff;

    /**
     * Offset to child IFDs for this image.
     */
    const FIELD_SUB_IFDS = 0x014a;

    /**
     * Options bitfield for use with {@link COMPRESSION_CCITT_T4} compression.
     */
    const FIELD_T4_OPTIONS = 0x0124;

    /**
     * Options bitfield for use with {@link COMPRESSION_CCITT_T6} compression.
     */
    const FIELD_T6_OPTIONS = 0x0125;

    /**
     * Technique to convert from gray to black and white pixels. Only used for
     * black and white files that represent shades of gray.
     */
    const FIELD_THRESHHOLDING = 0x0107;

    /**
     * Number of bytes for each tile of image data after compression.
     */
    const FIELD_TILE_BYTE_COUNTS = 0x0145;

    /**
     * Height of each image data tile in pixels.
     */
    const FIELD_TILE_LENGTH = 0x0143;

    /**
     * Byte offsets for each tile of image data.
     */
    const FIELD_TILE_OFFSETS = 0x0144;

    /**
     * Width of each image data tile in pixels.
     */
    const FIELD_TILE_WIDTH = 0x0142;

    /**
     * Transfer function for the image.
     */
    const FIELD_TRANSFER_FUNCTION = 0x012d;

    /**
     * Expands the range of the {@link FIELD_TRANSFER_FUNCTION} for the image.
     */
    const FIELD_TRANSFER_RANGE = 0x0156;

    /**
     * Chromacity of the white point of the image.
     */
    const FIELD_WHITE_POINT = 0x013e;

    /**
     * Transform coefficients to calculate the luminance component of Y-Cb-Cr
     * images.
     */
    const FIELD_YCBCR_COEFFICIENTS = 0x0211;

    /**
     * Positioning of subsampled chrominance components relative to luminance
     * samples of Y-Cb-Cr images.
     */
    const FIELD_YCBCR_POSITIONING = 0x0213;

    /**
     * Subsampling factors used for the chrominance components of Y-Cb-Cr
     * images.
     */
    const FIELD_YCBCR_SUBSAMPLING = 0x0212;


  /* Separated Color Space Field Types */

    /**
     * Component values corresponding to 0% and 100% dots.
     */
    const FIELD_DOT_RANGE = 0x0150;

    /**
     * Names of each ink.
     */
    const FIELD_INK_NAMES = 0x014d;

    /**
     * Set of inks.
     */
    const FIELD_INK_SET = 0x014c;

    /**
     * Number of inks.
     */
    const FIELD_NUMBER_OF_INKS = 0x014e;

    /**
     * Description of the intended printing environment.
     */
    const FIELD_TARGET_PRINTER = 0x0151;


  /* JPEG Compression Field Types */

    /**
     * JPEG process used to compress the image data.
     */
    const FIELD_JPEG_PROC = 0x0200;

    /**
     * Points to the JPEG interchange format bitstream.
     */
    const FIELD_JPEG_INTERCHANGE_FORMAT = 0x0201;

    /**
     * Byte length of the JPEG interchange format bitstream.
     */
    const FIELD_JPEG_INTERCHANGE_FORMAT_LENGTH = 0x0202;

    /**
     * Length of the restart interval used in the compressed image data.
     */
    const FIELD_JPEG_RESTART_INTERVAL = 0x0203;

    /**
     * Lossless predictor-selection values for each color component.
     */
    const FIELD_JPEG_LOSSLESS_PREDICTORS = 0x0205;

    /**
     * Point transform values for each color component.
     */
    const FIELD_JPEG_POINT_TRANSFORMS = 0x0206;

    /**
     * Offsets to the quantization tables for each color component.
     */
    const FIELD_JPEG_Q_TABLES = 0x0207;

    /**
     * Offsets to the DC or lossless Huffman tables for each color component.
     */
    const FIELD_JPEG_DC_TABLES = 0x0208;

    /**
     * Offsets to the Huffman AC tables for each color component.
     */
    const FIELD_JPEG_AC_TABLES = 0x0209;

    /**
     * Extracted quantization and/or Huffman tables for the JPEG image
     * segments.
     */
    const FIELD_JPEG_TABLES = 0x015b;


  /* Clipping Path Field Types */

    /**
     * Clipping path for the image.
     */
    const FIELD_CLIP_PATH = 0x0157;

    /**
     * Number of integer units that span the width of the image.
     */
    const FIELD_X_CLIP_PATH_UNITS = 0x0158;

    /**
     * Number of integer units that span the height of the image.
     */
    const FIELD_Y_CLIP_PATH_UNITS = 0x0159;


  /* OPI Field Types */

    /**
     * Full pathname or other identifying string of the original,
     * high-resolution image.
     */
    const FIELD_IMAGE_ID = 0x800d;

    /**
     * Indicates whether the image is a low-resolution OPI proxy image.
     */
    const FIELD_OPI_PROXY = 0x015f;


  /* Image Information Field Types */

    /**
     * Person who created the image.
     */
    const FIELD_ARTIST = 0x013b;

    /**
     * Image copyright notice.
     */
    const FIELD_COPYRIGHT = 0x8298;

    /**
     * Image creation date and time ('YYYY:MM:DD HH:MM:SS' format).
     */
    const FIELD_DATE_TIME = 0x0132;

    /**
     * Description of the image.
     */
    const FIELD_DESCRIPTION = 0x010e;

    /**
     * Computer and/or operating system used to create the image.
     */
    const FIELD_HOST_COMPUTER = 0x013c;

    /**
     * Name and version number of the software used to create the image.
     */
    const FIELD_SOFTWARE = 0x0131;


  /* Document Interchange Field Types */

    /**
     * Name of the document from which the image was scanned.
     */
    const FIELD_DOCUMENT_NAME = 0x010d;

    /**
     * Manufacturer name of the device used to create the image.
     */
    const FIELD_MAKE = 0x010f;

    /**
     * Model name or number of the device used to create the image.
     */
    const FIELD_MODEL = 0x0110;

    /**
     * The name of the page from which this image was scanned.
     */
    const FIELD_PAGE_NAME = 0x011d;

    /**
     * Page number from the source document from which this image was scanned.
     */
    const FIELD_PAGE_NUMBER = 0x0129;

    /**
     * Horizontal offset of the image in {@link FIELD_RESOLUTION_UNIT}s on the
     * page from which this image was scanned.
     */
    const FIELD_X_POSITION = 0x011e;

    /**
     * Vertical offset of the image in {@link FIELD_RESOLUTION_UNIT}s on the
     * page from which this image was scanned.
     */
    const FIELD_Y_POSITION = 0x011f;


  /* Image Data Compression Schemes */

    /**
     * No compression; bytes are packed as tightly as possible.
     */
    const COMPRESSION_UNCOMPRESSED = 0x01;

    /**
     * CCITT Group 3 1-Dimensional Modified Huffman run-length encoding. Used
     * primarily for black-and white faxes.
     */
    const COMPRESSION_CCITT = 0x02;

    /**
     * CCITT Group 3 T.4 bi-level facsimilie encoding.
     */
    const COMPRESSION_CCITT_T4 = 0x03;

    /**
     * CCITT Group 4 T.6 bi-level facsimilie encoding.
     */
    const COMPRESSION_CCITT_T6 = 0x04;

    /**
     * LZW compression.
     */
    const COMPRESSION_LZW = 0x05;

    /**
     * JPEG compression.
     */
    const COMPRESSION_JPEG = 0x06;

    /**
     * Deflate (zlib) compression.
     */
    const COMPRESSION_DEFLATE = 0x08;

    /**
     * Obsolete variation of {@link COMPRESSION_DEFLATE}.
     */
    const COMPRESSION_DEFLATE_OBSOLETE = 0x80b2;

    /**
     * PackBits, a simple byte-oriented run-length scheme.
     */
    const COMPRESSION_PACKBITS = 0x8005;


  /* Color Spaces */

    /**
     * For black-and-white and grayscale images, 0 is imaged as white.
     */
    const COLOR_SPACE_WHITE_IS_ZERO = 0x00;

    /**
     * For black-and-white and grayscale images, 0 is imaged as black.
     */
    const COLOR_SPACE_BLACK_IS_ZERO = 0x01;

    /**
     * Red-Green-Blue.
     */
    const COLOR_SPACE_RGB = 0x02;

    /**
     * RGB indexed color.
     */
    const COLOR_SPACE_RGB_PALETTE = 0x03;

    /**
     * 1-bit transparency mask.
     */
    const COLOR_SPACE_TRANSPARENCY_MASK = 0x04;

    /**
     * Separated (usually CMYK).
     */
    const COLOR_SPACE_SEPARATED = 0x05;

    /**
     * Y-Cb-Cr
     */
    const COLOR_SPACE_YCBCR = 0x06;

    /**
     * CIE L*a*b*
     */
    const COLOR_SPACE_CIELAB = 0x08;

    /**
     * ICCLab
     */
    const COLOR_SPACE_ICCLAB = 0x09;


  /* Pixel Sample Data Format Types */

    /**
     * Unsigned integer data.
     */
    const SAMPLE_FORMAT_UNSIGNED = 0x01;

    /**
     * Signed (two's complement) integer data.
     */
    const SAMPLE_FORMAT_SIGNED = 0x02;

    /**
     * IEEE floating point data.
     */
    const SAMPLE_FORMAT_FLOAT = 0x03;

    /**
     * Undefined or unknown data format.
     */
    const SAMPLE_FORMAT_UNDEFINED = 0x04;


  /* Extra Samples Description */

    /**
     * Unspecified.
     */
    const EXTRA_SAMPLES_UNSPECIFIED = 0x00;

    /**
     * Associated alpha data with pre-multiplied color.
     */
    const EXTRA_SAMPLES_ASSOCIATED_ALPHA = 0x01;

    /**
     * Unassociated alpha data.
     */
    const EXTRA_SAMPLES_UNASSOCIATED_ALPHA = 0x02;


  /* Pixel Fill Order */

    /**
     * Pixels with lower column values are stored in the higher-order bits.
     */
    const FILL_ORDER_HIGH = 0x01;

    /**
     * Pixels with lower column values are stored in the lower-order bits.
     */
    const FILL_ORDER_LOW = 0x02;


  /* Planar Configuration */

    /**
     * Pixel components are stored contiguously.
     */
    const PLANAR_CONFIGURATION_CHUNKY = 0x01;

    /**
     * Pixel components are stored in component planes.
     */
    const PLANAR_CONFIGURATION_PLANAR = 0x02;



  /**** Instance Variables ****/

    /**
     * Byte order used by the TIFF file.
     * @var array
     */
    protected $_byteOrder = null;
    
    /**
     * Array of Image File Directory (IFD) offsets in the TIFF file.
     * @var array
     */
    protected $_images = array();
    
    /**
     * Associative array of field information for the currently selected image.
     * @var array
     */
    protected $_ifdFields = array();



  /**** Public Interface ****/


  /* Concrete Subclass Implementation */

    /**
     * Verifies that the data source contains a valid TIFF image.
     *
     * @throws RE_FileParser_Exception
     */
    public function screen()
    {
        if ($this->_isScreened) {
            return;
        }

        $this->moveToOffset(0);

        $marker = $this->readBytes(2);
        if ($marker == 'II') {
            $this->_byteOrder = RE_FileParser::BYTE_ORDER_LITTLE_ENDIAN;
        } else if ($marker == 'MM') {
            $this->_byteOrder = RE_FileParser::BYTE_ORDER_BIG_ENDIAN;
        } else {
            throw new RE_FileParser_Exception(
                'File is not a TIFF image: byte order marker not found',
                RE_FileParser_Exception::NOT_AN_IMAGE_FILE);
        }

        $versionNumber = $this->readUInt(2);
        if ($versionNumber != 42) {
            throw new RE_FileParser_Exception(
                sprintf('File is not a TIFF image: incorrect version number; expected 42, read %d', $versionNumber),
                RE_FileParser_Exception::NOT_AN_IMAGE_FILE);
        }
        
        /* Read each of the Image File Directories (IFDs) in the TIFF file.
         * Each IFD describes a complete image; a TIFF file itself may contain
         * an arbitrary number of individual images.
         */
        $imageCount = 0;
        $maxOffset  = $this->getSize() - 18;

        $ifdOffset = $this->readUInt(4);
        while ($ifdOffset > 0) {
            
            if (($ifdOffset < 8) || ($ifdOffset > $maxOffset)) {
                throw new RE_FileParser_Exception(
                    sprintf('Invalid next IFD offset 0x%x at byte offset 0x%x',
                            $ifdOffset, ($this->getOffset() - 4)),
                    RE_FileParser_Exception::BAD_BYTE_OFFSET);
            }
            $this->moveToOffset($ifdOffset);
            
            /* Fail-safe to prevent infinite loops.
             */
            if (++$imageCount > 100) {
                throw new RE_FileParser_Exception(
                    'Improbable number of IFDs found', RE_FileParser_Exception::BAD_IMAGE_COUNT);
            }
            
            /* The first 2 bytes indicate the number of fields in this IFD.
             */
            $fieldCount = $this->readUInt(2);
            if ($fieldCount < 1) {
                throw new RE_FileParser_Exception(
                    sprintf('No fields in IFD at byte offset 0x%x', ($this->getOffset() - 6)),
                    RE_FileParser_Exception::BAD_FIELD_COUNT);
            }

            /* Skip over the individual field data for now. The fields will
             * by read by selectImage() later if needed. Each field entry is
             * 12 bytes long.
             */
            $this->skipBytes($fieldCount * 12);

            $this->_images[$imageCount] = $ifdOffset;
            
            /* The final 4 bytes are the offset of the next IFD in the file
             * or zero if this is the last IFD.
             */
            $ifdOffset = $this->readUInt(4);
        }

        if ($imageCount < 1) {
            throw new RE_FileParser_Exception(
                'File is not a TIFF image: no image file directories found',
                RE_FileParser_Exception::NOT_AN_IMAGE_FILE);
        }
        
        $this->_isScreened = true;
    }

    /**
     * Reads and parses the additional structural data which is skipped
     * by {@link screen()}.
     *
     * Does not read the entire file; specific objects are retrieved on-demand
     * by the various get() methods.
     *
     * @throws RE_FileParser_Exception
     */
    public function parse()
    {
        if ($this->_isParsed) {
            return;
        }

        /* Screen the document first, if it hasn't been done yet.
         */
        $this->screen();

        /* Automatically select the first image in the file. In the majority of
         * cases, there is only one image.
         */
        $this->selectImage(1);

        $this->_isParsed = true;
    }

    /**
     * Returns the width of the currently selected image in pixels (samples).
     *
     * @return integer
     */
    public function getPixelWidth()
    {
        return $this->getFieldValue(RE_FileParser_Image_Tiff::FIELD_IMAGE_WIDTH);
    }

    /**
     * Returns the width of the currently selected image in pixels (samples).
     *
     * @return integer
     */
    public function getPixelHeight()
    {
        return $this->getFieldValue(RE_FileParser_Image_Tiff::FIELD_IMAGE_LENGTH);
    }


  /* Accessor Methods */

    /**
     * Returns the color space used by the currently selected image.
     *
     * @return string
     */
    public function getColorSpace()
    {
        return $this->getFieldValue(RE_FileParser_Image_Tiff::FIELD_PHOTOMETRIC_INTERPRETATION);
    }

    /**
     * Returns the number of bits per component for each sample in the
     * currently selected image.
     *
     * @return integer
     */
    public function getBitsPerComponent()
    {
        /**
         * @todo Throw an exception if bits per component are different for
         *   each channel; PDF requires all channels to be the same. Or just
         *   reprocess the image data.
         */
        $value = $this->getFieldValue(RE_FileParser_Image_Tiff::FIELD_BITS_PER_SAMPLE);
        if (is_array($value)) {
            return $value[0];
        }
        return $value;
    }

    /**
     * If the image contains a clipping path, returns that path as
     * a {@link RE_Path} object. Otherwise, returns null.
     * 
     * @return RE_Path|null
     */
    public function getClippingPath()
    {
        /** @todo Add support for {@link FIELD_CLIP_PATH}. */
        
        /* If the image has a Photoshop image resources block, it may contain a
         * Photoshop-format clipping path.
         */
        $fieldType = RE_FileParser_Image_Tiff::FIELD_PHOTOSHOP_IMAGE_RESOURCES;
        if (isset($this->_ifdFields[$fieldType])) {

            $pathResources = array();
            
            $this->moveToOffset($this->_ifdFields[$fieldType]['Offset']);
            $this->moveToOffset($this->readUInt(4));

            $maxOffset = $this->getOffset() + $this->_ifdFields[$fieldType]['Count'];
            while ($this->getOffset() < $maxOffset) {
                
                /* Check for the Photoshop image resource signature. If missing, the
                 * file may be corrupt or we've seeked out of the resource block.
                 */
                if ($this->readBytes(4) !== '8BIM') {
                    break;
                }

                /* Photoshop resource data is always stored in big-endian byte
                 * order, regardless of the byte order of the TIFF file.
                 */
                $resourceID   = $this->readInt(2, RE_FileParser::BYTE_ORDER_BIG_ENDIAN);
                $resourceName = $this->readStringPascal('ASCII', 1);
                if ((strlen($resourceName) % 2) == 0) {
                    $this->skipBytes(1);  // zero-padded to word boundaries - skip ahead
                }
                $dataLength   = $this->readInt(4, RE_FileParser::BYTE_ORDER_BIG_ENDIAN);
                
                
                /* Resource IDs 2000 - 2998 store path data. We need to keep track of
                 * all of them because we have to find the clipping path later by name.
                 */
                if (($resourceID >= 0x07d0) && ($resourceID <= 0x0bb6) &&
                    (($dataLength % 26) == 0)) {
                        $pathResources[$resourceName] = array(
                            'Offset' => $this->getOffset(),
                            'Length' => $dataLength
                            );
                }
                
                /* The name of the clipping path is stored as a Pascal string in
                 * resource ID 2999. We don't care about any others.
                 */
                if ($resourceID != 0x0bb7) {
                    if (($dataLength % 2) == 1) {
                        $dataLength++;  // zero-padded to word boundaries - skip ahead
                    }
                    $this->skipBytes($dataLength);
                    continue;
                }
                
                $clippingPathName = $this->readStringPascal('ASCII', 1);
                if (! isset($pathResources[$clippingPathName])) {
                    break;  // named path not found - abort
                }

                require_once 'RE/Path.php';

                $path = new RE_Path();
                $path->setWindingRule(RE_Path::WINDING_RULE_EVEN_ODD);
                
                $firstPreceeding = new RE_Point();
                $firstAnchor     = new RE_Point();
                
                $preceeding = new RE_Point();
                $anchor     = new RE_Point();
                $leaving    = new RE_Point();
                
                $inPath        = false;
                $closedSubpath = false;
                
                $this->moveToOffset($pathResources[$clippingPathName]['Offset']);                

                $maxOffset = $this->getOffset() + $pathResources[$clippingPathName]['Length'];
                while ($this->getOffset() < $maxOffset) {

                    $recordType = $this->readInt(2, RE_FileParser::BYTE_ORDER_BIG_ENDIAN);
                    switch ($recordType) {
                        
                    case 0:  // closed subpath length
                    case 3:  // open   subpath length
                        if ($inPath && $closedSubpath) {
                            $path->curveTo($firstAnchor, $leaving, $firstPreceeding);
                            $path->closePath();
                        }
                        $inPath        = false;
                        $closedSubpath = ($recordType == 0);
                        $this->skipBytes(24);  // ignore length
                        break;

                    case 1:  // closed subpath bezier knot, linked
                    case 2:  // closed subpath bezier knot, unlinked
                    case 4:  // open   subpath bezier knot, linked
                    case 5:  // open   subpath bezier knot, unlinked
                        $preceeding->setY(1 - $this->readFixed(8, 24, RE_FileParser::BYTE_ORDER_BIG_ENDIAN));  // invert y axis
                        $preceeding->setX($this->readFixed(8, 24, RE_FileParser::BYTE_ORDER_BIG_ENDIAN));
                        $anchor->setY(1 - $this->readFixed(8, 24, RE_FileParser::BYTE_ORDER_BIG_ENDIAN));      // invert y axis
                        $anchor->setX($this->readFixed(8, 24, RE_FileParser::BYTE_ORDER_BIG_ENDIAN));
                        if ($inPath) {
                            $path->curveTo($anchor, $leaving, $preceeding);
                        } else {
                            $firstPreceeding = clone $preceeding;
                            $firstAnchor     = clone $anchor;
                            $path->moveTo($anchor);
                            $inPath = true;
                        }
                        $leaving->setY(1 - $this->readFixed(8, 24, RE_FileParser::BYTE_ORDER_BIG_ENDIAN));     // invert y axis
                        $leaving->setX($this->readFixed(8, 24, RE_FileParser::BYTE_ORDER_BIG_ENDIAN));
                        break;

                    case 6:  // path fill rule    - always even/odd
                    case 7:  // clipboard         - ignored
                    case 8:  // initial fill rule - ignored
                    default: // unknown           - ignored
                        $this->skipBytes(24);
                        break;
                    }
                }

                if ($inPath && $closedSubpath) {
                    $path->curveTo($firstAnchor, $leaving, $firstPreceeding);
                    $path->closePath();
                }

                return $path;
            }
            
        }
        
        return null;
    }


  /* IFD Interaction */

    /**
     * Returns the number of top-level images present in the TIFF file.
     * 
     * @return integer
     */
    public function getImageCount()
    {
        return count($this->_images);
    }
    
    /**
     * Selects the specified top-level image in the TIFF file.
     * 
     * @throws RangeException if the image number is out of range.
     */
    public function selectImage($imageNumber)
    {
        if (! isset($this->_images[$imageNumber])) {
            throw new RangeException("Invalid image number: $imageNumber");
        }
        $this->_readIFD($this->_images[$imageNumber]);
    }
    
    /**
     * Returns the number of child images (alternative resolutions, thumbnails,
     * etc.) available for the currently selected image or zero if the currently
     * selected image has no children.
     * 
     * @return integer
     */
    public function getChildImageCount()
    {
        $childIFDOffsets = $this->getFieldValue(RE_FileParser_Image_Tiff::FIELD_SUB_IFDS);
        if (empty($childIFDOffsets)) {
            return 0;
        }
        return count($childIFDOffsets);
    }

    /**
     * Selects the specified child image for the currently selected image.
     * 
     * @throws RangeException if the child image number is out of range or the
     *   currently selected image has no children.
     */
    public function selectChildImage($imageNumber)
    {
        $childIFDOffsets = $this->getFieldValue(RE_FileParser_Image_Tiff::FIELD_SUB_IFDS);
        if (! isset($childIFDOffsets[$imageNumber - 1])) {
            throw new RangeException("Invalid image number: $imageNumber");
        }
        $this->_readIFD($childIFDOffsets[$imageNumber - 1]);
    }
    
    /**
     * Selects the next image in the TIFF file. If the currently selected image
     * is a child, selects the next sibling. Returns true if a new image was
     * selected or false if there are no additional images.
     * 
     * @return boolean
     */
    public function selectNextImage()
    {
        if (empty($this->_ifdFields['NextIFD'])) {
            return false;
        }
        $this->_readIFD($this->_ifdFields['NextIFD']);
        return true;
    }
        
    /**
     * Returns the value of the specified field type for the currently selected
     * image. If the field contains one value, the scalar value is returned. If
     * the field contains multiple values, an array of values is returned. If
     * the field does not exist, returns null. Be sure to check the return type!
     * 
     * @param integer $fieldType One of the FIELD_ constants defined in this class.
     * @return mixed
     */
    public function getFieldValue($fieldType)
    {
        if (! isset($this->_ifdFields[$fieldType])) {
            return null;
        }

        $count = $this->_ifdFields[$fieldType]['Count'];
        if ($count < 1) {
            return null;
        }
        
        $this->moveToOffset($this->_ifdFields[$fieldType]['Offset']);
        
        switch ($this->_ifdFields[$fieldType]['DataType']) {
            
            
        case RE_FileParser_Image_Tiff::TYPE_BYTE:
            if ($count == 1) {
                return $this->readUInt(1);
            }
            if ($count > 4) {
                $this->moveToOffset($this->readUInt(4));
            }
            $values = array();
            for ($i = 0; $i < $count; $i++) {
                $values[] = $this->readUInt(1);
            }
            return $values;
            
        case RE_FileParser_Image_Tiff::TYPE_SBYTE:
            if ($count == 1) {
                return $this->readInt(1);
            }
            if ($count > 4) {
                $this->moveToOffset($this->readUInt(4));
            }
            $values = array();
            for ($i = 0; $i < $count; $i++) {
                $values[] = $this->readInt(1);
            }
            return $values;


        case RE_FileParser_Image_Tiff::TYPE_SHORT:
            if ($count == 1) {
                return $this->readUInt(2);
            }
            if ($count > 2) {
                $this->moveToOffset($this->readUInt(4));
            }
            $values = array();
            for ($i = 0; $i < $count; $i++) {
                $values[] = $this->readUInt(2);
            }
            return $values;
            
        case RE_FileParser_Image_Tiff::TYPE_SSHORT:
            if ($count == 1) {
                return $this->readInt(2);
            }
            if ($count > 2) {
                $this->moveToOffset($this->readUInt(4));
            }
            $values = array();
            for ($i = 0; $i < $count; $i++) {
                $values[] = $this->readInt(2);
            }
            return $values;
            
            
        case RE_FileParser_Image_Tiff::TYPE_LONG:
        case RE_FileParser_Image_Tiff::TYPE_IFD:
            if ($count == 1) {
                return $this->readUInt(4);
            }
            $this->moveToOffset($this->readUInt(4));
            $values = array();
            for ($i = 0; $i < $count; $i++) {
                $values[] = $this->readUInt(4);
            }
            return $values;
            
        case RE_FileParser_Image_Tiff::TYPE_SLONG:
            if ($count == 1) {
                return $this->readInt(4);
            }
            $this->moveToOffset($this->readUInt(4));
            $values = array();
            for ($i = 0; $i < $count; $i++) {
                $values[] = $this->readInt(4);
            }
            return $values;


        case RE_FileParser_Image_Tiff::TYPE_RATIONAL:
            $this->moveToOffset($this->readUInt(4));
            if ($count == 1) {
                return $this->readURational();
            }
            $values = array();
            for ($i = 0; $i < $count; $i++) {
                $values[] = $this->readURational();
            }
            return $values;
            
        case RE_FileParser_Image_Tiff::TYPE_SRATIONAL:
            $this->moveToOffset($this->readUInt(4));
            if ($count == 1) {
                return $this->readRational();
            }
            $values = array();
            for ($i = 0; $i < $count; $i++) {
                $values[] = $this->readRational();
            }
            return $values;


        case RE_FileParser_Image_Tiff::TYPE_FLOAT:
            if ($count == 1) {
                return $this->readFloat();
            }
            $this->moveToOffset($this->readUInt(4));
            $values = array();
            for ($i = 0; $i < $count; $i++) {
                $values[] = $this->readFloat();
            }
            return $values;
            
        case RE_FileParser_Image_Tiff::TYPE_DOUBLE:
            $this->moveToOffset($this->readUInt(4));
            if ($count == 1) {
                return $this->readDouble();
            }
            $values = array();
            for ($i = 0; $i < $count; $i++) {
                $values[] = $this->readDouble();
            }
            return $values;


        case RE_FileParser_Image_Tiff::TYPE_ASCII:
            if ($count > 4) {
                $this->moveToOffset($this->readUInt(4));
            }
            $values = explode("\x00", $this->readStringASCII($count - 1));
            if (count($values) > 1) {
                return $values;
            }
            return $values[0];


        case RE_FileParser_Image_Tiff::TYPE_UNDEFINED:
            if ($count > 4) {
                $this->moveToOffset($this->readUInt(4));
            }
            return $this->readBytes($count);
            

        default:
            /* If an unknown data type is found, we should ignore it.
             */
            return null;
        }
    }
    

  /* Parser Methods */

    /**
     * Overridden to use the TIFF file's byte order (read from the file header
     * by {@link screen()}) if a byte order is not explicitly provided.
     *
     * @param integer $size
     * @param integer $byteOrder (optional)
     * @return integer
     */
    public function readInt($size, $byteOrder = null)
    {
        if (is_null($byteOrder)) {
            $byteOrder = $this->_byteOrder;
        }
        return parent::readInt($size, $byteOrder);
    }

    /**
     * Overridden to use the TIFF file's byte order (read from the file header
     * by {@link screen()}) if a byte order is not explicitly provided.
     *
     * @param integer $size
     * @param integer $byteOrder (optional)
     * @return integer
     */
    public function readUInt($size, $byteOrder = null)
    {
        if (is_null($byteOrder)) {
            $byteOrder = $this->_byteOrder;
        }
        return parent::readUInt($size, $byteOrder);
    }

    /**
     * Overridden to use the TIFF file's byte order (read from the file header
     * by {@link screen()}) if a byte order is not explicitly provided.
     *
     * @param integer $mantissaBits
     * @param integer $fractionBits
     * @param integer $byteOrder    (optional)
     * @return float
     */
    public function readFixed($mantissaBits, $fractionBits, $byteOrder = null)
    {
        if (is_null($byteOrder)) {
            $byteOrder = $this->_byteOrder;
        }
        return parent::readFixed($mantissaBits, $fractionBits, $byteOrder);
    }

    /**
     * Overridden to use the TIFF file's byte order (read from the file header
     * by {@link screen()}) if a byte order is not explicitly provided.
     * 
     * @param integer $byteOrder (optional)
     * @return number
     */
    public function readFloat($byteOrder = null)
    {
        if (is_null($byteOrder)) {
            $byteOrder = $this->_byteOrder;
        }
        return parent::readFloat($byteOrder);
    }

    /**
     * Overridden to use the TIFF file's byte order (read from the file header
     * by {@link screen()}) if a byte order is not explicitly provided.
     *
     * @param integer $byteOrder (optional)
     * @return number
     */
    public function readDouble($byteOrder = null)
    {
        if (is_null($byteOrder)) {
            $byteOrder = $this->_byteOrder;
        }
        return parent::readDouble($byteOrder);
    }

    /**
     * Reads the signed rational number, two signed 4-byte integers,
     * the first representing the numerator of a fraction, the second,
     * the denominator, from the binary file at the current byte offset.
     * Advances the offset by the number of bytes read.
     * 
     * Uses the TIFF file's byte order (read from the file header by
     * {@link screen()}) if a byte order is not explicitly provided.
     *
     * @param integer $byteOrder (optional)
     * @return number
     */
    public function readRational($byteOrder = null)
    {
        if (is_null($byteOrder)) {
            $byteOrder = $this->_byteOrder;
        }
        return parent::readInt(4, $byteOrder) / parent::readInt(4, $byteOrder);
    }

    /**
     * Reads the unsigned rational number, two unsigned 4-byte integers,
     * the first representing the numerator of a fraction, the second,
     * the denominator, from the binary file at the current byte offset.
     * Advances the offset by the number of bytes read.
     *
     * Uses the TIFF file's byte order (read from the file header by
     * {@link screen()}) if a byte order is not explicitly provided.
     * 
     * @param integer $byteOrder (optional)
     * @return integer
     */
    public function readURational($byteOrder = null)
    {
        if (is_null($byteOrder)) {
            $byteOrder = $this->_byteOrder;
        }
        return parent::readUInt(4, $byteOrder) / parent::readUInt(4, $byteOrder);
    }



  /**** Internal Interface ****/
  
    /**
     * Reads the field information from the IFD at the specified offset,
     * making it the currently selected image.
     * 
     * @param number $ifdOffset
     */
    protected function _readIFD($ifdOffset)
    {
        $this->moveToOffset($ifdOffset);

        $ifdFields = array();
        
        $fieldCount = $this->readUInt(2);
        for ($i = 0; $i < $fieldCount; $i++) {
            
            $fieldType = $this->readUInt(2);
            $dataType  = $this->readUInt(2);
            $count     = $this->readUInt(4);
            $offset    = $this->getOffset();
            $this->skipBytes(4);
            
            /* Override certain field data types to suit our own needs.
             */
            if ($fieldType == RE_FileParser_Image_Tiff::FIELD_PHOTOSHOP_IMAGE_RESOURCES) {
                $dataType = RE_FileParser_Image_Tiff::TYPE_UNDEFINED;
            }

            $ifdFields[$fieldType] = array(
                'DataType' => $dataType,
                'Count'    => $count,
                'Offset'   => $offset
                );
        }
        
        $ifdFields['NextIFD'] = $this->readUInt(4);
        
        $this->_ifdFields = $ifdFields;
    }

}
