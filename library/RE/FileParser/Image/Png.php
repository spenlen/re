<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    FileParser
 * @subpackage Images
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */

/** RE_FileParser_Image */
require_once 'RE/FileParser/Image.php';


/**
 * Parses a PNG image file.
 *
 * @todo Add CRC check when reading chunks.
 * 
 * @package    FileParser
 * @subpackage Images
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_FileParser_Image_Png extends RE_FileParser_Image
{
  /**** Class Constants ****/


  /* Image Color Types */

    /**
     * Grayscale image.
     */
    const COLOR_TYPE_GRAYSCALE = 0;
    
    /**
     * RGB color image.
     */
    const COLOR_TYPE_TRUECOLOR = 2;
    
    /**
     * Indexed color (palette) image.
     */
    const COLOR_TYPE_INDEXED = 3;
    
    /**
     * Grayscale image with alpha channel.
     */
    const COLOR_TYPE_GRAYSCALE_ALPHA = 4;
    
    /**
     * RGB color image with alpha channel.
     */
    const COLOR_TYPE_TRUECOLOR_ALPHA = 6;
    
    
  /* Image Data Compression Methods */
  
    /**
     * Deflate compression (zlib format).
     */
    const COMPRESSION_METHOD_DEFLATE = 0;

    
  /* Image Data Preprocessing Filter Methods */
  
    /**
     * Adaptive filtering with five basic filter types.
     */
    const FILTER_METHOD_ADAPTIVE = 0;
    
    
  /* Image Data Interlace Methods */
  
    /**
     * No interlace.
     */
    const INTERLACE_METHOD_NONE = 0;
    
    /**
     * Adam7 interlace.
     */
    const INTERLACE_METHOD_ADAM7 = 0;
    


  /**** Instance Variables ****/

    /**
     * Two-dimensional array of byte offsets and lengths for the various
     * chunks contained within the PNG file.
     * @var array
     */
    protected $_chunkDirectory = array();
    
    /**
     * Width of the image in pixels.
     * @var integer
     */
    protected $_pixelWidth = 0;
    
    /**
     * Height of the image in pixels.
     * @var integer
     */
    protected $_pixelHeight = 0;
    
    /**
     * Bits per color sample or palette index.
     * @var integer
     */
    protected $_bitDepth = 0;
    
    /**
     * Image color type. See the COLOR_TYPE_ contants defined in this class.
     * @var integer
     */
    protected $_colorType = 0;
    
    /**
     * Compression method used to compress the image data. See the
     * COMPRESSION_METHOD_ constants defined in this class.
     * @var integer
     */
    protected $_compressionMethod = 0;
    
    /**
     * Preprocessing filter method which was applied to the image data before
     * compression. See the FILTER_METHOD_ constants defined in this class.
     * @var integer
     */
    protected $_filterMethod = 0;
    
    /**
     * Interlace method used for the image data. See the INTERLACE_METHOD_
     * constants defined in this class.
     * @var integer
     */
    protected $_interlaceMethod = 0;
    


  /**** Public Interface ****/


  /* Concrete Subclass Implementation */

    /**
     * Verifies that the data source contains a valid PNG image.
     *
     * @throws RE_FileParser_Exception
     */
    public function screen()
    {
        if ($this->_isScreened) {
            return;
        }

        $this->moveToOffset(0);
        
        /* The first four bytes are the PNG signature.
         */
        if (! (($this->readUInt(1) == 137) && ($this->readBytes(3) === 'PNG'))) {
            throw new RE_FileParser_Exception(
                'File is not a PNG image: PNG signature not found',
                RE_FileParser_Exception::NOT_AN_IMAGE_FILE);
        }
        
        /* The next four bytes indicate the chunk order and should always be
         * the same value.
         */
        if (! ($this->readUInt(4) == 0x0d0a1a0a)) {
            /**
             * @todo Add a detection for corruption due to line ending conversion
             *   and throw a more appropriate exception.
             */
            throw new RE_FileParser_Exception(
                'File is not a PNG image: incomplete PNG signature',
                RE_FileParser_Exception::NOT_AN_IMAGE_FILE);
        }
        
        /* Verify that the datastream does indeed start with an IHDR chunk.
         */
        $length    = $this->readUInt(4);
        $chunkType = $this->readBytes(4);
        
        if ($chunkType !== 'IHDR') {
            throw new RE_FileParser_Exception(
                'File is not a PNG image: expected IHDR chunk; saw ' . $chunkType,
                RE_FileParser_Exception::NOT_AN_IMAGE_FILE);
        }
        
        $this->_isScreened = true;
    }

    /**
     * Reads and parses the additional structural data which is skipped
     * by {@link screen()}.
     *
     * Does not read the image data from the file; that and other objects are
     * retrieved on-demand by the various get() methods.
     *
     * @throws RE_FileParser_Exception
     */
    public function parse()
    {
        if ($this->_isParsed) {
            return;
        }

        /* Screen the document first, if it hasn't been done yet.
         */
        $this->screen();

        /* Read the image header (IHDR) chunk to extract the essential image
         * information and check for parser compatibility.
         */
        $this->moveToOffset(8);

        $length    = $this->readUInt(4);
        $chunkType = $this->readBytes(4);

        $this->_pixelWidth  = $this->readUInt(4);
        $this->_pixelHeight = $this->readUInt(4);
        
        $this->_bitDepth  = $this->readUInt(1);
        $this->_colorType = $this->readUInt(1);

        $this->_compressionMethod = $this->readUInt(1);
        $this->_filterMethod      = $this->readUInt(1);
        $this->_interlaceMethod   = $this->readUInt(1);
        
        /**
         * @todo Read remaining bytes (if any) and calculate and verify CRC.
         */
        
        try {
            /* Scan through the remainder of the tables and build a chunk catalog
             * to enable later random access to the image data.
             */
            $this->moveToOffset(8 + 8 + $length + 4);
        
            while ($chunkType !== 'IEND') {
                $length    = $this->readUInt(4);
                $chunkType = $this->readBytes(4);
            
                $this->_chunkDirectory[$chunkType][] = array(
                    'offset' => $this->getOffset(),
                    'length' => $length
                    );
            
                /**
                 * @todo Enforce chunk ordering and chunk multiple rules.
                 */
            
                /* Skip the chunk data and CRC to move to the next chunk.
                 */
                $this->skipBytes($length + 4);
            }
            
        } catch (RangeException $e) {
            /* If a length was off and we've moved beyond the end of the file,
             * assume the PNG image is corrupt and throw an appropriate exception.
             */
            throw new RE_FileParser_Exception(
                'File is not a PNG image: Illegal length detected',
                RE_FileParser_Exception::NOT_AN_IMAGE_FILE);
        }
        
        if (empty($this->_chunkDirectory['IDAT'])) {
            throw new RE_FileParser_Exception("No image data was found!");
        }
        
        $this->_isParsed = true;
    }

    /**
     * Returns the width of the image in pixels.
     *
     * @return integer
     */
    public function getPixelWidth()
    {
        return $this->_pixelWidth;
    }

    /**
     * Returns the width of the image in pixels.
     *
     * @return integer
     */
    public function getPixelHeight()
    {
        return $this->_pixelHeight;
    }


  /* Accessor Methods */
  
    /**
     * Returns the number of bits per color sample or palette index.
     * 
     * @return integer
     */
    public function getBitDepth()
    {
        return $this->_bitDepth;
    }
    
    /**
     * Returns the image color type.
     * 
     * @return integer One of the COLOR_TYPE_ contants defined in this class.
     */
    public function getColorType()
    {
        return $this->_colorType;
    }
    
    /**
     * Returns the compression method used to compress the image data.
     * 
     * @return integer One of the COMPRESSION_METHOD_ constants defined in this class.
     */
    public function getCompressionMethod()
    {
        return $this->_compressionMethod;
    }
    
    /**
     * Returns the preprocessing filter method which was applied to the image
     * data before compression.
     * 
     * @return integer One of the FILTER_METHOD_ constants defined in this class.
     */
    public function getFilterMethod()
    {
        return $this->_filterMethod;
    }
    
    /**
     * Returns the interlace method used for the image data.
     * 
     * @return integer One of the INTERLACE_METHOD_ constants defined in this class.
     */
    public function getInterlaceMethod()
    {
        return $this->_interlaceMethod;
    }

    /**
     * Returns the compressed image data as a binary string.
     * 
     * @return string
     */
    public function getImageData()
    {
        $imageData = '';
        foreach ($this->_chunkDirectory['IDAT'] as $chunk) {
            $this->moveToOffset($chunk['offset']);
            $imageData .= $this->readBytes($chunk['length']);
        }
        return $imageData;
    }

    /**
     * Returns the color palette data as a binary string.
     * 
     * Note: Color palette data is usually only present for indexed-color images,
     * but may be present for truecolor images. If there is no palette data,
     * returns an empty string.
     * 
     * @return string
     */
    public function getPaletteData()
    {
        $length = $this->_jumpToChunk('PLTE');
        if ($length < 1) {
            return '';
        }
        return $this->readBytes($length);
    }

    /**
     * Returns the transparency data as a binary string.
     * 
     * Note: If there is no transparency data, returns an empty string.
     * 
     * @return string
     */
    public function getTransparency()
    {
        $length = $this->_jumpToChunk('tRNS');
        if ($length < 1) {
            return '';
        }
        return $this->readBytes($length);
    }


  /**** Internal Interface ****/
  
    /**
     * Moves the parser's read position to the start of the specified chunk
     * data. Returns the length of the chunk.
     *
     * @param string  $chunkType
     * @param integer $chunkIndex (optional) For PNG chunks that may appear
     *   multiple times, the desired chunk index.
     * @return integer
     */
    protected function _jumpToChunk($chunkType, $chunkIndex = 0)
    {
        if (! isset($this->_chunkDirectory[$chunkType][$chunkIndex])) {
            return 0;
        }
        $this->moveToOffset($this->_chunkDirectory[$chunkType][$chunkIndex]['offset']);
        return $this->_chunkDirectory[$chunkType][$chunkIndex]['length'];
    }

}
