<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    FileParser
 * @subpackage Images
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @copyright  Copyright (c) 2005-2008 Zend Technologies USA, Inc. (http://www.zend.com)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */

/* Portions of this source file are excerpted from the Zend Framework:
 * 
 * Copyright (c) 2005-2008, Zend Technologies USA, Inc.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 *  * Neither the name of Zend Technologies USA, Inc. nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/** RE_FileParser_Image */
require_once 'RE/FileParser/Image.php';


/**
 * Parses a JPEG image file.
 * 
 * @package    FileParser
 * @subpackage Images
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @copyright  Copyright (c) 2005-2008 Zend Technologies USA, Inc. (http://www.zend.com)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_FileParser_Image_Jpeg extends RE_FileParser_Image
{
  /**** Instance Variables ****/

    /**
     * Image information array as returned by GD's getimagesize() function;
     * @var array
     */
    protected $_imageInfo = array();
    


  /**** Public Interface ****/


  /* Concrete Subclass Implementation */

    /**
     * Verifies that the data source contains a valid JPEG image.
     *
     * @throws RE_FileParser_Exception
     */
    public function screen()
    {
        if ($this->_isScreened) {
            return;
        }

        $dataSource = $this->getDataSource();
        if (! $dataSource instanceof RE_FileParser_DataSource_File) {
            throw new RE_FileParser_Exception(
                'JPEG images currently can only be parsed from filesystem data sources.',
                RE_FileParser_Exception::NOT_IMPLEMENTED);
        }
        
        /* getimagesize() does not depend on the GD extension.
         */
        $this->_imageInfo = @getimagesize($dataSource->getFilePath());
        if ($this->_imageInfo === false) {
            throw new RE_FileParser_Exception(
                'Cannot read image file', RE_FileParser_Exception::NOT_AN_IMAGE_FILE);
        }
        
        if (($this->_imageInfo[2] != IMAGETYPE_JPEG) &&
            ($this->_imageInfo[2] != IMAGETYPE_JPEG2000)) {
                throw new RE_FileParser_Exception(
                    'File is not a JPEG image', RE_FileParser_Exception::NOT_AN_IMAGE_FILE);
        }

        $this->_isScreened = true;
    }

    /**
     * Reads and parses the additional structural data which is skipped
     * by {@link screen()}.
     * 
     * Does not read the entire file; specific objects are retrieved on-demand
     * by the various get() methods.
     *
     * @throws RE_FileParser_Exception
     */
    public function parse()
    {
        if ($this->_isParsed) {
            return;
        }

        /* Screen the document first, if it hasn't been done yet.
         */
        $this->screen();

        /**
         * @todo Completely parse the JPEG file to provide access to image sample
         *   data, EXIF information, etc., not just the info provided by the
         *   {@link getimagesize()} function.
         */
        
        $this->_isParsed = true;
    }

    /**
     * Returns the width of the image in pixels (samples).
     *
     * @return integer
     */
    public function getPixelWidth()
    {
        return $this->_imageInfo[0];
    }

    /**
     * Returns the width of the image in pixels (samples).
     *
     * @return integer
     */
    public function getPixelHeight()
    {
        return $this->_imageInfo[1];
    }
    
    
  /* Accessor Methods */
  
    /**
     * Returns the color space used by the image.
     *
     * @return string
     */
    public function getColorSpace()
    {
        switch ($this->_imageInfo['channels']) {
            case 3:
                return 'DeviceRGB';
            case 4:
                return 'DeviceCMYK';
            default:
                return 'DeviceGray';
        }
    }
    
    /**
     * Returns the number of bits per component for each sample in the image.
     *
     * @return integer
     */
    public function getBitsPerComponent()
    {
        return $this->_imageInfo['bits'];
    }
    
    /**
     * Returns IMAGETYPE_JPEG if the image is an ordinary JPEG image or
     * IMAGETYPE_JPEG2000 if the image is a JPEG 2000 image.
     * 
     * @return integer
     */
    public function getImageType()
    {
        return $this->_imageInfo[2];
    }

}
