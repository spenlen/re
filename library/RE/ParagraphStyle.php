<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Text
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */

/** RE_TabStop */
require_once 'RE/TabStop.php';


/**
 * Stores layout information that is common to all of the characters in a
 * paragraph.
 *
 * RE_ParagraphStyle objects are used as character attributes on
 * {@link RE_AttributedString} objects.
 *
 * @package    Text
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_ParagraphStyle
{
  /**** Class Constants ****/
  
  
  /* Text Alignment */
  
    /**
     * Natural text alignment. Left-aligned for left-to-right writing systems.
     * Right-aligned for right-to-left writing systems.
     */
    const ALIGN_NATURAL = 0;
    
    /**
     * Left-aligned text.
     */
    const ALIGN_LEFT = 1;
    
    /**
     * Center-aligned text.
     */
    const ALIGN_CENTER = 2;
    
    /**
     * Right-aligned text.
     */
    const ALIGN_RIGHT = 3;
    
    /**
     * Fully-justified text.
     */
    const ALIGN_JUSTIFIED = 4;
    
        
    
  /* Line Break Modes */
  
    /**
     * Wrap long lines of text at natural word boundaries.
     */
    const LINE_BREAK_WORD = 0;
    
    /**
     * Wrap long lines of text immediately before the first character that
     * doesn't fit.
     */
    const LINE_BREAK_CHARACTER = 1;
    
    /**
     * Long lines of text are not wrapped. They are clipped to the width of the
     * enclosing rectangle.
     */
    const LINE_BREAK_CLIP = 2;
    
    /**
     * If the line is too long, it is truncated such that the beginning of the
     * line is visible and the missing characters are indicated by an ellipsis
     * glyph at the end of the string.
     */
    const LINE_BREAK_TRUNCATE_TAIL = 3;

    /**
     * If the line is too long, it is truncated such that the end of the line
     * is visible and the missing characters are indicated by an ellipsis glyph
     * at the beginning of the string.
     */
    const LINE_BREAK_TRUNCATE_HEAD = 4;

    /**
     * If the line is too long, it is truncated such that both the beginning
     * and the end of the line are visible and the missing characters are
     * indicated by an ellipsis glyph in the middle of the string.
     */
    const LINE_BREAK_TRUNCATE_MIDDLE = 5;
  
  
  
  /**** Instance Variables ****/
  
    /**
     * Text alignment. Use the ALIGN_ constants defined in this class.
     * @var integer
     */
    protected $_alignment = RE_ParagraphStyle::ALIGN_NATURAL;
    
    /**
     * The distance in points from the head edge (the left edge for left-to-right
     * writing systems) of the enclosing rectangle to the beginning of all
     * lines of text in the paragraph. Must be positive or zero.
     * @var float
     */
    protected $_headMargin = 0.0;

    /**
     * The distance in points relative to the head margin for the first line of
     * text in the paragraph. Positive values create an 'indent,' negative 
     * values create an 'outdent.'
     * @var float
     */
    protected $_firstLineIndent = 0.0;
    
    /**
     * The distance in points from the head edge (the left edge for left-to-right
     * writing systems) of the enclosing rectangle to the end of all lines of
     * text in the paragraph. Must be positive or zero.
     * @var float
     */
    protected $_tailMargin = 0.0;
  
    /**
     * The minimum height in points that any line of text may occupy, regardless
     * of font size. Must be positive or zero. Zero means no minimum.
     * @var float
     */
    protected $_minimumLineHeight = 0.0;
  
    /**
     * The maximum height in points that any line of text may occupy, regardless
     * of font size. Zero means no maximum.
     * @var float
     */
    protected $_maximumLineHeight = 0.0;
    
    /**
     * Line break mode. Use the LINEBREAK_ constants defined in this class.
     * @var integer
     */
    protected $_lineBreakMode = RE_ParagraphStyle::LINE_BREAK_WORD;
    
    /**
     * The additional distance in points added between consecutive lines of 
     * text. Added after applying the line height multiple. Must be positive or
     * zero.
     * @var float
     */
    protected $_lineSpacing = 0.0;
    
    /**
     * The additional distance in points between the top of the paragraph and
     * its first line of text. Must be positive or zero.
     * @var float
     */
    protected $_paragraphSpacingBefore = 0.0;
    
    /**
     * The additional distance in points after the final line of text in the
     * paragraph. Must be positive or zero.
     * @var float
     */
    protected $_paragraphSpacingAfter = 0.0;
    
    /**
     * The line height multiple. Used to calculate line spacing. Must be
     * positive or zero.
     * @var float
     */
    protected $_lineHeightMultiple = 0.0;
    
    /**
     * Array of {@link RE_TabStop} objects for the paragraph.
     * @var array
     */
    protected $_tabStops = array();
    
    /**
     * The default tab interval in points for the virtual left-aligned tabs that
     * exist after the last real tab stop in {@link $_tabStops}. Must be
     * positive or zero. If zero, there are no virtual tab stops.
     * @var float.
     */
    protected $_defaultTabInterval = 0.0;
  
    
    
  /**** Class Methods ****/
  
    /**
     * Returns a shared paragraph style object that is always initailized to
     * the default values.
     * 
     * WARNING: You should consider this default paragraph style immutable! It
     * may already be in use by many different objects, and changing it while
     * in use may cause undesirable side-effects. The object returned by this
     * method may be changed to an immutable variant in the future without
     * notice.
     * 
     * @return RE_ParagraphStyle
     */
    public static function defaultParagraphStyle()
    {
        static $paragraphStyle = null;
        if (! isset($paragraphStyle)) {
            $paragraphStyle = new self();
            /** @todo Set default tabs? */
        }
        return $paragraphStyle;
    }
  
  
  
  /**** Public Interface ****/


  /* Accessor Methods */
  
    /**
     * Returns the text alignment.
     *
     * @return integer One of the ALIGN_ constants defined in this class.
     */
    public function getAlignment()
    {
        return $this->_alignment;
    }
    
    /**
     * Sets the text alignment.
     *
     * The alignment must be one of the following values:
     *  - {@link RE_ParagraphStyle::ALIGN_NATURAL}
     *  - {@link RE_ParagraphStyle::ALIGN_LEFT}
     *  - {@link RE_ParagraphStyle::ALIGN_CENTER}
     *  - {@link RE_ParagraphStyle::ALIGN_RIGHT}
     *  - {@link RE_ParagraphStyle::ALIGN_JUSTIFIED}
     *
     * @param integer $alignment
     * @throws InvalidArgumentException if the alignment type is invalid.
     */
    public function setAlignment($alignment)
    {
        switch ($alignment) {
        case RE_ParagraphStyle::ALIGN_NATURAL:
        case RE_ParagraphStyle::ALIGN_LEFT:
        case RE_ParagraphStyle::ALIGN_CENTER:
        case RE_ParagraphStyle::ALIGN_RIGHT:
        case RE_ParagraphStyle::ALIGN_JUSTIFIED:
            $this->_alignment = $alignment;
            break;
        default:
            throw new InvalidArgumentException("Invalid alignment type: $alignment");
        }
    }
    
    /**
     * Returns the head margin.
     *
     * @return float
     */
    public function getHeadMargin()
    {
        return $this->_headMargin;
    }
    
    /**
     * Sets the head margin.
     *
     * The head margin is the distance in points from the head edge (the left
     * edge for left-to-right writing systems) of the enclosing rectangle to the
     * beginning of all lines of text in the paragraph.
     *
     * @param float $headMargin Must be positive or zero.
     * @throws RangeException if the head margin is negative.
     */
    public function setHeadMargin($headMargin)
    {
        $margin = (float)$headMargin;
        if ($margin < 0) {
            throw new RangeException("Head margin may not be negative: $headMargin");
        }
        $this->_headMargin = $margin;
    }
  
    /**
     * Returns the first line indent.
     *
     * @return float
     */
    public function getFirstLineIndent()
    {
        return $this->_firstLineIndent;
    }
    
    /**
     * Sets the first line indent.
     *
     * The first line indent is the distance in points relative to the head
     * margin for the first line of text in the paragraph. Positive values
     * create an 'indent,' negative values create an 'outdent.'
     *
     * @param float $firstLineIndent
     */
    public function setFirstLineIndent($firstLineIndent)
    {
        $this->_firstLineIndent = (float)$firstLineIndent;
    }
  
    /**
     * Returns the tail margin.
     *
     * @return float
     */
    public function getTailMargin()
    {
        return $this->_tailMargin;
    }
    
    /**
     * Sets the tail margin.
     *
     * If the tail margin is a positive number, it is the distance in points
     * from the head edge (the left edge for left-to-right writing systems)
     * of the enclosing rectangle to the end of all lines of text in the
     * paragraph, defining an absolute width.
     * 
     * If the tail margin is zero or negative, it is the distance in points
     * from the tail edge (the right edge for left-to-right writing systems)
     * of the enclosing rectangle, creating a more traditional margin.
     *
     * @param float $tailMargin Must be positive or zero.
     */
    public function setTailMargin($tailMargin)
    {
        $this->_tailMargin = (float)$tailMargin;
    }
  
    /**
     * Returns the minimum line height.
     *
     * @return float
     */
    public function getMinimumLineHeight()
    {
        return $this->_minimumLineHeight;
    }
    
    /**
     * Sets the minimum line height.
     *
     * The minimum height in points that any line of text may occupy, regardless
     * of font size. Zero means no minimum.
     *
     * @param float $minimumLineHeight Must be positive or zero.
     * @throws RangeException if the minimum line height is negative.
     */
    public function setMinimumLineHeight($minimumLineHeight)
    {
        $height = (float)$minimumLineHeight;
        if ($height < 0) {
            throw new RangeException("Minimum line height may not be negative: $minimumLineHeight");
        }
        $this->_minimumLineHeight = $height;
    }
  
    /**
     * Returns the maximum line height.
     *
     * @return float
     */
    public function getMaximumLineHeight()
    {
        return $this->_maximumLineHeight;
    }
    
    /**
     * Sets the maximum line height.
     *
     * The maximum height in points that any line of text may occupy, regardless
     * of font size. Zero means no maximum.
     *
     * @param float $maximumLineHeight Must be positive or zero.
     * @throws RangeException if the minimum line height is negative.
     */
    public function setMaximumLineHeight($maximumLineHeight)
    {
        $height = (float)$maximumLineHeight;
        if ($height < 0) {
            throw new RangeException("Maximum line height may not be negative: $maximumLineHeight");
        }
        $this->_maximumLineHeight = $height;
    }
  
    /**
     * Returns the line break mode.
     *
     * @return integer One of the LINEBREAK_ constants defined in this class.
     */
    public function getLineBreakMode()
    {
        return $this->_lineBreakMode;
    }
    
    /**
     * Sets the line break mode.
     *
     * The line break mode must be one of the following values:
     *  - {@link RE_ParagraphStyle::LINE_BREAK_WORD}
     *  - {@link RE_ParagraphStyle::LINE_BREAK_CHARACTER}
     *  - {@link RE_ParagraphStyle::LINE_BREAK_CLIP}
     *  - {@link RE_ParagraphStyle::LINE_BREAK_TRUNCATE_TAIL}
     *  - {@link RE_ParagraphStyle::LINE_BREAK_TRUNCATE_HEAD}
     *  - {@link RE_ParagraphStyle::LINE_BREAK_TRUNCATE_MIDDLE}
     *
     * @param integer $lineBreakMode
     * @throws InvalidArgumentException if the line break mode is invalid.
     */
    public function setLineBreakMode($lineBreakMode)
    {
        switch ($lineBreakMode) {
        case RE_ParagraphStyle::LINE_BREAK_WORD:
        case RE_ParagraphStyle::LINE_BREAK_CHARACTER:
        case RE_ParagraphStyle::LINE_BREAK_CLIP:
        case RE_ParagraphStyle::LINE_BREAK_TRUNCATE_TAIL:
        case RE_ParagraphStyle::LINE_BREAK_TRUNCATE_HEAD:
        case RE_ParagraphStyle::LINE_BREAK_TRUNCATE_MIDDLE:
            $this->_lineBreakMode = $lineBreakMode;
            break;
        default:
            throw new InvalidArgumentException("Invalid line break mode: $lineBreakMode");
        }
    }
    
    /**
     * Returns the line spacing.
     *
     * @return float
     */
    public function getLineSpacing()
    {
        return $this->_lineSpacing;
    }
    
    /**
     * Sets the line spacing.
     *
     * The additional distance in points added between consecutive lines of 
     * text. Added after applying the line height multiple.
     *
     * @param float $lineSpacing Must be positive or zero.
     * @throws RangeException if the line spacing is negative.
     */
    public function setLineSpacing($lineSpacing)
    {
        $spacing = (float)$lineSpacing;
        if ($spacing < 0) {
            throw new RangeException("Line spacing may not be negative: $lineSpacing");
        }
        $this->_lineSpacing = $spacing;
    }
  
    /**
     * Returns the spacing before the paragraph.
     *
     * @return float
     */
    public function getParagraphSpacingBefore()
    {
        return $this->_paragraphSpacingBefore;
    }
    
    /**
     * Sets the spacing before the paragraph.
     *
     * Sets the additional distance in points between the top of the paragraph 
     * and its first line of text.
     *
     * @param float $paragraphSpacingBefore Must be positive or zero.
     * @throws RangeException if the paragraph spacing before is negative.
     */
    public function setParagraphSpacingBefore($paragraphSpacingBefore)
    {
        $spacing = (float)$paragraphSpacingBefore;
        if ($spacing < 0) {
            throw new RangeException("Paragraph spacing before may not be negative: $paragraphSpacingBefore");
        }
        $this->_paragraphSpacingBefore = $spacing;
    }
  
    /**
     * Returns the spacing after the paragraph.
     *
     * @return float
     */
    public function getParagraphSpacingAfter()
    {
        return $this->_paragraphSpacingAfter;
    }
    
    /**
     * Sets the spacing after the paragraph.
     *
     * Sets the additional distance in points after the final line of text in
     * the paragraph.
     *
     * @param float $paragraphSpacingAfter Must be positive or zero.
     * @throws RangeException if the paragraph spacing after is negative.
     */
    public function setParagraphSpacingAfter($paragraphSpacingAfter)
    {
        $spacing = (float)$paragraphSpacingAfter;
        if ($spacing < 0) {
            throw new RangeException("Paragraph spacing after may not be negative: $paragraphSpacingAfter");
        }
        $this->_paragraphSpacingAfter = $spacing;
    }
  
    /**
     * Returns the line height multiple.
     *
     * @return float
     */
    public function getLineHeightMultiple()
    {
        return $this->_lineHeightMultiple;
    }
    
    /**
     * Sets the line height multiple.
     *
     * Used to calculate line spacing. The natural line height is multiplied
     * by this value (if non-zero) before being constrained by the minimum
     * and maximum line heights.
     *
     * @param float $lineHeightMultiple Must be positive or zero.
     * @throws RangeException if the paragraph spacing after is negative.
     */
    public function setLineHeightMultiple($lineHeightMultiple)
    {
        $multiple = (float)$lineHeightMultiple;
        if ($multiple < 0) {
            throw new RangeException("Line height multiple may not be negative: $lineHeightMultiple");
        }
        $this->_lineHeightMultiple = $multiple;
    }
    
    /**
     * Returns an array of the current tab stops sorted by location. May be empty.
     * 
     * @return array
     */
    public function getTabStops()
    {
        return $this->_tabStops;
    }
    
    /**
     * Adds a new tab stop. If a tab stop already exists at the same location,
     * it is replaced.
     *
     * @param RE_TabStop $tabStop
     */
    public function addTabStop(RE_TabStop $tabStop)
    {
        $this->_tabStops[$tabStop->getLocation()] = $tabStop;
        ksort($this->_tabStops);
    }
    
    /**
     * Removes the tab stop at the specified location. If a RE_TabStop
     * object is provided, uses its location.
     *
     * @param float|RE_TabStop $location
     */
    public function removeTabStop($location)
    {
        if ($location instanceof RE_TabStop) {
            $location = $location->getLocation();
        }
        unset($this->_tabStops[$location]);
    }
    
    /**
     * Replaces the current tab stops with those contained in the array.
     *
     * @param array $tabStops Array of {@link RE_TabStop} objects.
     * @throws InvalidArgumentException if the array does not contain tab
     *   stop objects.
     */
    public function setTabStops($tabStops)
    {
        $newTabStops = array();
        foreach ($tabStops as $tabStop) {
            if (! $tabStop instanceof RE_TabStop) {
                throw new InvalidArgumentException("Array must only contain RE_TabStop objects");
            }
            $newTabStops[$tabStop->getLocation()] = $tabStop;
        }
        ksort($newTabStops);
        $this->_tabStops = $newTabStops;
    }
  
    /**
     * Returns the default tab interval.
     *
     * @return float
     */
    public function getDefaultTabInterval()
    {
        return $this->_defaultTabInterval;
    }
    
    /**
     * Sets the default tab interval.
     *
     * Sets the default tab interval in points for the virtual left-aligned tabs
     * that exist after the last real tab stop in {@link $_tabStops}. If zero,
     * there are no virtual tab stops.
     *
     * @param float $defaultTabInterval Must be positive or zero.
     * @throws RangeException if the default tab interval is negative.
     */
    public function setDefaultTabInterval($defaultTabInterval)
    {
        $interval = (float)$defaultTabInterval;
        if ($interval < 0) {
            throw new RangeException("Default tab interval may not be negative: $defaultTabInterval");
        }
        $this->_defaultTabInterval = $interval;
    }
  
}
