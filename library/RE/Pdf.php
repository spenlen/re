<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Pdf
 * @subpackage Core
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/* This file includes all of the PDF generation classes, maintaining
 * the correct dependency order.
 */


/**** PDF Core ****/


/* Base Classes */

/** RE_Pdf_Exception */
require_once 'RE/Pdf/Exception.php';

/** RE_Pdf_Object */
require_once 'RE/Pdf/Object.php';


/* Scalar Object Types **/

/** RE_Pdf_Object_Boolean */
require_once 'RE/Pdf/Object/Boolean.php';

/** RE_Pdf_Object_Name */
require_once 'RE/Pdf/Object/Name.php';

/** RE_Pdf_Object_Null */
require_once 'RE/Pdf/Object/Null.php';

/** RE_Pdf_Object_Numeric */
require_once 'RE/Pdf/Object/Numeric.php';

/** RE_Pdf_Object_Numeric_Integer */
require_once 'RE/Pdf/Object/Numeric/Integer.php';

/** RE_Pdf_Object_String */
require_once 'RE/Pdf/Object/String.php';


/* Collection Object Types */

/** RE_Pdf_Object_Array */
require_once 'RE/Pdf/Object/Array.php';

/** RE_Pdf_Object_Dictionary */
require_once 'RE/Pdf/Object/Dictionary.php';


/* Stream Filters */

/** RE_Pdf_Filter */
require_once 'RE/Pdf/Filter.php';

/** RE_Pdf_Filter_Interface */
require_once 'RE/Pdf/Filter/Interface.php';

/** RE_Pdf_Filter_AsciiHex */
require_once 'RE/Pdf/Filter/AsciiHex.php';

/** RE_Pdf_Filter_Compression */
require_once 'RE/Pdf/Filter/Compression.php';

/** RE_Pdf_Filter_Compression_Flate */
require_once 'RE/Pdf/Filter/Compression/Flate.php';


/* Complex Object Types */

/** RE_Pdf_Object_Indirect */
require_once 'RE/Pdf/Object/Indirect.php';

/** RE_Pdf_Object_StreamDataProvider_Interface */
require_once 'RE/Pdf/Object/StreamDataProvider/Interface.php';

/** RE_Pdf_Object_StreamDataProvider_Buffer */
require_once 'RE/Pdf/Object/StreamDataProvider/Buffer.php';

/** RE_Pdf_Object_StreamDataProvider_FileParser */
require_once 'RE/Pdf/Object/StreamDataProvider/FileParser.php';

/** RE_Pdf_Object_Stream */
require_once 'RE/Pdf/Object/Stream.php';


/* Geometry */

/** RE_Rect */
require_once 'RE/Rect.php';


/* Pseudo-Objects (Common PDF Data Structures) */

/** RE_Pdf_Object_Rectangle */
require_once 'RE/Pdf/Object/Rectangle.php';



/**** Document Structure ****/


/* PDF Output */

/** RE_Pdf_OutputDestination_Interface */
require_once 'RE/Pdf/OutputDestination/Interface.php';

/** RE_Pdf_Canvas_Interface */
require_once 'RE/Pdf/Canvas/Interface.php';

/** RE_Pdf_ContentStream */
require_once 'RE/Pdf/ContentStream.php';


/* Page and Page Hierarchy */

/** RE_Pdf_Page */
require_once 'RE/Pdf/Page.php';

/** RE_Pdf_Page_Tree */
require_once 'RE/Pdf/Page/Tree.php';


/* Document */

/** RE_Pdf_Object_Extraction_Interface */
require_once 'RE/Pdf/Object/Extraction/Interface.php';

/** RE_Pdf_Document_Catalog */
require_once 'RE/Pdf/Document/Catalog.php';

/** RE_Pdf_Document_Info */
require_once 'RE/Pdf/Document/Info.php';

/** RE_Pdf_Document_Trailer */
require_once 'RE/Pdf/Document/Trailer.php';

/** RE_Pdf_Document */
require_once 'RE/Pdf/Document.php';



/**** PDF Graphics ****/


/* Color */

/** RE_Color */
require_once 'RE/Color.php';


/* Drawing */

/** RE_Path */
require_once 'RE/Path.php';

/** RE_Pdf_Image */
require_once 'RE/Pdf/Image.php';

/** RE_Pdf_Image_Jpeg */
require_once 'RE/Pdf/Image/Jpeg.php';

/** RE_Pdf_Image_Png */
require_once 'RE/Pdf/Image/Png.php';

/** RE_Pdf_Image_Tiff */
require_once 'RE/Pdf/Image/Tiff.php';

/** RE_Pdf_Image_Pdf */
require_once 'RE/Pdf/Image/Pdf.php';



/**** Text Layout ****/


/* Fonts */

/** RE_Font_Cmap */
require_once 'RE/Font/Cmap.php';

/** RE_Pdf_Font */
require_once 'RE/Pdf/Font.php';

/** RE_Pdf_Font_Standard */
require_once 'RE/Pdf/Font/Standard.php';

/** RE_Pdf_Font_OpenType */
require_once 'RE/Pdf/Font/OpenType.php';

/** RE_Pdf_Font_OpenType_TrueType */
require_once 'RE/Pdf/Font/OpenType/TrueType.php';


/* Attributed Strings */

/** RE_AttributedString */
require_once 'RE/AttributedString.php';

/** RE_Pdf_Text */
require_once 'RE/Pdf/Text.php';


/* Layout */

/** RE_Pdf_TextContainer */
require_once 'RE/Pdf/TextContainer.php';

/** RE_Pdf_TextContainer_Rect */
require_once 'RE/Pdf/TextContainer/Rect.php';

/** RE_Pdf_TextContainer_Circle */
require_once 'RE/Pdf/TextContainer/Circle.php';

/** RE_Pdf_Typesetter */
require_once 'RE/Pdf/Typesetter.php';

/** RE_Pdf_Typesetter_Simple */
require_once 'RE/Pdf/Typesetter/Simple.php';

/** RE_Pdf_GlyphGenerator */
require_once 'RE/Pdf/GlyphGenerator.php';

/** RE_Pdf_LayoutManager */
require_once 'RE/Pdf/LayoutManager.php';

/** RE_Pdf_LayoutManager_DelegateInterface */
require_once 'RE/Pdf/LayoutManager/DelegateInterface.php';
