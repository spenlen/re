<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Text
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */

/** RE_Range */
require_once 'RE/Range.php';

/** RE_ParagraphStyle */
require_once 'RE/ParagraphStyle.php';


/**
 * A RE_AttributedString object represents a Unicode string, providing the
 * ability to add attributes to arbitrary ranges of characters within the
 * string. These attributes generally describe the style and layout intent of
 * specific characters (font, size, color, kerning, paragraph style, etc.).
 *
 * When assigning attribute values, you refer to the attribute by name, using
 * the ATTRIBUTE_ constants defined in this class. You may also create your own
 * custom attributes by assigning them a unique name. Attribute values can be
 * any PHP primitive data type: string, number, object, etc.
 *
 * RE_AttributedString stores its string internally as an array of Unicode
 * characters, providing a high-performance bridge to the other text layout
 * classes within the framework. Source strings may be supplied in any encoding
 * supported by {@link iconv()}.
 *
 * @package    Text
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_AttributedString
{
  /**** Class Constants ****/


  /* Standard Attributes */

    /**
     * Font (typeface). The value is a string and is the PostScript name of the
     * font. You can also set the value by supplying a {@link RE_Pdf_Font} object.
     * The default value is 'Helvetica'.
     */
    const ATTRIBUTE_FONT = 'RE_Attribute_Font';

    /**
     * Font size in points. The value is a positive float or zero. Zero means
     * glyphs for the characters will not be drawn. The default value is 12.0.
     */
    const ATTRIBUTE_FONT_SIZE = 'RE_Attribute_FontSize';

    /**
     * Color of the rectangular background behind the glyphs. The value is a
     * {@link RE_Color} object or null for no background color. You can also
     * set the value by supplying an HTML color as a string. The default value
     * is null.
     */
    const ATTRIBUTE_BACKGROUND_COLOR = 'RE_Attribute_BackgroundColor';

    /**
     * Glyph fill color. The value is a {@link RE_Color} object. You can also
     * set the value by supplying an HTML color as a string. The default value
     * is black.
     */
    const ATTRIBUTE_COLOR = 'RE_Attribute_Color';

    /**
     * Width in points of the gylph stroke (outline). The value is a float. Zero
     * means fill only, positive values mean stroke only, and negative values
     * mean both fill and stroke. The default value is 0.0.
     */
    const ATTRIBUTE_STROKE_WIDTH = 'RE_Attribute_StrokeWidth';

    /**
     * Glyph stroke (outline) color. The value is a {@link RE_Color} object or
     * null to use the glyph fill color. You can also set the value by supplying
     * an HTML color as a string. The default value is null.
     */
    const ATTRIBUTE_STROKE_COLOR = 'RE_Attribute_StrokeColor';

    /**
     * Style of the underline effect that is applied to the glyphs. The value is
     * an integer and is determined by OR-ing the UNDERLINE_ constants defined
     * below. The default value is {@link UNDERLINE_STYLE_NONE}.
     */
    const ATTRIBUTE_UNDERLINE_STYLE = 'RE_Attribute_UnderlineStyle';

    /**
     * Color of the underline effect that is applied to the glyphs. The value is
     * a {@link RE_Color} object or null to use the glyph fill color. You can
     * also set the value by supplying an HTML color as a string. The default
     * value is null.
     */
    const ATTRIBUTE_UNDERLINE_COLOR = 'RE_Attribute_UnderlineColor';

    /**
     * Style of the strikethrough effect that is applied to the glyphs. The value
     * is an integer and is determined by OR-ing the UNDERLINE_ constants defined
     * below. The default value is {@link UNDERLINE_STYLE_NONE}.
     */
    const ATTRIBUTE_STRIKETHROUGH_STYLE = 'RE_Attribute_StrikethroughStyle';

    /**
     * Color of the strikethrough effect that is applied to the glyphs. The value
     * is a {@link RE_Color} object or null to use the glyph fill color. You can
     * also set the value by supplying an HTML color as a string. The default
     * value is null.
     */
    const ATTRIBUTE_STRIKETHROUGH_COLOR = 'RE_Attribute_StrikethroughColor';

    /**
     * Superscript level. The value is an integer. Positive values set successive
     * levels of superscript; negative value set successive levels of subscript.
     * The default value is 0.
     */
    const ATTRIBUTE_SUPERSCRIPT_LEVEL = 'RE_Attribute_SuperscriptLevel';

    /**
     * Distance in points from the text baseline where the glyphs should be
     * drawn. The value is a float. Positive values mean shift above the
     * baseline, negative values mean shift below the baseline. The default
     * value is 0.0.
     */
    const ATTRIBUTE_BASELINE_SHIFT = 'RE_Attribute_BaselineShift';

    /**
     * Distance in points to shift the glyph for the next character in the
     * string. The value is a float. Null means to use the kerning values in
     * the font, zero disables kerning altogether, positive values shift the
     * glyph farther away, and negative values shift it closer. The default
     * value is null.
     */
    const ATTRIBUTE_KERN = 'RE_Attribute_Kern';

    /**
     * Creates a hyperlink to other content. The value is the link's URI as a
     * string or null for no link. The default value is null.
     */
    const ATTRIBUTE_LINK = 'RE_Attribute_Link';

    /**
     * Paragraph style. Determines block-level layout settings such as line
     * height, line spacing, indentation, tab stops, etc. The value is a
     * {@link RE_ParagraphStyle} object. The default value is the shared default
     * returned by {@link RE_ParagraphStyle::defaultParagraphStyle()}.
     */
    const ATTRIBUTE_PARAGRAPH_STYLE = 'RE_Attribute_ParagraphStyle';


  /* Underline and Strikethrough Styles */

    /**
     * Do not draw an underline.
     */
    const UNDERLINE_STYLE_NONE = 0;

    /**
     * Draw a single-line underline.
     */
    const UNDERLINE_STYLE_SINGLE = 1;

    /**
     * Draw a double-line underline.
     */
    const UNDERLINE_STYLE_DOUBLE = 2;


  /* Private Class Constants - Not intended for use outside this class */

    /* Array indices used for internal attribute storage. */
    const _ATT_RANGE_INDEX = 0;
    const _ATT_VALUE_INDEX = 1;



  /**** Instance Variables ****/

    /**
     * Array of Unicode character codes (code points) comprising the string.
     * @var array
     */
    protected $_characters = array();

    /**
     * The character length of the string. Cached for performance.
     * @var integer
     */
    protected $_length = 0;

    /**
     * Array of attribute values for the string.
     * @var array
     */
    protected $_attributes = array();

    /**
     * The default character encoding method to use when interpreting appended
     * source strings.
     * @var string
     */
    protected $_defaultCharEncoding = 'UTF-8';



  /**** Public Interface ****/


  /* Object Lifecycle */

    /**
     * Initializes the text object with the contents of the string provided (may
     * be empty).
     *
     * If a character encoding method is provided, all strings supplied to
     * subsequent {@link appendString()} calls will be interpreted using the same
     * character encoding.
     *
     * @param string $string       (optional) Initial contents.
     * @param array  $attributes   (optional) Array of initial attribute name/value pairs.
     * @param string $charEncoding (optional) Default character encoding of source
     *   strings. If omitted, uses 'UTF-8'.
     */
    public function __construct($string = '', array $attributes = null, $charEncoding = 'UTF-8')
    {
        $this->_defaultCharEncoding = $charEncoding;
        $this->appendString($string, $attributes);
    }


  /* Object Magic Methods */

    /**
     * Returns the first 50 characters of the string in 'current locale'
     * encoding.
     *
     * Characters that cannot be represented in the 'current locale' encoding
     * are transliterated.
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->_length > 50) {
            return $this->getSubstring(new RE_Range(0, 50), '//TRANSLIT') . '...';
        } else {
            return $this->getSubstring(new RE_Range(0, $this->_length), '//TRANSLIT');
        }
    }


  /* String Information */

    /**
     * Returns the character length of the string.
     *
     * @return integer
     */
    public function getLength()
    {
        return $this->_length;
    }


  /* String Manipulation */

    /**
     * Replaces the entire string with the new source string, applying the
     * optional attributes to the new characters.
     *
     * @param string $string
     * @param array  $attributes   (optional) Array of attribute name/value pairs.
     * @param string $charEncoding (optional) Character encoding of source
     *   string. If omitted, uses the default encoding method specified when
     *   the object was created.
     */
    public function setString($string, array $attributes = null, $charEncoding = null)
    {
        $this->_characters = array();
        $this->_length     = 0;
        $this->_attributes = array();

        $this->appendString($string, $attributes, $charEncoding);
    }

    /**
     * Appends the source string to the end of the string, applying the optional
     * attributes to the new characters.
     *
     * @param string $string
     * @param array  $attributes   (optional) Array of attribute name/value pairs.
     * @param string $charEncoding (optional) Character encoding of source
     *   string. If omitted, uses the default encoding method specified when
     *   the object was created.
     */
    public function appendString($string, array $attributes = null, $charEncoding = null)
    {
        if (strlen($string) < 1) {
            return;
        }

        /* Convert to UTF-16BE encoding.
         */
        if (empty($charEncoding)) {
            $charEncoding = $this->_defaultCharEncoding;
        }
        if ($charEncoding != 'UTF-16BE') {
            $string = iconv($charEncoding, 'UTF-16BE//IGNORE', $string);
        }

        $startCharacterIndex = $this->_length;
        $stringLength = 0;

        $len = strlen($string);
        for ($i = 0; $i < $len; $i++) {
            $characterCode = (ord($string[$i]) << 8) | ord($string[++$i]);
            if (($characterCode >= 0xd800) && ($characterCode <= 0xdbff)) {  // this is the start of a surrogate pair
                $pairCode = (ord($string[++$i]) << 8) | ord($string[++$i]);
                $characterCode = ((($characterCode & 0x03ff) << 10) | ($pairCode & 0x03ff)) + 0x10000;
            }
            $stringLength++;
            $this->_characters[] = $characterCode;
        }
        $this->_length += $stringLength;

        /* Extend all existing attributes to cover the new character range.
         */
        foreach ($this->_attributes as $namedAttribute) {
            $attribute = end($namedAttribute);
            $attribute[RE_AttributedString::_ATT_RANGE_INDEX]->setLength(
                $attribute[RE_AttributedString::_ATT_RANGE_INDEX]->getLength() + $stringLength);
        }

        /* Apply new attributes for the new characters only.
         */
        if (! empty($attributes)) {
            $this->setAttributesForRange($attributes, new RE_Range($startCharacterIndex, $stringLength));
        }
    }

    /**
     * Appends the text contained in the HTML to the end of the string, applying
     * the optional attributes to the new characters.
     * 
     * The HTML should include a <meta http-equiv="Content-Type"> tag in the <head>
     * element to define the character encoding used by the HTML. If the <head> or
     * <meta> elements are missing, they will be added and UTF-8 encoding assumed.
     *
     * @param string $html
     * @param array  $attributes (optional) Array of attribute name/value pairs.
     */
    public function appendHTML($html, array $attributes = array())
    {
        $attributes = array_merge($this->getAttributesAtIndex(array(
            RE_AttributedString::ATTRIBUTE_FONT,
            RE_AttributedString::ATTRIBUTE_FONT_SIZE,
            RE_AttributedString::ATTRIBUTE_COLOR,
            RE_AttributedString::ATTRIBUTE_PARAGRAPH_STYLE,
            ), $this->_length - 1), $attributes);

        require_once 'RE/AttributedString/HTMLParser.php';
        $parser = new RE_AttributedString_HTMLParser();

        $parser->parseAndAppendToString($this, $html, $attributes);
    }


  /* Linguistic Units */

    /**
     * Returns the index of the nearest character before the index, and within
     * the specified range, that can be used to start a new line of text.
     * Returns null if a line break opportunity could not be found in the
     * specified range.
     *
     * Used to determine linguistically-appropriate line breaks by applying the
     * Unicode Line Breaking Algorithm (UAX #14) described here:
     *   {@link http://www.unicode.org/unicode/reports/tr14/}.
     *
     * @param integer  $index
     * @param RE_Range $range
     * @return integer|null
     * @throws RangeException if any part of the range extends beyond the end
     *   of the string.
     */
    public function lineBreakBeforeIndex($index, RE_Range $range)
    {
        if ($range->maxRange() > $this->_length) {
            throw new RangeException('Range extends beyond end of string.');
        }

        /** @todo Actually implement UAX #14. */

        /* Until UAX #14 is implemented, line break locations are limited to
         * selected whitespace characters only.
         */
        $minLocation = $range->getLocation();
        $maxLocation = $range->maxRange() - 1;

        $location = $maxLocation;
        for (; $location >= $minLocation; $location--) {
            switch ($this->_characters[$location]) {

            /* Line Feed (LF) */
            case 0x000a:  // U+000A LINE FEED (LF)
                return $location + 1;

            /* Carriage Return (CR) */
            case 0x000d:  // U+000D CARRIAGE RETURN (CR)
                return $location + 1;

            /* Mandatory Break (BK) and Next Line (NL) */
            case 0x000b:  // U+000B LINE TABULATION
            case 0x000c:  // U+000C FORM FEED (FF)
            case 0x0085:  // U+0085 NEXT LINE (NEL)
            case 0x2028:  // U+2028 LINE SEPARATOR
            case 0x2029:  // U+2029 PARAGRAPH SEPARATOR
                return $location + 1;

            /* Selected Whitespace Characters */
            case 0x0009:  // U+0009 CHARACTER TABULATION
            case 0x0020:  // U+0020 SPACE
                return $location + 1;

            }
        }

        return null;
    }

    /**
     * Returns the index of the nearest character before the index, and within
     * the specified range, that can be used to start a new line of text by
     * hyphenating. Returns null if no hyphenation points could be found in the
     * specified range.
     *
     * @todo Determine how to integrate hyphenation dictionaries and implement
     *   this method.
     *
     * @param integer  $index
     * @param RE_Range $range
     * @return integer|null
     * @throws RangeException if any part of the range extends beyond the end
     *   of the string.
     */
    public function lineBreakByHyphenatingBeforeIndex($index, RE_Range $range)
    {
        return null;
    }


  /* Character Access */

    /**
     * Returns the entire string as an array of Unicode character codes
     * (code points). The keys of the returned array correspond to the
     * character's index within the string.
     *
     * @return array
     */
    public function getCharacters()
    {
        return $this->_characters;
    }

    /**
     * Returns a range of the string as an array of Unicode character codes
     * (code points). The keys of the returned array correspond to the
     * character indices within the string.
     *
     * @param RE_Range $range Desired character range.
     * @return array
     * @throws RangeException if any part of the range extends beyond the end
     *   of the string.
     */
    public function getCharactersInRange(RE_Range $range)
    {
        if ($range->isEmpty()) {
            return array();
        }
        if ($range->maxRange() > $this->_length) {
            throw new RangeException('Range extends beyond end of string.');
        }
        if ($range->getLength() == $this->_length) {
            return $this->_characters;
        }
        return array_slice($this->_characters, $range->getLocation(), $range->getLength(), true);
    }

    /**
     * Returns the character at the specified character index.
     *
     * @param integer  $index
     * @return integer
     * @throws RangeException if the index is outside the bounds of the string.
     */
    public function getCharacterAtIndex($index)
    {
        if (($index < 0) || ($index >= $this->_length)) {
            throw new RangeException("Invalid character index: $index");
        }
        return $this->_characters[$index];
    }

    /**
     * Returns the entire string using the specified character encoding.
     *
     * @param string $charEncoding (optional) Character encoding of resulting
     *   string. If omitted, uses UTF-8.
     * @return string
     */
    public function getString($charEncoding = 'UTF-8')
    {
        return $this->getSubstring(new RE_Range(0, $this->_length), $charEncoding);
    }

    /**
     * Returns a portion of the string using the specified character encoding.
     *
     * @param RE_Range $range        Desired character range.
     * @param string   $charEncoding (optional) Character encoding of
     *   resulting string. If omitted, uses UTF-8.
     * @return string
     * @throws RangeException if any part of the range extends beyond the end
     *   of the string.
     */
    public function getSubstring(RE_Range $range, $charEncoding = 'UTF-8')
    {
        $maxRange = $range->maxRange();
        if ($maxRange > $this->_length) {
            throw new RangeException('Range extends beyond end of string.');
        }

        /** @todo Consider storing an actual string copy that can be used with
         *    iconv_substring instead... Need to decide how frequently this
         *    method will actually be used and whether it performs adequately
         *    as-is.
         */
        $string = '';

        for ($i = $range->getLocation(); $i < $maxRange; $i++) {
            $characterCode = $this->_characters[$i];
            if ($characterCode > 0xffff) {    // must use a surrogate pair
                $characterCode -= 0x10000;
                $highWord = 0xd800 | (($characterCode >> 10) & 0x03ff);
                $lowWord  = 0xdc00 | ($characterCode & 0x03ff);
                $string .= chr(($highWord >> 8) & 0xff) . chr($highWord & 0xff)
                         . chr(($lowWord  >> 8) & 0xff) . chr($lowWord  & 0xff);
            } else if ($characterCode > 0xff) {
                $string .= chr(($characterCode >> 8) & 0xff) . chr($characterCode & 0xff);
            } else {
                $string .= "\x00" . chr($characterCode);
            }
        }

        if ($charEncoding != 'UTF-16BE') {
            $string = iconv('UTF-16BE', $charEncoding, $string);
        }

        return $string;
    }


  /* Attribute Manipulation */

    /**
     * Sets the specified attribute to the given value for the specified
     * character range of the string.
     *
     * @param string   $attributeName
     * @param mixed    $attributeValue
     * @param RE_Range $range
     * @throws RangeException if any part of the range extends beyond the end
     *   of the string.
     */
    public function setAttributeForRange($attributeName, $attributeValue, RE_Range $range)
    {
        if ($range->maxRange() > $this->_length) {
            throw new RangeException('Range extends beyond end of string.');
        }
        if ($range->isEmpty()) {
            return;
        }

        $attributeValue = $this->_checkAttributeValue($attributeName, $attributeValue);

        $newLocation = $range->getLocation();
        $newMaxRange = $range->maxRange();

        if (isset($this->_attributes[$attributeName])) {
            foreach ($this->_attributes[$attributeName] as $location => $attribute) {

                $attributeRange = $attribute[RE_AttributedString::_ATT_RANGE_INDEX];

                /* If the old range is identical to the new, we're done. Change
                 * the value of the old range and return.
                 */
                if ($attributeRange->isEqualToRange($range)) {
                    $this->_attributes[$attributeName][$location][RE_AttributedString::_ATT_VALUE_INDEX] = $attributeValue;
                    return;
                }

                /* If the new range is completely contained within the old
                 * range, make a hole for the new range.
                 */
                if ($range->isSubrangeOfRange($attributeRange)) {
                    $newEndRange = clone $attributeRange;
                    $newEndRange->setLength($newEndRange->maxRange() - $newMaxRange);
                    $newEndRange->setLocation($newMaxRange);
                    if (! $newEndRange->isEmpty()) {
                        $this->_attributes[$attributeName][$newEndRange->getLocation()] = array(
                            RE_AttributedString::_ATT_RANGE_INDEX => $newEndRange,
                            RE_AttributedString::_ATT_VALUE_INDEX => $attribute[RE_AttributedString::_ATT_VALUE_INDEX]
                            );
                    }
                    $attributeRange->setLength($newLocation - $location);
                    /* We can stop looking now--ranges are stored in ascending
                     * order by location with no overlap.
                     */
                    break;
                }

                /* If the old range is completely contained within the new
                 * range, it is no longer needed. Delete it.
                 */
                if ($attributeRange->isSubrangeOfRange($range)) {
                    unset($this->_attributes[$attributeName][$location]);
                    continue;
                }

                /* If the old range does not otherwise intersect with the new
                 * range, skip it.
                 */
                if (! $attributeRange->intersectsRange($range)) {
                    continue;
                }

                /* If the old range's location is before the new range, adjust
                 * its length.
                 */
                if ($location < $newLocation) {
                    $attributeRange->setLength($newLocation - $location);
                    continue;
                }

                /* Otherwise, the old range's location is after the new range.
                 * Adjust its location and length.
                 */
                $attributeRange->setLength($attributeRange->maxRange() - $newMaxRange);
                $attributeRange->setLocation($newMaxRange);

            }
        }

        $this->_attributes[$attributeName][$newLocation] = array(
            RE_AttributedString::_ATT_RANGE_INDEX => clone $range,
            RE_AttributedString::_ATT_VALUE_INDEX => $attributeValue
            );

        /* Sort the ranges in ascending order by location.
         */
        ksort($this->_attributes[$attributeName]);
    }

    /**
     * Sets multiple attribute values for the specified character range of the
     * string, all at once.
     *
     * @param array    $attributes Associative array of attribute names/values.
     * @param RE_Range $range
     * @throws RangeException if any part of the range extends beyond the end
     *   of the string.
     */
    public function setAttributesForRange(array $attributes, RE_Range $range)
    {
        foreach ($attributes as $attributeName => $attributeValue) {
            $this->setAttributeForRange($attributeName, $attributeValue, $range);
        }
    }

    /**
     * Returns the specified attribute value for the string at the specified
     * character index. Optionally sets $effectiveRange to a character range
     * for which the returned value also applies.
     *
     * Note that the range returned is not necessarily the maximum effective
     * range for the attribute value, merely some range for which it applies.
     * This is usually sufficient for most needs. However, if you need the
     * maximum range, use {@link getAttributeAtIndexLongestEffectiveRange()}
     * instead.
     *
     * If the attribute is not set at the requested index, returns null, and
     * $effectiveRange is set to a character range for which the attribute does
     * not exist.
     *
     * @param string   $attributeName
     * @param integer  $index
     * @param RE_Range $effectiveRange (optional)
     * @return mixed Attribute value or null if not set.
     * @throws RangeException if the index is outside the bounds of the string.
     */
    public function getAttributeAtIndex($attributeName, $index, RE_Range $effectiveRange = null)
    {
        if (($index < 0) || ($index >= $this->_length)) {
            if (! (($index == -1) && ($this->_length == 0))) {
                throw new RangeException("Invalid character index: $index");
            }
        }

        $attributeValue = null;

        if (! isset($this->_attributes[$attributeName])) {
            if (isset($effectiveRange)) {
                $effectiveRange->setLocation(0);
                $effectiveRange->setLength($this->_length);
            }

        } else {
            if (isset($effectiveRange)) {
                $effectiveRange->setLocation($index);
                $effectiveRange->setLength($this->_length - $index);
            }
            foreach ($this->_attributes[$attributeName] as $location => $attribute) {

                /* Attributes are stored internally in ascending order by range location.
                 * If we've overshot the index, there's no hope of finding a match. Stop now.
                 */
                if ($location > $index) {
                    if (isset($effectiveRange)) {
                        $effectiveRange->setLocation($index);
                        $effectiveRange->setLength($location - $index);
                    }
                    break;
                }

                /* Otherwise check to see if this is the range we're looking for.
                 */
                $attributeRange = $attribute[RE_AttributedString::_ATT_RANGE_INDEX];
                if ($attributeRange->containsLocation($index)) {
                    if (isset($effectiveRange)) {
                        $effectiveRange->setLocation($attributeRange->getLocation());
                        $effectiveRange->setLength($attributeRange->getLength());
                    }
                    $attributeValue = $attribute[RE_AttributedString::_ATT_VALUE_INDEX];
                    break;
                }

            }

        }

        if (! isset($attributeValue)) {
            switch ($attributeName) {
            case RE_AttributedString::ATTRIBUTE_BACKGROUND_COLOR:
            case RE_AttributedString::ATTRIBUTE_STROKE_COLOR:
            case RE_AttributedString::ATTRIBUTE_UNDERLINE_COLOR:
            case RE_AttributedString::ATTRIBUTE_STRIKETHROUGH_COLOR:
                /* These color attributes default to the foreground color. Maintain the
                 * already-calculated effective range.
                 */
                return $this->getAttributeAtIndex(RE_AttributedString::ATTRIBUTE_COLOR, $index);

            default:
                $attributeValue = $this->_getDefaultAttributeValue($attributeName);
            }
        }

        return $attributeValue;
    }

    /**
     * Returns the specified attribute values for the string at the specified
     * character index. Optionally sets $effectiveRange to a character range
     * for which the returned values also apply.
     *
     * Note that the range returned is not necessarily the maximum effective
     * range for the attribute values, merely some range for which they apply.
     * This is usually sufficient for most needs. However, if you need the
     * maximum range, use {@link getAttributesAtIndexLongestEffectiveRange()}
     * instead.
     *
     * @param array    $attributeNames
     * @param integer  $index
     * @param RE_Range $effectiveRange (optional)
     * @return array
     * @throws RangeException if the index is outside the bounds of the string.
     */
    public function getAttributesAtIndex(array $attributeNames, $index, RE_Range $effectiveRange = null)
    {
        if (isset($effectiveRange)) {
            $effectiveRange->setLocation($index);
            $effectiveRange->setLength($this->_length - $index);

            $attributeEffectiveRange = new RE_Range();
        }

        $attributes = array();

        if (isset($effectiveRange)) {
            foreach ($attributeNames as $attributeName) {
                $attributes[$attributeName] = $this->getAttributeAtIndex($attributeName, $index, $attributeEffectiveRange);
                $effectiveRange->setLength(min($effectiveRange->getLength(), $attributeEffectiveRange->getLength()));
            }

        } else {
            foreach ($attributeNames as $attributeName) {
                $attributes[$attributeName] = $this->getAttributeAtIndex($attributeName, $index);
            }

        }

        return $attributes;
    }



  /**** Internal Interface ****/

    /**
     * Returns the default value for the specified attribute. If the attribute
     * does not have a default value, returns null.
     *
     * Default attributes are managed in this way to distinguish them from
     * user-specified attributes, which may be the same values.
     *
     * @param string $attributeName
     * @return mixed
     */
    protected function _getDefaultAttributeValue($attributeName)
    {
        switch ($attributeName) {
        case RE_AttributedString::ATTRIBUTE_FONT:
            return 'Helvetica';

        case RE_AttributedString::ATTRIBUTE_FONT_SIZE:
            return 12.0;

        case RE_AttributedString::ATTRIBUTE_COLOR:
            return RE_Color::blackColor();

        case RE_AttributedString::ATTRIBUTE_STROKE_WIDTH:
        case RE_AttributedString::ATTRIBUTE_BASELINE_SHIFT:
            return 0.0;

        case RE_AttributedString::ATTRIBUTE_SUPERSCRIPT_LEVEL:
            return 0;

        case RE_AttributedString::ATTRIBUTE_UNDERLINE_STYLE:
        case RE_AttributedString::ATTRIBUTE_STRIKETHROUGH_STYLE:
            return RE_AttributedString::UNDERLINE_STYLE_NONE;

        case RE_AttributedString::ATTRIBUTE_PARAGRAPH_STYLE:
            return RE_ParagraphStyle::defaultParagraphStyle();

        }
        return null;
    }

    /**
     * Verifies that the value for the standard attribute is the correct type
     * and, if applicable, within the permissible range. Does nothing for
     * custom attributes.
     *
     * @param string $attributeName
     * @param mixed  $attributeValue
     * @throws InvalidArgumentException if the attribute value is the wrong
     *   type.
     * @throws RangeException if the attribute value is outside the valid range.
     */
    protected function _checkAttributeValue($attributeName, $attributeValue)
    {
        switch ($attributeName) {
            case RE_AttributedString::ATTRIBUTE_FONT:
                if ($attributeValue instanceof RE_Pdf_Font) {
                    $attributeValue = $attributeValue->getFontName(RE_Pdf_Font::NAME_POSTSCRIPT, '', '');
                } else if (empty($attributeValue)) {
                    throw new InvalidArgumentException("Attribute value may not be empty.");
                }
                break;

            case RE_AttributedString::ATTRIBUTE_BACKGROUND_COLOR:
            case RE_AttributedString::ATTRIBUTE_STROKE_COLOR:
            case RE_AttributedString::ATTRIBUTE_UNDERLINE_COLOR:
            case RE_AttributedString::ATTRIBUTE_STRIKETHROUGH_COLOR:
                if (is_null($attributeValue)) {
                    break;
                }
                // Break intentionally omitted
            case RE_AttributedString::ATTRIBUTE_COLOR:
                if (! $attributeValue instanceof RE_Color) {
                    $attributeValue = RE_Color::htmlColor($attributeValue);
                }
                break;

            case RE_AttributedString::ATTRIBUTE_FONT_SIZE:
                if ((float)$attributeValue < 0) {
                    throw new RangeException("Attribute value must be a positive number or zero.");
                }
                break;

            case RE_AttributedString::ATTRIBUTE_UNDERLINE_STYLE:
            case RE_AttributedString::ATTRIBUTE_STRIKETHROUGH_STYLE:
                switch ($attributeValue) {
                    case RE_AttributedString::UNDERLINE_STYLE_NONE:
                    case RE_AttributedString::UNDERLINE_STYLE_SINGLE:
                    case RE_AttributedString::UNDERLINE_STYLE_DOUBLE:
                        break;
                    default:
                        throw new InvalidArgumentException("Invalid underline style: $attributeValue");
                }
                break;

            case RE_AttributedString::ATTRIBUTE_PARAGRAPH_STYLE:
                if (! $attributeValue instanceof RE_ParagraphStyle) {
                    throw new InvalidArgumentException("Attribute value must be a RE_ParagraphStyle object.");
                }
                break;

        }

        return $attributeValue;
    }

}
