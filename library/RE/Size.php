<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Geometry
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/**
 * Represents a size in two dimensions: width and height.
 *
 * Frequently used in conjunction with {@link RE_Point} to represent
 * rectangles (see {@link RE_Rect}).
 *
 * @package    Geometry
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Size
{
  /**** Class Constants ****/

    /**
     * Precision for comparing equality of float values.
     */
    const FLOAT_COMPARISON_PRECISION = 1e-4;



  /**** Instance Variables ****/

    /**
     * Stores the width dimension.
     * @var float
     */
    public $width = 0.0;

    /**
     * Stores the height dimension.
     * @var float
     */
    public $height = 0.0;



  /**** Class Methods ****/

    /**
     * Returns a new RE_Size object from the string representation,
     * usually formatted as "{w, h}".
     *
     * The string is scanned for two numbers, which are used as the width and
     * height, in that order. If the string does not contain two numbers, any
     * numbers found are used, and the remaining values are assumed to be zero.
     * All values are interpreted as floats.
     * 
     * @param string $string
     * @return RE_Size
     */
    public static function fromString($string)
    {
        preg_match('/(-?\d+\.?\d*)(.+?(-?\d+\.?\d*))?/ms', $string, $matches);
        return new self((isset($matches[1]) ? $matches[1] : 0.0), (isset($matches[3]) ? $matches[3] : 0.0));
    }



  /**** Public Interface ****/


  /* Object Lifecycle */

    /**
     * Object constructor signatures:
     *
     *  - Initialize to an empty size:
     *      new RE_Size();
     *
     *  - Initialize to a specific width and height:
     *      new RE_Size(number $width, number $height);
     *
     *  - Clone an existing size:
     *      new RE_Size(RE_Size $size);
     *
     * Ordinarily, the width and height values are non-negative. You are not
     * prohibited from using negative values, but some methods that use
     * RE_Size objects may behave strangely if you do.
     *
     * @param mixed $param1
     * @param mixed $param2
     * @throws BadMethodCallException if the constructor arguments cannot be deduced.
     */
    public function __construct($param1 = null, $param2 = null)
    {
        if (isset($param1, $param2)) {
            /* Initialize with a specific width and height.
             */
            $this->width  = (float)$param1;
            $this->height = (float)$param2;

        } else if ((! isset($param1)) && (! isset($param2))) {
            /* Initialize to an empty size. Done.
             */

        } else if (($param1 instanceof RE_Size) &&
                   (! isset($param2))) {
            /* Clone an existing size. Copy the values.
             */
            $this->width  = $param1->width;
            $this->height = $param1->height;

        } else {
            throw new BadMethodCallException('Unrecognized arguments provided to object constructor.');
        }
    }


  /* Object Magic Methods */

    /**
     * Returns the width and height as a string of the form "{w, h}".
     *
     * @return string
     */
    public function __toString()
    {
        return '{' . $this->width . ', ' . $this->height . '}';
    }


  /* Accessor Methods */

    /**
     * Returns the width.
     *
     * @return float
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Sets the width.
     *
     * @param float $width
     */
    public function setWidth($width)
    {
        $this->width = (float)$width;
    }

    /**
     * Returns the height.
     *
     * @return float
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Sets the height.
     *
     * @param float $height
     */
    public function setHeight($height)
    {
        $this->height = (float)$height;
    }


  /* Test and Comparison */

    /**
     * Returns true if either the width or height dimension is less than or
     * equal to zero.
     *
     * @return boolean
     */
    public function isEmpty()
    {
        if (($this->width <= 0) || ($this->height <= 0)) {
            return true;
        } else if ((abs($this->width)  < RE_Size::FLOAT_COMPARISON_PRECISION) ||
                   (abs($this->height) < RE_Size::FLOAT_COMPARISON_PRECISION)) {
                       return true;
        }
        return false;
    }

    /**
     * Returns true if the specified size is equal in width and height to
     * this size.
     *
     * @param RE_Size $size
     * @return boolean
     */
    public function isEqualToSize(RE_Size $size)
    {
        if ((abs($this->width  - $size->width)  < RE_Size::FLOAT_COMPARISON_PRECISION) &&
            (abs($this->height - $size->height) < RE_Size::FLOAT_COMPARISON_PRECISION)) {
                return true;
        }
        return false;
    }

}
