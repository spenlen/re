<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Graphics
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */

/** RE_Path_Element */
require_once 'RE/Path/Element.php';


/**
 * RE_Path objects are used to draw geometric shapes using Bézier paths,
 * which are comprised of one or more linear, quadratic, and/or cubic curve
 * segments.
 * 
 * These segments are joined together to create subpaths, which determine the
 * way the path will be filled (see discussion in {@link setWindingRule()}).
 * A new subpath is created each time you call {@link moveTo()} or
 * {@link closePath()}. There may be many subpaths within a single RE_Path
 * object, allowing you to create arbitrarily complex shapes.
 *
 * For more information on the mathematics behind Bézier curves, see:
 *   http://en.wikipedia.org/wiki/Bezier_curves
 * 
 * @todo Add the following methods: moveToRelative(), lineToRelative(),
 *   curveToRelative()
 * 
 * @package    Graphics
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Path
{
  /**** Class Constants ****/


  /* Line Cap Styles */
  
    /**
     * Square path endpoints with no projection beyond the end of the path.
     */
    const LINE_CAP_BUTT = 0;
    
    /**
     * Rounded path endpoints which project half the line width beyond the end
     * of the path.
     */
    const LINE_CAP_ROUND = 1;
    
    /**
     * Square path endpoints which project half the line width beyond the end
     * of the path.
     */
    const LINE_CAP_SQUARE = 2;


  /* Line Join Styles */
  
    /**
     * Line segments are joined to form a mitered corner, as with a picture
     * frame. If the segments meet at too sharp an angle (determined by the
     * {@link $_miterLimit}), the miter will be beveled instead.
     */
    const LINE_JOIN_MITER = 0;
    
    /**
     * Line segments are joined with rounded corners.
     */
    const LINE_JOIN_ROUND = 1;
    
    /**
     * Line segments are joined with beveled corners.
     */
    const LINE_JOIN_BEVEL = 2;


  /* Winding Rules */

    /**
     * Determines whether a point is inside the path by drawing a ray in an
     * arbitrary direction and examining the path segments the ray crosses.
     * Starting at zero, the rule adds 1 for each left-to-right crossing and
     * subtracts 1 for each right-to-left crossing. If the result is non-zero,
     * the point is inside the path. Yields intuitive results for most simple
     * convex paths. This is the default winding rule.
     */
    const WINDING_RULE_NON_ZERO = 0;
    
    /**
     * Similar to {@link WINDING_RULE_NON_ZERO} but simply counts the number
     * of paths crossed, regardless of direction. If the result is even, the
     * point is outside; if odd, the point is inside.
     */
    const WINDING_RULE_EVEN_ODD = 1;



  /**** Class Variables ****/
  
    /**
     * Default width of the line used to stroke the path in points. Used if
     * no explicit line width is set for a given path object.
     * @var float
     */
    protected static $_defaultLineWidth = 1.0;
  
    /**
     * Default line cap style used when stroking the path. One of the LINE_CAP
     * constants defined in this class. Used if no explicit line cap style is
     * set for a given path object.
     * @var integer
     */
    protected static $_defaultLineCapStyle = RE_Path::LINE_CAP_BUTT;
  
    /**
     * Default line join style used when stroking the path. One of the LINE_JOIN
     * constants defined in this class. Used if no explicit line join style is
     * set for a given path object.
     * @var integer
     */
    protected static $_defaultLineJoinStyle = RE_Path::LINE_JOIN_MITER;
  
    /**
     * Default miter limit used when stroking the path. Used if no explicit
     * miter limit is set for a given path object.
     * @var float
     */
    protected static $_defaultMiterLimit = 10.0;
    
    /**
     * Default winding rule used when filling the path. One of the WINDING_RULE
     * constants defined in this class. Used if no explicit winding rule is set
     * for a given path object.
     * @var integer
     */
    protected static $_defaultWindingRule = RE_Path::WINDING_RULE_NON_ZERO;

    /**
     * Default flatness of the path, which determines how precise curves should
     * be rendered on the output device. Used if no explicit flatness is set
     * for a given path object.
     * @var float
     */
    protected static $_defaultFlatness = 1.0;
      
  

  /**** Instance Variables ****/
  
    /**
     * Array of {@link RE_Path_Element} objects comprising the path.
     * @var array
     */
    protected $_pathElements = array();
    
    /**
     * Width of the line used to stroke the path in points. Null means use
     * the default line width.
     * @var float
     */
    protected $_lineWidth = null;
  
    /**
     * Line cap style used when stroking the path. One of the LINE_CAP
     * constants defined in this class. Null means use the default line
     * cap style.
     * @var integer
     */
    protected $_lineCapStyle = null;
  
    /**
     * Line join style used when stroking the path. One of the LINE_JOIN
     * constants defined in this class. Null means use the default line
     * join style.
     * @var integer
     */
    protected $_lineJoinStyle = null;
  
    /**
     * Miter limit used when stroking the path. Null means use the default
     * miter limit.
     * @var float
     */
    protected $_miterLimit = null;
    
    /**
     * Array of float values that represent the lengths of the dashes and gaps
     * in points which are used to stroke the path. If the array is empty, a
     * solid line is used.
     * @var array
     */
    protected $_lineDashPattern = array();
    
    /**
     * Distance into the line dash pattern at which to start the dash.
     * @var integer
     */
    protected $_lineDashPhase = 0;
  
    /**
     * Winding rule used when filling the path. One of the WINDING_RULE
     * constants defined in this class. Null means use the default winding rule.
     * @var integer
     */
    protected $_windingRule = null;
  
    /**
     * Flatness of the path, which determines how precise curves should be
     * rendered on the output device. Null means use the default flatness.
     * @var float
     */
    protected $_flatness = null;
    
  
  
  /**** Class Methods ****/


  /* Drawing Convenience Methods */
  
    /**
     * Immediately strokes a line using the default line style attributes and
     * the canvas' current stroke color and other drawing attributes.
     * 
     * Method signatures:
     * 
     *  - Stroke a line between two {@link RE_Point}s:
     *      RE_Path::strokeLine($canvas, RE_Point $startPoint, RE_Point $endPoint);
     *   
     *  - Stroke a line between the start and end coordinates:
     *      RE_Path::strokeLine($canvas, number $startX, number $startY,
     *                                   number $endX,   number $endY);
     * 
     * @param RE_Pdf_Canvas_Interface $canvas
     * @param mixed $param1
     * @param mixed $param2
     * @param mixed $param3
     * @param mixed $param4
     */
    public static function strokeLine(
        RE_Pdf_Canvas_Interface $canvas, $param1, $param2, $param3 = null, $param4 = null)
    {
        $path = new self();
        if ($param1 instanceof RE_Point) {
            $path->moveTo($param1);
            $path->lineTo($param2);
        } else {
            $path->moveTo($param1, $param2);
            $path->lineTo($param3, $param4);
        }
        $path->stroke($canvas);
    }

    /**
     * Immediately strokes a rectangle using the default line style attributes
     * and the canvas' current stroke color and other drawing attributes. The
     * rectangle's origin is always its lower-left corner.
     * 
     * Method signatures:
     * 
     *  - Stroke a rectangle along the given {@link RE_Rect}:
     *      RE_Path::strokeRect($canvas, RE_Rect $rect);
     *   
     *  - Stroke a rectangle using two {@link RE_Point}s as opposite corners:
     *      RE_Path::strokeRect($canvas, RE_Point $originPoint, RE_Point $cornerPoint);
     *   
     *  - Stroke a rectangle using the specified coordinates as opposite corners:
     *      RE_Path::strokeRect($canvas, number $originX, number $originY,
     *                                   number $cornerX, number $cornerY);
     * 
     * @param RE_Pdf_Canvas_Interface $canvas
     * @param mixed $param1
     * @param mixed $param2
     * @param mixed $param3
     * @param mixed $param4
     */
    public static function strokeRect(
        RE_Pdf_Canvas_Interface $canvas, $param1, $param2 = null, $param3 = null, $param4 = null)
    {
        if ($param1 instanceof RE_Rect) {
            $x = $param1->getX();
            $y = $param1->getY();
            $width  = $param1->getWidth();
            $height = $param1->getHeight();
        } else if ($param1 instanceof RE_Point) {
            $x = min($param1->getX(), $param2->getX());
            $y = min($param1->getY(), $param2->getY());
            $width  = abs($param1->getX() - $param2->getX());
            $height = abs($param1->getY() - $param2->getY());
        } else {
            $x = min($param1, $param3);
            $y = min($param2, $param4);
            $width  = abs($param1 - $param3);
            $height = abs($param2 - $param4);
        }
        
        $path = new self();
        $path->moveTo($x         , $y);
        $path->lineTo($x + $width, $y);
        $path->lineTo($x + $width, $y + $height);
        $path->lineTo($x         , $y + $height);
        $path->closePath();
        $path->stroke($canvas);
    }

    /**
     * Immediately fills a rectangle using the canvas' current fill color.
     * The rectangle's origin is always its lower-left corner.
     * 
     * Method signatures:
     * 
     *  - Fill a rectangle inside the given {@link RE_Rect}:
     *      RE_Path::fillRect($canvas, RE_Rect $rect);
     *   
     *  - Fill a rectangle using two {@link RE_Point}s as opposite corners:
     *      RE_Path::fillRect($canvas, RE_Point $originPoint, RE_Point $cornerPoint);
     *   
     *  - Fill a rectangle using the specified coordinates as opposite corners:
     *      RE_Path::fillRect($canvas, number $originX, number $originY,
     *                                 number $cornerX, number $cornerY);
     * 
     * @param RE_Pdf_Canvas_Interface $canvas
     * @param mixed $param1
     * @param mixed $param2
     * @param mixed $param3
     * @param mixed $param4
     */
    public static function fillRect(
        RE_Pdf_Canvas_Interface $canvas, $param1, $param2 = null, $param3 = null, $param4 = null)
    {
        if ($param1 instanceof RE_Rect) {
            $x = $param1->getX();
            $y = $param1->getY();
            $width  = $param1->getWidth();
            $height = $param1->getHeight();
        } else if ($param1 instanceof RE_Point) {
            $x = min($param1->getX(), $param2->getX());
            $y = min($param1->getY(), $param2->getY());
            $width  = abs($param1->getX() - $param2->getX());
            $height = abs($param1->getY() - $param2->getY());
        } else {
            $x = min($param1, $param3);
            $y = min($param2, $param4);
            $width  = abs($param1 - $param3);
            $height = abs($param2 - $param4);
        }
        
        $path = new self();
        $path->moveTo($x         , $y);
        $path->lineTo($x + $width, $y);
        $path->lineTo($x + $width, $y + $height);
        $path->lineTo($x         , $y + $height);
        $path->closePath();
        $path->fill($canvas);
    }


  /* Default Path Attributes */
  
    /**
     * Returns the default line width in points used for all paths where no
     * line width has been explicitly set.
     * 
     * @return float
     */
    public static function getDefaultLineWidth()
    {
        return self::$_defaultLineWidth;
    }

    /**
     * Sets the default line width in points used for all paths where no
     * line width has been explicitly set. A line width of zero indicates
     * that the line should be drawn 1 device pixel wide.
     * 
     * @param float $lineWidth
     * @throws InvalidArgumentException if the line width is negative.
     */
    public static function setDefaultLineWidth($lineWidth)
    {
        if ($lineWidth < 0) {
            throw new InvalidArgumentException('Default line width cannot be negative.');
        }
        self::$_defaultLineWidth = $lineWidth;
    }
  
    /**
     * Returns the default line cap style used for all paths where no line
     * cap style has been explicitly set.
     * 
     * @return integer One of the LINE_CAP constants defined in this class.
     */
    public static function getDefaultLineCapStyle()
    {
        return self::$_defaultLineCapStyle;
    }
    
    /**
     * Sets the default line cap style used for all paths where no line
     * cap style has been explicitly set.
     * 
     * The line cap style must be one of the following values:
     *  - {@link RE_Path::LINE_CAP_BUTT}
     *  - {@link RE_Path::LINE_CAP_ROUND}
     *  - {@link RE_Path::LINE_CAP_SQUARE}
     * 
     * @param integer $lineCapStyle
     * @throws InvalidArgumentException if the line cap style is invalid.
     */
    public static function setDefaultLineCapStyle($lineCapStyle)
    {
        switch ($lineCapStyle) {
        case RE_Path::LINE_CAP_BUTT:
        case RE_Path::LINE_CAP_ROUND:
        case RE_Path::LINE_CAP_SQUARE:
            self::$_defaultLineCapStyle = $lineCapStyle;
            break;
            
        default:
            throw new InvalidArgumentException("Invalid default line cap style: $lineCapStyle");
        }
    }
  
    /**
     * Returns the default line join style used for all paths where no line
     * join style has been explicitly set.
     * 
     * @return integer One of the LINE_JOIN constants defined in this class.
     */
    public static function getDefaultLineJoinStyle()
    {
        return self::$_defaultLineJoinStyle;
    }
    
    /**
     * Sets the default line join style used for all paths where no line
     * join style has been explicitly set.
     * 
     * The line join style must be one of the following values:
     *  - {@link RE_Path::LINE_JOIN_MITER}
     *  - {@link RE_Path::LINE_JOIN_ROUND}
     *  - {@link RE_Path::LINE_JOIN_BEVEL}
     * 
     * @param integer $lineJoinStyle
     * @throws InvalidArgumentException if the line join style is invalid.
     */
    public static function setDefaultLineJoinStyle($lineJoinStyle)
    {
        switch ($lineJoinStyle) {
        case RE_Path::LINE_JOIN_MITER:
        case RE_Path::LINE_JOIN_ROUND:
        case RE_Path::LINE_JOIN_BEVEL:
            self::$_defaultLineJoinStyle = $lineJoinStyle;
            break;
            
        default:
            throw new InvalidArgumentException("Invalid default line join style: $lineJoinStyle");
        }
    }
  
    /**
     * Returns the miter limit used for all paths where no miter limit has
     * been explicitly set.
     * 
     * @return float
     */
    public static function getDefaultMiterLimit()
    {
        return self::$_defaultMiterLimit;
    }

    /**
     * Sets the miter limit used for all paths where no miter limit has
     * been explicitly set.
     * 
     * @param float $miterLimit
     */
    public static function setDefaultMiterLimit($miterLimit)
    {
        self::$_defaultMiterLimit = $miterLimit;
    }
  
    /**
     * Returns the winding rule used for all paths where no winding rule has
     * been explicitly set.
     * 
     * @return integer One of the WINDING_RULE constants defined in this class.
     */
    public static function getDefaultWindingRule()
    {
        return self::$_defaultWindingRule;
    }
    
    /**
     * Sets the winding rule used for all paths where no winding rule has
     * been explicitly set.
     * 
     * The winding rule must be one of the following values:
     *  - {@link RE_Path::WINDING_RULE_NON_ZERO}
     *  - {@link RE_Path::WINDING_RULE_EVEN_ODD}
     * 
     * @param integer $windingRule
     * @throws InvalidArgumentException if the winding rule is invalid.
     */
    public static function setDefaultWindingRule($windingRule)
    {
        switch ($windingRule) {
        case RE_Path::WINDING_RULE_NON_ZERO:
        case RE_Path::WINDING_RULE_EVEN_ODD:
            self::$_defaultWindingRule = $windingRule;
            break;
            
        default:
            throw new InvalidArgumentException("Invalid default winding rule: $windingRule");
        }
    }
  
    /**
     * Returns the flatness used for all paths where no flatness has been
     * explicitly set.
     * 
     * @return float
     */
    public static function getDefaultFlatness()
    {
        return self::$_defaultFlatness;
    }

    /**
     * Sets the flatness used for all paths where no flatness has been
     * explicitly set, which determines how precise curves should be rendered
     * on the output device. Higher numbers give more precision at the expense
     * of additional computation and memory use.
     * 
     * @param float $flatness
     * @throws InvalidArgumentException if the flatness is negative or zero.
     */
    public static function setDefaultFlatness($flatness)
    {
        if ($flatness <= 0) {
            throw new InvalidArgumentException('Default flatness must be a positive number.');
        }
        self::$_defaultFlatness = $flatness;
    }

    
    
  /**** Public Interface ****/


  /* Object Magic Methods */
  
    /**
     * Returns the entire path as a string of the form
     *   "{{element1}, {element2}, {element3}, ...}"
     *
     * @return string
     */
    public function __toString()
    {
        $pathString = '{';
        foreach ($this->_pathElements as $element) {
            $pathString .= $element->__toString() . ', ';
        }
        return substr($pathString, 0, -2) . '}';
    }
    
    /**
     * Returns a copy of the path.
     * 
     * @return RE_Path
     */
    public function __clone()
    {
        foreach ($this->_pathElements as &$element) {
            $element = clone $element;
        }
    }
    

  /* Path Construction */
  
    /**
     * Begins a new subpath at the specified end point location, implicitly
     * closing the previous subpath, if any.
     * 
     * The end point can be specified either as a RE_Point object or as a
     * pair of numeric x and y coordinates.
     * 
     * @param mixed $param1 End point (as RE_Point) or x coordinate
     * @param mixed $param2 (optional) y coordinate
     */
    public function moveTo($param1, $param2 = null)
    {
        if ($param1 instanceof RE_Point) {
            $endPoint = clone $param1;
        } else {
            $endPoint = new RE_Point($param1, $param2);
        }
        
        $this->_pathElements[] = new RE_Path_Element(RE_Path_Element::TYPE_MOVE, $endPoint);
    }
  
    /**
     * Appends a straight line segment to the current subpath, beginning at the
     * current drawing point and ending at the specified end point.
     * 
     * The end point can be specified either as a RE_Point object or as a
     * pair of numeric x and y coordinates.
     * 
     * @param mixed $param1 End point (as RE_Point) or x coordinate
     * @param mixed $param2 (optional) y coordinate
     * @throws BadMethodCallException if the path is empty; paths must be
     *   started by calling {@link moveTo()}.
     */
    public function lineTo($param1, $param2 = null)
    {
        if (empty($this->_pathElements)) {
            throw new BadMethodCallException('The path is empty; call moveTo() first.');
        }

        if ($param1 instanceof RE_Point) {
            $endPoint = clone $param1;
        } else {
            $endPoint = new RE_Point($param1, $param2);
        }

        $this->_pathElements[] = new RE_Path_Element(RE_Path_Element::TYPE_LINE, $endPoint);
    }
  
    /**
     * Appends a curved line segment to the current subpath, beginning at the
     * current drawing point and ending at the specified end point, using the
     * specified control points.
     * 
     * If one of the two control points is be null, a quadratic curve will be
     * appended instead of a cubic curve. Both control points may not be null.
     * 
     * The end and control points can be specified either as RE_Point
     * objects or as pairs of numeric x and y coordinates. All parameters must
     * be of the same type.
     * 
     * @param mixed $param1 End point       (as RE_Point) or end point x coordinate
     * @param mixed $param2 Control point 1 (as RE_Point) or end point y coordinate
     * @param mixed $param3 Control point 2 (as RE_Point) or control point 1 x coordinate
     * @param mixed $param4 (optional) control point 1 y coordinate
     * @param mixed $param5 (optional) control point 2 x coordinate
     * @param mixed $param6 (optional) control point 2 y coordinate
     * @throws BadMethodCallException if the path is empty; paths must be
     *   started by calling {@link moveTo()}.
     */
    public function curveTo($param1, $param2, $param3, $param4 = null, $param5 = null, $param6 = null)
    {
        if (empty($this->_pathElements)) {
            throw new BadMethodCallException('The path is empty; call moveTo() first.');
        }

        if ($param1 instanceof RE_Point) {
            $endPoint      = clone $param1;
            $controlPoint1 = clone $param2;
            $controlPoint2 = clone $param3;
        } else {
            $endPoint = new RE_Point($param1, $param2);
            if ((is_null($param3)) || (is_null($param4))) {
                $controlPoint1 = null;
            } else {
                $controlPoint1 = new RE_Point($param3, $param4);
            }
            if ((is_null($param5)) || (is_null($param6))) {
                $controlPoint2 = null;
            } else {
                $controlPoint2 = new RE_Point($param5, $param6);
            }
        }

        $this->_pathElements[] = new RE_Path_Element(
            RE_Path_Element::TYPE_CURVE, $endPoint, $controlPoint1, $controlPoint2);
    }
  
    /**
     * Closes the current subpath, creating an implicit straight line segment
     * between the current drawing point and the first point of the subpath.
     * Moves the current drawing point to the end of subpath.
     * 
     * @throws BadMethodCallException if the path is empty; paths must be
     *   started by calling {@link moveTo()}.
     */
    public function closePath()
    {
        if (empty($this->_pathElements)) {
            throw new BadMethodCallException('The path is empty; call moveTo() first.');
        }

        /* Work backwards to find the starting point for the subpath so we
         * know where to move the current drawing point.
         */
        $endPoint = null;
        for ($i = count($this->_pathElements) - 2; $i >= 0; $i--) {
            switch ($this->_pathElements[$i]->getType()) {
            case RE_Path_Element::TYPE_MOVE:
                $endPoint = $this->_pathElements[$i]->getEndPoint();
                break 2;

            case RE_Path_Element::TYPE_CLOSE:
                break 2;
            
            default: 
                $endPoint = $this->_pathElements[$i]->getEndPoint();
            }
        }

        $this->_pathElements[] = new RE_Path_Element(RE_Path_Element::TYPE_CLOSE, $endPoint);
        $this->_pathElements[] = new RE_Path_Element(RE_Path_Element::TYPE_MOVE,  $endPoint);
    }
  

  /* Common Shapes */
  
    /**
     * Appends the rectangle as a complete subpath, implicitly closing the
     * current subpath (if any). Constructs the subpath starting at the origin
     * of the rectangle (the lower-left corner) and proceeding in a counter-
     * clockwise direction.
     * 
     * @param RE_Rect $rect
     */
    public function appendRect(RE_Rect $rect)
    {
        $minX = $rect->minX();
        $minY = $rect->minY();
        $maxX = $rect->maxX();
        $maxY = $rect->maxY();
        
        $this->moveTo($minX, $minY);
        $this->lineTo($maxX, $minY);
        $this->lineTo($maxX, $maxY);
        $this->lineTo($minX, $maxY);
        $this->closePath();
    }
  
  
  /* Path Information */
  
    /**
     * Returns the current drawing point, the end point specified in the most
     * recently added line segment.
     * 
     * @return RE_Point
     * @throws BadMethodCallException if the path is empty; paths must be
     *   started by calling {@link moveTo()}.
     */
    public function getCurrentPoint()
    {
        if (empty($this->_pathElements)) {
            throw new BadMethodCallException('The path is empty; call moveTo() first.');
        }
        return clone $this->_pathElements[count($this->_pathElements) - 1]->getEndPoint();
    }
    
    /**
     * Returns true if the path is empty, false if it contains at least one
     * path element.
     * 
     * @return boolean
     */
    public function isEmpty()
    {
        if (empty($this->_pathElements)) {
            return true;
        }
        return false;
    }
    
  
  /* Accessor Methods */
    
    /**
     * Returns the line width of the path in points. If no line width has been
     * explicitly set, returns the default line width.
     * 
     * @return float
     */
    public function getLineWidth()
    {
        if (is_null($this->_lineWidth)) {
            return self::getDefaultLineWidth();
        }
        return $this->_lineWidth;
    }

    /**
     * Sets the line width of the path in points. A line width of zero
     * indicates that the line should be drawn 1 device pixel wide.
     * 
     * Set the line width to null to cause the path to use the default
     * line width.
     * 
     * @param float|null $lineWidth
     * @throws InvalidArgumentException if the line width is negative.
     */
    public function setLineWidth($lineWidth)
    {
        if ((! is_null($lineWidth)) && ($lineWidth < 0)) {
            throw new InvalidArgumentException('Line width cannot be negative.');
        }
        $this->_lineWidth = $lineWidth;
    }
  
    /**
     * Returns the line cap style of the path. If no line cap style has been
     * explicitly set, returns the default line cap style.
     * 
     * @return integer One of the LINE_CAP constants defined in this class.
     */
    public function getLineCapStyle()
    {
        if (is_null($this->_lineCapStyle)) {
            return self::getDefaultLineCapStyle();
        }
        return $this->_lineCapStyle;
    }
    
    /**
     * Sets the line cap style of the path.
     * 
     * The line cap style must be one of the following values:
     *  - {@link RE_Path::LINE_CAP_BUTT}
     *  - {@link RE_Path::LINE_CAP_ROUND}
     *  - {@link RE_Path::LINE_CAP_SQUARE}
     * 
     * Set the line cap style to null to cause the path to use the default
     * line cap style.
     * 
     * @param integer|null $lineCapStyle
     * @throws InvalidArgumentException if the line cap style is invalid.
     */
    public function setLineCapStyle($lineCapStyle)
    {
        switch ($lineCapStyle) {
        case null:
        case RE_Path::LINE_CAP_BUTT:
        case RE_Path::LINE_CAP_ROUND:
        case RE_Path::LINE_CAP_SQUARE:
            $this->_lineCapStyle = $lineCapStyle;
            break;
            
        default:
            throw new InvalidArgumentException("Invalid line cap style: $lineCapStyle");
        }
    }
  
    /**
     * Returns the line join style of the path. If no line join style has
     * been explicitly set, returns the default line join style.
     * 
     * @return integer One of the LINE_JOIN constants defined in this class.
     */
    public function getLineJoinStyle()
    {
        if (is_null($this->_lineJoinStyle)) {
            return self::getDefaultLineJoinStyle();
        }
        return $this->_lineJoinStyle;
    }
    
    /**
     * Sets the line join style of the path.
     * 
     * The line join style must be one of the following values:
     *  - {@link RE_Path::LINE_JOIN_MITER}
     *  - {@link RE_Path::LINE_JOIN_ROUND}
     *  - {@link RE_Path::LINE_JOIN_BEVEL}
     * 
     * Set the line join style to null to cause the path to use the default
     * line join style.
     * 
     * @param integer|null $lineJoinStyle
     * @throws InvalidArgumentException if the line join style is invalid.
     */
    public function setLineJoinStyle($lineJoinStyle)
    {
        switch ($lineJoinStyle) {
        case null:
        case RE_Path::LINE_JOIN_MITER:
        case RE_Path::LINE_JOIN_ROUND:
        case RE_Path::LINE_JOIN_BEVEL:
            $this->_lineJoinStyle = $lineJoinStyle;
            break;
            
        default:
            throw new InvalidArgumentException("Invalid line join style: $lineJoinStyle");
        }
    }
  
    /**
     * Returns the miter limit of the path. If no miter limit has been
     * explicitly set, returns the default miter limit.
     * 
     * @return float
     */
    public function getMiterLimit()
    {
        if (is_null($this->_miterLimit)) {
            return self::getDefaultMiterLimit();
        }
        return $this->_miterLimit;
    }

    /**
     * Sets the miter limit of the path, the maximum length of mitered joins
     * for stroked paths.
     * 
     * Set the miter limit to null to cause the path to use the default
     * miter limit.
     * 
     * @param float|null $miterLimit
     */
    public function setMiterLimit($miterLimit)
    {
        $this->_miterLimit = $miterLimit;
    }
  
    /**
     * Returns an array of float values that represent the lengths of the
     * dashes and gaps in points which are used to stroke the path. If the
     * array is empty, a solid line is used.
     * 
     * @return array
     */
    public function getLineDashPattern()
    {
        return $this->_lineDashPattern;
    }
    
    /**
     * Sets the line dash pattern as an array of float values that represent
     * the lengths of the dashes and gaps in points which are used to stroke
     * the path. If the array is empty, a solid line is used.
     * 
     * @param array $lineDashPattern
     */
    public function setLineDashPattern(array $lineDashPattern)
    {
        $this->_lineDashPattern = $lineDashPattern;
    }
    
    /**
     * Returns the phase of the line dash pattern, indicating the distance
     * into the line dash pattern at which to start the dash.
     * 
     * @return integer
     */
    public function getLineDashPhase()
    {
        return $this->_lineDashPhase;
    }
    
    /**
     * Sets the phase of the line dash pattern, indicating the distance into
     * the line dash pattern at which to start the dash.
     * 
     * @param integer $lineDashPhase
     */
    public function setLineDashPhase($lineDashPhase)
    {
        $this->_lineDashPhase = $lineDashPhase;
    }
    
    /**
     * Returns the winding rule of the path. If no winding rule has been
     * explicitly set, returns the default line miter style.
     * 
     * @return integer One of the WINDING_RULE constants defined in this class.
     */
    public function getWindingRule()
    {
        if (is_null($this->_windingRule)) {
            return self::getDefaultWindingRule();
        }
        return $this->_windingRule;
    }
    
    /**
     * Sets the winding rule of the path.
     * 
     * The winding rule must be one of the following values:
     *  - {@link RE_Path::WINDING_RULE_NON_ZERO}
     *  - {@link RE_Path::WINDING_RULE_EVEN_ODD}
     * 
     * Set the winding rule to null to cause the path to use the default
     * winding rule.
     * 
     * @param integer|null $windingRule
     * @throws InvalidArgumentException if the winding rule is invalid.
     */
    public function setWindingRule($windingRule)
    {
        switch ($windingRule) {
        case null:
        case RE_Path::WINDING_RULE_NON_ZERO:
        case RE_Path::WINDING_RULE_EVEN_ODD:
            $this->_windingRule = $windingRule;
            break;
            
        default:
            throw new InvalidArgumentException("Invalid winding rule: $windingRule");
        }
    }
  
    /**
     * Returns the flatness of the path. If no flatness has been explicitly
     * set, returns the default flatness.
     * 
     * @return float
     */
    public function getFlatness()
    {
        if (is_null($this->_flatness)) {
            return self::getDefaultFlatness();
        }
        return $this->_flatness;
    }

    /**
     * Sets the flatness of the path, which determines how precise curves
     * should be rendered on the output device. Higher numbers give more
     * precision at the expense of additional computation and memory use.
     * 
     * Set the flatness to null to cause the path to use the default flatness.
     * 
     * @param float|null $flatness
     * @throws InvalidArgumentException if the flatness is negative or zero.
     */
    public function setFlatness($flatness)
    {
        if ((! is_null($flatness)) && ($flatness <= 0)) {
            throw new InvalidArgumentException('Flatness must be a positive number.');
        }
        $this->_flatness = $flatness;
    }
  

  /* PDF Drawing */
  
    /**
     * Draws the path on the canvas but does not fill or stroke it. In most
     * cases, you will want to use {@link fill()}, {@link stroke()}, or
     * {@link fillAndStroke()} instead.
     * 
     * @param RE_Pdf_Canvas_Interface $canvas
     */
    public function draw(RE_Pdf_Canvas_Interface $canvas)
    {
        foreach ($this->_pathElements as $element) {
            $element->draw($canvas);
        }
    }
    
    /**
     * Fills the contents of the path using the path's winding rule attribute
     * and the canvas' current fill color and other drawing attributes.
     * 
     * @param RE_Pdf_Canvas_Interface $canvas
     */
    public function fill(RE_Pdf_Canvas_Interface $canvas)
    {
        $canvas->saveGraphicsState();
        $this->_setGraphicsState($canvas);

        $this->draw($canvas);
                
        if ($this->getWindingRule() == RE_Path::WINDING_RULE_NON_ZERO) {
            $canvas->writeBytes(" f\n");
        } else {
            $canvas->writeBytes(" f*\n");
        }
        
        $canvas->restoreGraphicsState();
    }
    
    /**
     * Draws a line along the path using the path's line style attributes and
     * the canvas' current stroke color and other drawing attributes.
     * 
     * @param RE_Pdf_Canvas_Interface $canvas
     */
    public function stroke(RE_Pdf_Canvas_Interface $canvas)
    {
        $canvas->saveGraphicsState();
        $this->_setGraphicsState($canvas);
        
        $this->draw($canvas);
        
        $canvas->writeBytes(" S\n");
        
        $canvas->restoreGraphicsState();
    }
    
    /**
     * Fills then strokes the path using the path's winding rule and line style
     * attributes and the canvas' current fill color, stroke color, and other
     * drawing attributes.
     * 
     * @param RE_Pdf_Canvas_Interface $canvas
     */
    public function fillAndStroke(RE_Pdf_Canvas_Interface $canvas)
    {
        $canvas->saveGraphicsState();
        $this->_setGraphicsState($canvas);
        
        $this->draw($canvas);

        if ($this->getWindingRule() == RE_Path::WINDING_RULE_NON_ZERO) {
            $canvas->writeBytes(" B\n");
        } else {
            $canvas->writeBytes(" B*\n");
        }
        
        $canvas->restoreGraphicsState();
    }
    
    /**
     * Intersects the path with the canvas' current clipping path using the
     * path's winding rule and makes the resulting shape the new clipping
     * path.
     * 
     * Note that there is no way to enlarge a clipping path. However, the
     * chipping path is part of the current graphics state, so be sure to
     * bracket calls to this method with $canvas->saveGraphicsState() and
     * $canvas->restoreGraphicsState() where appropriate.
     * 
     * @param RE_Pdf_Canvas_Interface $canvas
     */
    public function clip(RE_Pdf_Canvas_Interface $canvas)
    {
        $this->draw($canvas);

        if ($this->getWindingRule() == RE_Path::WINDING_RULE_NON_ZERO) {
            $canvas->writeBytes(" W\n n\n");
        } else {
            $canvas->writeBytes(" W*\n n\n");
        }
    }
    


  /**** Internal Interface ****/
  
    /**
     * Sets the graphics state for the path based on the path's line style
     * attributes.
     * 
     * @param RE_Pdf_Canvas_Interface $canvas
     */
    protected function _setGraphicsState(RE_Pdf_Canvas_Interface $canvas)
    {
        RE_Pdf_Object_Numeric::writeValue($canvas, $this->getLineWidth());
        $canvas->writeBytes(" w\n");
        
        RE_Pdf_Object_Numeric::writeValue($canvas, $this->getLineCapStyle());
        $canvas->writeBytes(" J\n");

        RE_Pdf_Object_Numeric::writeValue($canvas, $this->getLineJoinStyle());
        $canvas->writeBytes(" j\n");

        RE_Pdf_Object_Numeric::writeValue($canvas, $this->getMiterLimit());
        $canvas->writeBytes(" M\n");

        $canvas->writeBytes('[');
        foreach ($this->_lineDashPattern as $value) {
            RE_Pdf_Object_Numeric::writeValue($canvas, $value);
        }
        $canvas->writeBytes(']');
        RE_Pdf_Object_Numeric::writeValue($canvas, $this->getLineDashPhase());
        $canvas->writeBytes(" d\n");

        RE_Pdf_Object_Numeric::writeValue($canvas, $this->getFlatness());
        $canvas->writeBytes(" i\n");
    }

}
