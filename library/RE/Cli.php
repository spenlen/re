<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Cli
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */

/**
 * Command Line Interface library class providing access to common operations.
 *
 * @package    Cli
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
class RE_Cli {
    
    /**
     * Interactively prompts for user input on the CLI.
     * 
     * Supports a question and default value [optional]. If a default is provided,
     * pressing enter will have the function return the default. To allow an empty
     * value pass a default of '', otherwise an exception will be thrown if no
     * value is provided from user input.
     *
     * @param string $mesg A question for the prompt.
     * @param string $default A default value for the prompt.
     * @return string
     */
    static function prompt($mesg, $default=null) {
        if(isset($default)) {
            echo $mesg . ' ['.$default.'] > ';
        } else {
            echo $mesg . ' > ';
        }
        
        $fp=fopen("php://stdin", "r");
        $input=fgets($fp, 255);
        fclose($fp);
        
        $input = trim($input);
        if(empty($input) && (isset($default))) {
            return $default;
        } else if(empty($input)) {
            throw new RuntimeException("A required prompt was not answered and no default exists.");
        }
        return $input;
    }
}