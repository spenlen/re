<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 * 
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    FileParser
 * @subpackage Core
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */

/** RE_FileParser_Exception */
require_once 'RE/FileParser/Exception.php';


/**
 * Abstract base class for binary file parsers.
 *
 * Provides a library of methods to quickly navigate and extract various data
 * types (signed and unsigned integers, floating- and fixed-point numbers,
 * strings, etc.) from the binary file.
 *
 * File access is managed via a {@link RE_FileParser_DataSource_Interface} object.
 * This allows the same parser code to work with many different data sources:
 * in-memory objects, filesystem files, etc.
 *
 * @package    FileParser
 * @subpackage Core
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
abstract class RE_FileParser
{
  /**** Class Constants ****/

    /**
     * Little-endian byte order (0x04 0x03 0x02 0x01).
     */
    const BYTE_ORDER_LITTLE_ENDIAN = 0;

    /**
     * Big-endian byte order (0x01 0x02 0x03 0x04).
     */
    const BYTE_ORDER_BIG_ENDIAN    = 1;



  /**** Instance Variables ****/

    /**
     * Flag indicating that the file has passed a cursory validation check.
     * @var boolean
     */
    protected $_isScreened = false;

    /**
     * Flag indicating that the file has been sucessfully parsed.
     * @var boolean
     */
    protected $_isParsed = false;

    /**
     * Object representing the data source to be parsed.
     * @var RE_FileParser_DataSource_Interface
     */
    protected $_dataSource = null;

    /**
     * Flag indicating whether or not debug logging is active.
     * @var boolean
     */
    protected $_debug = false;


  /**** Abstract Interface ****/

    /**
     * Performs a cursory check to verify that the binary file is in the expected
     * format. Intended to quickly weed out obviously bogus files.
     *
     * Must set $this->_isScreened to true if successful.
     *
     * @throws RE_FileParser_Exception
     */
    abstract public function screen();

    /**
     * Reads and parses the complete binary file.
     *
     * Must set $this->_isParsed to true if successful.
     *
     * @throws RE_FileParser_Exception
     */
    abstract public function parse();



  /**** Public Interface ****/


  /* Object Lifecycle */

    /**
     * Creates a new RE_FileParser object.
     *
     * @param RE_FileParser_DataSource_Interface $dataSource
     */
    public function __construct(RE_FileParser_DataSource_Interface $dataSource)
    {
        $this->_dataSource = $dataSource;
    }

    /**
     * Discards the data source object.
     */
    public function __destruct()
    {
        $this->_dataSource = null;
    }


  /* Accessor Methods */

    /**
     * Returns true if the file has passed a cursory validation check.
     *
     * @return boolean
     */
    public function isScreened()
    {
        return $this->_isScreened;
    }

    /**
     * Returns true if the file has been successfully parsed.
     *
     * @return boolean
     */
    public function isParsed()
    {
        return $this->_isParsed;
    }

    /**
     * Returns the data source object representing the file being parsed.
     *
     * @return RE_FileParser_DataSource_Interface
     */
    public function getDataSource()
    {
        return $this->_dataSource;
    }


  /* Data Source Bridge */

    /**
     * Returns the size in bytes of the binary file.
     *
     * @return integer
     */
    public function getSize()
    {
        return $this->_dataSource->getSize();
    }

    /**
     * Returns the byte offset of the current read position within the binary
     * file.
     *
     * @return integer
     */
    public function getOffset()
    {
        return $this->_dataSource->getOffset();
    }

    /**
     * Moves the current read position of the binary file to the specified byte
     * offset.
     *
     * @param integer $offset Destination byte offset.
     * @throws RangeException if the byte offset is moved before the beginning
     *   or beyond the end of the data source.
     */
    public function moveToOffset($offset)
    {
        $this->_dataSource->moveToOffset($offset);
    }

    /**
     * Shifts the current read position within the binary file by the specified
     * number of bytes. You may move forward (positive numbers) or backward
     * (negative numbers).
     *
     * @param integer $byteCount Number of bytes to skip.
     * @throws RangeException if the new byte offset is before the beginning or
     *   beyond the end of the data source.
     */
    public function skipBytes($byteCount)
    {
        $this->_dataSource->skipBytes($byteCount);
    }

    /**
     * Returns the specified number of raw bytes from the binary file starting
     * at the current read position.
     *
     * Advances the read position by the number of bytes read.
     *
     * @param integer $byteCount              Number of bytes to read.
     * @param boolean $failOnInsufficientData (optional) Default is true.
     * @return string
     * @throws RangeException if $failOnInsufficientData is true and there is
     *   insufficient data to completely fulfill the request.
     * @throws RE_FileParser_Exception if an error was encountered while reading
     *   the data.
     */
    public function readBytes($byteCount, $failOnInsufficientData = true)
    {
        return $this->_dataSource->readBytes($byteCount, $failOnInsufficientData);
    }

    /**
     * Like {@link readBytes()} but does not advance the current read position.
     * If there is insufficient data to fulfill the request, returns as much as
     * available.
     *
     * @param integer $byteCount Number of bytes to read.
     * @return string
     * @throws RE_FileParser_Exception if an error was encountered while reading
     *   the data.
     */
    public function peekBytes($byteCount)
    {
        return $this->_dataSource->peekBytes($byteCount);
    }
    
    /**
     * Returns a string containing the raw bytes from the binary file starting
     * at the current read position and continuing until the end of binary file
     * is reached, the specified maximum number bytes is exceeded, or an
     * end-of-line sequence (which is *not* included in the return value) is
     * encountered. Returns false if there is no more data to read.
     * 
     * The end-of-line sequence is usually a single LINE FEED character "\n",
     * which is the standard end-of-line on Linux, Unix, and Mac OS X platforms.
     * If you are reading data wich may use Mac OS 9 (a single CARRIAGE RETURN
     * character "\r") or Windows (a CARRIAGE RETURN-LINE FEED sequence "\r\n")
     * line endings, you may request that those endings be used as well. This
     * behavior is disabled by default as it incurs a slight performance penalty.
     *
     * Advances the read position by the number of bytes read, including the
     * end-of-line sequence.
     *
     * @param integer $maxBytes   (optional) Maximum bytes to return. Must be
     *   non-zero. If omitted, uses 8192 (8k).
     * @param boolean $checkForCR (optional) Also check for CR or CRLF end-of-line.
     *   If omitted, uses false.
     * @return string Returns false if there is no more data to read.
     * @throws RangeException if the maximum bytes is less than 1.
     */
    public function readLine($maxBytes = 8192, $checkForCR = false)
    {
        return $this->_dataSource->readLine($maxBytes, $checkForCR);
    }

    /**
     * Returns the entire contents of the binary file as a binary string.
     *
     * This method preserves the byte offset of the current read position, and
     * so may safely be called at any time.
     *
     * @return string
     */
    public function readAllBytes()
    {
        return $this->_dataSource->readAllBytes();
    }


  /* Parser Methods */

    /**
     * Reads the signed integer value from the binary file at the current byte
     * offset. Advances the offset by the number of bytes read.
     *
     * @param integer $size      Size of integer in bytes: 1-4
     * @param integer $byteOrder (optional) Use the BYTE_ORDER_ constants defined
     *   in this class. If omitted, uses big-endian.
     * @return integer
     * @throws InvalidArgumentException if the integer size or byte order are invalid.
     * @throws RangeException if there is insufficient data to completely fulfill
     *   the request.
     */
    public function readInt($size, $byteOrder = RE_FileParser::BYTE_ORDER_BIG_ENDIAN)
    {
        if (($size < 1) || ($size > 4)) {
            throw new InvalidArgumentException("Invalid signed integer size: $size");
        }
        $bytes = $this->_dataSource->readBytes($size);

        /* unpack() will not work for this method because it always works in
         * the host byte order for signed integers. It also does not allow for
         * variable integer sizes.
         */
        if ($byteOrder == RE_FileParser::BYTE_ORDER_BIG_ENDIAN) {
            $number = ord($bytes[0]);
            if (($number & 0x80) == 0x80) {
                /* This number is negative. Extract the positive equivalent.
                 */
                $number = (~ $number) & 0xff;
                for ($i = 1; $i < $size; $i++) {
                    $number = ($number << 8) | ((~ ord($bytes[$i])) & 0xff);
                }
                /* Now turn this back into a negative number by taking the
                 * two's complement (we didn't add one above so won't
                 * subtract it below). This works reliably on both 32- and
                 * 64-bit systems.
                 */
                $number = ~$number;
            } else {
                for ($i = 1; $i < $size; $i++) {
                    $number = ($number << 8) | ord($bytes[$i]);
                }
            }

        } else if ($byteOrder == RE_FileParser::BYTE_ORDER_LITTLE_ENDIAN) {
            $number = ord($bytes[$size - 1]);
            if (($number & 0x80) == 0x80) {
                /* Negative number. See discussion above.
                 */
                $number = 0;
                for ($i = --$size; $i >= 0; $i--) {
                    $number |= ((~ ord($bytes[$i])) & 0xff) << ($i * 8);
                }
                $number = ~$number;
            } else {
                $number = 0;
                for ($i = --$size; $i >= 0; $i--) {
                    $number |= ord($bytes[$i]) << ($i * 8);
                }
            }

        } else {
            throw new InvalidArgumentException("Invalid byte order: $byteOrder");
        }

        return $number;
    }

    /**
     * Reads the unsigned integer value from the binary file at the current byte
     * offset. Advances the offset by the number of bytes read.
     *
     * NOTE: If you ask for a 4-byte unsigned integer on a 32-bit machine, the
     * resulting value WILL BE SIGNED because PHP uses signed integers internally
     * for everything. To guarantee portability, be sure to use bitwise operators
     * operators on large unsigned integers!
     *
     * @param integer $size      Size of integer in bytes: 1-4
     * @param integer $byteOrder (optional) Use the BYTE_ORDER_ constants defined
     *   in this class. If omitted, uses big-endian.
     * @return integer
     * @throws InvalidArgumentException if the integer size or byte order are invalid.
     * @throws RangeException if there is insufficient data to completely fulfill
     *   the request.
     */
    public function readUInt($size, $byteOrder = RE_FileParser::BYTE_ORDER_BIG_ENDIAN)
    {
        if (($size < 1) || ($size > 4)) {
            throw new InvalidArgumentException("Invalid unsigned integer size: $size");
        }
        $bytes = $this->_dataSource->readBytes($size);

        /* unpack() is a bit heavyweight for this simple conversion. Just
         * work the bytes directly.
         */
        if ($byteOrder == RE_FileParser::BYTE_ORDER_BIG_ENDIAN) {
            $number = ord($bytes[0]);
            for ($i = 1; $i < $size; $i++) {
                $number = ($number << 8) | ord($bytes[$i]);
            }

        } else if ($byteOrder == RE_FileParser::BYTE_ORDER_LITTLE_ENDIAN) {
            $number = 0;
            for ($i = --$size; $i >= 0; $i--) {
                $number |= ord($bytes[$i]) << ($i * 8);
            }

        } else {
            throw new InvalidArgumentException("Invalid byte order: $byteOrder");
        }

        return $number;
    }

    /**
     * Returns true if the specified bit is set in the integer bitfield.
     *
     * @param integer $bit Bit number to test (i.e. - 0-31)
     * @param integer $bitField
     * @return boolean
     */
    public function isBitSet($bit, $bitField)
    {
        $bitMask = 1 << $bit;
        if (($bitField & $bitMask) == $bitMask) {
            return true;
        }
        return false;
    }

    /**
     * Reads the signed fixed-point number from the binary file at the current
     * byte offset. Advances the offset by the number of bytes read.
     *
     * Common fixed-point sizes are 2.14 and 16.16.
     *
     * @param integer $mantissaBits Number of bits in the mantissa
     * @param integer $fractionBits Number of bits in the fraction
     * @param integer $byteOrder    (optional) Use the BYTE_ORDER_ constants defined
     *   in this class. If omitted, uses big-endian.
     * @return float
     * @throws InvalidArgumentException if total number of bits is not an even byte
     *   multiple or the byte order is invalid.
     * @throws RangeException if there is insufficient data to completely fulfill
     *   the request.
     */
    public function readFixed($mantissaBits, $fractionBits,
                              $byteOrder = RE_FileParser::BYTE_ORDER_BIG_ENDIAN)
    {
        $bitsToRead = $mantissaBits + $fractionBits;
        if (($bitsToRead % 8) !== 0) {
            throw new InvalidArgumentException('Fixed-point numbers are whole bytes');
        }
        $number = $this->readInt(($bitsToRead >> 3), $byteOrder) / (1 << $fractionBits);
        return $number;
    }

    /**
     * Reads the single-precision (4-byte) IEEE floating point number from the
     * binary file at the current byte offset. Advances the offset by the number
     * of bytes read.
     *
     * @param integer $byteOrder (optional) Use the BYTE_ORDER_ constants defined
     *   in this class. If omitted, uses big-endian.
     * @return number
     */
    public function readFloat($byteOrder = RE_FileParser::BYTE_ORDER_BIG_ENDIAN)
    {
        throw Exception('Not yet implemented');
    }

    /**
     * Reads the double-precision (8-byte) IEEE floating point number from the
     * binary file at the current byte offset. Advances the offset by the number
     * of bytes read.
     *
     * @param integer $byteOrder (optional) Use the BYTE_ORDER_ constants defined
     *   in this class. If omitted, uses big-endian.
     * @return number
     */
    public function readDouble($byteOrder = RE_FileParser::BYTE_ORDER_BIG_ENDIAN)
    {
        throw Exception('Not yet implemented');
    }

    /**
     * Reads the Unicode UTF-16-encoded string from the binary file at the
     * current byte offset. Advances the offset by the number of bytes read.
     *
     * The byte order of the UTF-16 string must be specified. You must also
     * supply the desired resulting character set.
     *
     * @todo Consider changing $byteCount to a character count. They are not
     *   always equivalent (in the case of surrogates).
     * @todo Make $byteOrder optional if there is a byte-order mark (BOM) in the
     *   string being extracted.
     *
     * @param integer $byteCount   Number of bytes (characters * 2) to return.
     * @param integer $byteOrder   (optional) Use the BYTE_ORDER_ constants defined
     *   in this class. If omitted, uses big-endian.
     * @param string $characterSet (optional) Desired resulting character set.
     *   You may use any character set supported by {@link iconv()}. If omitted,
     *   uses 'UTF-8'.
     * @return string
     * @throws InvalidArgumentException if the byte order is invalid.
     * @throws RangeException if there is insufficient data to completely fulfill
     *   the request.
     */
    public function readStringUTF16($byteCount,
                                    $byteOrder = RE_FileParser::BYTE_ORDER_BIG_ENDIAN,
                                    $characterSet = 'UTF-8')
    {
        if ($byteCount == 0) {
            return '';
        }
        $bytes = $this->_dataSource->readBytes($byteCount);

        if ($byteOrder == RE_FileParser::BYTE_ORDER_BIG_ENDIAN) {
            if ($characterSet == 'UTF-16BE') {
                return $bytes;
            }
            return iconv('UTF-16BE', $characterSet, $bytes);

        } else if ($byteOrder == RE_FileParser::BYTE_ORDER_LITTLE_ENDIAN) {
            if ($characterSet == 'UTF-16LE') {
                return $bytes;
            }
            return iconv('UTF-16LE', $characterSet, $bytes);

        } else {
            throw new InvalidArgumentException("Invalid byte order: $byteOrder");
        }
    }

    /**
     * Reads the ASCII string from the binary file at the current byte offset.
     * Advances the offset by the number of bytes read.
     *
     * @param integer $byteCount Number of bytes (characters) to return.
     * @return string
     * @throws RangeException if there is insufficient data to completely fulfill
     *   the request.
     */
    public function readStringASCII($byteCount)
    {
        if ($byteCount == 0) {
            return '';
        }
        return $this->_dataSource->readBytes($byteCount);
    }

    /**
     * Reads the Mac Roman-encoded string from the binary file at the current
     * byte offset. Advances the offset by the number of bytes read.
     *
     * You must supply the desired resulting character set.
     *
     * @param integer $byteCount    Number of bytes (characters) to return.
     * @param string  $characterSet (optional) Desired resulting character set.
     *   You may use any character set supported by {@link iconv()}. If omitted,
     *   uses 'UTF-8'.
     * @return string
     * @throws RangeException if there is insufficient data to completely fulfill
     *   the request.
     */
    public function readStringMacRoman($byteCount, $characterSet = 'UTF-8')
    {
        if ($byteCount == 0) {
            return '';
        }
        $bytes = $this->_dataSource->readBytes($byteCount);

        if ($characterSet == 'MacRoman') {
            return $bytes;
        }
        return iconv('MacRoman', $characterSet, $bytes);
    }

    /**
     * Reads the Pascal string from the binary file at the current byte offset.
     * Advances the offset by the number of bytes read.
     *
     * The length of the Pascal string is determined by reading the length bytes
     * which preceed the character data. You must supply the desired resulting
     * character set.
     *
     * @param string $characterSet (optional) Desired resulting character set.
     *   You may use any character set supported by {@link iconv()}. If omitted,
     *   uses 'UTF-8'.
     * @param integer $lengthBytes (optional) Number of bytes that make up the
     *   length. Default is 1.
     * @return string
     * @throws RangeException if there is insufficient data to completely fulfill
     *   the request.
     */
    public function readStringPascal($characterSet = 'UTF-8', $lengthBytes = 1)
    {
        $byteCount = $this->readUInt($lengthBytes);
        if ($byteCount == 0) {
            return '';
        }
        $bytes = $this->_dataSource->readBytes($byteCount);

        if ($characterSet == 'ASCII') {
            return $bytes;
        }
        return iconv('ASCII', $characterSet, $bytes);
    }



  /**** Internal Methods ****/


  /* Internal Utility Methods */

    /**
     * If debug logging is enabled, writes the log message.
     *
     * The log message is a sprintf() style string and any number of arguments
     * may accompany it as additional parameters.
     *
     * @param string $message
     * @param mixed (optional, multiple) Additional arguments
     */
    protected function _debugLog($message)
    {
        if (! $this->_debug) {
            return;
        }
        if (func_num_args() > 1) {
            $args = func_get_args();
            $message = array_shift($args);
            $message = vsprintf($message, $args);
        }
        echo $message . "\n";
    }

}
