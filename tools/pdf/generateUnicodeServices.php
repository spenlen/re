<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Tools
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */


/* This script reads and parses several of the Unicode Character Database (UCD)
 * files and generates the following PHP classes, which provide essential
 * layout services based on that data:
 *   RE_Unicode_Bidi
 *   RE_Unicode_GeneralCategory
 *   RE_Unicode_LineBreak
 *   RE_Unicode_Names
 *
 * The class files are generated in this way due to the time required to read
 * and parse the Unicode data files (10+ seconds). In contrast, the generated
 * classes can be loaded in a fraction of a second. Incurring the cost of
 * parsing this data anew for each PDF file generated would be unacceptable in
 * a production environment.
 *
 * Fully functional classes based on this script's output can be found in the
 * framework distribution. This script is maintained in case it should ever be
 * necessary to regenerate the classes to fix a bug, add new capability, or
 * update the code tables to support a newer version of the Unicode standard.
 *
 * NOTE: This script was written against version 5.1.0 of the UCD. Previous
 * versions of the UCD data files are not guaranteed to work, and successive
 * versions may require changes to the parsing code.
 */



/**** Initialization ****/


/* There are no command-line arguments. The generated classes are written
 * directly to the appropriate location inside the /library/RE/Unicode
 * directory relative to this script.
 */
if ($argc != 1) {
    fwrite(STDERR, "Usage: php " . basename(__FILE__) . "\n");
    exit(1);
}

/* The parser expects the following files to be in these locations:
 *   ./unicode_support/LineBreak.txt
 *   ./unicode_support/UnicodeData.txt
 *
 * Verify that the files exist and are readable.
 */
$basePath = dirname(__FILE__) . DIRECTORY_SEPARATOR
          . 'unicode_support' . DIRECTORY_SEPARATOR
          . 'ucd'             . DIRECTORY_SEPARATOR;
$filePaths = array(
    'LineBreak'   => $basePath . 'LineBreak.txt',
    'UnicodeData' => $basePath . 'UnicodeData.txt'
    );

foreach ($filePaths as $title => $filePath) {
    if (! (is_file($filePath) || is_link($filePath))) {
        fwrite(STDERR, "The required $title file was not in the expected location: $filePath\n");
        exit(1);
    }
    if (! is_readable($filePath)) {
        fwrite(STDERR, "Cannot read file: $filePath\n");
        exit(1);
    }
}


/* Set up a few utility functions before doing the heavy lifting below.
 */

/* Creates or overwrites the class file in the /library/RE/Unicode
 * directory relative to this script. Returns the file pointer.
 */
function openClassFile($fileName) {
    $basePath = dirname(dirname(dirname(__FILE__)))
              . DIRECTORY_SEPARATOR . 'library'
              . DIRECTORY_SEPARATOR . 'RE'
              . DIRECTORY_SEPARATOR . 'Unicode'
              . DIRECTORY_SEPARATOR;
    if (! is_dir($basePath)) {
        fwrite(STDERR, "Destination directory is not in the expected location: $basePath");
        exit(1);
    }
    progressMessage("Path: " . $basePath . $fileName);
    $fp = fopen($basePath . $fileName, 'w+b');
    if ($fp === false) {
        exit(1);
    }
    return $fp;
}

/* Writes a progress line to STDOUT (the script takes a while to run). If the
 * message is a single full-stop character (.), will not prepend a newline.
 */
function progressMessage($message) {
    if ($message != '.') {
        $message = "\n$message";
    }
    fwrite(STDOUT, $message);
}

/* Some constants to label the fields in the UnicodeData.txt file.
 */
define('FIELD_CODE_POINTS',                0);
define('FIELD_CHARACTER_NAME',             1);
define('FIELD_GENERAL_CATEGORY',           2);
define('FIELD_CANONICAL_COMBINING_CLASS',  3);
define('FIELD_BIDI_CLASS',                 4);
define('FIELD_DECOMPOSITION',              5);
define('FIELD_NUMERIC_DECIMAL_DIGIT',      6);
define('FIELD_NUMERIC_DIGIT',              7);
define('FIELD_NUMERIC',                    8);
define('FIELD_BIDI_MIRRORED',              9);
define('FIELD_UNICODE_1_CHARACTER_NAME',  10);
define('FIELD_ISO_COMMENT',               11);
define('FIELD_SIMPLE_UPPERCASE_MAPPING',  12);
define('FIELD_SIMPLE_LOWERCASE_MAPPING',  13);
define('FIELD_SIMPLE_TITLECASE_MAPPING',  14);

/* Parses a line from the UCD file and returns its field values as an
 * associative array:
 *   'firstChar'  => first code point in the range. null if comment-only.
 *   'lastChar'   => last code point in the range. null if comment-only.
 *   'comment'    => comment. null if no comment.
 *   'fieldCount' => total number of fields in the line
 *   '0'          => first field value
 *   '1'          => second field value
 *   '2'          => third field value
 *   ...          => additional field values
 */
function getFields($line) {
    $startOffset = 0;
    $maxOffset = strlen($line) - 1;

    $fields = array('firstChar' => null, 'lastChar' => null, 'comment' => null);
    $fieldCount = 0;

    for ($i = 0; $i <= $maxOffset; $i++) {

        if ($line[$i] == '#') {    // start of comment
            $fields[$fieldCount++] = trim(substr($line, $startOffset, ($i - $startOffset)));
            $fields['comment'] = trim(substr($line, ($i + 1)));
            $i = $maxOffset + 1;

        } else if ($line[$i] == ';') {    // field delimiter
            $fields[$fieldCount++] = trim(substr($line, $startOffset, ($i - $startOffset)));
            $startOffset = $i + 1;

        } else if ($i == $maxOffset) {    // final character
            $fields[$fieldCount++] = trim(substr($line, $startOffset));

        }
    }

    if ($fieldCount > 0) {
        /* The first field is always the code point this line covers. It can be
         * a single 4-6 digit code point, or a range separated by '..'.
         */
        $range = explode('..', $fields[FIELD_CODE_POINTS]);
        if (count($range) == 1) {
            $fields['firstChar'] = hexdec($fields[FIELD_CODE_POINTS]);
            $fields['lastChar']  = $fields['firstChar'];
        } else {
            $fields['firstChar'] = hexdec($range[0]);
            $fields['lastChar']  = hexdec($range[1]);
        }
    }

    $fields['fieldCount'] = $fieldCount;

    return $fields;
}



/**** Parsing Phase ****/


/* For more information regarding the specific file format of any of the files
 * being parsed here, please refer to the UCD.html document which is included
 * in the unicode_support directory.
 */


/* When parsing is complete, we will have generated several PHP code blocks that
 * create arrays, set variables, etc. These code blocks will be assembled into a
 * logical order at class creation time at the bottom of this file.
 */
$codeFragments = array();


/* Begin with the UnicodeData.txt file. We will extract the character names and
 * bidirectional text fields.
 *
 * For the character names, we build a simple lookup array. The key is the code
 * point and the value is the name in ASCII encoding.
 *
 * The bidi mirrored array will indicate those characters that should be
 * mirrored when rendering bidirectional text. Some characters have natural
 * mirror characters (parenthesis, for example). Others must be synthesized.
 * The known mirror characters will be assigned later when parsing the
 * 'BidiMirroring.txt' file.
 *
 * For the bidi class, we will build an array that describes the contiguious
 * character ranges in effect for each of the distinct values. This array can
 * be efficiently traversed using a binary search algorithm, providing good
 * performance with a low memory footprint.
 */
$characterNames  = array();
$bidiMirrored    = array();

$latinGeneralCategories = array();
$generalCategoryRanges  = array();
$lastGeneralStartCode   = 0;
$lastGeneralEndCode     = 0;
$lastGeneralCategory    = null;

$latinBidiClasses  = array();
$bidiClassRanges   = array();
$lastBidiStartCode = 0;
$lastBidiEndCode   = 0;
$lastBidiClass     = null;

progressMessage('Parsing UnicodeData.txt');
$progressCounter = 0;

$unicodeDataFile = file($filePaths['UnicodeData']);
foreach ($unicodeDataFile as $line) {

    if ($progressCounter++ > 100) {
        progressMessage('.');
        $progressCounter = 0;
    }

    $fields = getFields($line);

    /* Skip over comment-only lines.
     */
    if ((is_null($fields['firstChar'])) || ($fields['fieldCount'] < 15)) {
        continue;
    }


    $char = $fields['firstChar'];


    /* For historical reasons, no character ranges are specified in this file.
     * Certain ranges do exist, but their values can be derived.
     */
    switch ($char) {

    case 0x3400:  // <CJK Ideograph Extension A>
        $fields['lastChar'] = 0x4db5; break;

    case 0x4e00:  // <CJK Ideograph>
        $fields['lastChar'] = 0x9fc3; break;

    case 0xac00:  // <Hangul Syllable>
        $fields['lastChar'] = 0xd7a3; break;

    case 0xd800:  // <Non Private Use High Surrogate>
        $fields['lastChar'] = 0xdb7f; break;

    case 0xdb80:  // <Private Use High Surrogate>
        $fields['lastChar'] = 0xdbff; break;

    case 0xdc00:  // <Low Surrogate>
        $fields['lastChar'] = 0xdfff; break;

    case 0xe000:  // <Private Use>
        $fields['lastChar'] = 0xf8ff; break;

    case 0x20000:  // <CJK Ideograph Extension B>
        $fields['lastChar'] = 0x2a6d6; break;

    case 0xf0000:  // <Plane 15 Private Use>
        $fields['lastChar'] = 0xffffd; break;

    case 0x100000:  // <Plane 16 Private Use>
        $fields['lastChar'] = 0x10fffd; break;

    case 0x4db5:
    case 0x9fc3:
    case 0xd7a3:
    case 0xdb7f:
    case 0xdbff:
    case 0xdfff:
    case 0xf8ff:
    case 0x2a6d6:
    case 0xffffd:
    case 0x10fffd:
        /* These entries represent the end of the ranges. Skip them. */
        continue(2);
    }


    /* Control characters all share the generic name '<control>'.
     * More descriptive names for most of them can be found in the
     * FIELD_UNICODE_1_CHARACTER_NAME field.
     */
    if ($fields[FIELD_CHARACTER_NAME] == '<control>') {
        if (! empty($fields[FIELD_UNICODE_1_CHARACTER_NAME])) {
            $characterNames[$char] = $fields[FIELD_UNICODE_1_CHARACTER_NAME];
        } else {
            $characterNames[$char] = $fields[FIELD_CHARACTER_NAME];
        }
    } else {
        $characterNames[$char] = $fields[FIELD_CHARACTER_NAME];
    }


    /* Track the ranges for general category.
     */
    $generalCategory = $fields[FIELD_GENERAL_CATEGORY];

    if ($fields['firstChar'] < 256) {
        $lastChar = (int)$fields['lastChar'];
        if ($lastChar > 255) {
            $lastChar = 255;
        }
        for ($char = (int)$fields['firstChar']; $char <= $lastChar; $char++) {
            $latinGeneralCategories[$char] = $generalCategory;
        }
        if ($fields['lastChar'] > 255) {
            $lastGeneralStartCode = 255;
            $lastGeneralEndCode   = $fields['lastChar'];
            $lastGeneralCategory  = $generalCategory;
        }

    } else if (is_null($lastGeneralCategory)) {
        /* This is the first lap through the loop.
         */
        $lastGeneralStartCode = $fields['firstChar'];
        $lastGeneralEndCode   = $fields['lastChar'];
        $lastGeneralCategory  = $generalCategory;

    } else if ($fields['firstChar'] > ($lastGeneralEndCode + 1)) {
        /* Any time there is a discontiguious range, add a new entry.
         */
        $generalCategoryRanges[] = array($lastGeneralStartCode, $lastGeneralEndCode, $lastGeneralCategory);
        $lastGeneralStartCode = $fields['firstChar'];
        $lastGeneralEndCode   = $fields['lastChar'];
        $lastGeneralCategory  = $generalCategory;

    } else if ($generalCategory == $lastGeneralCategory) {
        /* If the bidi class is the same as the last, just extend the end code.
         */
        $lastGeneralEndCode = $fields['lastChar'];

    } else {
        /* There is a new bidi class. Start a new range.
         */
        $generalCategoryRanges[] = array($lastGeneralStartCode, $lastGeneralEndCode, $lastGeneralCategory);
        $lastGeneralStartCode = $fields['firstChar'];
        $lastGeneralEndCode   = $fields['lastChar'];
        $lastGeneralCategory  = $generalCategory;

    }


    /* Track the ranges for bidi class.
     */
    $bidiClass = $fields[FIELD_BIDI_CLASS];

    if ($fields['firstChar'] < 256) {
        $lastChar = (int)$fields['lastChar'];
        if ($lastChar > 255) {
            $lastChar = 255;
        }
        for ($char = (int)$fields['firstChar']; $char <= $lastChar; $char++) {
            $latinBidiClasses[$char] = $bidiClass;
        }
        if ($fields['lastChar'] > 255) {
            $lastBidiStartCode = 255;
            $lastBidiEndCode   = $fields['lastChar'];
            $lastBidiClass     = $bidiClass;
        }

    } else if (is_null($lastBidiClass)) {
        /* This is the first lap through the loop.
         */
        $lastBidiStartCode = $fields['firstChar'];
        $lastBidiEndCode   = $fields['lastChar'];
        $lastBidiClass     = $bidiClass;

    } else if ($fields['firstChar'] > ($lastBidiEndCode + 1)) {
        /* Any time there is a discontiguious range, add a new entry.
         */
        $bidiClassRanges[] = array($lastBidiStartCode, $lastBidiEndCode, $lastBidiClass);
        $lastBidiStartCode = $fields['firstChar'];
        $lastBidiEndCode   = $fields['lastChar'];
        $lastBidiClass     = $bidiClass;

    } else if ($bidiClass == $lastBidiClass) {
        /* If the bidi class is the same as the last, just extend the end code.
         */
        $lastBidiEndCode = $fields['lastChar'];

    } else {
        /* There is a new bidi class. Start a new range.
         */
        $bidiClassRanges[] = array($lastBidiStartCode, $lastBidiEndCode, $lastBidiClass);
        $lastBidiStartCode = $fields['firstChar'];
        $lastBidiEndCode   = $fields['lastChar'];
        $lastBidiClass     = $bidiClass;

    }

    /* There are only about 500 characters identified as "mirrored" characters
     * for bidirectional text. Just keep track those that have been marked 'Y'.
     * A value of false means there is no natural mirror character. See the
     * parser for 'BidiMirroring.txt' for more information.
     */
    if ($fields[FIELD_BIDI_MIRRORED] == 'Y') {
        $bidiMirrored[$char] = false;
    }

}
unset($unicodeDataFile);

/* Add the entries for the final ranges;
 */
$generalCategoryRanges[] = array($lastGeneralStartCode, $lastGeneralEndCode, $lastGeneralCategory);
$bidiClassRanges[]       = array($lastBidiStartCode,    $lastBidiEndCode,    $lastBidiClass);


/* Generate the PHP code that builds the character names array.
 */
progressMessage('Generating character names data array');
$code = " array(\n";
foreach ($characterNames as $char => $name) {
    $code .= "0x" . dechex($char) . " => '" . $name . "',\n";
}
$codeFragments['nameArray'] = substr($code, 0, -2) . ");\n";


/* Generate the PHP code that builds the general category arrays.
 */
progressMessage('Generating general category data arrays');

$code = " array(";
for ($i = 0; $i < 256; $i++) {
    $code .= "'" . $latinGeneralCategories[$i] . "'";
    if (($i % 8) != 0) {
        $code .= ', ';
    } else {
        $code .= ",\n        ";
    }
}
$codeFragments['latinGeneralCagegoryArray'] = substr($code, 0, -2) . ");\n";

$code = " array(null,\n        ";
$segmentCount = count($generalCategoryRanges);
for ($i = 0; $i < $segmentCount; $i++) {
    $code .= "array(0x" . dechex($generalCategoryRanges[$i][0]) . ", 0x"
             . dechex($generalCategoryRanges[$i][1]) . ", '" . $generalCategoryRanges[$i][2] ."')";
    if (($i % 2) == 0) {
        $code .= ', ';
    } else {
        $code .= ",\n        ";
    }
}
$codeFragments['generalCagegoryArray']  = substr(rtrim($code), 0, -1) . ");\n";

/* Calculate the search range values to use with this array.
 */
$searchRange      = pow(2, floor(log($segmentCount, 2)));
$searchIterations = (log(($searchRange / 2), 2)) + 2;
$code = "        \$segmentCount     = $segmentCount;\n"
      . "        \$searchRange      = $searchRange;\n"
      . "        \$searchIterations = $searchIterations;\n";

$codeFragments['generalCagegorySearchRanges'] = $code;


/* Generate the PHP code that builds the bidi classes arrays.
 */
progressMessage('Generating bidi classes data arrays');

$code = " array(";
for ($i = 0; $i < 256; $i++) {
    $code .= "'" . $latinBidiClasses[$i] . "'";
    if (($i % 8) != 0) {
        $code .= ', ';
    } else {
        $code .= ",\n        ";
    }
}
$codeFragments['latinBidiClassArray'] = substr($code, 0, -2) . ");\n";

$code = " array(null,\n        ";
$segmentCount = count($bidiClassRanges);
for ($i = 0; $i < $segmentCount; $i++) {
    $code .= "array(0x" . dechex($bidiClassRanges[$i][0]) . ", 0x"
             . dechex($bidiClassRanges[$i][1]) . ", '" . $bidiClassRanges[$i][2] ."')";
    if (($i % 2) == 0) {
        $code .= ', ';
    } else {
        $code .= ",\n        ";
    }
}
$codeFragments['bidiClassArray']  = substr(rtrim($code), 0, -1) . ");\n";

/* Calculate the search range values to use with this array.
 */
$searchRange      = pow(2, floor(log($segmentCount, 2)));
$searchIterations = (log(($searchRange / 2), 2)) + 2;
$code = "        \$segmentCount     = $segmentCount;\n"
      . "        \$searchRange      = $searchRange;\n"
      . "        \$searchIterations = $searchIterations;\n";

$codeFragments['bidiSearchRanges'] = $code;


/** @todo Write the BidiMirroring.txt file parser to obtain mirror characters. */


/* Generate the PHP code that builds the bidi mirrored array.
 */
progressMessage('Generating bidi mirrored data array');
$code = " array(\n        ";
$linebreakCounter = 0;
foreach ($bidiMirrored as $char => $mirrorCharacter) {
    $code .= "0x" . dechex($char) . " => ";
    if ($mirrorCharacter === false) {
        $code .= "false";
    } else {
        $code .= "0x" . dechex($char);
    }
    if (++$linebreakCounter > 3) {
        $code .= ",\n        ";
        $linebreakCounter = 0;
    } else {
        $code .= ', ';
    }
}
$codeFragments['bidiMirroredArray'] = substr(rtrim($code), 0, -1) . ");\n";



/* Now parse the LineBreak.txt file, which assigns rules to characters that
 * govern where long lines of text may be broken.
 *
 * We will build an array that describes the contiguious character ranges in
 * effect for each of the line break classes. This array can be efficiently
 * traversed using a binary search algorithm, providing good performance with a
 * low memory footprint.
 */
$latinLineBreaks    = array();
$lineBreakRanges    = array();
$lastBreakStartCode = 0;
$lastBreakEndCode   = 0;
$lastBreakClass     = null;

progressMessage('Parsing LineBreak.txt');
$progressCounter = 0;

$unicodeDataFile = file($filePaths['LineBreak']);
foreach ($unicodeDataFile as $line) {

    if ($progressCounter++ > 100) {
        progressMessage('.');
        $progressCounter = 0;
    }

    $fields = getFields($line);

    /* Skip over comment-only lines.
     */
    if ((is_null($fields['firstChar'])) || ($fields['fieldCount'] < 2)) {
        continue;
    }

    $breakClass = $fields[1];

    if ($fields['firstChar'] < 256) {
        $lastChar = (int)$fields['lastChar'];
        if ($lastChar > 255) {
            $lastChar = 255;
        }
        for ($char = (int)$fields['firstChar']; $char <= $lastChar; $char++) {
            $latinLineBreaks[$char] = $breakClass;
        }
        if ($fields['lastChar'] > 255) {
            $lastBreakStartCode = 255;
            $lastBreakEndCode   = $fields['lastChar'];
            $lastBreakClass     = $breakClass;
        }

    } else if (is_null($lastBreakClass)) {
        /* This is the first lap through the loop.
         */
        $lastBreakStartCode = $fields['firstChar'];
        $lastBreakEndCode   = $fields['lastChar'];
        $lastBreakClass     = $breakClass;

    } else if ($fields['firstChar'] > ($lastBreakEndCode + 1)) {
        /* Any time there is a discontiguious range, add a new entry.
         */
        $lineBreakRanges[]  = array($lastBreakStartCode, $lastBreakEndCode, $lastBreakClass);
        $lastBreakStartCode = $fields['firstChar'];
        $lastBreakEndCode   = $fields['lastChar'];
        $lastBreakClass     = $breakClass;

    } else if ($breakClass == $lastBreakClass) {
        /* If the break class is the same as the last, just extend the end code.
         */
        $lastBreakEndCode = $fields['lastChar'];

    } else {
        /* There is a new line break class. Start a new range.
         */
        $lineBreakRanges[]  = array($lastBreakStartCode, $lastBreakEndCode, $lastBreakClass);
        $lastBreakStartCode = $fields['firstChar'];
        $lastBreakEndCode   = $fields['lastChar'];
        $lastBreakClass     = $breakClass;

    }

}
unset($unicodeDataFile);

/* Add the entry for the final range.
 */
$lineBreakRanges[] = array($lastBreakStartCode, $lastBreakEndCode, $lastBreakClass);


/* Generate the PHP code that builds the line break classes arrays.
 */
progressMessage('Generating line break classes data arrays');

$code = " array(";
for ($i = 0; $i < 256; $i++) {
    $code .= "'" . $latinLineBreaks[$i] . "'";
    if (($i % 8) != 0) {
        $code .= ', ';
    } else {
        $code .= ",\n        ";
    }
}
$codeFragments['latinLineBreakArray'] = substr($code, 0, -2) . ");\n";

$code = " array(null,\n        ";
$segmentCount = count($lineBreakRanges);
for ($i = 0; $i < $segmentCount; $i++) {
    $code .= "array(0x" . dechex($lineBreakRanges[$i][0]) . ", 0x"
             . dechex($lineBreakRanges[$i][1]) . ", '" . $lineBreakRanges[$i][2] ."')";
    if (($i % 2) == 0) {
        $code .= ', ';
    } else {
        $code .= ",\n        ";
    }
}
$codeFragments['lineBreakArray'] = substr(rtrim($code), 0, -1) . ");\n";

/* Calculate the search range values to use with this array.
 */
$searchRange      = pow(2, floor(log($segmentCount, 2)));
$searchIterations = (log(($searchRange / 2), 2)) + 2;
$code = "        \$segmentCount     = $segmentCount;\n"
      . "        \$searchRange      = $searchRange;\n"
      . "        \$searchIterations = $searchIterations;\n";

$codeFragments['lineBreakSearchRanges'] = $code;



/**** Code Generation Phase ****/


/* The file header is boilerplate text.
 */
$fileHeader = <<<EOT
<?php
/**
 * This file is part of the RE Framework.
 *
 * The RE Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The RE Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * A copy of the GNU Lesser General Public License is included in the
 * RE Framework distribution in the file COPYING.LESSER.txt. If you did
 * not receive this copy, see <http://www.gnu.org/licenses/>.
 *
 * @package    Text
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */

/* Portions of this source file contain data obtained from Unicode Data Files:
 *
 * COPYRIGHT AND PERMISSION NOTICE
 *
 * Copyright (c) 1991-2008 Unicode, Inc. All rights reserved. Distributed under
 * the Terms of Use in http://www.unicode.org/copyright.html.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of the Unicode data files and any associated documentation (the "Data Files")
 * or Unicode software and any associated documentation (the "Software") to deal
 * in the Data Files or Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute,
 * and/or sell copies of the Data Files or Software, and to permit persons to
 * whom the Data Files or Software are furnished to do so, provided that (a) the
 * above copyright notice(s) and this permission notice appear with all copies
 * of the Data Files or Software, (b) both the above copyright notice(s) and this
 * permission notice appear in associated documentation, and (c) there is clear
 * notice in each modified Data File or in the Software as well as in the
 * documentation associated with the Data File(s) or Software that the data or
 * software has been modified.
 *
 * THE DATA FILES AND SOFTWARE ARE PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF
 * THIRD PARTY RIGHTS. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS
 * INCLUDED IN THIS NOTICE BE LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT OR
 * CONSEQUENTIAL DAMAGES, OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE,
 * DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
 * OF THE DATA FILES OR SOFTWARE.
 *
 * Except as contained in this notice, the name of a copyright holder shall
 * not be used in advertising or otherwise to promote the sale, use or other
 * dealings in these Data Files or Software without prior written authorization
 * of the copyright holder.
 */

EOT;


/* Generate RE_Unicode_GeneralCategory
 */
progressMessage("\nGenerating RE_Unicode_GeneralCategory class file");
$fp = openClassFile('GeneralCategory.php');

fwrite($fp, $fileHeader);
fwrite($fp, <<<EOT


/**
 * Abstract utility class which provides general categorical information
 * for Unicode characters which are critical for text layout engines.
 *
 * IMPORTANT: This class file was generated automatically by the
 * generateUnicodeServices.php script, located in the /tools/pdf directory
 * of the framework distribution. If you need to make changes to this class,
 * please modify that script and regenerate the class instead of changing this
 * file by hand.
 *
 * @package    Text
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
abstract class RE_Unicode_GeneralCategory
{
  /**** Class Variables ****/

    /**
     * Lookup array containing general categories for Latin-1 characters
     * only. Used as an optimization in {@link getGeneralCategories()}.
     *
     * @var array
     * @ignore Ignored to prevent phpDocumentor from attempting to make a
     *   complete copy of the array in the generated API documentation.
     */
    private static \$_latinGeneralCategories =
EOT
);
fwrite($fp, $codeFragments['latinGeneralCagegoryArray']);
fwrite($fp, <<<EOT

    /**
     * Lookup array describing the text direction class ranges. See discussion
     * in {@link getGeneralCategories()}.
     *
     * The first element is intentionally null. The calculations return offsets
     * which are 1-based, not zero-based.
     *
     * @var array
     * @ignore Ignored to prevent phpDocumentor from attempting to make a
     *   complete copy of the array in the generated API documentation.
     */
    private static \$_generalCategories =
EOT
);
fwrite($fp, $codeFragments['generalCagegoryArray']);
fwrite($fp, <<<EOT



  /**** Class Methods ****/

    /**
     * Returns an array containing the Unicode general categories for each of
     * the character codes. The keys of the resulting array match the keys of
     * the source array.
     *
     * @param array \$characterCodes
     * @return array
     */
    public static function getGeneralCategories(array \$characterCodes) {

EOT
);
fwrite($fp, $codeFragments['generalCagegorySearchRanges']);
fwrite($fp, <<<EOT

        \$generalCategories = array();
        foreach (\$characterCodes as \$key => \$characterCode) {

            /* If the character code is invalid, use the unassigned category.
             */
            if ((\$characterCode < 0) || (\$characterCode > 0x10fffd)) {
                \$generalCategories[\$key] = 'Cn';
                continue;
            }

            /* Optimize for Latin-1 characters, one of the more common cases.
             */
            if (\$characterCode < 256) {
                \$generalCategories[\$key] = self::\$_latinGeneralCategories[\$characterCode];
                continue;
            }

            /* Determine where to start the binary search. The segments are
             * ordered from lowest-to-highest. We are looking for the first
             * segment whose end code is greater than or equal to our character
             * code.
             *
             * If the end code at the top of the search range is larger, then
             * our target is probably below it.
             *
             * If it is smaller, our target is probably above it, so move the
             * search range to the end of the segment list.
             */
            if (self::\$_generalCategories[\$searchRange][1] >= \$characterCode) {
                \$searchIndex = \$searchRange;
            } else {
                \$searchIndex = \$segmentCount;
            }

            /* The general category is located by doing a binary search of the
             * array. No matter the size of the array, this allows us to locate
             * the value in exactly \$searchIterations iterations.
             */
            \$segmentIndex = 0;
            for (\$i = 1; \$i <= \$searchIterations; \$i++) {
                if (self::\$_generalCategories[\$searchIndex][1] >= \$characterCode) {
                    \$segmentIndex = \$searchIndex;
                    \$searchIndex -= \$searchRange >> \$i;
                } else {
                    \$searchIndex += \$searchRange >> \$i;
                }
            }

            /* If the segment's start code is greater than our character code,
             * that character does not have an explicit value. Use unassigned.
             */
            if (self::\$_generalCategories[\$segmentIndex][0] > \$characterCode) {
                \$generalCategories[\$key] = 'Cn';
            } else {
                \$generalCategories[\$key] = self::\$_generalCategories[\$segmentIndex][2];
            }

        }
        return \$generalCategories;
    }

}

EOT
);
fclose($fp);


/* Generate RE_Unicode_Bidi
 */
progressMessage("\nGenerating RE_Unicode_Bidi class file");
$fp = openClassFile('Bidi.php');

fwrite($fp, $fileHeader);
fwrite($fp, <<<EOT


/**
 * Abstract utility class which provides bi-directional (bidi) character
 * lookup and classification information for Unicode characters which are
 * critical for text layout engines.
 *
 * IMPORTANT: This class file was generated automatically by the
 * generateUnicodeServices.php script, located in the /tools/pdf directory
 * of the framework distribution. If you need to make changes to this class,
 * please modify that script and regenerate the class instead of changing this
 * file by hand.
 *
 * @package    Text
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
abstract class RE_Unicode_Bidi
{
  /**** Class Variables ****/

    /**
     * Lookup array containing text direction classes for Latin-1 characters
     * only. Used as an optimization in {@link getTextDirectionClasses()}.
     *
     * @var array
     * @ignore Ignored to prevent phpDocumentor from attempting to make a
     *   complete copy of the array in the generated API documentation.
     */
    private static \$_latinBidiDirectionClasses =
EOT
);
fwrite($fp, $codeFragments['latinBidiClassArray']);
fwrite($fp, <<<EOT

    /**
     * Lookup array describing the text direction class ranges. See discussion
     * in {@link getTextDirectionClasses()}.
     *
     * The first element is intentionally null. The calculations return offsets
     * which are 1-based, not zero-based.
     *
     * @var array
     * @ignore Ignored to prevent phpDocumentor from attempting to make a
     *   complete copy of the array in the generated API documentation.
     */
    private static \$_bidiDirectionClasses =
EOT
);
fwrite($fp, $codeFragments['bidiClassArray']);
fwrite($fp, <<<EOT

    /**
     * Lookup array describing mirrored characters for bidirectional text. See
     * discussion in {@link getBidiMirroredCharacters()}.
     *
     * @var array
     * @ignore Ignored to prevent phpDocumentor from attempting to make a
     *   complete copy of the array in the generated API documentation.
     */
    private static \$_bidiMirroredCharacters =
EOT
);
fwrite($fp, $codeFragments['bidiMirroredArray']);
fwrite($fp, <<<EOT



  /**** Class Methods ****/

    /**
     * Returns an array containing the Unicode text direction classes for each of
     * the character codes. The keys of the resulting array match the keys of
     * the source array.
     *
     * @param array \$characterCodes
     * @return array
     */
    public static function getTextDirectionClasses(array \$characterCodes) {

EOT
);
fwrite($fp, $codeFragments['bidiSearchRanges']);
fwrite($fp, <<<EOT

        \$textDirectionClasses = array();
        foreach (\$characterCodes as \$key => \$characterCode) {

            /* If the character code is invalid, use neutral.
             */
            if ((\$characterCode < 0) || (\$characterCode > 0x10fffd)) {
                \$lineBreakClasses[\$key] = 'ON';
                continue;
            }

            /* Optimize for Latin-1 characters, one of the more common cases.
             */
            if (\$characterCode < 256) {
                \$textDirectionClasses[\$key] = self::\$_latinBidiDirectionClasses[\$characterCode];
                continue;
            }

            /* Determine where to start the binary search. The segments are
             * ordered from lowest-to-highest. We are looking for the first
             * segment whose end code is greater than or equal to our character
             * code.
             *
             * If the end code at the top of the search range is larger, then
             * our target is probably below it.
             *
             * If it is smaller, our target is probably above it, so move the
             * search range to the end of the segment list.
             */
            if (self::\$_bidiDirectionClasses[\$searchRange][1] >= \$characterCode) {
                \$searchIndex = \$searchRange;
            } else {
                \$searchIndex = \$segmentCount;
            }

            /* The text direction class is located by doing a binary search of the
             * array. No matter the size of the array, this allows us to locate
             * the value in exactly \$searchIterations iterations.
             */
            \$segmentIndex = 0;
            for (\$i = 1; \$i <= \$searchIterations; \$i++) {
                if (self::\$_bidiDirectionClasses[\$searchIndex][1] >= \$characterCode) {
                    \$segmentIndex = \$searchIndex;
                    \$searchIndex -= \$searchRange >> \$i;
                } else {
                    \$searchIndex += \$searchRange >> \$i;
                }
            }

            /* If the segment's start code is greater than our character code,
             * that character does not have an explicit value. Use neutral.
             */
            if (self::\$_bidiDirectionClasses[\$segmentIndex][0] > \$characterCode) {
                \$textDirectionClasses[\$key] = 'ON';
            } else {
                \$textDirectionClasses[\$key] = self::\$_bidiDirectionClasses[\$segmentIndex][2];
            }

        }
        return \$textDirectionClasses;
    }

}

EOT
);
fclose($fp);


/* Generate RE_Unicode_LineBreak
 */
progressMessage("\nGenerating RE_Unicode_LineBreak class file");
$fp = openClassFile('LineBreak.php');

fwrite($fp, $fileHeader);
fwrite($fp, <<<EOT


/**
 * Abstract utility class which provides line breaking information
 * for Unicode characters which are critical for text layout engines.
 *
 * IMPORTANT: This class file was generated automatically by the
 * generateUnicodeServices.php script, located in the /tools/pdf directory
 * of the framework distribution. If you need to make changes to this class,
 * please modify that script and regenerate the class instead of changing this
 * file by hand.
 *
 * @package    Text
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
abstract class RE_Unicode_LineBreak
{
  /**** Class Variables ****/

    /**
     * Lookup array containing line breaking classes for Latin-1 characters
     * only. Used as an optimization in {@link getLineBreakClasses()}.
     *
     * @var array
     * @ignore Ignored to prevent phpDocumentor from attempting to make a
     *   complete copy of the array in the generated API documentation.
     */
    private static \$_latinLineBreakClasses =
EOT
);
fwrite($fp, $codeFragments['latinLineBreakArray']);
fwrite($fp, <<<EOT

    /**
     * Lookup array describing the line breaking class ranges. See discussion in
     * {@link getLineBreakClasses()}.
     *
     * The first element is intentionally null. The calculations return offsets
     * which are 1-based, not zero-based.
     *
     * @var array
     * @ignore Ignored to prevent phpDocumentor from attempting to make a
     *   complete copy of the array in the generated API documentation.
     */
    private static \$_lineBreakClasses =
EOT
);
fwrite($fp, $codeFragments['lineBreakArray']);
fwrite($fp, <<<EOT



  /**** Class Methods ****/

    /**
     * Returns an array containing the Unicode line break classes for each of
     * the character codes. The keys of the resulting array match the keys of
     * the source array.
     *
     * @param array \$characterCodes
     * @return array
     */
    public static function getLineBreakClasses(array \$characterCodes) {

EOT
);
fwrite($fp, $codeFragments['lineBreakSearchRanges']);
fwrite($fp, <<<EOT

        \$lineBreakClasses = array();
        foreach (\$characterCodes as \$key => \$characterCode) {

            /* If the character code is invalid, use the default class.
             */
            if ((\$characterCode < 0) || (\$characterCode > 0x10fffd)) {
                \$lineBreakClasses[\$key] = 'XX';
                continue;
            }

            /* Optimize for Latin-1 characters, one of the more common cases.
             */
            if (\$characterCode < 256) {
                \$lineBreakClasses[\$key] = self::\$_latinLineBreakClasses[\$characterCode];
                continue;
            }

            /* Determine where to start the binary search. The segments are
             * ordered from lowest-to-highest. We are looking for the first
             * segment whose end code is greater than or equal to our character
             * code.
             *
             * If the end code at the top of the search range is larger, then
             * our target is probably below it.
             *
             * If it is smaller, our target is probably above it, so move the
             * search range to the end of the segment list.
             */
            if (self::\$_lineBreakClasses[\$searchRange][1] >= \$characterCode) {
                \$searchIndex = \$searchRange;
            } else {
                \$searchIndex = \$segmentCount;
            }

            /* The line break class is located by doing a binary search of the
             * array. No matter the size of the array, this allows us to locate
             * the value in exactly \$searchIterations iterations.
             */
            \$segmentIndex = 0;
            for (\$i = 1; \$i <= \$searchIterations; \$i++) {
                if (self::\$_lineBreakClasses[\$searchIndex][1] >= \$characterCode) {
                    \$segmentIndex = \$searchIndex;
                    \$searchIndex -= \$searchRange >> \$i;
                } else {
                    \$searchIndex += \$searchRange >> \$i;
                }
            }

            /* If the segment's start code is greater than our character code,
             * that character does not have an explicit value. Use the default.
             */
            if (self::\$_lineBreakClasses[\$segmentIndex][0] > \$characterCode) {
                \$lineBreakClasses[\$key] = 'XX';
            } else {
                \$lineBreakClasses[\$key] = self::\$_lineBreakClasses[\$segmentIndex][2];
            }

        }
        return \$lineBreakClasses;
    }

}

EOT
);
fclose($fp);


/* Generate RE_Unicode_Names
 */
progressMessage("\nGenerating RE_Unicode_Names class file");
$fp = openClassFile('Names.php');

fwrite($fp, $fileHeader);
fwrite($fp, <<<EOT


/**
 * Abstract utility class which translates Unicode character codes (code points)
 * into their respective character names.
 *
 * IMPORTANT: This class file was generated automatically by the
 * generateUnicodeServices.php script, located in the /tools/pdf directory
 * of the framework distribution. If you need to make changes to this class,
 * please modify that script and regenerate the class instead of changing this
 * file by hand.
 *
 * @package    Text
 * @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
 * @license    http://www.gnu.org/licenses/lgpl.html
 */
abstract class RE_Unicode_Names
{
  /**** Class Variables ****/

    /**
     * Lookup array whose keys are Unicode code points as integers and whose
     * values are the corresponding character names as ASCII text.
     *
     * NOTE: The framework coding standards for leading white space are broken
     * here intentionally to keep the file size down. Adding the required white
     * space would increase the size of this already large file by almost 100k
     * for no real benefit.
     *
     * @var array
     * @ignore Ignored to prevent phpDocumentor from attempting to make a
     *   complete copy of the array in the generated API documentation.
     */
    private static \$_characterNames =
EOT
);
fwrite($fp, $codeFragments['nameArray']);
fwrite($fp, <<<EOT



  /**** Class Methods ****/

    /**
     * Returns the Unicode character name corresponding to the character code
     * (code point) as an ASCII string.
     *
     * Some code points are not explicitly named (for example, the CJK Ideograph
     * Extensions between U+3400 and U+4DB5). For these characters, a derived
     * name based on the name of the character range is returned.
     *
     * Returns the name '<unassigned>' if the character code is within the valid
     * Unicode range, but is not currently in use.
     *
     * @param integer \$characterCode
     * @return string
     * @throws RangeException if the character code is outside the valid Unicode
     *   range (U+0000 - U+10FFFD).
     */
    public static function nameForCharacter(\$characterCode) {
        \$characterCode = (integer)\$characterCode;
        if ((\$characterCode < 0) || (\$characterCode > 0x10fffd)) {
            throw new RangeException("'\$characterCode' is not a valid Unicode character code.");
        }
        if (isset(self::\$_characterNames[\$characterCode])) {
            return self::\$_characterNames[\$characterCode];

        } else if ((\$characterCode > 0x3400) && (\$characterCode < 0x4db5)) {  // <CJK Ideograph Extension A>
            return str_replace(', First', '', self::\$_characterNames[0x3400]);

        } else if ((\$characterCode > 0x4e00) && (\$characterCode < 0x9fc3)) {  // <CJK Ideograph>
            return str_replace(', First', '', self::\$_characterNames[0x4e00]);

        } else if ((\$characterCode > 0xac00) && (\$characterCode < 0xd7a3)) {  // <Hangul Syllable>
            return str_replace(', First', '', self::\$_characterNames[0xac00]);

        } else if ((\$characterCode > 0xd800) && (\$characterCode < 0xdb7f)) {  // <Non Private Use High Surrogate>
            return str_replace(', First', '', self::\$_characterNames[0xd800]);

        } else if ((\$characterCode > 0xdb80) && (\$characterCode < 0xdbff)) {  // <Private Use High Surrogate>
            return str_replace(', First', '', self::\$_characterNames[0xdb80]);

        } else if ((\$characterCode > 0xdc00) && (\$characterCode < 0xdfff)) {  // <Low Surrogate>
            return str_replace(', First', '', self::\$_characterNames[0xdc00]);

        } else if ((\$characterCode > 0xe000) && (\$characterCode < 0xf8ff)) {  // <Private Use>
            return str_replace(', First', '', self::\$_characterNames[0xe000]);

        } else if ((\$characterCode > 0x20000) && (\$characterCode < 0x2a6d6)) {  // <CJK Ideograph Extension B>
            return str_replace(', First', '', self::\$_characterNames[0x20000]);

        } else if ((\$characterCode > 0xf0000) && (\$characterCode < 0xffffd)) {  // <Plane 15 Private Use>
            return str_replace(', First', '', self::\$_characterNames[0xf0000]);

        } else if ((\$characterCode > 0x100000) && (\$characterCode < 0x10fffd)) {  // <Plane 16 Private Use>
            return str_replace(', First', '', self::\$_characterNames[0x100000]);

        } else {
            return '<unassigned>';
        }
    }

    /**
     * Like {@link nameForCharacter()} but operates on an array of character
     * codes (code points). Returns a similarly-keyed array of character names.
     *
     * @param array \$characterCodes
     * @return array
     */
    public static function namesForCharacters(array \$characterCodes) {
        \$characterNames = array();
        foreach (\$characterCodes as \$key => \$characterCode) {
            \$characterNames[\$key] = self::nameForCharacter(\$characterCode);
        }
        return \$characterNames;
    }

    /**
     * Returns the character code corresponding to the specified name as an
     * integer. Returns false if the name is invalid.
     *
     * Note: This search is not optimized. Do not rely on this function in
     * performance-sensitive code.
     *
     * @param string \$characterName
     * @return integer Returns false if the name is invalid.
     */
    public static function characterCodeForName(\$characterName) {
        return array_search(strtoupper(\$characterName), self::\$_characterNames);
    }

}

EOT
);
fclose($fp);


progressMessage("Done!\n\n");

exit(0);
