#!/bin/sh

# This file is part of the RE Framework.
#
# The RE Framework is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
# 
# The RE Framework is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
# License for more details.
# 
# A copy of the GNU Lesser General Public License is included in the
# RE Framework distribution in the file COPYING.LESSER.txt. If you did
# not receive this copy, see <http://www.gnu.org/licenses/>.
#
# @package    Tools
# @copyright  Copyright (c) 2008-2015 the RE Framework authors (see the file AUTHORS.txt for a complete list)
# @license    http://www.gnu.org/licenses/lgpl.html


# Automatically generates the PHP classes for the core 14 PDF fonts.

SOURCEPATH="./core14_afm"
DESTPATH="../../library/RE/Pdf/Font/Standard"

echo "Courier-Bold.afm"
php processCoreAFM.php "$SOURCEPATH/Courier-Bold.afm" > "$DESTPATH/CourierBold.php"

echo "Courier-BoldOblique.afm"
php processCoreAFM.php "$SOURCEPATH/Courier-BoldOblique.afm" > "$DESTPATH/CourierBoldOblique.php"

echo "Courier-Oblique.afm"
php processCoreAFM.php "$SOURCEPATH/Courier-Oblique.afm" > "$DESTPATH/CourierOblique.php"

echo "Courier.afm"
php processCoreAFM.php "$SOURCEPATH/Courier.afm" > "$DESTPATH/Courier.php"

echo "Helvetica-Bold.afm"
php processCoreAFM.php "$SOURCEPATH/Helvetica-Bold.afm" > "$DESTPATH/HelveticaBold.php"

echo "Helvetica-BoldOblique.afm"
php processCoreAFM.php "$SOURCEPATH/Helvetica-BoldOblique.afm" > "$DESTPATH/HelveticaBoldOblique.php"

echo "Helvetica-Oblique.afm"
php processCoreAFM.php "$SOURCEPATH/Helvetica-Oblique.afm" > "$DESTPATH/HelveticaOblique.php"

echo "Helvetica.afm"
php processCoreAFM.php "$SOURCEPATH/Helvetica.afm" > "$DESTPATH/Helvetica.php"

echo "Symbol.afm"
php processCoreAFM.php "$SOURCEPATH/Symbol.afm" > "$DESTPATH/Symbol.php"

echo "Times-Bold.afm"
php processCoreAFM.php "$SOURCEPATH/Times-Bold.afm" > "$DESTPATH/TimesBold.php"

echo "Times-BoldItalic.afm"
php processCoreAFM.php "$SOURCEPATH/Times-BoldItalic.afm" > "$DESTPATH/TimesBoldItalic.php"

echo "Times-Italic.afm"
php processCoreAFM.php "$SOURCEPATH/Times-Italic.afm" > "$DESTPATH/TimesItalic.php"

echo "Times-Roman.afm"
php processCoreAFM.php "$SOURCEPATH/Times-Roman.afm" > "$DESTPATH/TimesRoman.php"

echo "ZapfDingbats.afm"
php processCoreAFM.php "$SOURCEPATH/ZapfDingbats.afm" > "$DESTPATH/ZapfDingbats.php"

echo "Done!"
